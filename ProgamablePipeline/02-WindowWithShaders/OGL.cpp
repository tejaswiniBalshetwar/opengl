#include<windows.h>
#include<stdio.h>
#include<GL/glew.h>
#include<GL/gl.h>

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

bool gbIsFullscreen=false;
bool gbIsActiveWindow=false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{
	void display(void);
	int initialize(void);


	WNDCLASSEX wndclass;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
	HWND hwnd;
	MSG msg;

	int iRet=0;
	bool bDone=false;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR!!"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n File opened successfully");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\nError in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\n Error in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile, "\nError in initialize at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\nError in initialize at wglMakeCurrent");
		exit(0);
	}
	else 
	{
		fprintf(gpFile,"\n initialize function executed successfully");
	}

	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd,iCmdShow);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if(gbIsActiveWindow)
			{
				//update();
			}
			display();
		}
	}

	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void uninitialize(void);
	void resize(int,int);
	void ToggleFullscreen(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;

			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

			}
		}
		gbIsFullscreen=true;
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_FRAMECHANGED|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

}

int initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;

	void uninitialize();
	void resize(int,int);

	GLenum result;
	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_SUPPORT_OPENGL|PFD_DRAW_TO_WINDOW|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);

	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return (-1);
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}
	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile,"\nError while glewInit");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//create shader object for shader binding
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar* gVertexShaderSourceCode[] = {
		"void main(void)" \
		"{" \
		
		"}"	
	};

	//specify abouve source code to vetex shader
	glShaderSource(gVertexShaderObject,1,(const GLchar** )&gVertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	//error checking for shader
	GLint iShaderSourceStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderSourceStatus);
	if (iShaderSourceStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
			if (szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);

				fprintf(gpFile,("\n\nError while compiling Vertex Shadedr: %s \n\n"),szInfoLog);

				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//fragment shader
	//creating fragment shader object for binding
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write source code for fragment shader
	const GLchar* fragmentShaderSourceCode[] = {
		"void main(void)" \
		"{" \
			
		"}"
	};

	//specify  abouve shader source code to fragment shader
	glShaderSource(gFragmentShaderObject,1,(const GLchar**)fragmentShaderSourceCode,NULL);

	//compile fragment Shader
	glCompileShader(gFragmentShaderObject);

	//error checking for compilation
	iShaderSourceStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	 glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderSourceStatus);
	 if (iShaderSourceStatus==GL_FALSE)
	 {	
		 glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		 if (iInfoLogLength > 0)
		 {
			 szInfoLog = (GLchar*)malloc(sizeof(GLchar)*iInfoLogLength);
			 if (szInfoLog!=NULL)
			 {
				 GLsizei written;
				 glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);

				 fprintf(gpFile,("\nError Fragment Shader : %s"),szInfoLog);
				 free(szInfoLog);
				 uninitialize();
				 DestroyWindow(ghwnd);
			 }
		 }
	 }

	 //create shader program object

	 gShaderProgramObject = glCreateProgram();

	 //attach vertex shader to shader program
	 glAttachShader(gShaderProgramObject,gVertexShaderObject);

	 //attch fragment shader program
	 glAttachShader(gShaderProgramObject,gFragmentShaderObject);

	 //link shader program
	 glLinkProgram(gShaderProgramObject);
	 //Error checking for linking

	 GLint iLinkProgramStatus = 0;
	 iInfoLogLength = 0;
	 szInfoLog = NULL;
	 glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iLinkProgramStatus);
	 if (iLinkProgramStatus == GL_FALSE)
	 {
		 glGetShaderiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		 if (iInfoLogLength > 0)
		 {
			 szInfoLog = (GLchar*)malloc(iInfoLogLength * sizeof(GLchar));
			 if (szInfoLog != NULL)
			 {
				 GLsizei written;
				 glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);

				 fprintf(gpFile,("\nError while linking program object: %s\n\n"),szInfoLog);

				 free(szInfoLog);
				 szInfoLog = NULL;
				 uninitialize();
				 uninitialize();
				 DestroyWindow(ghwnd);
				 exit(0);
			 }
		 }
	 }


	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	resize(WIN_WIDTH,WIN_HEIGHT);
	return(0);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	 
	glUseProgram(gShaderProgramObject);

	glUseProgram(0);

	SwapBuffers(ghdc);
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

void uninitialize()
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_FRAMECHANGED|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);
	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}
	if (gpFile)
	{
		fprintf(gpFile,"Program Executed successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}