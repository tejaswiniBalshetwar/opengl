#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include"Sphere.h"

#include<GL/glew.h>
#include<GL/gl.h>
#include "./../../vmath.h"


#pragma comment(lib,"Sphere.lib")
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600




float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;


using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;

FILE *gpFile = NULL;

GLfloat lightAngleForX = 0.0f;
GLfloat lightAngleForY = 0.0f;
GLfloat lightAngleForZ = 0.0f;
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject = 0;

GLuint vao_sphere = 0;
GLuint vbo_position_sphere = 0;
GLuint vbo_normal_sphere = 0;
GLuint vbo_element_sphere = 0;


GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShininessUniform;

bool bRotation = false;
bool bLight = false;

int LKeyIsPressed=0;
int keyPress = 0;

mat4 perspectiveProjectionMatrix;

mat4 modelMatrix;
mat4 viewMatrix;

struct light
{
	float light_ambient[4];
	float light_diffuse[4];
	float light_specular[4];
	float light_position[4];
};

light lights;

vec4 lightPosition;
float glMaterialAmbient[4];
float glMaterialDiffuse[4];
float glMaterialSpecular[4];
float glMaterialShininess;

GLfloat windowWidth = 0;
GLfloat windowHeight = 0;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("MY PPOGL APP");
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while file opening"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file openied successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY OPENGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\nError in initialize at choosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n ERROR in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function executed successfully");

	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
			case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
					LKeyIsPressed = 1;
				}
				else
				{
					bLight = false;
					LKeyIsPressed = 0;
				}
				break;
			case 'x':
			case 'X':
				keyPress = 1;
				break;
		
			case 'y':
			case 'Y':
				keyPress = 2;
				break;
			case 'z':
			case 'Z':
				keyPress = 3;
				break;
		}
		break;
	case WM_SIZE:
		windowWidth = LOWORD(lParam);
		windowHeight = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	GLenum status;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghdc == NULL)
	{
		return -3;

	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile, "\n\nError in initialize at glewInit");
		uninitialize();
		exit(0);
	}

	//creating the shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		
		"out vec3 out_light_direction;					\n" \
		
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_la;								\n" \
		
		"uniform vec3 u_ld;								\n" \
		
		"uniform vec3 u_ls;								\n" \
		
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
	
		"in vec3 out_t_normal;							\n" \
		
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		
		"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
		
		"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		
		"		vec3 ambient= u_la * u_ka;																						\n " \
		
		"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																			\n" \
		
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		
		"		phong_ads_light=ambient + diffuse + specular;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	
	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyIsPressed");
	
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	//vertices for sphere

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();




	//recording start

	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//posiiotn data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elementes), sphere_elementes, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//recording stop
	glBindVertexArray(0);


	//initializeing lights
	lights.light_ambient[0] = 0.0f;
	lights.light_ambient[1] = 0.0f;
	lights.light_ambient[2] = 0.0f;
	lights.light_ambient[3] = 0.0f;

	lights.light_diffuse[0] = 1.0f;
	lights.light_diffuse[1] = 1.0f;
	lights.light_diffuse[2] = 1.0f;
	lights.light_diffuse[3] = 0.0f;

	lights.light_specular[0] = 1.0f;
	lights.light_specular[1] = 1.0f;
	lights.light_specular[2] = 1.0f;
	lights.light_specular[3] = 0.0f;


	lights.light_position[0] = 0.0f;
	lights.light_position[1] = 0.0f;
	lights.light_position[2] = 0.0f;
	lights.light_position[3] = 1.0f;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	//orthographic projection la load identity
	perspectiveProjectionMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void display()
{
	void Spheres24();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 rotationMatrix;

	//initialize abouve matrices

	modelMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	//do necessary translation
	
	if (bLight)
	{
		glUniform1i(LKeyIsPressedUniform,1);
		if (keyPress == 1)
		{
			lights.light_position[1] = 200.0 * cos(lightAngleForX);
			lights.light_position[2] = 200.0 * sin(lightAngleForX);
		}
		else if (keyPress == 2)
		{
			lights.light_position[2] = 200.0 * cos(lightAngleForY);
			lights.light_position[0] = 200.0 * sin(lightAngleForY);
		}
		else
		{
			lights.light_position[0] = 200.0 * cos(lightAngleForZ);
			lights.light_position[1] = 200.0 * sin(lightAngleForZ);
		}
		glUniform3fv(laUniform,1,lights.light_ambient);		//glLightfv() 
		glUniform3fv(ldUniform,1, lights.light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniform,1, lights.light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniform,1, lights.light_position); //glLightfv() for position


	}
	else
	{
		glUniform1i(LKeyIsPressedUniform,0);
	}

	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
	
	Spheres24();
	//drawSphere();
	
	glUseProgram(0);
	SwapBuffers(ghdc);
}

void drawSphere()
{
	glBindVertexArray(vao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	glBindVertexArray(0);
}

void Spheres24()
{
	void drawSphere();
	mat4 translationMatrix;

	//make matrix identity 
	translationMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, 4.0f, -25.0f);

	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0215;
	glMaterialAmbient[1] = 0.1745;
	glMaterialAmbient[2] = 0.0215;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.07568;
	glMaterialDiffuse[1] = 0.61424;
	glMaterialDiffuse[2] = 0.07568;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.633;
	glMaterialSpecular[1] = 0.727811;
	glMaterialSpecular[2] = 0.633;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.6 *128.0f;

	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.135f;
	glMaterialAmbient[1] = 0.2225f;
	glMaterialAmbient[2] = 0.1575f;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.54f;
	glMaterialDiffuse[1] = 0.89f;
	glMaterialDiffuse[2] = 0.63f;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.316228;
	glMaterialSpecular[1] = 0.316228;
	glMaterialSpecular[2] = 0.316228;
	glMaterialSpecular[3] = 1.0f;

	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.1 *128.0f;

	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05375;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.06625;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.18275;
	glMaterialDiffuse[1] = 0.17;
	glMaterialDiffuse[2] = 0.22525;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.332741;
	glMaterialSpecular[1] = 0.328634;
	glMaterialSpecular[2] = 0.346435;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.3 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.25;
	glMaterialAmbient[1] = 0.20725;
	glMaterialAmbient[2] = 0.20725;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 1.0;
	glMaterialDiffuse[1] = 0.829;
	glMaterialDiffuse[2] = 0.829;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.296648;
	glMaterialSpecular[1] = 0.296648;
	glMaterialSpecular[2] = 0.296648;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.088 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.1745;
	glMaterialAmbient[1] = 0.01175;
	glMaterialAmbient[2] = 0.01175;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.61424;
	glMaterialDiffuse[1] = 0.04136;
	glMaterialDiffuse[2] = 0.04136;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.727811;
	glMaterialSpecular[1] = 0.626959;
	glMaterialSpecular[2] = 0.626959;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.6 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.1;
	glMaterialAmbient[1] = 0.18725;
	glMaterialAmbient[2] = 0.1745;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.396;
	glMaterialDiffuse[1] = 0.74151;
	glMaterialDiffuse[2] = 0.69102;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.297254;
	glMaterialSpecular[1] = 0.30829;
	glMaterialSpecular[2] = 0.306678;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.1 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//2nd column
	//2nd row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, 4.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.329412;
	glMaterialAmbient[1] = 0.223529;
	glMaterialAmbient[2] = 0.027451;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.780392;
	glMaterialDiffuse[1] = 0.568627;
	glMaterialDiffuse[2] = 0.113725;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.992157;
	glMaterialSpecular[1] = 0.941176;
	glMaterialSpecular[2] = 0.807843;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.21794872 *128.0f;

	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.2125;
	glMaterialAmbient[1] = 0.1275f;
	glMaterialAmbient[2] = 0.054f;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.714;
	glMaterialDiffuse[1] = 0.4284;
	glMaterialDiffuse[2] = 0.18144;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.393548;
	glMaterialSpecular[1] = 0.271906;
	glMaterialSpecular[2] = 0.166721;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.2 *128.0f;

	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.25;
	glMaterialAmbient[1] = 0.25;
	glMaterialAmbient[2] = 0.25;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.4;
	glMaterialDiffuse[1] = 0.4;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.774597;
	glMaterialSpecular[1] = 0.774597;
	glMaterialSpecular[2] = 0.774597;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.6 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.19125;
	glMaterialAmbient[1] = 0.0735;
	glMaterialAmbient[2] = 0.0225;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.7038;
	glMaterialDiffuse[1] = 0.27048;
	glMaterialDiffuse[2] = 0.0828;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.256777;
	glMaterialSpecular[1] = 0.137622;
	glMaterialSpecular[2] = 0.086014;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.1 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.24725;
	glMaterialAmbient[1] = 0.1995;
	glMaterialAmbient[2] = 0.0745;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.75164;
	glMaterialDiffuse[1] = 0.60648;
	glMaterialDiffuse[2] = 0.22648;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.628281;
	glMaterialSpecular[1] = 0.555802;
	glMaterialSpecular[2] = 0.366065;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.4 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.19225;
	glMaterialAmbient[1] = 0.19225;
	glMaterialAmbient[2] = 0.19225;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.50754;
	glMaterialDiffuse[1] = 0.50754;
	glMaterialDiffuse[2] = 0.50754;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.508273;
	glMaterialSpecular[1] = 0.508273;
	glMaterialSpecular[2] = 0.508273;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.4 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3rd column
	//2nd row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, 4.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.01;
	glMaterialDiffuse[1] = 0.01;
	glMaterialDiffuse[2] = 0.01;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.50;
	glMaterialSpecular[1] = 0.50;
	glMaterialSpecular[2] = 0.50;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.1f;
	glMaterialAmbient[2] = 0.06f;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.0;
	glMaterialDiffuse[1] = 0.50980392;
	glMaterialDiffuse[2] = 0.50980392;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.50196078;
	glMaterialSpecular[1] = 0.50196078;
	glMaterialSpecular[2] = 0.50195078;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;

	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.1;
	glMaterialDiffuse[1] = 0.35;
	glMaterialDiffuse[2] = 0.1;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.45;
	glMaterialSpecular[1] = 0.55;
	glMaterialSpecular[2] = 0.45;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.0;
	glMaterialDiffuse[2] = 0.0;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.6;
	glMaterialSpecular[2] = 0.6;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.55;
	glMaterialDiffuse[1] = 0.55;
	glMaterialDiffuse[2] = 0.55;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.70;
	glMaterialSpecular[1] = 0.70;
	glMaterialSpecular[2] = 0.70;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.0;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.60;
	glMaterialSpecular[1] = 0.60;
	glMaterialSpecular[2] = 0.50;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	
	//4th column
	//1st 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, 4.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.02;
	glMaterialAmbient[1] = 0.02;
	glMaterialAmbient[2] = 0.02;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.01;
	glMaterialDiffuse[1] = 0.01;
	glMaterialDiffuse[2] = 0.01;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.4;
	glMaterialSpecular[1] = 0.4;
	glMaterialSpecular[2] = 0.4;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.05;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.4;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.5;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.04;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.7;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;

	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.4;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.04;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.04;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.4;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.04;
	glMaterialSpecular[2] = 0.04;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.05;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.5;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.7;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.04;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	

}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(25.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}



void uninitialize()
{
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}

	if (vbo_element_sphere)
	{
		glDeleteBuffers(1, &vbo_element_sphere);
		vbo_element_sphere = 0;
	}
	
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}


	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nProgram executed successfully");
		fclose(gpFile);
		gpFile = NULL;

	}

}

void update()
{
	lightAngleForX = lightAngleForX + 0.020f;
	if (lightAngleForX >= 360.0f)
	{
		lightAngleForX = 0.0f;
	}

	lightAngleForY = lightAngleForY + 0.020f;
	if (lightAngleForY > 360.0f)
	{
		lightAngleForY = 0.0f;
	}

	lightAngleForZ = lightAngleForZ + 0.020f;
	if (lightAngleForZ > 360.0f)
	{
		lightAngleForZ = 0.0f;
	}
	
}
