#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include "Header.h"


#include<GL/glew.h>
#include<GL/gl.h>
#include "../../vmath.h"
#include <string.h>

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600


using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;

FILE *gpFile = NULL;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};
GLuint gShaderProgramObject = 0;
GLuint vbo_position = 0;
GLuint vbo_texture = 0;
GLuint vbo_Normal = 0;
GLuint vbo_element = 0;
GLuint vao = 0;

GLuint mvpUniform;

mat4 perspectiveProjectionMatrix;


//mesh loading code
struct vec_float {
	float *p;
	int size;
};


struct vec_int {
	int *p;
	int size;
};
vec_float *gpVertex;
vec_float *gpNormal;
vec_float *gpTexCoords;

vec_int *gpTextureIndices;
vec_int *gpNormalIndices;
vec_int *gpVertexIndices;

vec_float *gpVertexSorted;
vec_float *gpNormalSorted;
vec_float *gpTextureSorted;
FILE *gpFile_Mesh = NULL;
char buffer[1024];


GLuint texture;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("MY PPOGL APP");
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while file opening"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file openied successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY OPENGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\nError in initialize at choosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n ERROR in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function executed successfully");

	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				//update
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();
	void loadMesh();
	BOOL LoadTexture(GLuint*, TCHAR[]);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	GLenum status;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghdc == NULL)
	{
		return -3;

	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile, "\n\nError in initialize at glewInit");
		uninitialize();
		exit(0);
	}

	//creating the shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"out vec2 out_texCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"	out_texCoord=vTexCoord;" \
		"gl_Position= u_mvp_matrix * vPosition ;"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 fragColor;" \
		"in vec2 out_texCoord;" \
		"uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
		"	vec4 tex=texture(u_sampler,out_texCoord);" \
		"	fragColor=vec4(tex);" \
		"}";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL,"vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	//loadMesh call
	loadMesh();

	const GLfloat triangleVertices[] = {
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f
	};

	//create the recoding casets reel == vao
	glGenVertexArrays(1, &vao);

	//bind with cassate  and start recording
	glBindVertexArray(vao);

	//gen buffers
	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpVertexSorted->size, gpVertexSorted->p, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpTextureSorted->size, gpTextureSorted->p, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1,&vbo_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,gpVertexIndices->size*sizeof(float),gpVertexIndices->p,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);


	//recording stop
	glBindVertexArray(0);

	//glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	//glFrontFace(GL_CCW);

	LoadTexture(&texture, MAKEINTRESOURCE(IDBTMAP_KUNDALI));

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	//orthographic projectioon la load identity
	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

BOOL LoadTexture(GLuint* texture, TCHAR imageResourceId[])
{
	BOOL bStatus = FALSE;
	HBITMAP hBitmap;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	return bStatus;
}

void loadMesh(void)
{
	struct vec_float* create_vec_float(void);
	struct vec_int* create_vec_int(void);
	int push_back_vec_float(struct vec_float* , float );
	int push_back_vec_int(struct vec_int* , int );

	gpVertex = create_vec_float();
	gpNormal = create_vec_float();
	gpTexCoords = create_vec_float();
	
	gpVertexIndices = create_vec_int();
	gpTextureIndices = create_vec_int();
	gpNormalIndices = create_vec_int();

	gpVertexSorted = create_vec_float();
	gpNormalSorted = create_vec_float();
	gpTextureSorted = create_vec_float();

	gpFile_Mesh = fopen("Tree.obj","r");
	if (gpFile_Mesh==NULL)
	{
		fprintf(gpFile,"\n\n\n::Error while opening OBJ file");
		exit(0);
	}
	else
	{
		char* firstToken = NULL;
		char* token = NULL;
		const char* space =" ";
		int num = 0;
		const char* slash = "/";
		char* fEntries[] = {NULL,NULL,NULL};
		while ((fgets(buffer, 1024, gpFile_Mesh)) != NULL)
		{
			firstToken = strtok(buffer,space);
			if (strcmp(firstToken, "v") == 0)
			{
				token = strtok(NULL,space);	
				float x = atof(token);
				push_back_vec_float(gpVertex,x);
				

				token = strtok(NULL, space);
				float y = atof(token);
				push_back_vec_float(gpVertex,y);
				num++; 
			
				token = strtok(NULL, space);
				float z = atof(token);
				push_back_vec_float(gpVertex,z);
				num++;
			}
			else if (strcmp(firstToken,"vt") == 0)
			{
				token = strtok(NULL, space);
				float s = atof(token);
				push_back_vec_float(gpTexCoords, s);

				token = strtok(NULL, space);
				float t = atof(token);
				push_back_vec_float(gpTexCoords, t);
			}
			else if (strcmp(firstToken, "vn") == 0)
			{
				token = strtok(NULL, space);
				float u = atof(token);
				push_back_vec_float(gpNormal, u);

				token = strtok(NULL, space);
				float v = atof(token);
				push_back_vec_float(gpNormal, v);

				token = strtok(NULL, space);
				float w = atof(token);
				push_back_vec_float(gpNormal, w);
			}
			else if (strcmp(firstToken, "f") == 0)
			{
				for (int i = 0; i < 3; i++)
				{
					fEntries[i] = strtok(NULL,space);
				}
				for (int i = 0; i < 3; i++)
				{
					token = strtok(fEntries[i],slash);
					push_back_vec_int(gpVertexIndices,atoi(token)-1);
					
					token = strtok(NULL, slash);
					push_back_vec_int(gpTextureIndices, atoi(token)-1);
				
					token = strtok(NULL, slash);
					push_back_vec_int(gpNormalIndices, atoi(token)-1);
					
				}
			}
		}

	}
	fclose(gpFile_Mesh);
	
		for (unsigned int i = 0; i < gpVertexIndices->size; i++)
		{
			fprintf(gpFile,"\ngpVertexIndices->i=%d",i);
			
			fprintf(gpFile,"\ngpVertex->p[%d]=%f", gpVertexIndices->p[i],gpVertex->p[gpVertexIndices->p[i]*3+0]);
			fprintf(gpFile,"\ngpVertex->p[%d]=%f", gpVertexIndices->p[i+1],gpVertex->p[gpVertexIndices->p[i]*3+1]);
			fprintf(gpFile,"\ngpVertex->p[%d]=%f", gpVertexIndices->p[i+2],gpVertex->p[gpVertexIndices->p[i]*3+2]);
			
			push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+0]);
			push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+1]);
			push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+2]);
		}

		for (int i = 0; i < gpNormalIndices->size; i=i+3)
		{
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+0]);
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+1]);
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+2]);
			
		}

		for (int i = 0; i < gpTextureIndices->size; i=i+2)
		{
			push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i]*2+0]);
			push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i]*2+1]);
		}
}
struct vec_int* create_vec_int()
{
	struct vec_int* p_new = NULL;

	p_new = (struct vec_int*)malloc(sizeof(struct vec_int));
	if (p_new == NULL)
	{
		printf("Error in malloc");
		exit(0);
	}

	memset(p_new, 0, sizeof(struct vec_int));

	return p_new;
}


int push_back_vec_int(struct vec_int* p_vec, int new_data)
{
	p_vec->p = (int*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(int));

	if (p_vec->p == NULL)
	{
		printf("Errorr  realloc");
		exit(0);
	}

	p_vec->size = p_vec->size + 1;

	p_vec->p[p_vec->size - 1] = new_data;


	return TRUE;
}

struct vec_float* create_vec_float()
{
	struct vec_float* p_new = NULL;

	p_new = (struct vec_float*)malloc(sizeof(struct vec_float));
	if (p_new == NULL)
	{
		printf("Error in malloc");
		exit(0);
	}

	memset(p_new, 0, sizeof(struct vec_float));

	return p_new;
}


int push_back_vec_float(struct vec_float* p_vec, float new_data)
{
	p_vec->p = (float*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(float));


	if (p_vec->p == NULL)
	{
		printf("Errorr  realloc");
		exit(0);
	}

	p_vec->size = p_vec->size + 1;

	p_vec->p[p_vec->size - 1] = new_data;
	

	return TRUE;
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;
	//initialize abouve matrices
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	//do necessary for transformation
	translationMatrix = translate(0.0f, -2.0f, -12.0f);
	//do necessary for matrix multiplication
	modelViewMatrix = modelViewMatrix * translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,texture);
	glUniform1i(glGetUniformLocation(gShaderProgramObject,"u_sampler"),0);

	//bind with vao so that it starts sound which is recorded
	glBindVertexArray(vao);

	//similaraly bind for textures if any
	/*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
	glDrawElements(GL_TRIANGLES,gpVertexIndices->size*sizeof(int), GL_UNSIGNED_SHORT,NULL);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
*/
	glDrawArrays(GL_TRIANGLES,0,gpVertexSorted->size);
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(ghdc);
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);

}



void uninitialize()
{
	int destroy_vec_float(struct vec_float*);
	int destroy_vec_int(struct vec_int*);
	if (vbo_position)
	{
		glDeleteBuffers(1, &vbo_position);
		vbo_position= 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gpVertex)
	{
		destroy_vec_float(gpVertex);
	}

	if (gpNormal)
	{
		destroy_vec_float(gpNormal);
	}

	if (gpTexCoords)
	{
		destroy_vec_float(gpTexCoords);
	}
	
	if (gpVertexIndices)
	{
		destroy_vec_int(gpVertexIndices);
	}

	if (gpNormalIndices)
	{
		destroy_vec_int(gpNormalIndices);
	}

	if (gpTextureIndices)
	{
		destroy_vec_int(gpTextureIndices);
	}

	if (gpVertexSorted)
	{
		destroy_vec_float(gpVertexSorted);
	}

	if (gpNormalSorted)
	{
		destroy_vec_float(gpNormalSorted);
	}

	if (gpTextureSorted)
	{
		destroy_vec_float(gpTextureSorted);
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nProgram executed successfully");
		fclose(gpFile);
		gpFile = NULL;

	}

}


int destroy_vec_int(struct vec_int* p_vec)
{
	if (p_vec->p)
	{
		free(p_vec->p);
		p_vec->p = NULL;

	}
	if (p_vec)
	{
		free(p_vec);
	}
	p_vec = NULL;
	return true;
}


int destroy_vec_float(struct vec_float* p_vec)
{
	if (p_vec->p)
	{
		free(p_vec->p);
		p_vec->p = NULL;
	}

	if (p_vec)
	{
		free(p_vec);
	}
	p_vec = NULL;
	return true;
}