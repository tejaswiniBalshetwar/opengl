#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include"Sphere.h"

#include<GL/glew.h>
#include<GL/gl.h>
#include "./../../vmath.h"


#pragma comment(lib,"Sphere.lib")
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600




float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;


using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;

FILE *gpFile = NULL;

GLfloat lightAngleForOne = 0.0f;
GLfloat lightAngleForTwo = 0.0f;
GLfloat lightAngleForThree = 0.0f;
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject = 0;

GLuint vao_sphere = 0;
GLuint vbo_position_sphere = 0;
GLuint vbo_normal_sphere = 0;
GLuint vbo_element_sphere = 0;


GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniformForRed;
GLuint laUniformForBlue;
GLuint laUniformForGreen;
GLuint ldUniformForRed;
GLuint ldUniformForBlue;
GLuint ldUniformForGreen;
GLuint lsUniformForRed;
GLuint lsUniformForBlue;
GLuint lsUniformForGreen;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniformForRed;
GLuint lightPositionUniformForBlue;
GLuint lightPositionUniformForGreen;
GLuint materialShininessUniform;

bool bRotation = false;
bool bLight = false;

int LKeyIsPressed=0;

mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

struct light
{
	float light_ambient[4];
	float light_diffuse[4];
	float light_specular[4];
	float light_position[4];
};

light lights[3];

vec4 lightPosition;
float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("MY PPOGL APP");
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while file opening"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file openied successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY OPENGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\nError in initialize at choosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n ERROR in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function executed successfully");

	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
			case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
					LKeyIsPressed = 1;
				}
				else
				{
					bLight = false;
					LKeyIsPressed = 0;
				}
				break;
		
		}
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	GLenum status;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghdc == NULL)
	{
		return -3;

	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile, "\n\nError in initialize at glewInit");
		uninitialize();
		exit(0);
	}

	//creating the shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_positionForRedLight;					\n" \
		"uniform vec4 u_light_positionForBlueLight;					\n" \
		"uniform vec4 u_light_positionForGreenLight;					\n" \
		
		"out vec3 out_light_directionForRedLight;					\n" \
		"out vec3 out_light_directionForBlueLight;					\n" \
		"out vec3 out_light_directionForGreenLight;					\n" \
		
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_directionForRedLight=vec3(u_light_positionForRedLight - eye_coordinates);				\n" \
		"		out_light_directionForBlueLight=vec3(u_light_positionForBlueLight - eye_coordinates);				\n" \
		"		out_light_directionForGreenLight=vec3(u_light_positionForGreenLight - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_laForRedLight;								\n" \
		"uniform vec3 u_laForBlueLight;								\n" \
		"uniform vec3 u_laForGreenLight;								\n" \
		
		"uniform vec3 u_ldForRedLight;								\n" \
		"uniform vec3 u_ldForBlueLight;								\n" \
		"uniform vec3 u_ldForGreenLight;								\n" \
		
		"uniform vec3 u_lsForRedLight;								\n" \
		"uniform vec3 u_lsForBlueLight;								\n" \
		"uniform vec3 u_lsForGreenLight;								\n" \
		
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_directionForRedLight;					\n" \
		"in vec3 out_light_directionForBlueLight;					\n" \
		"in vec3 out_light_directionForGreenLight;					\n" \
		
		"in vec3 out_t_normal;							\n" \
		
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_directionForRedLight=normalize(out_light_directionForRedLight);													\n"	\
		"		vec3 normalized_light_directionForBlueLight=normalize(out_light_directionForBlueLight);													\n"	\
		"		vec3 normalized_light_directionForGreenLight=normalize(out_light_directionForGreenLight);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		
		"		float t_dot_ldForRed=max(dot(normalized_light_directionForRedLight,normalized_t_norm),0.0);										\n" \
		"		float t_dot_ldForBlue=max(dot(normalized_light_directionForBlueLight,normalized_t_norm),0.0);										\n" \
		"		float t_dot_ldForGreen=max(dot(normalized_light_directionForGreenLight,normalized_t_norm),0.0);										\n" \
		
		"		vec3 reflection_vetorForRed=reflect(-normalized_light_directionForRedLight ,normalized_t_norm);									\n" \
		"		vec3 reflection_vetorForBlue=reflect(-normalized_light_directionForBlueLight ,normalized_t_norm);									\n" \
		"		vec3 reflection_vetorForGreen=reflect(-normalized_light_directionForGreenLight ,normalized_t_norm);									\n" \
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		
		"		vec3 ambientRed= u_laForRedLight * u_ka;																						\n " \
		"		vec3 ambientBlue= u_laForBlueLight * u_ka;																						\n " \
		"		vec3 ambientGreen= u_laForGreenLight * u_ka;																						\n " \
		
		"		vec3 diffuseForRed=u_ldForRedLight * u_kd * t_dot_ldForRed;																			\n" \
		"		vec3 diffuseForBlue=u_ldForBlueLight * u_kd * t_dot_ldForBlue;																			\n" \
		"		vec3 diffuseForGreen=u_ldForGreenLight * u_kd * t_dot_ldForGreen;																			\n" \
	
		"		vec3 specularRed = u_lsForRedLight * u_ks * pow(max(dot(reflection_vetorForRed,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		vec3 specularBlue = u_lsForBlueLight * u_ks * pow(max(dot(reflection_vetorForBlue,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		vec3 specularGreen = u_lsForGreenLight * u_ks * pow(max(dot(reflection_vetorForGreen,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		
		"		phong_ads_light=ambientRed + ambientBlue + ambientGreen + diffuseForRed + diffuseForBlue + diffuseForGreen + specularRed + specularBlue + specularGreen;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	laUniformForRed = glGetUniformLocation(gShaderProgramObject, "u_laForRedLight");
	laUniformForBlue = glGetUniformLocation(gShaderProgramObject, "u_laForBlueLight");
	laUniformForGreen = glGetUniformLocation(gShaderProgramObject, "u_laForGreenLight");
	ldUniformForRed = glGetUniformLocation(gShaderProgramObject, "u_ldForRedLight");
	ldUniformForBlue = glGetUniformLocation(gShaderProgramObject, "u_ldForBlueLight");
	ldUniformForGreen = glGetUniformLocation(gShaderProgramObject, "u_ldForGreenLight");
	lsUniformForRed = glGetUniformLocation(gShaderProgramObject, "u_lsForRedLight");
	lsUniformForBlue = glGetUniformLocation(gShaderProgramObject, "u_lsForBlueLight");
	lsUniformForGreen = glGetUniformLocation(gShaderProgramObject, "u_lsForGreenLight");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	
	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyIsPressed");
	
	lightPositionUniformForRed = glGetUniformLocation(gShaderProgramObject, "u_light_positionForRedLight");
	lightPositionUniformForBlue = glGetUniformLocation(gShaderProgramObject, "u_light_positionForBlueLight");
	lightPositionUniformForGreen = glGetUniformLocation(gShaderProgramObject, "u_light_positionForGreenLight");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	//vertices for sphere

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();




	//recording start

	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//posiiotn data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elementes), sphere_elementes, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//recording stop
	glBindVertexArray(0);


	//initializeing lights
	lights[0].light_ambient[0] = 0.0f;
	lights[0].light_ambient[1] = 0.0f;
	lights[0].light_ambient[2] = 0.0f;
	lights[0].light_ambient[3] = 0.0f;

	lights[0].light_diffuse[0] = 1.0f;
	lights[0].light_diffuse[1] = 0.0f;
	lights[0].light_diffuse[2] = 0.0f;
	lights[0].light_diffuse[3] = 0.0f;

	lights[0].light_specular[0] = 1.0f;
	lights[0].light_specular[1] = 0.0f;
	lights[0].light_specular[2] = 0.0f;
	lights[0].light_specular[3] = 0.0f;


	lights[0].light_position[0] = 0.0f;
	lights[0].light_position[1] = 0.0f;
	lights[0].light_position[2] = 0.0f;
	lights[0].light_position[3] = 1.0f;

	// lights for 2nd light
	lights[1].light_ambient[0] = 0.0f;
	lights[1].light_ambient[1] = 0.0f;
	lights[1].light_ambient[2] = 0.0f;
	lights[1].light_ambient[3] = 0.0f;

	lights[1].light_diffuse[0] = 0.0f;
	lights[1].light_diffuse[1] = 1.0f;
	lights[1].light_diffuse[2] = 0.0f;
	lights[1].light_diffuse[3] = 0.0f;

	lights[1].light_specular[0] = 0.0f;
	lights[1].light_specular[1] = 1.0f;
	lights[1].light_specular[2] = 0.0f;
	lights[1].light_specular[3] = 0.0f;


	lights[1].light_position[0] = 0.0f;
	lights[1].light_position[1] = 0.0f;
	lights[1].light_position[2] = 0.0f;
	lights[1].light_position[3] = 1.0f;

	//3 rd light
	lights[2].light_ambient[0] = 0.0f;
	lights[2].light_ambient[1] = 0.0f;
	lights[2].light_ambient[2] = 0.0f;
	lights[2].light_ambient[3] = 0.0f;

	lights[2].light_diffuse[0] = 0.0f;
	lights[2].light_diffuse[1] = 0.0f;
	lights[2].light_diffuse[2] = 1.0f;
	lights[2].light_diffuse[3] = 0.0f;

	lights[2].light_specular[0] = 0.0f;
	lights[2].light_specular[1] = 0.0f;
	lights[2].light_specular[2] = 1.0f;
	lights[2].light_specular[3] = 0.0f;


	lights[2].light_position[0] = 0.0f;
	lights[2].light_position[1] = 0.0f;
	lights[2].light_position[2] = 0.0f;
	lights[2].light_position[3] = 1.0f;


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	//orthographic projectioon la load identity
	perspectiveProjectionMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 translationMatrix;
	mat4 rotationMatrix;

	//initialize abouve matrices

	modelMatrix = mat4::identity();
	//modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	//do necessary translation

	translationMatrix = translate(0.0f, 0.0f, -3.0f);

	//do matrix multipliacation
	
	 modelMatrix = modelMatrix * translationMatrix  ;
	
	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	
	
	if (bLight)
	{
		glUniform1i(LKeyIsPressedUniform,1);

		lights[0].light_position[1] = 200.0 * cos(lightAngleForOne);
		lights[0].light_position[2] = 200.0 * sin(lightAngleForOne);
		
		glUniform3fv(laUniformForRed,1,lights[0].light_ambient);		//glLightfv() 
		glUniform3fv(ldUniformForRed,1, lights[0].light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniformForRed,1, lights[0].light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniformForRed,1, lights[0].light_position); //glLightfv() for position


		//FOR GREEN LIGHT
		
		lights[1].light_position[2] = 200.0f * cos(lightAngleForTwo);
		lights[1].light_position[0] = 200.0f * sin(lightAngleForTwo);

		glUniform3fv(laUniformForGreen, 1, lights[1].light_ambient);		//glLightfv() 
		glUniform3fv(ldUniformForGreen, 1, lights[1].light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniformForGreen, 1, lights[1].light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniformForGreen, 1, lights[1].light_position); //glLightfv() for position
		

		//FOR BLUE LIGHT
		
		lights[2].light_position[0] = 200.0f * cos(lightAngleForThree);
		lights[2].light_position[1] = 200.0f * sin(lightAngleForThree);


		glUniform3fv(laUniformForBlue, 1, lights[2].light_ambient);		//glLightfv() 
		glUniform3fv(ldUniformForBlue, 1, lights[2].light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniformForBlue, 1, lights[2].light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniformForBlue, 1, lights[2].light_position); //glLightfv() for position



		glUniform3fv(kaUniform,1,material_ambient);	//glMaterialfv();
		glUniform3fv(kdUniform,1,material_diffuse);	//glMaterialfv();
		glUniform3fv(ksUniform,1,material_specular);	//glMaterialfv();
		glUniform1f(materialShininessUniform,material_shininess);	//glMaterialfv();
	}
	else
	{
		glUniform1i(LKeyIsPressedUniform,0);
	}

	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(ghdc);
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);

}



void uninitialize()
{
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}


	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nProgram executed successfully");
		fclose(gpFile);
		gpFile = NULL;

	}

}

void update()
{
	lightAngleForOne = lightAngleForOne + 0.020f;
	if (lightAngleForOne >= 360.0f)
	{
		lightAngleForOne = 0.0f;
	}

	lightAngleForTwo = lightAngleForTwo + 0.020f;
	if (lightAngleForTwo > 360.0f)
	{
		lightAngleForTwo = 0.0f;
	}

	lightAngleForThree = lightAngleForThree + 0.020f;
	if (lightAngleForThree > 360.0f)
	{
		lightAngleForThree = 0.0f;
	}
	
}
