#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include"Header.h"

#include<GL/glew.h>
#include<GL/gl.h>
#include"./../../vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

enum
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint texture_smiley=0;

GLuint gShaderProgramObject=0;

GLuint vao_square=0;

GLuint vbo_square_position=0;
GLuint vbo_square_texCoord=0;

GLuint mvpUniform;
GLuint samplerUniform;

mat4 perspectiveProjectionMatrix;

int keyPressed=0;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdshow)
{
	void update();
	void display();
	int initialize();

	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
	MSG msg;

	bool bDone=false;
	int iRet=0;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR!!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"File opened successfully");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.lpszMenuName=NULL;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);
	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error in ChoosePixelFormat\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in SetPixelFormat\n");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"Error in wglCreateContext\n");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"Error in wglMakeCurrent\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\ninitialize function executed successfully\n");
	}

	ShowWindow(hwnd,iCmdshow);
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			/*if(gbIsActiveWindow)
			{
				update();
			}*/
			display();			
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen();
	void uninitialize();
	void resize(int,int);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
				case VK_NUMPAD1:
				case 0x60:
					keyPressed = 1;
					break;
				case VK_NUMPAD2:
					keyPressed = 2;
					break;
				case VK_NUMPAD3:
					keyPressed = 3;
					break;
				case VK_NUMPAD4:
					keyPressed = 4;
					break;
				default:
					keyPressed = 0;
					break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;

	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
		}
		gbIsFullscreen=true;
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,WS_OVERLAPPEDWINDOW|dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}

int initialize()
{
	void resize(int,int);
	BOOL LoadTextures(GLuint* , TCHAR[]);
	void uninitialize();

	GLuint vertexShaderObject=0;
	GLuint fragmentShaderObject=0;

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);

	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==GL_FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	GLenum status=glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile,"\nError in initialzing glewInit()");
		uninitialize();
		exit(0);

	}

	//shaders work starts
	//creating shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);


	//writing source code
	const GLchar *vertexShaderSourceCode=
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCood;" \
		"uniform mat4 u_mvp_uniform;" \
		"out vec2 out_texCoord;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform * vPosition;" \
			"out_texCoord= vTexCood;" \

		"}";

	//specify vertexShader code to object
	glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL); 

	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking for vertex shader
	GLint iShaderCompileStatus=0;
	GLint iInfoLogLength=0;
	char *szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
			if(szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);

				fprintf(gpFile,"\n Error in vertexShaderObject : %s",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//fragment shader creation
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//source code to fragment shader source
	const GLchar *fragmentShaderSourceCode=
		"#version 450 core" \
		"\n" \
		"in vec2 out_texCoord;" \
		"out vec4 fragColor;" \
		"uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
			"fragColor=texture(u_sampler,out_texCoord);" \
		"}";

		//specify shader to shader object
		glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

		//compile shader
		glCompileShader(fragmentShaderObject);

		//error checking for fragment shader obejct
		iShaderCompileStatus=0;
		iInfoLogLength=0;
		szInfoLog=NULL;

		glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
		if(iShaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			if(iInfoLogLength>0)
			{
				szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
				if(szInfoLog)
				{
					GLsizei  written;
					glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
					
					fprintf(gpFile,"\n Error in fragment shader compilation: %s",szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);

				}
			}
		}

		//create program for shaders
		gShaderProgramObject=glCreateProgram();

		//attach shader to program
		glAttachShader(gShaderProgramObject,vertexShaderObject);
		glAttachShader(gShaderProgramObject,fragmentShaderObject);

		//prelinkng attributes position
		glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
		glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_TEXCOORD0,"vTexCood");

		//linking the program for gpu specfic shader processor compilation

		glLinkProgram(gShaderProgramObject);

		GLint iProgramLinkStatus=0;
		iInfoLogLength=0;
		szInfoLog=NULL;

		glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);
		if(iProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			if(iInfoLogLength>0)
			{	
				szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
				if(szInfoLog)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);

					fprintf(gpFile,"\nError while linking program: %s",szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

		//post linking uniform 
		//matrix
		mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_uniform");
		//texture
		samplerUniform=glGetUniformLocation(gShaderProgramObject,"u_sampler");



		//drawing related code

		const GLfloat squareVertices[]={
			1.0f,1.0f,0.0f,
			-1.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};


		//create recoreder
		glGenVertexArrays(1,&vao_square);
		//recording start for showing the data
		glBindVertexArray(vao_square);

			//generate the drawing commands buffers for position
			glGenBuffers(1,&vbo_square_position);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
				glBufferData(GL_ARRAY_BUFFER,sizeof(squareVertices),squareVertices,GL_STATIC_DRAW);
				glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
				glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);
			
			//generate buffers drawing commands for texcoord
			glGenBuffers(1,&vbo_square_texCoord);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_square_texCoord);
				glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
				glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
				glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
			glBindBuffer(GL_ARRAY_BUFFER,0);
		
		glBindVertexArray(0);


		glClearColor(0.0f,0.0f,0.0f,1.0f);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glClearDepth(1.0f);
		
		glEnable(GL_TEXTURE_2D);
		LoadTextures(&texture_smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));

		//glEnable(GL_FRONT_FACE);
		//glDisable(GL_CULL_FACE);

		//perspctive projection matrix to identity
		perspectiveProjectionMatrix=mat4::identity();

		resize(WIN_WIDTH,WIN_HEIGHT);
	return 0;
}
BOOL LoadTextures(GLuint *texture, TCHAR imageResourceId[])
{
	BOOL bStatus = FALSE;
	HBITMAP hBitmap;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),imageResourceId,IMAGE_BITMAP,0,0,LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap,sizeof(bmp),&bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glGenTextures(1,texture);
		glBindTexture(GL_TEXTURE_2D,*texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,bmp.bmWidth,bmp.bmHeight,0,GL_BGR_EXT,GL_UNSIGNED_BYTE,bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,0);

	}
	return bStatus;
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	//declarations of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;

	//texcoord vertices
	GLfloat texCoordVertices[8]; 

	//load identity 
	modelViewMatrix=mat4::identity();
	modelViewProjectionMatrix=mat4::identity();
	translationMatrix=mat4::identity();

	//do necessary for transformation
	translationMatrix=translate(0.0f,0.0f,-4.0f);

	modelViewMatrix=modelViewMatrix* translationMatrix;

	modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;

	//sending the matrix to gpu
	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_smiley);
	glUniform1i(samplerUniform, 0);
	
	if(keyPressed==1)
	{
		fprintf(gpFile,"\n In keypressed1");
		texCoordVertices[0]={0.5f};
		texCoordVertices[1]={0.5f};
		texCoordVertices[2]={0.0f};
		texCoordVertices[3]={0.5f};
		texCoordVertices[4]={0.0f};
		texCoordVertices[5]={0.0f};
		texCoordVertices[6]={0.5f};
		texCoordVertices[7]={0.0f};
	}
	else if(keyPressed==2)
	{
		fprintf(gpFile, "\n In keypressed2");
		texCoordVertices[0] = { 2.0f };
		texCoordVertices[1] = { 2.0f };
		texCoordVertices[2] = { 0.0f };
		texCoordVertices[3] = { 2.0f };
		texCoordVertices[4] = { 0.0f };
		texCoordVertices[5] = { 0.0f };
		texCoordVertices[6] = { 2.0f };
		texCoordVertices[7] = { 0.0f };
	}
	else if(keyPressed==3)
	{
		texCoordVertices[0] = { 1.0f };
		texCoordVertices[1] = { 1.0f };
		texCoordVertices[2] = { 0.0f };
		texCoordVertices[3] = { 1.0f };
		texCoordVertices[4] = { 0.0f };
		texCoordVertices[5] = { 0.0f };
		texCoordVertices[6] = { 1.0f };
		texCoordVertices[7] = { 0.0f };

	}
	else 
	{
		fprintf(gpFile, "\n In keypressed default");
		texCoordVertices[0] = { 0.5f };
		texCoordVertices[1] = { 0.5f };
		texCoordVertices[2] = { 0.5f };
		texCoordVertices[3] = { 0.5f };
		texCoordVertices[4] = { 0.5f };
		texCoordVertices[5] = { 0.5f };
		texCoordVertices[6] = { 0.5f };
		texCoordVertices[7] = { 0.5f };
	}
	

	
	glBindVertexArray(vao_square);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_square_texCoord);
		glBufferData(GL_ARRAY_BUFFER,sizeof(texCoordVertices),texCoordVertices,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);
	
	glDrawArrays(GL_TRIANGLE_FAN,0,4);	

	glBindVertexArray(0);
	glUseProgram(0);

	SwapBuffers(ghdc);
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);
}

void uninitialize()
{
	if(vao_square)
	{
		glDeleteVertexArrays(1,&vao_square);
		vao_square=0;
	}
	if(vbo_square_position)
	{
		glDeleteBuffers(1,&vbo_square_position);
		vbo_square_position = 0;
	}
	if(vbo_square_texCoord)
	{
		glDeleteBuffers(1,&vbo_square_texCoord);
		vbo_square_texCoord=0;
	}

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		GLsizei shaderNumber=0;
		GLsizei shaderCount=0;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		GLuint *pShaders=(GLuint*)malloc(sizeof(GLuint)*shaderCount);
		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&shaderCount,pShaders);
			for(shaderNumber=0;shaderNumber<shaderCount;shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber]=0;
			}
			free(pShaders);
		}

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;
	}

	if(gbIsFullscreen)
	{

		SetWindowLong(ghwnd,GWL_STYLE,WS_OVERLAPPEDWINDOW|dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}

	if(gpFile)
	{
		fprintf(gpFile,"\n\nProgram executed successfully\n");
		fclose(gpFile);
		gpFile=NULL;
	}
}

void update()
{

}