#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<GL/glew.h>
#include<GL/gl.h>

#include"./../../vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc = NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMALS,
    AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject=0;

GLuint vao_line=0;

GLuint vbo_line_position=0;
GLuint vbo_line_color=0;

mat4 perspectiveProjectionMatrix;

GLuint mvpUniform;
GLuint colorUniform;
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
    void display();
    void update();
    int initialize(void);


    WNDCLASSEX wndclass;
    MSG msg;
    TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
    HWND hwnd;

    bool bDone=false;
    int iRet=0;

    fopen_s(&gpFile,"log.txt","w");
    if(gpFile==NULL)
    {
        MessageBox(NULL,TEXT("ERROR WHILE OPENING FILE"),TEXT("ERROR!!"),MB_OK);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\nFile Opened successfully");
    }

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wndclass.cbWndExtra=0;
    wndclass.cbClsExtra=0;
    wndclass.hInstance=hInstance;
    wndclass.lpszClassName=lpszClassName;
    wndclass.lpfnWndProc=WndProc;
    wndclass.lpszMenuName=NULL;
    wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

    RegisterClassEx(&wndclass);

    hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY PPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

    ghwnd=hwnd;

    iRet=initialize();
    if(iRet==-1)
    {
        fprintf(gpFile,"\nError in ChoosePixelFormat");
        exit(0);
    }
    else if(iRet==-2)
    {
        fprintf(gpFile,"\n Error i SetPixelFormat");
        exit(0);
    }
    else if(iRet==-3)
    {
        fprintf(gpFile,"\nError in wglCreateContext");
        exit(0);
    }
    else if(iRet==-4)
    {
        fprintf(gpFile,"\n Error in wglMakeCurrent");
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\n initialize function executed successfully");
    }

    SetFocus(hwnd);
    SetForegroundWindow(hwnd);
    ShowWindow(hwnd,iCmdShow);

    //game loop
    while(bDone==false)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            
        }
        else
        {
            if(gbIsActiveWindow)
            {
                update();
            }
            display();
        }
    }

    return((int)msg.wParam);
    
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    void ToggleFullscreen();
    void uninitialize();
    void resize(int,int);

    switch(iMsg)
    {
        case WM_SETFOCUS:
            gbIsActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbIsActiveWindow=false;
            break;
        case WM_SIZE:
            resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
                case 0x46:
                    ToggleFullscreen();
                    break;
            }
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            uninitialize();
            PostQuitMessage(0);
            break;

    }

    return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
    MONITORINFO mi;

    if(gbIsFullscreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi={sizeof(MONITORINFO)};
            if(GetWindowPlacement(ghwnd,&wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
            {
                SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

            }
        }
        gbIsFullscreen=true;
        ShowCursor(FALSE);
    }
    else
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
        gbIsFullscreen=false;
    }
}

int initialize()
{
    void resize(int,int);
    void uninitialize();


    PIXELFORMATDESCRIPTOR pfd;
    int iPixelIndex=0;
    memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));


    GLuint vertexShaderObject=0;
    GLuint fragmentShaderObject=0;

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cGreenBits=8;
    pfd.cBlueBits=8;
    pfd.cAlphaBits=8;
    pfd.cDepthBits=32;

    ghdc=GetDC(ghwnd);

    iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelIndex==0)
    {
        return -1;
    }
    
    if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
    {
        return -2;
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
        return -3;
    }

    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
        return -4;
    }

    GLenum status;
    status=glewInit();
    if(status!=GLEW_OK)
    {
        fprintf(gpFile,"\n Error while initialization of GLEW");
        uninitialize();
        exit(0);
    }

    //vertex shader code

    //1.create vertex shader object
    vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

    //2 source code for vertex shader
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
			"out_color=vColor;" \
		"}";

	/*const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
		"}";*/
     

    //3 specify the shader source
	
    glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
    
    //4 compile shader
    glCompileShader(vertexShaderObject);

    //error checking vertex shader object
    GLint iShaderCompileStatus=0;
    int iInfoLogLength=0;
    char* szInfoLog=NULL;

    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,& iShaderCompileStatus);
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\n Vertex Shader Compile Error: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"Vertex Shader compiler successfully");
	}

    //fragment shader obejct
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

    //source code for fragment shader
    const GLchar *fragmentShaderSourceCode=
	{ "#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor=out_color;" \
		"}" \
	};
    //shader source code to shder object
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    //compile shader
    glCompileShader(fragmentShaderObject);

    //error checking for fragment shader object
    iShaderCompileStatus=0;
	iInfoLogLength = 0;
    szInfoLog=NULL;

    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\n Error in Fragment shader Object: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"\nFragment Shader compiled succesfully");
	}

    //creating shader program object
    gShaderProgramObject=glCreateProgram();

    //attach shader to the program
    glAttachShader(gShaderProgramObject,vertexShaderObject);
    glAttachShader(gShaderProgramObject,fragmentShaderObject);

    //prelinking the attributes
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

    //linking the program
    glLinkProgram(gShaderProgramObject);

    GLint iProgramLinkStatus=0;
    iInfoLogLength=0;
    szInfoLog=NULL;

    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);

    if(iProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\nError while Linking the Program: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"\nProgram linked successfully");
	}

    //post uniform location
    mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_uniform");
    //colorUniform=glGetUniformLocation(gShaderProgramObject,"out_color");

    //actual code for geometry
 
	
	//crating VAO for recording 
    glGenVertexArrays(1,&vao_line);
    glBindVertexArray(vao_line);
        glGenBuffers(1,&vbo_line_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
            glBufferData(GL_ARRAY_BUFFER,3*2*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    


    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    perspectiveProjectionMatrix= mat4::identity();

    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}

void resize(int width,int height)
{
    if(height==0)
    {
        height=1;
    }

    glViewport(0,0,GLsizei(width),GLsizei(height));

    perspectiveProjectionMatrix=perspective(45.0f, GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
	GLfloat lineColor[8];
	
	GLfloat lineColorForVerticalLine[3];
	GLfloat horizontalLines[6];
	GLfloat verticalLines[6];

    glUseProgram(gShaderProgramObject);

        modelViewMatrix=mat4::identity();
        modelViewProjectionMatrix=mat4::identity();
        translationMatrix=mat4::identity();

		translationMatrix= translate(0.0f, 0.0f, -3.0f);
        modelViewMatrix=modelViewMatrix*translationMatrix;

        modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		//glLineWidth(5.0f);

		GLfloat lineVerticesHorizontal[] = {
			8.0f,0.0f,0.0f,
			-8.0f,0.0f,0.0f,
		};

		lineColor[0] = 1.0f;
		lineColor[1] = 0.0f;
		lineColor[2] = 0.0f;
		lineColor[3] = 0.0f;
		lineColor[4] = 1.0f;
		lineColor[5] = 0.0f;
		lineColor[6] = 0.0f;
		lineColor[7] = 0.0f;

		//glUniform4fv(colorUniform,1,lineColor);

		//X Axis
		glBindVertexArray(vao_line);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(lineVerticesHorizontal),lineVerticesHorizontal,GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glBindBuffer(GL_ARRAY_BUFFER, vbo_line_color);
			glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDrawArrays(GL_LINES,0,2);
		glBindVertexArray(0);
		

		//Y Axix
		GLfloat lineVerticesVertical[] = {
			0.0f,4.0f,0.0f,
			0.0f,-4.0f,0.0f
		};

		lineColor[0] = 0.0f;
		lineColor[1] = 1.0f;
		lineColor[2] = 0.0f;
		lineColor[3] = 0.0f;
		lineColor[4] = 0.0f;
		lineColor[5] = 1.0f;
		lineColor[6] = 0.0f;
		lineColor[7] = 0.0f;

		//glUniform4fv(colorUniform,1,lineColor);

		glBindVertexArray(vao_line);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(lineVerticesVertical), lineVerticesVertical, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_line_color);
		glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);


		//vertical Lines
		lineColor[0] = 0.0f;
		lineColor[1] = 0.0f;
		lineColor[2] = 1.0f;
		lineColor[3] = 0.0f;
		//vertical Lines
		lineColor[4] = 0.0f;
		lineColor[5] = 0.0f;
		lineColor[6] = 1.0f;
		lineColor[7] = 0.0f;


		//glUniform4fv(colorUniform,1,lineColor);


		glBindVertexArray(vao_line);
			for (int i =1; i < 21; i++)
			{	
				GLfloat width = 2.20f*i / 20;
				verticalLines[0] = width;
				verticalLines[1] = 4.0f;
				verticalLines[2] = 0.0f;

				verticalLines[3] = width;
				verticalLines[4] = -4.0f;
				verticalLines[5] = 0.0f;
				glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
					glBufferData(GL_ARRAY_BUFFER,sizeof(lineColor),lineColor,GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER,0);
				
				glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
					glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLines), verticalLines, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glDrawArrays(GL_LINES, 0, 2);


				 width =-2.20f*i / 20;
				verticalLines[0] = width;
				verticalLines[1] = 4.0f;
				verticalLines[2] = 0.0f;

				verticalLines[3] = width;
				verticalLines[4] = -4.0f;
				verticalLines[5] = 0.0f;
				glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
				glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLines), verticalLines, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glDrawArrays(GL_LINES, 0, 2);
			
			}
		glBindVertexArray(0);


		glBindVertexArray(vao_line);
			for (int i = 1; i < 21; i++)
			{
				GLfloat width = 1.20f*i / 20;
				horizontalLines[0] = 2.2f;
				horizontalLines[1] = width;
				horizontalLines[2] = 0.0f;

				horizontalLines[3] = -2.2f;
				horizontalLines[4] = width;
				horizontalLines[5] = 0.0f;
				glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
				glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLines), horizontalLines, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glDrawArrays(GL_LINES, 0, 2);


				width = -1.20f*i / 20;
				horizontalLines[0] = 2.2f;
				horizontalLines[1] = width;
				horizontalLines[2] = 0.0f;

				horizontalLines[3] = -2.20f;
				horizontalLines[4] = width;
				horizontalLines[5] = 0.0f;
				glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
				glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLines), horizontalLines, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glDrawArrays(GL_LINES, 0, 2);

			}
		glBindVertexArray(0);

    glUseProgram(0);
    SwapBuffers(ghdc);
}


void uninitialize()
{
    if(gbIsFullscreen)
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
        gbIsFullscreen=false;
    }

    if(vao_line)
    {
        glDeleteVertexArrays(1,&vao_line);
        vao_line=0;
    }
    if(vbo_line_position)
    {
        glDeleteBuffers(1,&vbo_line_position);
        vbo_line_position=0;
    }
    if(vbo_line_color)
    {
        glDeleteBuffers(1,&vbo_line_color);
        vbo_line_color=0;
    }

	if (gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		GLsizei shaderNumber = 0;
		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}
    
    if(ghrc==wglGetCurrentContext())
    {
        wglMakeCurrent(NULL,NULL);

    }

    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }

    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"\nProgram executed successfully");
        fclose(gpFile);
        gpFile=NULL;
    }

}

void update()
{

}