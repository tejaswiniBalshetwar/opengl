#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1

#include<math.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include"./../../vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define CHECKIMAGEWIDTH 64
#define CHECKIMAGEHEIGHT 64

GLubyte checkImage[CHECKIMAGEWIDTH][CHECKIMAGEHEIGHT][4];

using namespace vmath;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM ,LPARAM);

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

enum
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject=0;

GLuint vao_square;

GLuint vbo_square_position=0;
GLuint vbo_square_texCoord=0;

mat4 perspectiveProjectionMatrix;

GLuint mvpUniform;
GLuint samplerUniform;

GLuint texture_square=0;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{
	void display();
	int initialize();

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");

	bool bDone=false;
	int iRet=0;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\nFile opened successfully\n");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszMenuName=NULL;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);
	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY PP OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;
	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\n Error in ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\n Error in SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\n Error in wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\n Error in wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n initialize function executed successfully");
	}
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd,iCmdShow);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				//update
			}
			display();
		}
	}

	return(int(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen();
	void uninitialize(void);
	void resize(int,int);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;

			}
			break;
		case WM_SIZE:	
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		break;

	 	case WM_DESTROY:
	 		uninitialize();
	 		PostQuitMessage(0);
	 		break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbIsFullscreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,WS_OVERLAPPEDWINDOW |dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}

int initialize()
{
	void resize(int,int);
	void uninitialize();
	void LoadTexture(void);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;
	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	GLuint vertexShaderObject=0;
	GLuint fragmentShaderObject=0;

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);

	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==GL_FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}
	if(wglMakeCurrent(ghdc,ghrc)==GL_FALSE)
	{
		return -4;
	}

	GLenum status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile,"\nError in glew initialize");
		uninitialize();
		exit(0);
	}

	//shaders code

	//making shader object
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//source code
	const GLchar *vertexShaderSourceCode=
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_uniform;" \
		"out vec2 out_texCoord;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
			"out_texCoord=vTexCoord;" \
		"}"; 

		//specify shader source code to shader object
		glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

		//compile shader
		glCompileShader(vertexShaderObject);

		//error checking vertex shader object
		GLint iShaderCompileStauts=0;
		int iInfoLogLength=0;
		char* szInfoLog=NULL;

		glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStauts);
		if(iShaderCompileStauts==GL_FALSE)
		{
			glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			if(iInfoLogLength>0)
			{
				szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
				if(szInfoLog)
				{
					GLsizei written;
					glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
					fprintf(gpFile,"\n Error in compilation of vertexShader: %s",szInfoLog);
					free(szInfoLog);

					uninitialize();
					exit(0);

				}
			}
		}

		//fragment shader object
		fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		//write source code for fragment shader
		const GLchar *fragmentShaderSourceCode=
			"#version 450 core" \
			"\n" \
			"in vec2 out_texCoord;" \
			"out vec4 fragColor;" \
			"uniform sampler2D u_sampler;" \
			"void main(void)" \
			"{" \
				"fragColor=texture(u_sampler,out_texCoord);" \
			"}";

		//specify the source code to fragment shader object
		glShaderSource(fragmentShaderObject,1,(GLchar**)&fragmentShaderSourceCode,NULL);

		//compile shader
		glCompileShader(fragmentShaderObject);

		//error checking for fragment shader object
		iShaderCompileStauts=0;
		iInfoLogLength=0;
		szInfoLog=NULL;

		glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStauts);
		if(iShaderCompileStauts==GL_FALSE)
		{
			glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			if(iInfoLogLength>0)
			{
				szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
				if(szInfoLog)
				{
					GLsizei written;
					glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);	
					
					fprintf(gpFile,"Error in fragment Shader: %s",szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}


		//creating program object for the gpu to run
		gShaderProgramObject=glCreateProgram();

		//attach shader to the program
		glAttachShader(gShaderProgramObject,vertexShaderObject);
		glAttachShader(gShaderProgramObject,fragmentShaderObject);

		//prelinking attributes storage
		glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
		glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");

		glLinkProgram(gShaderProgramObject);

		GLint iProgramLinkStatus=0;
		iInfoLogLength=0;
		szInfoLog=NULL;

		glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);
		if(iProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			if(iInfoLogLength>0)
			{
				szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
				if(szInfoLog)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
					fprintf(gpFile,"\nError in Linking program:= %s",szInfoLog);
					free(szInfoLog);

					uninitialize();
					exit(0);

				}
			}
		}

		//post uniform location storing
		mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_uniform");
		samplerUniform = glGetUniformLocation(gShaderProgramObject,"u_sampler");

		//texture coordinates
		const GLfloat texCoord[] = {
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f
		};

		glGenVertexArrays(1,&vao_square);

		glBindVertexArray(vao_square);
			glGenBuffers(1,&vbo_square_position);
				glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
					glBufferData(GL_ARRAY_BUFFER,4*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
					glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
					glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
				glBindBuffer(GL_ARRAY_BUFFER,0);

				//for texcoord
				glGenBuffers(1,&vbo_square_texCoord);
				glBindBuffer(GL_ARRAY_BUFFER,vbo_square_texCoord);
					glBufferData(GL_ARRAY_BUFFER,sizeof(texCoord),texCoord,GL_STATIC_DRAW);
					glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
					glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
				glBindBuffer(GL_ARRAY_BUFFER,0);
		glBindVertexArray(0);


		glClearColor(0.0f,0.0f,0.0f,1.0f);
		glClearDepth(1.0f);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		perspectiveProjectionMatrix = mat4::identity();

		glEnable(GL_TEXTURE_2D);
		LoadTexture();

		resize(WIN_WIDTH,WIN_HEIGHT);
		return 0;
}

void LoadTexture()
{

	void makeCheckImage();

	makeCheckImage();

	glPixelStorei(1,GL_UNPACK_ALIGNMENT);
	glGenTextures(1, &texture_square);
	glBindTexture(GL_TEXTURE_2D,texture_square);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CHECKIMAGEWIDTH,CHECKIMAGEHEIGHT,0,GL_RGBA,GL_UNSIGNED_BYTE,checkImage);
	glBindTexture(GL_TEXTURE_2D,0);

}


void makeCheckImage()
{
	int i, j, c;
	for (i = 0; i < CHECKIMAGEHEIGHT; i++)
	{
		for (j = 0; j < CHECKIMAGEWIDTH; j++)
		{
			c =( ((i & 0x8)==0) ^ ((j& 0x8)==0) )* 255;

			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = 255;
		}
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	translationMatrix = translate(0.0f,0.0f,-3.0f);
	modelViewMatrix = modelViewMatrix * translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,texture_square);
	glUniform1i(samplerUniform,0);

	GLfloat vertices[12] = {
		-2.0f,-1.0f,0.0f,
		-2.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0
	};

	glBindVertexArray(vao_square);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		glDrawArrays(GL_TRIANGLE_FAN,0,4);
	glBindVertexArray(0);

	vertices[0] = 1.0f;
	vertices[1] = -1.0f;
	vertices[2] = 0.0f;
	vertices[3] = 1.0f;
	vertices[4] = 1.0f;
	vertices[5] = 0.0f;
	vertices[6] = 2.414210f;
	vertices[7] = 1.0f;
	vertices[8] = -1.414210f;
	vertices[9] = 2.414210f;
	vertices[10] = -1.0f;
	vertices[11] = -1.414210f;

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,texture_square);
	glUniform1i(samplerUniform,0);
	
	glBindVertexArray(vao_square);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_square_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);

}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize()
{
	if (vao_square)
	{
		glDeleteVertexArrays(1, &vao_square);
		vao_square = 0;
	}
	if (vbo_square_position)
	{
		glDeleteBuffers(1, &vbo_square_position);
		vbo_square_position = 0;
	}
	if (vbo_square_texCoord)
	{
		glDeleteBuffers(1, &vbo_square_texCoord);
		vbo_square_texCoord = 0;
	}

	if (gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		GLsizei shaderNumber = 0;
		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}


	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,WS_OVERLAPPEDWINDOW |dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(0,0);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}
	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}



	if(gpFile)
	{
		fprintf(gpFile,"\n Program Executed successfully");
		fclose(gpFile);
		gpFile=NULL;
	}
}


