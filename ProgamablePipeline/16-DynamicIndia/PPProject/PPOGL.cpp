#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<GL/glew.h>
#include<GL/gl.h>

#include"./../../vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc = NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

GLfloat colorForCircle[4000];
GLfloat verticesForInnerCircle[4000];


enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMALS,
    AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject=0;

GLuint vao_line=0;
GLuint vbo_line_position=0;
GLuint vbo_line_color=0;


GLuint vao_circle = 0;
GLuint vbo_circle_position = 0;
GLuint vbo_circle_color = 0;

GLuint vao_square = 0;
GLuint vbo_square_position = 0;
GLuint vbo_square_color = 0;

mat4 perspectiveProjectionMatrix;

GLuint mvpUniform;
GLuint colorUniform;


//dynamic india translations
GLfloat translateIInX = -4.0f;

GLfloat translateNInY = 4.0f;

GLfloat translateD = 1.0f;

GLfloat translateI2InY = -4.0f;

GLfloat translateAInX = 5.0f;

bool showPlanes = false;
bool showTricolor = false;



int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
    void display();
    void update();
    int initialize(void);


    WNDCLASSEX wndclass;
    MSG msg;
    TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
    HWND hwnd;

    bool bDone=false;
    int iRet=0;

    fopen_s(&gpFile,"log.txt","w");
    if(gpFile==NULL)
    {
        MessageBox(NULL,TEXT("ERROR WHILE OPENING FILE"),TEXT("ERROR!!"),MB_OK);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\nFile Opened successfully");
    }

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wndclass.cbWndExtra=0;
    wndclass.cbClsExtra=0;
    wndclass.hInstance=hInstance;
    wndclass.lpszClassName=lpszClassName;
    wndclass.lpfnWndProc=WndProc;
    wndclass.lpszMenuName=NULL;
    wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

    RegisterClassEx(&wndclass);

    hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY PPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

    ghwnd=hwnd;

    iRet=initialize();
    if(iRet==-1)
    {
        fprintf(gpFile,"\nError in ChoosePixelFormat");
        exit(0);
    }
    else if(iRet==-2)
    {
        fprintf(gpFile,"\n Error i SetPixelFormat");
        exit(0);
    }
    else if(iRet==-3)
    {
        fprintf(gpFile,"\nError in wglCreateContext");
        exit(0);
    }
    else if(iRet==-4)
    {
        fprintf(gpFile,"\n Error in wglMakeCurrent");
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\n initialize function executed successfully");
    }

    SetFocus(hwnd);
    SetForegroundWindow(hwnd);
    ShowWindow(hwnd,iCmdShow);

    //game loop
    while(bDone==false)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            
        }
        else
        {
            if(gbIsActiveWindow)
            {
                update();
            }
            display();
        }
    }

    return((int)msg.wParam);
    
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    void ToggleFullscreen();
    void uninitialize();
    void resize(int,int);

    switch(iMsg)
    {
        case WM_SETFOCUS:
            gbIsActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbIsActiveWindow=false;
            break;
        case WM_SIZE:
            resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
                case 0x46:
                    ToggleFullscreen();
                    break;
            }
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            uninitialize();
            PostQuitMessage(0);
            break;

    }

    return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
    MONITORINFO mi;

    if(gbIsFullscreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi={sizeof(MONITORINFO)};
            if(GetWindowPlacement(ghwnd,&wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
            {
                SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

            }
        }
        gbIsFullscreen=true;
        ShowCursor(FALSE);
    }
    else
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
        gbIsFullscreen=false;
    }
}

int initialize()
{
    void resize(int,int);
    void uninitialize();
	void calculateVerticesForCircle(GLfloat, GLfloat, GLfloat, GLfloat*,GLfloat*);


    PIXELFORMATDESCRIPTOR pfd;
    int iPixelIndex=0;
    memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));


    GLuint vertexShaderObject=0;
    GLuint fragmentShaderObject=0;

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cGreenBits=8;
    pfd.cBlueBits=8;
    pfd.cAlphaBits=8;
    pfd.cDepthBits=32;

    ghdc=GetDC(ghwnd);

    iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelIndex==0)
    {
        return -1;
    }
    
    if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
    {
        return -2;
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
        return -3;
    }

    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
        return -4;
    }

    GLenum status;
    status=glewInit();
    if(status!=GLEW_OK)
    {
        fprintf(gpFile,"\n Error while initialization of GLEW");
        uninitialize();
        exit(0);
    }

    //vertex shader code

    //1.create vertex shader object
    vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

    //2 source code for vertex shader
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
			"out_color=vColor;" \
		"}";

	/*const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
		"}";*/
     

    //3 specify the shader source
	
    glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
    
    //4 compile shader
    glCompileShader(vertexShaderObject);

    //error checking vertex shader object
    GLint iShaderCompileStatus=0;
    int iInfoLogLength=0;
    char* szInfoLog=NULL;

    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,& iShaderCompileStatus);
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\n Vertex Shader Compile Error: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"Vertex Shader compiler successfully");
	}

    //fragment shader obejct
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

    //source code for fragment shader
    const GLchar *fragmentShaderSourceCode=
	{ "#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor=out_color;" \
		"}" \
	};
    //shader source code to shder object
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    //compile shader
    glCompileShader(fragmentShaderObject);

    //error checking for fragment shader object
    iShaderCompileStatus=0;
	iInfoLogLength = 0;
    szInfoLog=NULL;

    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\n Error in Fragment shader Object: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"\nFragment Shader compiled succesfully");
	}

    //creating shader program object
    gShaderProgramObject=glCreateProgram();

    //attach shader to the program
    glAttachShader(gShaderProgramObject,vertexShaderObject);
    glAttachShader(gShaderProgramObject,fragmentShaderObject);

    //prelinking the attributes
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

    //linking the program
    glLinkProgram(gShaderProgramObject);

    GLint iProgramLinkStatus=0;
    iInfoLogLength=0;
    szInfoLog=NULL;

    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);

    if(iProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\nError while Linking the Program: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"\nProgram linked successfully");
	}

    //post uniform location
    mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_uniform");
    //colorUniform=glGetUniformLocation(gShaderProgramObject,"out_color");

    //actual code for geometry
	
	
	//crating VAO for recording 
	//1.line
    glGenVertexArrays(1,&vao_line);
    glBindVertexArray(vao_line);
        glGenBuffers(1,&vbo_line_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
            glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,2*4*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

	//square
	GLfloat square_vertices[] = {
		1.0f,1.20f,0.0f,
		-1.0f,1.20f,0.0f,
		-1.0f,-1.20f,0.0f,
		1.0f,-1.20f,0.0f
	};

	GLfloat square_color[] = {
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
	};

	glGenVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		glGenBuffers(1,&vbo_square_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(square_vertices),square_vertices,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_square_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(square_color),square_color,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	
	//half circle
	
	GLfloat XCenterOfCircle = 0.0f;
	GLfloat YCenterOfCircle = 0.0f;
	GLfloat radius = 1.0f;

	memset(verticesForInnerCircle,0,sizeof(verticesForInnerCircle));
	memset(colorForCircle,0,sizeof(colorForCircle));
	calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);

	//circle
	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);
		glGenBuffers(1, &vbo_circle_position);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(verticesForInnerCircle), verticesForInnerCircle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &vbo_circle_color);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, sizeof(colorForCircle), colorForCircle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



	//geometry end here
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    perspectiveProjectionMatrix= mat4::identity();

    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
	GLfloat x = 0.0f, y = 0.0f;
	GLfloat RColor = 0.0706f;
	GLfloat GColor = 0.3831f;
	GLfloat BColor = 0.02745f;
	//vertices[2000];
	for (int i = 0; i < 4000-3; i=i+3)
	{
		GLfloat angle =GLfloat(((M_PI/2)*i) / 2000);
		x =GLfloat (XPointOfCenter + radius * cos(angle));
		y =GLfloat (YPointOfCenter + radius * sin(angle));

		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = 0.0f; 
	}

	for (int i = 0; i < 4000 - 3; i = i + 3)
	{

		if (RColor <= 1.0)
		{
			RColor += 0.001f;

		}
		if (GColor <= 0.6)
		{
			GColor += 0.001f;
		}
		if (BColor <= 0.2)
		{
			BColor += 0.001f;
		}
		color[i] = RColor;
		color[i + 1] = GColor;
		color[i + 2] = BColor;
		//color[i + 3] = 1.0f;
	}

}



void resize(int width,int height)
{
    if(height==0)
    {
        height=1;
    }

    glViewport(0,0,GLsizei(width),GLsizei(height));

    perspectiveProjectionMatrix=perspective(45.0f, GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void display()
{
	
	void drawLine(GLfloat*,int,GLfloat*,int,int);
	void drawTriColor(GLfloat*);
	
	void drawCircle(GLfloat *,int,GLfloat*,int);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	
    glUseProgram(gShaderProgramObject);

        modelViewMatrix=mat4::identity();
        modelViewProjectionMatrix=mat4::identity();
        translationMatrix=mat4::identity();

		translationMatrix= translate(translateIInX, 0.0f, -6.0f);
        modelViewMatrix=modelViewMatrix*translationMatrix;

        modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		glLineWidth(5.0f);
		GLfloat lineVerticesI[]={
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};
		GLfloat colorForLineI[] = {
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f
		};
		drawLine(lineVerticesI,sizeof(lineVerticesI),colorForLineI,sizeof(colorForLineI),2);


		//N
		modelViewMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(-2.0f,translateNInY,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);



		GLfloat lineVerticesN[] = {
			1.0f,1.0f,0.0f,
			1.0f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f

		};
		GLfloat colorForLineN[] = {
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,

		};
		drawLine(lineVerticesN, sizeof(lineVerticesN), colorForLineN, sizeof(colorForLineN),4);

		//D

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(-0.50f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//circle
		//loadIdentity for circle
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		//for circle Translations 
		translationMatrix = translate(-0.50f, 0.0f,-6.0f);
		rotationMatrix = rotate(-90.0f,0.0f,0.0f,1.0f);
		
		modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

		//for blendEffect
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		//for circle Translations 
		translationMatrix = translate(0.10f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		 GLfloat colorForSquare[16];
		colorForSquare[0] = 0.0f;
		colorForSquare[1] = 0.0f;
		colorForSquare[2] = 0.0f;
		colorForSquare[3] = translateD;
		
		colorForSquare[4] = 0.0f;
		colorForSquare[5] = 0.0f;
		colorForSquare[6] = 0.0f;
		colorForSquare[7] = translateD;
		
		colorForSquare[8] = 0.0f;
		colorForSquare[9] = 0.0f;
		colorForSquare[10] = 0.0f;
		colorForSquare[11] = translateD;
		
		colorForSquare[12] = 0.0f;
		colorForSquare[13] = 0.0f;
		colorForSquare[14] = 0.0f;
		colorForSquare[15] = translateD;


		glBindVertexArray(vao_square);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
				glBufferData(GL_ARRAY_BUFFER,sizeof(colorForSquare),colorForSquare,GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER,0);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
		glBindVertexArray(0);



		//for I

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(1.0f, translateI2InY, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//A
		translationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(translateAInX,0.0f,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		GLfloat verticesForA[] = {
			-0.5f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.5f,-1.0f,0.0f,
		};

		GLfloat colorForA[] = {
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,

		};

		drawLine(verticesForA,sizeof(verticesForA),colorForA,sizeof(colorForA),3);
		drawTriColor(verticesForA);
			
    glUseProgram(0);
    SwapBuffers(ghdc);
}

void drawTriColor(GLfloat* verticesForA)
{
	void drawLine(GLfloat*, int, GLfloat*, int, int);

	GLfloat triColorVertex[6];

	GLfloat X1ForLine = (verticesForA[0] + verticesForA[3]) / 2;
	GLfloat Y1ForLine = (verticesForA[1] + verticesForA[4]) / 2;

	GLfloat X2ForLine = (verticesForA[3] + verticesForA[6]) / 2;
	GLfloat Y2ForLine = (verticesForA[1] + verticesForA[4]) / 2;

	GLfloat colorForTricolor[8];

	triColorVertex[0] = X1ForLine;
	triColorVertex[1] = Y1ForLine;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 1.0f;
	colorForTricolor[1] = 0.6f;
	colorForTricolor[2] = 0.2f;
	colorForTricolor[3] = 0.0f;

	colorForTricolor[4] = 1.0f;
	colorForTricolor[5] = 0.6f;
	colorForTricolor[6] = 0.2f;
	colorForTricolor[7] = 0.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

	triColorVertex[0] = X1ForLine - 0.01f;
	triColorVertex[1] = Y1ForLine - 0.04f;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine - 0.04f;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 1.0f;
	colorForTricolor[1] = 1.0f;
	colorForTricolor[2] = 1.0f;
	colorForTricolor[3] = 0.0f;

	colorForTricolor[4] = 1.0f;
	colorForTricolor[5] = 1.0f;
	colorForTricolor[6] = 1.0f;
	colorForTricolor[7] = 0.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

	triColorVertex[0] = X1ForLine;
	triColorVertex[1] = Y1ForLine - 0.07f;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine - 0.07f;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 0.0706f;
	colorForTricolor[1] = 0.3831f;
	colorForTricolor[2] = 0.02745f;
	colorForTricolor[3] = 0.0f;

	colorForTricolor[4] = 0.0706f;
	colorForTricolor[5] = 0.3831f;
	colorForTricolor[6] = 0.02745f;
	colorForTricolor[7] = 0.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);
}

void drawCircle(GLfloat * vertices,int verticesSize,GLfloat *color,int colorSize)
{
	glBindVertexArray(vao_circle);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,verticesSize,(vertices),GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, colorSize,color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		glPointSize(3.0f);
		glDrawArrays(GL_POINTS,0,4000);
	glBindVertexArray(0);
}



void drawLine(GLfloat *vertices ,int sizeOfVertices, GLfloat * color, int sizeofColor, int numberOfPoints)
{
	glBindVertexArray(vao_line);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER,sizeOfVertices,vertices,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
		glBufferData(GL_ARRAY_BUFFER,sizeofColor,color,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
	glBindVertexArray(0);
}

void uninitialize()
{
    if(gbIsFullscreen)
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
        gbIsFullscreen=false;
    }

    if(vao_line)
    {
        glDeleteVertexArrays(1,&vao_line);
        vao_line=0;
    }
    if(vbo_line_position)
    {
        glDeleteBuffers(1,&vbo_line_position);
        vbo_line_position=0;
    }
    if(vbo_line_color)
    {
        glDeleteBuffers(1,&vbo_line_color);
        vbo_line_color=0;
    }

	if (vao_circle)
	{
		glDeleteVertexArrays(1, &vao_circle);
		vao_circle = 0;
	}
	if (vbo_circle_position)
	{
		glDeleteBuffers(1, &vbo_circle_position);
		vbo_circle_position = 0;
	}
	if (vbo_circle_color)
	{
		glDeleteBuffers(1, &vbo_circle_color);
		vbo_circle_color = 0;
	}

	
	if (gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		GLsizei shaderNumber = 0;
		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}
    
    if(ghrc==wglGetCurrentContext())
    {
        wglMakeCurrent(NULL,NULL);

    }

    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }

    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"\nProgram executed successfully");
        fclose(gpFile);
        gpFile=NULL;
    }

}

void update()
{
	if (translateIInX <= -2.5f)
	{
		translateIInX = translateIInX + 0.0005;
	}

	if (translateIInX >= -2.500609f && translateAInX >= 2.0f)
	{
		translateAInX = translateAInX - 0.0005f;
	}

	if (translateIInX >= -2.500609f && translateAInX <= 2.0f &&  translateNInY >= 0.0f)
	{
		translateNInY = translateNInY - 0.0005f;
	}

	if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY <= 0.0f)
	{
		translateI2InY = translateI2InY + 0.0005f;
	}

	if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY >= 0.0f &&translateD >=0.0f)
	{
		translateD = translateD - 0.0005f;
	}
}