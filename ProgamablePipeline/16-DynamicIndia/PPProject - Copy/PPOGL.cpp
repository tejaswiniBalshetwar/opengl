#include<windows.h>
#include<stdio.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<GL/glew.h>
#include<GL/gl.h>

#include"./../../vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc = NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

GLfloat colorForCircle[4000];
GLfloat verticesForInnerCircle[4000];

struct vertex
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

enum
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMALS,
    AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject=0;

GLuint vao_line=0;
GLuint vbo_line_position=0;
GLuint vbo_line_color=0;


GLuint vao_circle = 0;
GLuint vbo_circle_position = 0;
GLuint vbo_circle_color = 0;

GLuint vao_square = 0;
GLuint vbo_square_position = 0;
GLuint vbo_square_color = 0;

//for plane

vertex positions[39];

GLuint vao_triangle = 0;
GLuint vbo_triangle_position = 0;
GLuint vbo_triangle_color = 0;


mat4 perspectiveProjectionMatrix;

GLuint mvpUniform;
GLuint colorUniform;


//dynamic india translations
GLfloat translateIInX = -4.0f;

GLfloat translateNInY = 4.0f;

GLfloat translateD = 1.0f;

GLfloat translateI2InY = -4.0f;

GLfloat translateAInX = 5.0f;

bool ShowPlanes = false;
bool ShowTricolor = false;

GLfloat incAngle = 270.0f;
GLfloat upperPlaneAngle = M_PI;
GLfloat upperPlaneStopAngle = (3.0f*M_PI) / 2.0f;

GLfloat lowerPlaneAngle = M_PI;
GLfloat decAngle = 90.0f;

GLfloat incAngle2 = 0.0f;
GLfloat upperPlaneAngle2 = (3.0f*M_PI) / 2.0f;
GLfloat upperPlaneStopAngle2 = 2.5f*M_PI;

GLfloat lowerPlaneAngle2 = (M_PI / 2.0f);
GLfloat decAngle2 = 360.0f;

GLfloat translateAllInXDirection = -2.499705f;

bool meet = false;
bool crossTheA = false;
GLfloat translateMiddle = -7.1f;

GLfloat XTranslatePointUP = 0.0f;
GLfloat YTranslatePointUP = 0.0f;

GLfloat XTranslatePointDown = 0.0f;
GLfloat YTranslatePointDown = 0.0f;




int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
    void display();
    void update();
    int initialize(void);
	void PlaneTranslations();

    WNDCLASSEX wndclass;
    MSG msg;
    TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
    HWND hwnd;

    bool bDone=false;
    int iRet=0;

    fopen_s(&gpFile,"log.txt","w");
    if(gpFile==NULL)
    {
        MessageBox(NULL,TEXT("ERROR WHILE OPENING FILE"),TEXT("ERROR!!"),MB_OK);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\nFile Opened successfully");
    }

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wndclass.cbWndExtra=0;
    wndclass.cbClsExtra=0;
    wndclass.hInstance=hInstance;
    wndclass.lpszClassName=lpszClassName;
    wndclass.lpfnWndProc=WndProc;
    wndclass.lpszMenuName=NULL;
    wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

    RegisterClassEx(&wndclass);

    hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY PPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

    ghwnd=hwnd;

    iRet=initialize();
    if(iRet==-1)
    {
        fprintf(gpFile,"\nError in ChoosePixelFormat");
        exit(0);
    }
    else if(iRet==-2)
    {
        fprintf(gpFile,"\n Error i SetPixelFormat");
        exit(0);
    }
    else if(iRet==-3)
    {
        fprintf(gpFile,"\nError in wglCreateContext");
        exit(0);
    }
    else if(iRet==-4)
    {
        fprintf(gpFile,"\n Error in wglMakeCurrent");
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\n initialize function executed successfully");
    }

    SetFocus(hwnd);
    SetForegroundWindow(hwnd);
    ShowWindow(hwnd,iCmdShow);

    //game loop
    while(bDone==false)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            
        }
        else
        {
            if(gbIsActiveWindow)
            {
                update();
				if (ShowPlanes)
				{
					PlaneTranslations();
				}
            }
            display();
        }
    }

    return((int)msg.wParam);
    
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    void ToggleFullscreen();
    void uninitialize();
    void resize(int,int);

    switch(iMsg)
    {
        case WM_SETFOCUS:
            gbIsActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbIsActiveWindow=false;
            break;
        case WM_SIZE:
            resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
                case 0x46:
                    ToggleFullscreen();
                    break;
            }
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            uninitialize();
            PostQuitMessage(0);
            break;

    }

    return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
    MONITORINFO mi;

    if(gbIsFullscreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi={sizeof(MONITORINFO)};
            if(GetWindowPlacement(ghwnd,&wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
            {
                SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

            }
        }
        gbIsFullscreen=true;
        ShowCursor(FALSE);
    }
    else
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
        gbIsFullscreen=false;
    }
}

int initialize()
{
    void resize(int,int);
    void uninitialize();
	void calculateVerticesForCircle(GLfloat, GLfloat, GLfloat, GLfloat*,GLfloat*);


	void createPlane();
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelIndex=0;
    memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));


    GLuint vertexShaderObject=0;
    GLuint fragmentShaderObject=0;

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cGreenBits=8;
    pfd.cBlueBits=8;
    pfd.cAlphaBits=8;
    pfd.cDepthBits=32;

    ghdc=GetDC(ghwnd);

    iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelIndex==0)
    {
        return -1;
    }
    
    if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
    {
        return -2;
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
        return -3;
    }

    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
        return -4;
    }

    GLenum status;
    status=glewInit();
    if(status!=GLEW_OK)
    {
        fprintf(gpFile,"\n Error while initialization of GLEW");
        uninitialize();
        exit(0);
    }

    //vertex shader code

    //1.create vertex shader object
    vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

    //2 source code for vertex shader
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
			"out_color=vColor;" \
		"}";

	/*const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
		"}";*/
     

    //3 specify the shader source
	
    glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
    
    //4 compile shader
    glCompileShader(vertexShaderObject);

    //error checking vertex shader object
    GLint iShaderCompileStatus=0;
    int iInfoLogLength=0;
    char* szInfoLog=NULL;

    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,& iShaderCompileStatus);
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\n Vertex Shader Compile Error: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"Vertex Shader compiler successfully");
	}

    //fragment shader obejct
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

    //source code for fragment shader
    const GLchar *fragmentShaderSourceCode=
	{ "#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor=out_color;" \
		"}" \
	};
    //shader source code to shder object
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    //compile shader
    glCompileShader(fragmentShaderObject);

    //error checking for fragment shader object
    iShaderCompileStatus=0;
	iInfoLogLength = 0;
    szInfoLog=NULL;

    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\n Error in Fragment shader Object: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"\nFragment Shader compiled succesfully");
	}

    //creating shader program object
    gShaderProgramObject=glCreateProgram();

    //attach shader to the program
    glAttachShader(gShaderProgramObject,vertexShaderObject);
    glAttachShader(gShaderProgramObject,fragmentShaderObject);

    //prelinking the attributes
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

    //linking the program
    glLinkProgram(gShaderProgramObject);

    GLint iProgramLinkStatus=0;
    iInfoLogLength=0;
    szInfoLog=NULL;

    glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);

    if(iProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength*sizeof(char));
            if(szInfoLog)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"\nError while Linking the Program: %s",szInfoLog);
                free(szInfoLog);
                uninitialize();
                exit(0);
            }
        }
    }
	else
	{
		fprintf(gpFile,"\nProgram linked successfully");
	}

    //post uniform location
    mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_uniform");
    //colorUniform=glGetUniformLocation(gShaderProgramObject,"out_color");

    //actual code for geometry
	
	
	//crating VAO for recording 
	//1.line
    glGenVertexArrays(1,&vao_line);
    glBindVertexArray(vao_line);
        glGenBuffers(1,&vbo_line_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
            glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,2*4*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

	//triangle for plane
	createPlane();


	glGenVertexArrays(1,&vao_triangle);
	glBindVertexArray(vao_triangle);
		glGenBuffers(1,&vbo_triangle_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_triangle_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(positions),positions,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glVertexAttrib4f(AMC_ATTRIBUTE_COLOR, 0.0729f, 0.886f, 0.9333f, 1.0f);
	glBindVertexArray(0);


	//square
	GLfloat square_vertices[] = {
		1.0f,1.20f,0.0f,
		-1.0f,1.20f,0.0f,
		-1.0f,-1.20f,0.0f,
		1.0f,-1.20f,0.0f
	};

	GLfloat square_color[] = {
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,

	};

	glGenVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		glGenBuffers(1,&vbo_square_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(square_vertices),square_vertices,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_square_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(square_color),square_color,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	
	//half circle
	
	GLfloat XCenterOfCircle = 0.0f;
	GLfloat YCenterOfCircle = 0.0f;
	GLfloat radius = 1.0f;

	memset(verticesForInnerCircle,0,sizeof(verticesForInnerCircle));
	memset(colorForCircle,0,sizeof(colorForCircle));
	calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);

	//circle
	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);
		glGenBuffers(1, &vbo_circle_position);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(verticesForInnerCircle), verticesForInnerCircle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &vbo_circle_color);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, sizeof(colorForCircle), colorForCircle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



	//geometry end here
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    perspectiveProjectionMatrix= mat4::identity();

    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}

void createPlane()
{
	
	positions[0].x = 1.0f;
	positions[0].y = 0.0f;
	positions[0].z = 0.0f;

	positions[1].x = 0.0f;
	positions[1].y = -0.35f;
	positions[1].z = 0.0f;

	positions[2].x = 0.0f;
	positions[2].y = 0.35f;
	positions[2].z = 0.0f;

	//body
	positions[3].x = 0.0f;
	positions[3].y = 0.35f;
	positions[3].z = 0.0f;

	positions[4].x = -1.0f;
	positions[4].y = 0.35f;
	positions[4].z = 0.0f;

	positions[5].x = -1.0f;
	positions[5].y = -0.35f;
	positions[5].z = 0.0f;

	positions[6].x = -1.0f;
	positions[6].y = -0.35f;
	positions[6].z = 0.0f;

	positions[7].x = 0.0f;
	positions[7].y = -0.35f;
	positions[7].z = 0.0f;

	positions[8].x = 0.0f;
	positions[8].y = 0.35f;
	positions[8].z = 0.0f;
	
	//polygon start
	positions[9].x = -1.0f;
	positions[9].y = -0.35f;
	positions[9].z = 0.0f;

	positions[10].x = -2.0f;
	positions[10].y = -1.75f;
	positions[10].z = 0.0f;
	
	positions[11].x = -2.5f;
	positions[11].y = -1.75f;
	positions[11].z = 0.0f;

	positions[12].x = -2.5f;
	positions[12].y = -1.75f;
	positions[12].z = 0.0f;

	positions[13].x = -1.8f;
	positions[13].y = -0.35f;
	positions[13].z = 0.0f;

	positions[14].x = -1.0f;
	positions[14].y = -0.35f;
	positions[14].z = 0.0f;

	
	//upper start half
	positions[15].x = -1.0f;
	positions[15].y = 0.35f;
	positions[15].z = 0.0f;

	positions[16].x = -2.0f;
	positions[16].y = 1.75f;
	positions[16].z = 0.0f;

	positions[17].x = -2.5f;
	positions[17].y = 1.75f;
	positions[17].z = 0.0f;

	positions[18].x = -2.5f;
	positions[18].y = 1.75f;
	positions[18].z = 0.0f;

	positions[19].x = -1.8f;
	positions[19].y = 0.35f;
	positions[19].z = 0.0f;

	positions[20].x = -1.0f;
	positions[20].y = 0.35f;
	positions[20].z = 0.0f;
	
	//quad
	positions[21].x = -1.35f;
	positions[21].y = 0.35f;
	positions[21].z = 0.0f;

	positions[22].x = -3.0f;
	positions[22].y = 0.35f;
	positions[22].z = 0.0f;

	positions[23].x = -3.0f;
	positions[23].y = -0.35f;
	positions[23].z = 0.0f;

	positions[24].x = -3.0f;
	positions[24].y = -0.35f;
	positions[24].z = 0.0f;

	positions[25].x = -1.0f;
	positions[25].y = -0.35f;
	positions[25].z = 0.0f;

	positions[26].x = -1.0f;
	positions[26].y =0.35f;
	positions[26].z = 0.0f;
	
	
	//lower part of planes last
	positions[27].x = -3.0f;
	positions[27].y = -0.35f;
	positions[27].z = 0.0f;

	positions[28].x = -3.5f;
	positions[28].y = -1.0f;
	positions[28].z = 0.0f;

	positions[29].x = -3.90f;
	positions[29].y = -1.0f;
	positions[29].z = 0.0f;
	
	positions[30].x = -3.90f;
	positions[30].y = -1.0f;
	positions[30].z = 0.0f;

	positions[31].x = -3.0f;
	positions[31].y = 0.35f;
	positions[31].z = 0.0f;

	positions[32].x = -3.0f;
	positions[32].y = -0.35f;
	positions[32].z = 0.0f;
	
	//plane upper part of last side
	positions[33].x = -3.0f;
	positions[33].y = 0.35f;
	positions[33].z = 0.0f;

	positions[34].x = -3.5f;
	positions[34].y = 1.0f;
	positions[34].z = 0.0f;
	
	positions[35].x = -3.90f;
	positions[35].y = 1.0f;
	positions[35].z = 0.0f;
	
	positions[36].x = -3.90f;
	positions[36].y = 1.0f;
	positions[36].z = 0.0f;

	positions[37].x = -3.0f;
	positions[37].y = -0.35f;
	positions[37].z = 0.0f;

	positions[38].x = -3.0f;
	positions[38].y = -0.35f;
	positions[38].z = 0.0f;
	
}

void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
	GLfloat x = 0.0f, y = 0.0f;
	GLfloat RColor = 0.0706f;
	GLfloat GColor = 0.3831f;
	GLfloat BColor = 0.02745f;
	//vertices[2000];
	for (int i = 0; i < 4000-3; i=i+3)
	{
		GLfloat angle =GLfloat(((M_PI/2)*i) / 2000);
		x =GLfloat (XPointOfCenter + radius * cos(angle));
		y =GLfloat (YPointOfCenter + radius * sin(angle));

		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = 0.0f; 
	}

	for (int i = 0; i < 4000 - 3; i = i + 3)
	{

		if (RColor <= 1.0)
		{
			RColor += 0.001f;

		}
		if (GColor <= 0.6)
		{
			GColor += 0.001f;
		}
		if (BColor <= 0.2)
		{
			BColor += 0.001f;
		}
		color[i] = RColor;
		color[i + 1] = GColor;
		color[i + 2] = BColor;
		//color[i + 3] = 1.0f;
	}

}



void resize(int width,int height)
{
    if(height==0)
    {
        height=1;
    }

    glViewport(0,0,GLsizei(width),GLsizei(height));

    perspectiveProjectionMatrix=perspective(45.0f, GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void display()
{
	
	void drawLine(GLfloat*,int,GLfloat*,int,int);
	void drawTriColor(GLfloat*);
	void drawPlane();
	void drawUpperPlane(void);
	void drawLowerPlane(void);
	void drawMiddelePlane(void);
	
	void drawCircle(GLfloat *,int,GLfloat*,int);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
    glUseProgram(gShaderProgramObject);

        modelViewMatrix=mat4::identity();
        modelViewProjectionMatrix=mat4::identity();
        translationMatrix=mat4::identity();

		translationMatrix= translate(translateIInX, 0.0f, -6.0f);
        modelViewMatrix=modelViewMatrix*translationMatrix;

        modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		glLineWidth(5.0f);
		GLfloat lineVerticesI[]={
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};
		GLfloat colorForLineI[] = {
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f
		};
		drawLine(lineVerticesI,sizeof(lineVerticesI),colorForLineI,sizeof(colorForLineI),2);


		//N
		modelViewMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(-2.0f,translateNInY,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);



		GLfloat lineVerticesN[] = {
			1.0f,1.0f,0.0f,
			1.0f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f

		};
		GLfloat colorForLineN[] = {
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,

		};
		drawLine(lineVerticesN, sizeof(lineVerticesN), colorForLineN, sizeof(colorForLineN),4);

		//D

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(-0.50f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//circle
		//loadIdentity for circle
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		//for circle Translations 
		translationMatrix = translate(-0.50f, 0.0f,-6.0f);
		rotationMatrix = rotate(-90.0f,0.0f,0.0f,1.0f);
		
		modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

		//for blendEffect
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		//for circle Translations 
		translationMatrix = translate(0.10f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		 GLfloat colorForSquare[16];
		colorForSquare[0] = 0.0f;
		colorForSquare[1] = 0.0f;
		colorForSquare[2] = 0.0f;
		colorForSquare[3] = translateD;
		
		colorForSquare[4] = 0.0f;
		colorForSquare[5] = 0.0f;
		colorForSquare[6] = 0.0f;
		colorForSquare[7] = translateD;
		
		colorForSquare[8] = 0.0f;
		colorForSquare[9] = 0.0f;
		colorForSquare[10] = 0.0f;
		colorForSquare[11] = translateD;
		
		colorForSquare[12] = 0.0f;
		colorForSquare[13] = 0.0f;
		colorForSquare[14] = 0.0f;
		colorForSquare[15] = translateD;


		glBindVertexArray(vao_square);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
				glBufferData(GL_ARRAY_BUFFER,sizeof(colorForSquare),colorForSquare,GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER,0);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
		glBindVertexArray(0);



		//for I

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(1.0f, translateI2InY, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//A
		translationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(translateAInX,0.0f,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		GLfloat verticesForA[] = {
			-0.5f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.5f,-1.0f,0.0f,
		};

		GLfloat colorForA[] = {
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,
			0.0706f,0.3831f,0.02745f,1.0f,
			1.0f,0.6f,0.2f,1.0f,

		};

		drawLine(verticesForA,sizeof(verticesForA),colorForA,sizeof(colorForA),3);

		if (ShowTricolor)
		{
			drawTriColor(verticesForA);
		}
			
		

		if (ShowPlanes)
		{
			//upperplane
			drawUpperPlane();

			//Lower Plane
			drawLowerPlane();

			//Middle Plane
			drawMiddelePlane();

		}


    glUseProgram(0);
    SwapBuffers(ghdc);
}
void drawUpperPlane(void)
{
	void drawPlane(void);
	mat4 translationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
	modelViewMatrix= mat4::identity();
	modelViewProjectionMatrix=mat4::identity();
	translationMatrix=mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glLineWidth(8.0f);
	if (meet == false)
	{
		XTranslatePointUP = -1.20f + 4.60f*cos(upperPlaneAngle);
		YTranslatePointUP = 4.6f + 4.60f*sin(upperPlaneAngle);
		//fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointUP,YTranslatePointUP);
		translationMatrix = translate(XTranslatePointUP, YTranslatePointUP, -6.0f);
		//glTranslatef(XTranslatePointUP, YTranslatePointUP, -10.0f);
		if (incAngle <= 360.0f || incAngle > 0.0f)
		{
			rotationMatrix = rotate(incAngle,0.0f,0.0f,1.0f);
			//glRotatef(incAngle, 0.0f, 0.0f, 1.0f);
		}
	}
	else if (translateAllInXDirection > 3.0f)
	{

		XTranslatePointUP = 3.0f + 4.60f*cos(upperPlaneAngle2);
		YTranslatePointUP = 4.60f + 4.60f*sin(upperPlaneAngle2);
		fprintf(gpFile, "XTranslatePoint2:%f  YTranslatePoint2%f\n",XTranslatePointUP,YTranslatePointUP);
		
		//glTranslatef(XTranslatePointUP, YTranslatePointUP, -10.0f);
		translationMatrix = translate(XTranslatePointUP, YTranslatePointUP, -6.0f);
		if (incAngle2 <= 90.0f || incAngle2 > 0.0f)
		{

			//glRotatef(incAngle2, 0.0f, 0.0f, 1.0f);
			rotationMatrix = rotate(incAngle2, 0.0f, 0.0f, 1.0f);
		}
	}
	//else
	//{
	//	fprintf(gpFile, "translateAllInXDirection %f\n", translateAllInXDirection);
	//	translationMatrix = translate(translateAllInXDirection, 0.0f, -6.0f);
	//	//glTranslatef(translateAllInXDirection, 0.0f, -10.0f);
	//}

	

	scaleMatrix = scale(0.350f, 0.350f, 0.350f);
	modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	drawPlane();


}

void drawLowerPlane(void)
{
	void drawPlane(void);
	mat4 translationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
	
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();


	if (meet == false)
	{
		XTranslatePointDown = -1.2f + 4.60f*cos(lowerPlaneAngle);
		YTranslatePointDown = -4.6f + 4.60f*sin(lowerPlaneAngle);
		//fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
		//glTranslatef(XTranslatePointDown, YTranslatePointDown, -10.0f);
		translationMatrix = translate(XTranslatePointDown, YTranslatePointDown, -6.0f);
		if (decAngle <= 90.0f || decAngle > 0.0f)
		{
			rotationMatrix = rotate(decAngle, 0.0f, 0.0f, 1.0f);
			//glRotatef(decAngle, 0.0f, 0.0f, 1.0f);
		}
	}

	else if (translateAllInXDirection > 3.0f)
	{
		XTranslatePointDown = 3.0f + 4.60f*cos(lowerPlaneAngle2);
		YTranslatePointDown = -4.60f + 4.60f*sin(lowerPlaneAngle2);
		//fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
		translationMatrix = translate(XTranslatePointDown, YTranslatePointDown, -6.0f);
		glTranslatef(XTranslatePointDown, YTranslatePointDown, -10.0f);
		if (decAngle2 <= 360.0f || decAngle2 > 270.0f)
		{
			rotationMatrix = rotate(decAngle2, 0.0f, 0.0f, 1.0f);
			//glRotatef(decAngle2, 0.0f, 0.0f, 1.0f);
		}
	}
	else
	{
		//glTranslatef(translateAllInXDirection, 0.0f, -10.0f);
		translationMatrix = translate(translateAllInXDirection, 0.0f, -6.0f);
	}


	scaleMatrix = scale(0.350f, 0.350f, 0.350f);
	modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	drawPlane();

}

void drawMiddelePlane(void)
{
	void drawPlane(void);
	mat4 translationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	//glTranslatef(translateMiddle, 0.0f, -10.0f);
	translationMatrix = translate(translateMiddle, 0.0f, -6.0f);
	
	scaleMatrix = scale(0.350f, 0.350f, 0.350f);
	modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	drawPlane();
}

void drawPlane()
{
	void drawLine(GLfloat*,int,GLfloat*,int,int);


	glBindVertexArray(vao_triangle);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawArrays(GL_TRIANGLE_FAN, 3, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 9, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 15, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 21, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 27, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 33, 6);
	glBindVertexArray(0);

	GLfloat linePositions[] = {
		-3.3f,0.12f,0.0f,
		-5.0f,0.12f,0.0f,
		-5.0f,0.04f,0.0f,
		-3.3f,0.04f,0.0f,
		-3.3f,-0.04f,0.0f,
		-5.0f,-0.04f,0.0f
	};

	GLfloat color[] = {
		1.0f,0.6f,0.2f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		1.0f,1.0f,1.0f,1.0f,
		0.0706f,0.3831f,0.02745f,1.0f,
		0.0f,0.0f,0.0f,1.0f
	};
	drawLine(linePositions, sizeof(linePositions), color, sizeof(color), 6);
}
void drawTriColor(GLfloat* verticesForA)
{
	void drawLine(GLfloat*, int, GLfloat*, int, int);

	GLfloat triColorVertex[6];

	GLfloat X1ForLine = (verticesForA[0] + verticesForA[3]) / 2;
	GLfloat Y1ForLine = (verticesForA[1] + verticesForA[4]) / 2;

	GLfloat X2ForLine = (verticesForA[3] + verticesForA[6]) / 2;
	GLfloat Y2ForLine = (verticesForA[1] + verticesForA[4]) / 2;

	GLfloat colorForTricolor[8];

	triColorVertex[0] = X1ForLine;
	triColorVertex[1] = Y1ForLine;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 1.0f;
	colorForTricolor[1] = 0.6f;
	colorForTricolor[2] = 0.2f;
	colorForTricolor[3] = 1.0f;

	colorForTricolor[4] = 1.0f;
	colorForTricolor[5] = 0.6f;
	colorForTricolor[6] = 0.2f;
	colorForTricolor[7] = 1.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

	triColorVertex[0] = X1ForLine - 0.01f;
	triColorVertex[1] = Y1ForLine - 0.04f;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine - 0.04f;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 1.0f;
	colorForTricolor[1] = 1.0f;
	colorForTricolor[2] = 1.0f;
	colorForTricolor[3] = 1.0f;

	colorForTricolor[4] = 1.0f;
	colorForTricolor[5] = 1.0f;
	colorForTricolor[6] = 1.0f;
	colorForTricolor[7] = 1.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

	triColorVertex[0] = X1ForLine;
	triColorVertex[1] = Y1ForLine - 0.07f;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine - 0.07f;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 0.0706f;
	colorForTricolor[1] = 0.3831f;
	colorForTricolor[2] = 0.02745f;
	colorForTricolor[3] = 1.0f;

	colorForTricolor[4] = 0.0706f;
	colorForTricolor[5] = 0.3831f;
	colorForTricolor[6] = 0.02745f;
	colorForTricolor[7] = 1.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);
}

void drawCircle(GLfloat * vertices,int verticesSize,GLfloat *color,int colorSize)
{
	glBindVertexArray(vao_circle);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,verticesSize,(vertices),GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, colorSize,color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		glPointSize(3.0f);
		glDrawArrays(GL_POINTS,0,4000);
	glBindVertexArray(0);
}



void drawLine(GLfloat *vertices ,int sizeOfVertices, GLfloat * color, int sizeofColor, int numberOfPoints)
{
	glBindVertexArray(vao_line);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER,sizeOfVertices,vertices,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
		glBufferData(GL_ARRAY_BUFFER,sizeofColor,color,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
	glBindVertexArray(0);
}

void uninitialize()
{
    if(gbIsFullscreen)
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
        gbIsFullscreen=false;
    }

    if(vao_line)
    {
        glDeleteVertexArrays(1,&vao_line);
        vao_line=0;
    }
    if(vbo_line_position)
    {
        glDeleteBuffers(1,&vbo_line_position);
        vbo_line_position=0;
    }
    if(vbo_line_color)
    {
        glDeleteBuffers(1,&vbo_line_color);
        vbo_line_color=0;
    }

	if (vao_circle)
	{
		glDeleteVertexArrays(1, &vao_circle);
		vao_circle = 0;
	}
	if (vbo_circle_position)
	{
		glDeleteBuffers(1, &vbo_circle_position);
		vbo_circle_position = 0;
	}
	if (vbo_circle_color)
	{
		glDeleteBuffers(1, &vbo_circle_color);
		vbo_circle_color = 0;
	}

	
	if (gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		GLsizei shaderNumber = 0;
		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}
    
    if(ghrc==wglGetCurrentContext())
    {
        wglMakeCurrent(NULL,NULL);

    }

    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }

    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"\nProgram executed successfully");
        fclose(gpFile);
        gpFile=NULL;
    }

}

void update()
{
	if (translateIInX <= -2.5f)
	{
		translateIInX = translateIInX + 0.005;
	}

	if (translateIInX >= -2.500609f && translateAInX >= 2.0f)
	{
		translateAInX = translateAInX - 0.005f;
	}

	if (translateIInX >= -2.500609f && translateAInX <= 2.0f &&  translateNInY >= 0.0f)
	{
		translateNInY = translateNInY - 0.005f;
	}

	if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY <= 0.0f)
	{
		translateI2InY = translateI2InY + 0.005f;
	}

	if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY >= 0.0f &&translateD >=0.0f)
	{
		translateD = translateD - 0.005f;
	}
	if (translateI2InY >= 0.0f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateIInX >= -2.5f &&translateD <= 0.0f)
	{
		ShowPlanes = true;
	}

	if (XTranslatePointUP >= 3.499705 && XTranslatePointDown >= 3.499705 && translateMiddle >= 3.499705)
	{
		ShowTricolor = true;
	}
}

void PlaneTranslations(void)
{
	if (upperPlaneAngle < upperPlaneStopAngle)
	{
		upperPlaneAngle = upperPlaneAngle + 0.002f;
		// upperPlaneAngle=0.0f;
	}
	if (incAngle <= 360.0f)
	{
		incAngle = incAngle + 0.13f;
		// incAngle=0.0f;
	}


	//lower Plane
	if (lowerPlaneAngle > (M_PI / 2))
	{
		lowerPlaneAngle = lowerPlaneAngle - 0.002;
	}
	if (decAngle >= 0.0f)
	{
		decAngle = decAngle - 0.13f;
		// incAngle=0.0f;
	}

	if (translateMiddle <= -2.494244f)
	{
		translateMiddle = translateMiddle + 0.0072f;
	}


	if (translateAllInXDirection <= 3.0f && meet == true)
	{
		translateAllInXDirection = translateAllInXDirection + 0.005f;
		translateMiddle = translateAllInXDirection;
	}


	/**********************************************cheking the meet condition********************************/
	if (XTranslatePointUP >= -2.494244f && XTranslatePointDown >= -2.494244f && translateMiddle >= -2.494244f)
	{
		meet = true;
	}



	/*******************************----------------2Nd Part---------------------*****************************/

	if (translateAllInXDirection >= 3.0f && meet == true && translateMiddle < 11.50f)
	{
		translateMiddle = translateMiddle + 0.008f;

	}
	if (upperPlaneAngle2 < upperPlaneStopAngle2 && translateAllInXDirection >= 3.0f)
	{
		upperPlaneAngle2 = upperPlaneAngle2 + 0.002f;

	}
	if (incAngle2 <= 90.0f && translateAllInXDirection >= 3.0f)
	{
		incAngle2 = incAngle2 + 0.013f;

	}

	if (lowerPlaneAngle2 > -0.5f && translateAllInXDirection >= 3.0f)
	{
		lowerPlaneAngle2 = lowerPlaneAngle2 - 0.002f;

	}
	if (decAngle2 >= 270.0f && translateAllInXDirection >= 2.50f)
	{
		decAngle2 = decAngle2 - 0.013f;

	}
}