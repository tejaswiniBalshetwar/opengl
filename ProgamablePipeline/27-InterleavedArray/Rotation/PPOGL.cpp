#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include "resource.h"

#include<GL/glew.h>
#include<GL/gl.h>
#include "./../../vmath.h"


#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600


using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;

FILE *gpFile = NULL;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject = 0;

GLuint vao_cube = 0;
GLuint vbo_cube = 0;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShininessUniform;


GLuint samplerUniform;
GLuint texture_marble;

bool bLight = false;
bool bRotation = false;


float light_ambient[] = { 0.250f,0.250f,0.250f,0.250f };
float light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[] = { 10.0f,10.0f,10.0f,1.0f };

float material_ambient[] = { 0.250f,0.250f,0.250f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;

mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("MY PPOGL APP");
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while file opening"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file openied successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY OPENGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\nError in initialize at choosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n ERROR in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function executed successfully");

	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				if (bRotation)
				{
					update();
				}
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (bLight == false)
			{
				bLight = true;
				
			}
			else
			{
				bLight = false;
			}
			break;
		case 'A':
		case 'a':
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;
		}
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();
	BOOL LoadTexture(GLuint* , TCHAR[]);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	GLenum status;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghdc == NULL)
	{
		return -3;

	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile, "\n\nError in initialize at glewInit");
		uninitialize();
		exit(0);
	}

	//creating the shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vColor; " \
		"in vec2 vTexCoord;" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec3 out_color;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
			"out_color=vColor;								\n" \
			"out_TexCoord=vTexCoord;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 out_color;								\n" \
		"in vec2 out_TexCoord;							\n" \
		"uniform sampler2D u_sampler;					\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"	vec4 texSampler=texture(u_sampler,out_TexCoord);" \
			"	vec3 phong_ads_light;						\n" \
			"	if(u_LKeyIsPressed==1)						\n" \
			"	{											\n"	\
			"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
			"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
			"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
			"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
			"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
			"		vec3 ambient= u_la * u_ka;																							\n " \
			"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																				\n" \
			"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);	\n" \
			"		phong_ads_light=ambient + diffuse + specular;																		\n" \
			"	}																													\n" \
			"	else																												\n" \
			"	{																													\n" \
			"			phong_ads_light=vec3(1.0,1.0,1.0);																			\n" \
			"	}																													\n" \
			"	fragColor=vec4((vec3(texSampler)*vec3(out_color)*phong_ads_light),1.0);													\n" \
		"}";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyIsPressed");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");


	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");


	GLfloat vcntCube[]=
	{
				1.0f,1.0f,1.0f,		1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,  	1.0f,1.0f,
				-1.0f,1.0f,1.0f,  	1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,  	0.0f,1.0f,
				-1.0f,-1.0f,1.0f, 	1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,	  	0.0f,0.0f,
				1.0f,-1.0f,1.0f,  	1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,	  	1.0f,0.0f,

				1.0f,1.0f,-1.0f,	0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,  	1.0f,1.0f,
				1.0f,1.0f,1.0f,		0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,		0.0f,1.0f,
				1.0f,-1.0f,1.0f,	0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,		0.0f,0.0f,
				1.0f,-1.0f,-1.0f,	0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,		1.0f,0.0f,

				1.0f,1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	1.0f,1.0f,
				-1.0f,1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	0.0f,1.0f,
				-1.0f,-1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	0.0f,0.0f,
				1.0f,-1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	1.0f,0.0f,

				-1.0f,1.0f,1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	1.0f,1.0f,
				-1.0f,1.0f,-1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	0.0f,1.0f,
				-1.0f,-1.0f,-1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	0.0f,0.0f,
				-1.0f,-1.0f,1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	1.0f,0.0f,

				1.0f,1.0f,-1.0f,	1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		1.0f,1.0f,
				-1.0f,1.0f,-1.0f,	1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		0.0f,1.0f,
				-1.0f,1.0f,1.0f,	1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		0.0f,0.0f,
				1.0f,1.0f,1.0f,		1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		1.0f,0.0f,

				1.0f,-1.0f,-1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	1.0f,1.0f,
				-1.0f,-1.0f,-1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	0.0f,1.0f,
				-1.0f,-1.0f,1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	0.0f,0.0f,
				1.0f,-1.0f,1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	1.0f,0.0f,
	};



	//rectangle
	//recording start

	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);

	//posiiotn data
	glGenBuffers(1, &vbo_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vcntCube), vcntCube, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat),(void*) (0*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(9 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//recording stop
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	
	perspectiveProjectionMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();


	LoadTexture(&texture_marble, MAKEINTRESOURCE(ID_BITMAPMARBLE));

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

BOOL LoadTexture(GLuint* texture, TCHAR imageResourceId[])
{
	BOOL bStatus = FALSE;
	HBITMAP hBitmap;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	return bStatus;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 rotationMatrixX;
	mat4 rotationMatrixY;
	mat4 rotationMatrixZ;
	mat4 scaleMatrix;
	


	modelMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrixX = mat4::identity();
	rotationMatrixY = mat4::identity();
	rotationMatrixZ = mat4::identity();



	//do necessary translation
	translationMatrix = translate(0.0f, 0.0f, -6.0f);
	
	//rotationMatrixX = rotate(angleCube, 1.0f, 0.0f, 0.0f);
	//rotationMatrixY = rotate(angleCube, 0.0f, 1.0f, 0.0f);
	//rotationMatrixZ = rotate(angleCube, 0.0f, 0.0f, 1.0f);

	rotationMatrix=rotate(angleCube, angleCube, angleCube);


	//do matrix multipliacation
	modelMatrix = modelMatrix * translationMatrix;
	
	//modelViewMatrix = modelViewMatrix * rotationMatrixX * rotationMatrixY* rotationMatrixZ;
	modelMatrix = modelMatrix * rotationMatrix;

	

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_marble);
	glUniform1i(samplerUniform, 0);

	if (bLight)
	{
		glUniform1i(LKeyIsPressedUniform, 1);
		glUniform3fv(laUniform, 1, light_ambient);		//glLightfv() 
		glUniform3fv(ldUniform, 1, light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniform, 1, light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position

		glUniform3fv(kaUniform, 1, material_ambient);	//glMaterialfv();
		glUniform3fv(kdUniform, 1, material_diffuse);	//glMaterialfv();
		glUniform3fv(ksUniform, 1, material_specular);	//glMaterialfv();
		glUniform1f(materialShininessUniform, material_shininess);	//glMaterialfv();
	}
	else
	{
		glUniform1i(LKeyIsPressedUniform, 0);
	}

	glBindVertexArray(vao_cube);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(ghdc);
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);

}



void uninitialize()
{
	if (vbo_cube)
	{
		glDeleteBuffers(1, &vbo_cube);
		vbo_cube = 0;
	}
	
	
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}


	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nProgram executed successfully");
		fclose(gpFile);
		gpFile = NULL;

	}

}

void update()
{
	anglePyramid = anglePyramid + 0.1f;
	if (anglePyramid > 360.0f)
	{
		anglePyramid = 0.0f;
	}
	angleCube = angleCube + 0.1f;
	if (angleCube > 360.0f)
	{
		angleCube = 0.0f;
	}

}
