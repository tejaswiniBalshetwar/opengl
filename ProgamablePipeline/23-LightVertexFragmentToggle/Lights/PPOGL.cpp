#include<windows.h>
#include<stdio.h>
#include<stdlib.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include "./../../vmath.h"

#include "Sphere.h"



#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"Sphere.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600


float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;


using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;


//TOGGGLING RELATED	VARIABLES
bool gbIsVertexShader = true;
bool gbIsFragmentShader = false;
bool bLight = false;


FILE *gpFile = NULL;

//1 - for pervertex lighting variables
//2 - for per fragment lighting variables

enum
{
	AMC_ATTRIBUTE_POSITION1 = 0,
	AMC_ATTRIBUTE_COLOR1,
	AMC_ATTRIBUTE_NORMAL1,
	AMC_ATTRIBUTE_TEXCOORD01,

	AMC_ATTRIBUTE_POSITION2 ,
		AMC_ATTRIBUTE_COLOR2,
		AMC_ATTRIBUTE_NORMAL2,
		AMC_ATTRIBUTE_TEXCOORD02
};

//enum
//{
//	AMC_ATTRIBUTE_POSITION2 = 0,
//	AMC_ATTRIBUTE_COLOR2,
//	AMC_ATTRIBUTE_NORMAL2,
//	AMC_ATTRIBUTE_TEXCOORD02
//};

GLuint gShaderProgramObjectVertex = 0;
GLuint gShaderProgramObjectFragment = 0;

GLuint vao_sphere = 0;


GLuint vbo_position_sphere= 0;
GLuint vbo_normal_sphere = 0;
GLuint vbo_element_sphere = 0;

//fragment lighting
GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShininessUniform;


//vertex lighting
GLuint modelUniformVertex;
GLuint viewUniformVertex;
GLuint projectionUniformVertex;
GLuint LKeyIsPressedUniformVertex;
GLuint laUniformVertex=0;
GLuint ldUniformVertex=0;
GLuint lsUniformVertex=0;
GLuint kaUniformVertex=0;
GLuint kdUniformVertex=0;
GLuint ksUniformVertex=0;
GLuint lightPositionUniformVertex=0;
GLuint materialShininessUniformVertex=0;


mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;


float light_ambient[] = {0.0f,0.0f,0.0f,0.0f};
float light_diffuse[] = {1.0f,0.0f,0.0f,1.0f};
float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
float lightPosition[] = {100.0f,100.0f,100.0f,1.0f};

float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("MY PPOGL APP");
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while file opening"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file openied successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY OPENGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\nError in initialize at choosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n ERROR in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function executed successfully");

	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			ToggleFullscreen();
			break;
		case 0x46:
			if (gbIsFragmentShader == false)
			{
				gbIsFragmentShader = true;
			}
			else
			{
				gbIsFragmentShader = false;
			}
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
			case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
				}
				else
				{
					bLight = false;
				}
				break;
			case 'q':
			case 'Q':
				DestroyWindow(hwnd);
				break;

			case 'v':
			case 'V':
				if (gbIsVertexShader==false)
				{
					gbIsVertexShader = true;
				}
				else
				{
					gbIsVertexShader = false;
				}
			break;
		}
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();
	void CreateProgramForPerFragment(void);
	void CreateProgramForPerVertex(void);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;


	GLenum status;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghdc == NULL)
	{
		return -3;

	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile, "\n\nError in initialize at glewInit");
		uninitialize();
		exit(0);
	}

	//creating the shader
	CreateProgramForPerFragment();
	CreateProgramForPerVertex();
	//vertices

	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	//sphere
	//recording start
	// for per vertex lighting
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//position data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	//for per vertex
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION1);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION2,3,GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	
		//for per vertex
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL1);
		
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL2,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL2);
		glBindBuffer(GL_ARRAY_BUFFER,0);

	
	glGenBuffers(1,&vbo_element_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);



	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	//orthographic projectioon la load identity
	perspectiveProjectionMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void CreateProgramForPerFragment()
{
	void uninitialize();
	GLuint vertexShaderObjectFragment = 0;
	GLuint fragmentShaderObjectFragment = 0;
	vertexShaderObjectFragment = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCodeFragment =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObjectFragment, 1, (const GLchar**)&vertexShaderSourceCodeFragment, NULL);


	//compile shader
	glCompileShader(vertexShaderObjectFragment);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObjectFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObjectFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCodeFragment =
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
		"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		"		vec3 ambient= u_la * u_ka;																						\n " \
		"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																			\n" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		phong_ads_light=ambient + diffuse + specular;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";

	//specify source code to object
	glShaderSource(fragmentShaderObjectFragment, 1, (const GLchar**)&fragmentShaderSourceCodeFragment, NULL);

	//compile shader
	glCompileShader(fragmentShaderObjectFragment);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObjectFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObjectFragment = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObjectFragment, vertexShaderObjectFragment);
	glAttachShader(gShaderProgramObjectFragment, fragmentShaderObjectFragment);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObjectFragment, AMC_ATTRIBUTE_POSITION2, "vPosition");
	glBindAttribLocation(gShaderProgramObjectFragment, AMC_ATTRIBUTE_NORMAL2, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObjectFragment);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectFragment, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_projection_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ks");

	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_LKeyIsPressed");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_material_shininess");

}

void CreateProgramForPerVertex()
{
	void uninitialize();

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	//create source code object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the shader source code
	const GLchar *vertexShaderSourceCode =
		"	#version 450 core											\n" \
		"	\n															\n" \
		"	in vec4 vPosition1;											\n" \
		"	in vec3 vNormal1;											\n" \
		"	uniform mat4 u_model_matrix1;								\n" \
		"	uniform mat4 u_view_matrix1;								\n" \
		"	uniform mat4 u_projection_matrix1;							\n" \
		"	uniform vec3 u_la1;											\n" \
		"	uniform vec3 u_ld1;											\n" \
		"	uniform vec3 u_ls1;											\n" \
		"	uniform vec3 u_ka1;											\n" \
		"	uniform vec3 u_kd1;											\n" \
		"	uniform vec3 u_ks1;											\n" \
		"	uniform int u_LKeyIsPressed1;															\n" \
		"	uniform float u_materialShininess1;														\n" \
		"	uniform vec4 u_light_position1;															\n" \
		"	out vec3 out_phong_ads_light1 ;															\n" \
		"	void main(void)																			\n" \
		"	{																						\n" \
		"		if(u_LKeyIsPressed1==1)																\n" \
		"		{																					\n" \
		"			vec4 eye_coordinates=u_view_matrix1 * u_model_matrix1 * vPosition1;															\n" \
		"			vec3 t_normal=normalize(mat3(u_view_matrix1*u_model_matrix1) * vNormal1) ;													\n" \
		"			vec3 light_direction=normalize(vec3(u_light_position1 - eye_coordinates));												\n" \
		"			float t_dot_ld=max(dot(light_direction,t_normal),0.0);																	\n" \
		"			vec3 reflection_vector=reflect(-light_direction,t_normal);																\n" \
		"			vec3 viewer_vector=normalize(vec3(-eye_coordinates));																	\n" \
		"			vec3 ambient= u_la1 * u_ka1;																								\n" \
		"			vec3 diffuse= u_ld1 * u_kd1 * t_dot_ld;																					\n" \
		"			vec3 specular= u_ls1 * u_ks1 * pow(max(dot(reflection_vector,viewer_vector),0.0),u_materialShininess1);					\n" \
		"			out_phong_ads_light1= ambient + diffuse + specular;																			\n" \
		"		}																															\n" \
		"		else																														\n" \
		"		{																															\n" \
		"			out_phong_ads_light1=vec3(1.0,1.0,1.0);																					\n" \
		"																																	\n" \
		"		}																															\n" \
		"		gl_Position= u_projection_matrix1 * u_view_matrix1 * u_model_matrix1 * vPosition1;																															\n" \
		"	}																																\n" \
		;

	//specify source code to object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking

	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char * szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n Error while compiling Vertex Shader: %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				exit(0);
			}
		}

	}

	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code for the fragment shader
	const GLchar* fragmentShaderSourceCode =
		"	#version 450 core																		" \
		"	\n																						" \
		"	in vec3 out_phong_ads_light1;															\n" \
		"	out vec4 fragColor;																		\n" \
		"	void main(void)																			\n" \
		"	{																						\n" \
		"		fragColor=vec4(out_phong_ads_light1,1.0);											\n" \
		"	}																						\n" \
		;

	//specify shader code to shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error cheking for compilation
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"\nError in Fragment Shader:=%s\n",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObjectVertex =glCreateProgram();
	
	//cattach shader to program
	glAttachShader(gShaderProgramObjectVertex, vertexShaderObject);
	glAttachShader(gShaderProgramObjectVertex,fragmentShaderObject);
	
	//prelinkg for attributes
	glBindAttribLocation(gShaderProgramObjectVertex,AMC_ATTRIBUTE_POSITION1,"vPosition1");
	glBindAttribLocation(gShaderProgramObjectVertex, AMC_ATTRIBUTE_NORMAL1, "vNormal1");

	//link the program
	glLinkProgram(gShaderProgramObjectVertex);

	//errror checking for linking the program

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectVertex,GL_LINK_STATUS,&iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectVertex,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			GLsizei written;
			glGetProgramInfoLog(gShaderProgramObjectVertex,iInfoLogLength,&written,szInfoLog);
			fprintf(gpFile,"\nError while linking the Program:=%s\n",szInfoLog);

			free(szInfoLog);

			uninitialize();
			exit(0);
		}	
	}

	//post linking binding uniform
	modelUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_model_matrix1");
	viewUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_view_matrix1");
	projectionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_projection_matrix1");
	
	laUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_la1");
	ldUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ld1");
	lsUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ls1");

	kaUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ka1");
	kdUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_kd1");
	ksUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ks1");

	LKeyIsPressedUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_LKeyIsPressed1");

	lightPositionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_light_position1");
	materialShininessUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_materialShininess1");


}



void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	

	// declaration of matrices
	mat4 modelViewMatrix;
	
	mat4 translationMatrix;
	

	//initialize abouve matrices

	modelMatrix = mat4::identity();
	//modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	
	//do necessary translation

	translationMatrix = translate(0.0f, 0.0f, -2.0f);

	//do matrix multipliacation
	modelMatrix = modelMatrix * translationMatrix;
	

	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	if (gbIsFragmentShader == true)
	{
		glUseProgram(gShaderProgramObjectFragment);
		glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);

		glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

		if (bLight)
		{
			glUniform1i(LKeyIsPressedUniform, 1);
			glUniform3fv(laUniform, 1, light_ambient);		//glLightfv() 
			glUniform3fv(ldUniform, 1, light_diffuse);		//glLightfv() 
			glUniform3fv(lsUniform, 1, light_specular);		//glLightfv() 
			glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position

			glUniform3fv(kaUniform, 1, material_ambient);	//glMaterialfv();
			glUniform3fv(kdUniform, 1, material_diffuse);	//glMaterialfv();
			glUniform3fv(ksUniform, 1, material_specular);	//glMaterialfv();
			glUniform1f(materialShininessUniform, material_shininess);	//glMaterialfv();
		}
		else
		{
			glUniform1i(LKeyIsPressedUniform, 0);
		}
	}
	else
	{
		glUseProgram(gShaderProgramObjectVertex);

		glUniformMatrix4fv(modelUniformVertex, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniformVertex, 1, GL_FALSE, viewMatrix);

		glUniformMatrix4fv(projectionUniformVertex, 1, GL_FALSE, perspectiveProjectionMatrix);

		if (bLight)
		{
			glUniform1i(LKeyIsPressedUniformVertex, 1);
			glUniform3fv(laUniformVertex, 1, light_ambient);		//glLightfv() 
			glUniform3fv(ldUniformVertex, 1, light_diffuse);		//glLightfv() 
			glUniform3fv(lsUniformVertex, 1, light_specular);		//glLightfv() 
			glUniform4fv(lightPositionUniformVertex, 1, lightPosition); //glLightfv() for position

			glUniform3fv(kaUniformVertex, 1, material_ambient);	//glMaterialfv();
			glUniform3fv(kdUniformVertex, 1, material_diffuse);	//glMaterialfv();
			glUniform3fv(ksUniformVertex, 1, material_specular);	//glMaterialfv();
			glUniform1f(materialShininessUniformVertex, material_shininess);	//glMaterialfv();
		}
		else
		{
			glUniform1i(LKeyIsPressedUniformVertex, 0);
		}
	}

	

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
	glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
	//glDrawArrays(GL_TRIANGLE_STRIP,0,gNumElements);
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(ghdc);
}




void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);

}



void uninitialize()
{
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}


	if (gShaderProgramObjectFragment)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObjectFragment);
		glGetProgramiv(gShaderProgramObjectFragment, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObjectFragment, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObjectFragment, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObjectFragment);
		gShaderProgramObjectFragment = 0;
		glUseProgram(0);
	}

	if (gShaderProgramObjectVertex)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObjectVertex);
		glGetProgramiv(gShaderProgramObjectVertex, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObjectVertex, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObjectVertex, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObjectVertex);
		gShaderProgramObjectVertex = 0;
		glUseProgram(0);
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nProgram executed successfully");
		fclose(gpFile);
		gpFile = NULL;

	}

}

void update()
{

}
