#include<windows.h>
#include<stdio.h>
#include<stdlib.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include "Header.h"
#include "./../../vmath.h"


#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

int windowWidth = WIN_WIDTH;
int windowHeight = WIN_HEIGHT;

using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;

FILE *gpFile = NULL;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

enum
{
	AMC_ATTRIBUTE_POSITION_FRAMEBUFFER = 0,
	AMC_ATTRIBUTE_COLOR_FRAMEBUFFER,
	AMC_ATTRIBUTE_NORMAL_FRAMEBUFFER,
	AMC_ATTRIBUTE_TEXCOORD0_FRAMEBUFFER 
};

GLuint gShaderProgramObject = 0;
GLuint gShaderProgramObjectFrameBuffer = 0;
GLuint vbo_cube_position = 0;
GLuint vbo_cube_texture = 0;
GLuint vao_cube = 0;

GLuint fbo;

GLuint mvpUniform;
GLuint mvpUniformFrameBuffer;
GLuint samplerUniform;
GLuint samplerUniformFrameBuffer;

GLuint depth_texture;
GLuint color_texture;
GLuint kundali_texture = 0;

GLfloat cubeangle = 0.0f;
mat4 perspectiveProjectionMatrix;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("MY PPOGL APP");
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while file opening"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file openied successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY OPENGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "\nError in initialize at choosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n ERROR in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n ERROR in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function executed successfully");

	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				//update
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_SIZE:
		windowWidth = LOWORD(lParam);
		windowHeight = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();
	BOOL LoadTexture(GLuint*,TCHAR[]);
	//frame buffer object binding function
	void frameBufferBinding();

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;
	GLuint geometryShaderObject = 0;

	GLenum status;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghdc == NULL)
	{
		return -3;

	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile, "\n\nError in initialize at glewInit");
		uninitialize();
		exit(0);
	}


	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glGenTextures(1,&color_texture);
	glBindTexture(GL_TEXTURE_2D,color_texture);
	//glTexStorage2D(GL_TEXTURE_2D,1,GL_RGBA8,512,512);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,512,512,0,GL_RGBA,GL_UNSIGNED_BYTE,0);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	glGenTextures(1,&depth_texture);
	glBindTexture(GL_TEXTURE_2D,depth_texture);
	glTexStorage2D(GL_TEXTURE_2D,1,GL_DEPTH_COMPONENT32F,512,512);

	glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,color_texture,0);
	glFramebufferTexture(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,depth_texture,0);

	static const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1,draw_buffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(gpFile,"Error in frameBuffer setup");
		uninitialize();
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Framebuffer setup complete");
	}

	//frameBufferBinding();

	//creating the shader

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoords;" \
		"out vec2 out_texcoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
			"gl_Position= u_mvp_matrix * vPosition ;"  \
			"out_texcoord=vTexCoords;" \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	

	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 fragColor;" \
		"uniform sampler2D u_sampler;" \
		"in vec2 out_texcoord;" \
		"void main(void)" \
		"{" \
		"fragColor=texture(u_sampler,out_texcoord);" \
		"}";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoords");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	const GLfloat cubeVertices[] = {
		//front
			1.0f,1.0f,1.0f,
			-1.0f,1.0f,1.0f,
			-1.0f,-1.0f,1.0f,
			1.0f,-1.0f,1.0f,
			//right
				1.0f,1.0f,-1.0f,
				1.0f,1.0f,1.0f,
				1.0f,-1.0f,1.0f,
				1.0f,-1.0f,-1.0f,

				1.0f,1.0f,-1.0f,
				-1.0f,1.0f,-1.0f,
				-1.0f,-1.0f,-1.0f,
				1.0f,-1.0f,-1.0f,

				-1.0f,1.0f,1.0f,
				-1.0f,1.0f,-1.0f,
				-1.0f,-1.0f,-1.0f,
				-1.0f,-1.0f,1.0f,

				1.0f,1.0f,-1.0f,
				-1.0f,1.0f,-1.0f,
				-1.0f,1.0f,1.0f,
				1.0f,1.0f,1.0f,

				1.0f,-1.0f,-1.0f,
				-1.0f,-1.0f,-1.0f,
				-1.0f,-1.0f,1.0f,
				1.0f,-1.0f,1.0f
	};

	const GLfloat cubeTexCoord[] = {

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

	};
	//create the recoding casets reel == vao
	glGenVertexArrays(1, &vao_cube);

	//bind with cassate  and start recording
	glBindVertexArray(vao_cube);

	//gen buffers
	glGenBuffers(1, &vbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glGenBuffers(1, &vbo_cube_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texture);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoord), cubeTexCoord, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR,0.0f,1.0f,1.0f);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//recording stop
	glBindVertexArray(0);

	LoadTexture(&kundali_texture, MAKEINTRESOURCE(IDBTMAP_KUNDALI));

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glEnable(GL_TEXTURE_2D);

	//orthographic projectioon la load identity
	perspectiveProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}
BOOL LoadTexture(GLuint* texture, TCHAR imageResourceId[])
{
	BOOL bStatus = FALSE;
	HBITMAP hBitmap;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	return bStatus;
}
void frameBufferBinding()
{
	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;
	void uninitialize();

	//vertex shader 
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCode =
		"#version 460 core"								\
		"\n"											\
		"in vec4 vPosition;								\n" \
		"in vec2 vTexCoord;								\n" \
		"uniform mat4 u_mvp_matrix2; 								\n" \
		"out vec2 out_texCoord;		 								\n" \
		"void main() 												\n" \
		"{ 															\n" \
		"	gl_Position=u_mvp_matrix2*vPosition;	 													\n" \
		" 	out_texCoord=vTexCoord;														\n" \
		" 															\n" \
		"}";

	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
	
	glCompileShader(vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	char *szInfoLog = NULL;
	GLint iInfologLength = 0;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfologLength);
		if (iInfologLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iShaderCompileStatus);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iShaderCompileStatus, &written, szInfoLog);

				fprintf(gpFile, "\n Error in vertexShaderObject: %s", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Fragment shader 
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec2 out_texCoord;				\n"	\
		"uniform smapler2D u_sampler;		\n" \
		"out vec4 fragColor;				\n"	\
		"void main(void)						\n" \
		"{										\n" \
			"fragColor=texture(u_sampler,out_texCoord);									\n" \
		"}";

	glShaderSource(fragmentShaderObject,1,(GLchar**)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	//error checking for fragment shader
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	iInfologLength = 0;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfologLength);
		if (iInfologLength > 0)
		{
			GLsizei written;
			szInfoLog = (char*)malloc(iInfologLength * sizeof(char));
			if (!szInfoLog)
			{

				glGetShaderInfoLog(fragmentShaderObject, iInfologLength, &written, szInfoLog);
				fprintf(gpFile, "Error in compiling fragment shader : %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}

		}
	}

	gShaderProgramObjectFrameBuffer = glCreateProgram();

	glAttachShader(gShaderProgramObjectFrameBuffer,vertexShaderObject);
	glAttachShader(gShaderProgramObjectFrameBuffer,fragmentShaderObject);

	glBindAttribLocation(gShaderProgramObjectFrameBuffer, AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(gShaderProgramObjectFrameBuffer, AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");
	
	glLinkProgram(gShaderProgramObjectFrameBuffer);
	
	GLint iShaderPorgramLinkStatus = 0;
	iInfologLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderPorgramLinkStatus);

	if (iShaderPorgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfologLength);
		if (iInfologLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iShaderPorgramLinkStatus);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iShaderPorgramLinkStatus, &written, szInfoLog);

				fprintf(gpFile, "\nError while linking Program: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	mvpUniformFrameBuffer = glGetUniformLocation(gShaderProgramObjectFrameBuffer,"u_mvp_matrix2");
	samplerUniform = glGetUniformLocation(gShaderProgramObjectFrameBuffer,"u_sampler");


	
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER,fbo);
	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	//initialize abouve matrices
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();


	//do necessary for transformation
	translationMatrix = translate(0.0f,0.0f,-6.0f);
	rotationMatrix=rotate(cubeangle,cubeangle,cubeangle);
	//do necessary for matrix multiplication
	modelViewMatrix = modelViewMatrix * translationMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(samplerUniform,0.0f);

	GLfloat green[] = { 0.0f,1.0f,1.0f };
	GLfloat one = 1.0f;
	glClearBufferfv(GL_COLOR,0,green);
	glClearBufferfv(GL_DEPTH,0,&one);

	glBindTexture(GL_TEXTURE_2D,kundali_texture);
	glUniform1i(samplerUniform,0);
	glViewport(0,0,512,512);

	//bind with vao so that it starts sound which is recorded
	glBindVertexArray(vao_cube);

	//similaraly bind for textures if any

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 24, 4);

	glBindVertexArray(0);

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER,0);

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	
	glViewport(0,0,windowWidth,windowHeight);
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	//do necessary for transformation
	translationMatrix = translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = rotate(cubeangle,cubeangle,cubeangle);
	//do necessary for matrix multiplication
	modelViewMatrix = modelViewMatrix * translationMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(samplerUniform, 0.0f);
	
	//GLfloat blue[] = {0.0f,0.0f,1.0f};
	//glClearBufferfv(GL_COLOR, 0, blue);
	//glClearBufferfv(GL_DEPTH, 0, &one);
	glBindTexture(GL_TEXTURE_2D,color_texture);

	//bind with vao so that it starts sound which is recorded
	glBindVertexArray(vao_cube);

	//similaraly bind for textures if any

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	
	glBindTexture(GL_TEXTURE_2D,0);

	glUseProgram(0);
	
	SwapBuffers(ghdc);

	cubeangle = cubeangle + 0.2f;
	if (cubeangle >= 360.0f)
	{
		cubeangle = 0.0f;
	}
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}



void uninitialize()
{
	if (vbo_cube_position)
	{
		glDeleteBuffers(1, &vbo_cube_position);
		vbo_cube_position = 0;
	}
	if (vbo_cube_texture)
	{
		glDeleteBuffers(1, &vbo_cube_texture);
		vbo_cube_texture = 0;
	}
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;

			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);
		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nProgram executed successfully");
		fclose(gpFile);
		gpFile = NULL;

	}

}