#include<windows.h>
#include<stdio.h>
#include<stdlib.h>

#include<GL/glew.h>
#include<GL/gl.h>

#include"./../../vmath.h"
#include"Header.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsFullscreen = false;
bool gbIsActiveWindow = false;

FILE *gpFile = NULL;
GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;

enum
{
	AMC_ATTRIBUTE_POSTION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject = 0;

GLuint vao_square = 0;


GLuint vbo_position_square = 0;
GLuint vbo_texture_square = 0;

mat4 perspectiveProjectionMatrix;

GLuint mvpUniform;
GLuint samplerUniform;


GLuint texture_smiley;


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	int initialize();
	void display();
	void update();

	WNDCLASSEX wndclass;
	TCHAR lpszClassName[] = TEXT("PPOGL APPLICATION");
	MSG msg;
	HWND hwnd;

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while opening File"), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n file opened successfully");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY PPOGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
	ghwnd = hwnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "\n Error in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n Error in initialize at SetPixelFormat");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n Error in initialize at wglCreateContext");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n Error in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\n initialize function successfully");
	}

	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd, iCmdShow);

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen();
	void resize(int, int);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;

		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;

	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.left, SWP_FRAMECHANGED | SWP_NOZORDER);

			}

		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	void uninitialize();
	BOOL LoadTexture(GLuint*,TCHAR[]);

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex = 0;


	sizeof(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelIndex = ChoosePixelFormat(ghdc,&pfd);
	if (iPixelIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelIndex, &pfd) == GL_FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}
	if (wglMakeCurrent(ghdc, ghrc) == GL_FALSE)
	{
		return -4;
	}

	GLenum status;
	status = glewInit();
	if (status != GLEW_OK)
	{
		fprintf(gpFile,"\n Error in glewInit");
		uninitialize();
		exit(0);
	}


	//create Shader object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write shader source
	const GLchar *vertexShaderSoureCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_matrix * vPosition;" \
			"out_TexCoord=vTexCoord;" \
		"}";

	//specify the shader source to shader object;
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSoureCode,NULL);
	
	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking for vertex shader object
	GLint iShaderCompileStatus=0;
	char *szInfoLog=NULL;
	GLint iInfologLength=0;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfologLength);
		if (iInfologLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iShaderCompileStatus);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iShaderCompileStatus,&written,szInfoLog);

				fprintf(gpFile,"\n Error in vertexShaderObject: %s",szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//steps for creating fragment shader obejct
	//creaate fragment shader obejct
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write for fragment shader source code
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 out_TexCoord;" \
		"uniform sampler2D u_sampler;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"FragColor=texture(u_sampler,out_TexCoord);" \
		"}";

	//shader source specify
	glShaderSource(fragmentShaderObject,1,(GLchar**)&fragmentShaderSourceCode,NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);
	//error checking for fragment shader
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	iInfologLength = 0;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfologLength);
		if (iInfologLength > 0)
		{
			GLsizei written;
			szInfoLog = (char*)malloc(iInfologLength* sizeof(char));
			if (!szInfoLog)
			{

				glGetShaderInfoLog(fragmentShaderObject,iInfologLength,&written,szInfoLog);
				fprintf(gpFile,"Error in compiling fragment shader : %s",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}

		}
	}


	//create shader program obeject
	gShaderProgramObject=glCreateProgram();
	//attach shaders to program object
	glAttachShader(gShaderProgramObject,vertexShaderObject);
	glAttachShader(gShaderProgramObject,fragmentShaderObject);


	//prelinking the vertex attributes;
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSTION,"vPosition");
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");


	//link program for gpu
	glLinkProgram(gShaderProgramObject);

	GLint iShaderPorgramLinkStatus = 0;
	iInfologLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderPorgramLinkStatus);

	if (iShaderPorgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfologLength);
		if (iInfologLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iShaderPorgramLinkStatus);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,iShaderPorgramLinkStatus,&written,szInfoLog);
				
				fprintf(gpFile,"\nError while linking Program: %s",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//uniform loactions for runtime data
	mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");
	samplerUniform=glGetUniformLocation(gShaderProgramObject,"u_sampler");

	//specifying the data for drawing;
	const GLfloat squareVertices[] = {
										 1.0f,1.0f,0.0f,
										-1.0f,1.0f,0.0f,
										-1.0f,-1.0f,0.0f,
										 1.0f,-1.0f,0.0f
									};

	const GLfloat texCoords[] = {
								1.0f,1.0f,
								0.0f,1.0f,
								0.0f,0.0f,
								1.0f,0.0f

							};

	//create the vao recoreder for display
	glCreateVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		
		//create vbo for actual transfering the data
		glGenBuffers(1,&vbo_position_square);
		//binding for data to the gpu at target
		glBindBuffer(GL_ARRAY_BUFFER,vbo_position_square);
		//sending the data and its size to gpu
		glBufferData(GL_ARRAY_BUFFER,sizeof(squareVertices),squareVertices,GL_STATIC_DRAW);
		//sending the reading info to gpu and accessing info through cpu end
		glVertexAttribPointer(AMC_ATTRIBUTE_POSTION,3,GL_FLOAT,GL_FALSE,0,NULL);
		//enableing pur access point for data at cpu
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSTION);
		//for bindng again in display convention
		glBindBuffer(GL_ARRAY_BUFFER,0);


		//texture bufferes;
		glGenBuffers(1,&vbo_texture_square);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_square);

		glBufferData(GL_ARRAY_BUFFER,sizeof(texCoords),texCoords,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
		glBindBuffer(GL_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);



	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	LoadTexture(&texture_smiley,MAKEINTRESOURCE(ID_BITMAPSMILEY));

	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}


BOOL LoadTexture(GLuint* texture, TCHAR imageResourceId[])
{
	BOOL bStatus=FALSE;
	HBITMAP hBitmap;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap,sizeof(bmp),&bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glGenTextures(1,texture);
		glBindTexture(GL_TEXTURE_2D,*texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,bmp.bmWidth,bmp.bmHeight,0,GL_BGR_EXT,GL_UNSIGNED_BYTE,bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,0);
	}
	return bStatus;
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	//declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;
	//making them identity glLaodIdentity()
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	//for transpformation multiplication
	translationMatrix = translate(0.0f,0.0f,-4.0f);
	modelViewMatrix = modelViewMatrix * translationMatrix;


	//do necesasry rotation

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//creating the projection for viewport and sending is to the gpu using uniform dynamically data transpher
	glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,texture_smiley);
	glUniform1i(samplerUniform,0);
	
	//actual drawing start
	glBindVertexArray(vao_square);
		glDrawArrays(GL_TRIANGLE_FAN,0,4);
	glBindVertexArray(0);
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, GLsizei(width), GLsizei(height));

	perspectiveProjectionMatrix = perspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);
}

void uninitialize()
{
	if (vbo_position_square)
	{
		glDeleteBuffers(1,&vbo_position_square);
		vbo_position_square = 0;
	}
	if (vbo_texture_square)
	{
		glDeleteBuffers(1,&vbo_texture_square);
		vbo_texture_square = 0;
	}

	if (vao_square)
	{
		glDeleteVertexArrays(1,&vao_square);
		vao_square = 0;
	}
	
	if (gShaderProgramObject)
	{
		GLsizei shaderCount=0;
		GLsizei shaderNumber=0;
		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);
		GLuint* pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);
		if (pShaders)
		{
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
		
	}

	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(0, 0 );
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "\n Program Executed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void update()
{

}