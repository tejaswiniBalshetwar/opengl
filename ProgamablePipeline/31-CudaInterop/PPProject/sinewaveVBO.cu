__global__ void sinewave_VBO_Kernel(float4* pos, unsigned int width,unsigned int height, float animeTime)
{
	unsigned int x =blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y =blockIdx.y * blockDim.y + threadIdx.y;

	float u=x/float(width);
	float v=y/float(height);

	u=(u*2.0)-1.0;
	v=(v*2.0)-1.0;

	float frequency=4.0;

	float w =sinf(frequency * u + animeTime) * cosf(frequency * v  + animeTime)*0.5;

	pos[y*width+x]=make_float4(u,w,v,1.0);
}

void launchCudaKernel(float4 *pos , unsigned int width, unsigned int height, float time)
{
	dim3 block(8,8,1);
	dim3 grid(width/block.x, height/block.y, 1.0);

	sinewave_VBO_Kernel <<<grid,block>>>(pos,width,height,time);
}

