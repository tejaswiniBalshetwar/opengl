

//
//  GLESView.m
//  02-BlueWindow
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"



enum
{
    THB_ATTRIBUTE_VERTEX=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};
using namespace vmath;

GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;
GLuint vao_circle;
GLuint vbo_circle_position;
GLuint vbo_circle_color;


@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    mat4 perspectiveProjectionMatrix;
    GLuint vao_square;
    GLuint vbo_square_position ;
    GLuint vbo_square_color;
    
    GLuint vao_triangle;
    GLuint vbo_triangle_position;
    GLuint vbo_triangle_color;
    
    
    GLfloat verticesForOuterCircle[2000];
    GLfloat colorForCircle[2000];
    
    GLfloat verticesForInnerCircle[2000];
    
    GLuint mvpUniform;

    
}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        //Opengl GL shader code
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const char* vertexShaderSourceCode=
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "out vec4 out_color;" \
        "uniform mat4 u_mvp_uniform;" \
        "void main(void)" \
        "{" \
        "gl_Position=u_mvp_uniform*vPosition;" \
        "gl_PointSize=5.0;" \
        "out_color=vColor;" \
        "}";
        
        
        //specify the shader to vertex shader object
        glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength=0;
        GLint iShaderCompileStatus=0;
        char* szInfoLog=NULL;
        
        glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Vertex Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf( "\nVertex shader compiled successfully");
        }
        
        
        //fragment shader
        
        //creating fragment shader
        fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
        
        //writting shader source code
        const char* fragmentShaderSourceCode=
        "#version 300 es " \
        "\n" \
        "precision highp float;" \
        "in vec4 out_color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "fragColor=out_color;" \
        "}" ;
        
        //specifying shader source to fragment shader object
        glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
        
        //compiling shader
        glCompileShader(fragmentShaderObject);
        
        //error checking
        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;
        
        glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in fragment Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf("\nFragement shader compiled successfully");
        }
        
        //Shader Program
        
        shaderProgramObject=glCreateProgram();
        
        //attching the sahder to object
        glAttachShader(shaderProgramObject,vertexShaderObject);
        glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        //pre-linking attrinbuts
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_VERTEX,"vPosition");
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_COLOR,"vColor");
        
        glLinkProgram(shaderProgramObject);
        
        iInfoLogLength=0;
        GLint iShaderLinkStatus=0;
        szInfoLog=NULL;
        
        glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
        if(iShaderLinkStatus==GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in Linking Shader ....");
                    free(szInfoLog);
                    [self release];
                    // [NSApp terminate];
                }
            }
        }
        else
        {
            printf( "\n Linking of shader successfull");
        }
        
        //post linking
        mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_uniform");
    
        
        //crating VAO for recording
        //crating VAO for recording
        glGenVertexArrays(1,&vao_line);
        glBindVertexArray(vao_line);
        glGenBuffers(1,&vbo_line_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
        glBufferData(GL_ARRAY_BUFFER,3*2*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
        glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);
        
        
        GLfloat verticesForSquare[] = {
            1.0f,1.0f,0.0f,
            -1.0f,1.0f,0.0f,
            -1.0f,-1.0f,0.0f,
            1.0f,-1.0f,0.0f
        };
        
        GLfloat colorForSquare[] = {
            1.0f,1.0f,0.0f,
            1.0f,1.0f,0.0f,
            1.0f,1.0f,0.0f,
            1.0f,1.0f,0.0f,
        };
        
        
        //for square
        glGenVertexArrays(1,&vao_square);
        glBindVertexArray(vao_square);
        glGenBuffers(1,&vbo_square_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
        glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForSquare), verticesForSquare,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        //for color
        glGenBuffers(1,&vbo_square_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
        glBufferData(GL_ARRAY_BUFFER,sizeof(colorForSquare),colorForSquare,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);
        
        //for outer circle
        
        GLfloat lengthOfASide = 0.0f;
        GLfloat lengthOfBSide = 0.0f;
        GLfloat lengthOfCSide = 0.0f;

        lengthOfASide = calculateLengthByDistanceFormula(verticesForSquare[0], verticesForSquare[1], verticesForSquare[2], verticesForSquare[3], verticesForSquare[4], verticesForSquare[5]);
        lengthOfBSide = calculateLengthByDistanceFormula(verticesForSquare[3], verticesForSquare[4], verticesForSquare[5], verticesForSquare[6], verticesForSquare[7], verticesForSquare[8]);
        
        /*
         GLfloat verticesForOuterCircle[2000];
         memset(verticesForOuterCircle,0,sizeof(verticesForOuterCircle));
         GLfloat colorForCircle[2000];
         memset(colorForCircle,0,sizeof(colorForCircle));*/
        GLfloat radius = 0.0f;
        radius = calculateRadiusOfOuterCircle(lengthOfASide, lengthOfBSide);
        
        calculateVerticesForCircle(radius, 0.0f, 0.0f, verticesForOuterCircle,colorForCircle);
        
        
        glGenVertexArrays(1,&vao_circle);
        glBindVertexArray(vao_circle);
        glGenBuffers(1,&vbo_circle_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
        glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForOuterCircle),verticesForOuterCircle,GL_DYNAMIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        glGenBuffers(1,&vbo_circle_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_color);
        glBufferData(GL_ARRAY_BUFFER,sizeof(colorForCircle),colorForCircle,GL_DYNAMIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);
        
        //trinagle
        //triangle
        GLfloat verticesForTriangle[] = {
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,0.0f,
            1.0f,-1.0f,0.0f
        };
        
        GLfloat colorForTriangle[] = {
            0.0f,1.0f,1.0f,
            0.0f,1.0f,1.0f,
            0.0f,1.0f,1.0f
        };
        
        glGenVertexArrays(1,&vao_triangle);
        glBindVertexArray(vao_triangle);
        glGenBuffers(1,&vbo_triangle_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_triangle_position);
        glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForTriangle),verticesForTriangle,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        glGenBuffers(1,&vbo_triangle_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_color);
        glBufferData(GL_ARRAY_BUFFER,sizeof(colorForTriangle),colorForTriangle,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        
        //inner circle
        lengthOfASide = calculateLengthByDistanceFormula(verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2], verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5]);
        lengthOfBSide = calculateLengthByDistanceFormula(verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5], verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8]);
        lengthOfCSide = calculateLengthByDistanceFormula(verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8], verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2]);
        
        
        GLfloat XCenterOfCircle = ((verticesForTriangle[0] * lengthOfBSide) + (verticesForTriangle[3] * lengthOfCSide) + (verticesForTriangle[6] * lengthOfASide))/(lengthOfASide+lengthOfBSide+lengthOfCSide);
        GLfloat YCenterOfCircle = ((verticesForTriangle[1] * lengthOfBSide) + (verticesForTriangle[4] * lengthOfCSide) + (verticesForTriangle[7] * lengthOfASide))/ (lengthOfASide + lengthOfBSide + lengthOfCSide);
        
        radius = calculateInnerCircleRadius(lengthOfASide, lengthOfBSide, lengthOfCSide);
        
        calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);
        


        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

GLfloat calculateLengthByDistanceFormula(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2 )
{
    GLfloat length = 0.0f;
    
    length = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2) + pow((z2 - z1), 2));
    
    return length;
    
}
GLfloat calculateRadiusOfOuterCircle(GLfloat lengthA, GLfloat lengthB)
{
    GLfloat radius = 0.0f;
    GLfloat hypotenious = 0.0f;
    
    hypotenious = sqrt(pow(lengthA, 2) + pow(lengthB, 2));
    
    radius = hypotenious / 2;
    return radius;
    
}
GLfloat calculateInnerCircleRadius(GLfloat A, GLfloat B, GLfloat C)
{
    GLfloat semiperimeter = (A + B + C) / 2.0f;
    GLfloat area = (GLfloat)sqrt(semiperimeter*((semiperimeter - A)*(semiperimeter - B)*(semiperimeter - C)));
    
    GLfloat radius =GLfloat (area / semiperimeter);
    
    return radius;
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
    GLfloat x = 0.0f, y = 0.0f;
    //vertices[2000];
    for (int i = 0; i < 2000-3; i=i+3)
    {
        GLfloat angle = (2 * M_PI*i) / 2000;
        x =(GLfloat) XPointOfCenter + radius * cos(angle);
        y =(GLfloat) YPointOfCenter + radius * sin(angle);
        
        vertices[i] = x;
        vertices[i + 1] = y;
        vertices[i + 2] = 0.0f;
        
        color[i] = 1.0f;
        color[i + 1] = 0.0f;
        color[i + 2] = 1.0f;
    }
    
}




+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    modelViewMatrix=mat4::identity();
    modelViewProjectionMatrix=mat4::identity();
    translationMatrix=mat4::identity();
    
    translationMatrix= translate(0.0f, 0.0f, -5.0f);
    modelViewMatrix=modelViewMatrix*translationMatrix;
    
    modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
    glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
    
    //glLineWidth(5.0f);
    [self drawGraph];
    
    [self drawSquare];
    
    drawCircle(verticesForOuterCircle,sizeof(verticesForOuterCircle),colorForCircle,sizeof(colorForCircle));
    
    [self drawTriangle];
    
    drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}
-(void) drawGraph
{
    void drawLine(GLfloat*, int, GLfloat*, int);
    GLfloat lineColor[8];
    
    GLfloat horizontalLines[6];
    GLfloat verticalLines[6];
    
    GLfloat lineVerticesHorizontal[] = {
        8.0f,0.0f,0.0f,
        -8.0f,0.0f,0.0f,
    };
    
    lineColor[0] = 1.0f;
    lineColor[1] = 0.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 0.0f;
    lineColor[4] = 1.0f;
    lineColor[5] = 0.0f;
    lineColor[6] = 0.0f;
    lineColor[7] = 0.0f;
    
    //glUniform4fv(colorUniform,1,lineColor);
    
    //X Axis
    drawLine(lineVerticesHorizontal, sizeof(lineVerticesHorizontal), lineColor, sizeof(lineColor));
    
    
    //Y Axix
    GLfloat lineVerticesVertical[] = {
        0.0f,4.0f,0.0f,
        0.0f,-4.0f,0.0f
    };
    
    lineColor[0] = 0.0f;
    lineColor[1] = 1.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 0.0f;
    lineColor[4] = 0.0f;
    lineColor[5] = 1.0f;
    lineColor[6] = 0.0f;
    lineColor[7] = 0.0f;
    
    //glUniform4fv(colorUniform,1,lineColor);
    
    drawLine(lineVerticesVertical, sizeof(lineVerticesVertical), lineColor, sizeof(lineColor));
    
    
    //vertical Lines
    lineColor[0] = 0.0f;
    lineColor[1] = 0.0f;
    lineColor[2] = 1.0f;
    lineColor[3] = 0.0f;
    //vertical Lines
    lineColor[4] = 0.0f;
    lineColor[5] = 0.0f;
    lineColor[6] = 1.0f;
    lineColor[7] = 0.0f;
    
    
    //glUniform4fv(colorUniform,1,lineColor);
    
    
    glBindVertexArray(vao_line);
    for (int i = 1; i < 21; i++)
    {
        GLfloat width = 3.53f*i / 20;
        verticalLines[0] = width;
        verticalLines[1] = 4.0f;
        verticalLines[2] = 0.0f;
        
        verticalLines[3] = width;
        verticalLines[4] = -4.0f;
        verticalLines[5] = 0.0f;
        drawLine(verticalLines, sizeof(verticalLines), lineColor, sizeof(lineColor));
        
        width = -3.53f*i / 20;
        verticalLines[0] = width;
        verticalLines[1] = 4.0f;
        verticalLines[2] = 0.0f;
        
        verticalLines[3] = width;
        verticalLines[4] = -4.0f;
        verticalLines[5] = 0.0f;
        drawLine(verticalLines, sizeof(verticalLines), lineColor, sizeof(lineColor));
        
    }
    glBindVertexArray(0);
    
    
    glBindVertexArray(vao_line);
    for (int i = 1; i < 21; i++)
    {
        GLfloat width = 2.0f*i / 20;
        horizontalLines[0] = 3.53f;
        horizontalLines[1] = width;
        horizontalLines[2] = 0.0f;
        
        horizontalLines[3] = -3.5f;
        horizontalLines[4] = width;
        horizontalLines[5] = 0.0f;
        drawLine(horizontalLines, sizeof(horizontalLines), lineColor, sizeof(lineColor));
        
        width = -2.0f*i / 20;
        horizontalLines[0] = 3.53f;
        horizontalLines[1] = width;
        horizontalLines[2] = 0.0f;
        
        horizontalLines[3] = -3.53f;
        horizontalLines[4] = width;
        horizontalLines[5] = 0.0f;
        drawLine(horizontalLines, sizeof(horizontalLines), lineColor, sizeof(lineColor));
        
    }
    glBindVertexArray(0);
}


-(void) drawSquare
{
    glBindVertexArray(vao_square);
    glLineWidth(2.0f);
    glDrawArrays(GL_LINE_LOOP,0,4);
    glLineWidth(0.0f);
    glBindVertexArray(0);
}

void drawCircle(GLfloat * vertices,GLfloat verticesSize,GLfloat *color,GLfloat colorSize)
{
    glBindVertexArray(vao_circle);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
    glBufferData(GL_ARRAY_BUFFER,verticesSize,vertices,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
    glBufferData(GL_ARRAY_BUFFER, colorSize, color, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_POINTS,0,1000);
    glBindVertexArray(0);
}

-(void) drawTriangle
{
    glBindVertexArray(vao_triangle);
    glDrawArrays(GL_LINE_LOOP,0,3);
    glBindVertexArray(0);
}

void drawLine(GLfloat* lineVertices,int lineArraySize,GLfloat* lineColor,int lineColorSize)
{
    glBindVertexArray(vao_line);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
    glBufferData(GL_ARRAY_BUFFER, lineArraySize, lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_line_color);
    glBufferData(GL_ARRAY_BUFFER,lineColorSize, lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
}


-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
    
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


@end
