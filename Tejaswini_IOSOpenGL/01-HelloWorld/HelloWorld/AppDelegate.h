//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

