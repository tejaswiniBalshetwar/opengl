//
//  MyView.h
//  HelloWorld
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView<UIGestureRecognizerDelegate>

@end
