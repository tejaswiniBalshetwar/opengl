//
//  MyView.m
//  HelloWorld
//
//  Created by Saurabh Bhalgat on 23/01/20.
//

#import "MyView.h"

@implementation MyView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
{
    NSString *centralText;
    
}

-(id)initWithFrame:(CGRect)frameRect
{
    self=[super initWithFrame:frameRect];
    
    if(self)
    {
        [self setBackgroundColor:[UIColor whiteColor]];
        centralText=@"Hello World";
        
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    return self;
}

-(void)drawRect:(CGRect)rect
{
    UIColor *fillColor=[UIColor blackColor];
    [fillColor set];
    
    UIRectFill(rect);
    
    NSDictionary *tbDictonaryWithAttributes=[NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"HelVetica" size:24],NSFontAttributeName,[UIColor greenColor],
                                             NSForegroundColorAttributeName,nil];
    
    CGSize textSize=[centralText sizeWithAttributes:tbDictonaryWithAttributes];
    
    CGPoint point;
    point.x=(rect.size.width/2)-(textSize.width/2);
    point.y=(rect.size.height/2)-(textSize.height/2)+12;
    
    [centralText drawAtPoint:point withAttributes:tbDictonaryWithAttributes];
    
    
}
-(BOOL)acceptsFirstResponder
{
    return YES;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    centralText=@"'On Single Tap Event'";
    [self  setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    centralText=@"'On Double Tap Event'";
    [self  setNeedsDisplay];
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    centralText=@"'On Long Press Tap Event'";
    [self  setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    centralText=@"'On Swipe Event'";
  
    [self  setNeedsDisplay];
      [self release];
    exit(0);
}

- (void)dealloc
{
    [super dealloc];
}

@end
