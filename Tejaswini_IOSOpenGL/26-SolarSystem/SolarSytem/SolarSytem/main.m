//
//  main.m
//  DynamicIndia
//
//  Created by Chetan Balshetwar on 09/04/20.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
       
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
         appDelegateClassName = NSStringFromClass([AppDelegate class]);
        
        int ret=  UIApplicationMain(argc, argv, nil, appDelegateClassName);
        
        [pool release];
    return ret;
}
