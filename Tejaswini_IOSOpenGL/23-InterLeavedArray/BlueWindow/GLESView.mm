

//
//  GLESView.m
//  02-BlueWindow
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"



enum
{
    THB_ATTRIBUTE_POSITION=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};

float light_ambient[] = { 0.250f,0.250f,0.250f,0.250f };
float light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[] = { 10.0f,10.0f,10.0f,1.0f };

float material_ambient[] = { 0.250f,0.250f,0.250f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;

using namespace vmath;
@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    mat4 perspectiveProjectionMatrix;
    
    GLuint vao_cube;
    GLuint vbo_cube;
    
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;
    GLuint LKeyIsPressedUniform;
    GLuint laUniform;
    GLuint ldUniform;
    GLuint lsUniform;
    GLuint kaUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint lightPositionUniform;
    GLuint materialShininessUniform;
    
    
    GLuint samplerUniform;
    GLuint texture_marble;
    
    bool bLight ;
    bool bRotation ;
    
    GLfloat angleCube;

}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        //Opengl GL shader code
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const char* vertexShaderSourceCode=
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vColor; " \
        "in vec2 vTexCoord;" \
        "in vec3 vNormal;                                \n" \
        "uniform mat4 u_model_matrix;                    \n" \
        "uniform mat4 u_view_matrix;                    \n" \
        "uniform mat4 u_projection_matrix;                \n" \
        "uniform int u_LKeyIsPressed;                    \n" \
        "uniform vec4 u_light_position;                    \n" \
        "out vec3 out_light_direction;                    \n" \
        "out vec3 out_t_normal;                            \n" \
        "out vec3 out_viewer_vector;                    \n" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec3 out_color;" \
        "out vec2 out_TexCoord;" \
        "void main(void)" \
        "{" \
        "out_color=vColor;                                \n" \
        "out_TexCoord=vTexCoord;                        \n" \
        "    if(u_LKeyIsPressed==1)                        \n" \
        "    {                                            \n" \
        "        vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;            \n" \
        "        out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;                    \n" \
        "        out_light_direction=vec3(u_light_position - eye_coordinates);                \n" \
        "        out_viewer_vector =vec3(-eye_coordinates);                                    \n" \
        "    }                                                                                \n" \
        "    gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;    \n"  \
        "}";
        


        
        //specify the shader to vertex shader object
        glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength=0;
        GLint iShaderCompileStatus=0;
        char* szInfoLog=NULL;
        
        glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Vertex Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf( "\nVertex shader compiled successfully");
        }
        
        
        //fragment shader
        
        //creating fragment shader
        fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
        
        //writting shader source code
        const char* fragmentShaderSourceCode=
        "#version 300 es " \
        "\n" \
        "precision highp float;" \
         "precision highp int;" \
        "in vec3 out_color;                                \n" \
        "in vec2 out_TexCoord;                            \n" \
        "uniform sampler2D u_sampler;                    \n" \
        "uniform vec3 u_la;                                \n" \
        "uniform vec3 u_ld;                                \n" \
        "uniform vec3 u_ls;                                \n" \
        "uniform int u_LKeyIsPressed;                    \n" \
        "uniform vec3 u_ka;                                \n" \
        "uniform vec3 u_kd;                                \n" \
        "uniform vec3 u_ks;                                \n" \
        "uniform float u_material_shininess;            \n" \
        "in vec3 out_light_direction;                    \n" \
        "in vec3 out_t_normal;                            \n" \
        "in vec3 out_viewer_vector;                        \n" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "    vec4 texSampler=texture(u_sampler,out_TexCoord);" \
        "    vec3 phong_ads_light;                        \n" \
        "    if(u_LKeyIsPressed==1)                        \n" \
        "    {                                            \n"    \
        "        vec3 normalized_light_direction=normalize(out_light_direction);                                                    \n"    \
        "        vec3 normalized_t_norm=normalize(out_t_normal);                                                                    \n"    \
        "        float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);                                        \n" \
        "        vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);                                    \n" \
        "        vec3 normalized_viewer_vector=normalize(out_viewer_vector);                                                            \n" \
        "        vec3 ambient= u_la * u_ka;                                                                                            \n " \
        "        vec3 diffuse=u_ld * u_kd * t_dot_ld;                                                                                \n" \
        "        vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);    \n" \
        "        phong_ads_light=ambient + diffuse + specular;                                                                        \n" \
        "    }                                                                                                                    \n" \
        "    else                                                                                                                \n" \
        "    {                                                                                                                    \n" \
        "            phong_ads_light=vec3(1.0,1.0,1.0);                                                                            \n" \
        "    }                                                                                                                    \n" \
        "    fragColor=vec4((vec3(texSampler)*vec3(out_color)*phong_ads_light),1.0);                                                    \n" \
        "}";
        

        
        //specifying shader source to fragment shader object
        glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
        
        //compiling shader
        glCompileShader(fragmentShaderObject);
        
        //error checking
        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;
        
        glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in fragment Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf("\nFragement shader compiled successfully");
        }
        
        //Shader Program
        
        shaderProgramObject=glCreateProgram();
        
        //attching the sahder to object
        glAttachShader(shaderProgramObject,vertexShaderObject);
        glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        //pre-linking attrinbuts
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_POSITION,"vPosition");
         glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_TEXCOORD0,"vTexCoord");
        glBindAttribLocation(shaderProgramObject, THB_ATTRIBUTE_COLOR, "vColor");
        glBindAttribLocation(shaderProgramObject, THB_ATTRIBUTE_NORMAL, "vNormal");

        glLinkProgram(shaderProgramObject);
        
        iInfoLogLength=0;
        GLint iShaderLinkStatus=0;
        szInfoLog=NULL;
        
        glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
        if(iShaderLinkStatus==GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in Linking Shader=%s ....",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    // [NSApp terminate];
                }
            }
        }
        else
        {
            printf( "\n Linking of shader successfull");
        }
        
        //post linking
        modelUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
        ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
        lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
        
        kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
        kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
        ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
        
        LKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyIsPressed");
        
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        
        
        samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
        
        
        GLfloat vcntCube[]=
        {
            1.0f,1.0f,1.0f,        1.0f,0.0f,0.0f,        0.0f,0.0f,1.0f,      1.0f,1.0f,
            -1.0f,1.0f,1.0f,      1.0f,0.0f,0.0f,        0.0f,0.0f,1.0f,      0.0f,1.0f,
            -1.0f,-1.0f,1.0f,     1.0f,0.0f,0.0f,        0.0f,0.0f,1.0f,          0.0f,0.0f,
            1.0f,-1.0f,1.0f,      1.0f,0.0f,0.0f,        0.0f,0.0f,1.0f,          1.0f,0.0f,
            
            1.0f,1.0f,-1.0f,    0.0f,1.0f,0.0f,        1.0f,0.0f,0.0f,      1.0f,1.0f,
            1.0f,1.0f,1.0f,        0.0f,1.0f,0.0f,        1.0f,0.0f,0.0f,        0.0f,1.0f,
            1.0f,-1.0f,1.0f,    0.0f,1.0f,0.0f,        1.0f,0.0f,0.0f,        0.0f,0.0f,
            1.0f,-1.0f,-1.0f,    0.0f,1.0f,0.0f,        1.0f,0.0f,0.0f,        1.0f,0.0f,
            
            1.0f,1.0f,-1.0f,    0.0f,0.0f,1.0f,        0.0f,0.0f,-1.0f,    1.0f,1.0f,
            -1.0f,1.0f,-1.0f,    0.0f,0.0f,1.0f,        0.0f,0.0f,-1.0f,    0.0f,1.0f,
            -1.0f,-1.0f,-1.0f,    0.0f,0.0f,1.0f,        0.0f,0.0f,-1.0f,    0.0f,0.0f,
            1.0f,-1.0f,-1.0f,    0.0f,0.0f,1.0f,        0.0f,0.0f,-1.0f,    1.0f,0.0f,
            
            -1.0f,1.0f,1.0f,    0.0f,1.0f,1.0f,        -1.0f,0.0f,0.0f,    1.0f,1.0f,
            -1.0f,1.0f,-1.0f,    0.0f,1.0f,1.0f,        -1.0f,0.0f,0.0f,    0.0f,1.0f,
            -1.0f,-1.0f,-1.0f,    0.0f,1.0f,1.0f,        -1.0f,0.0f,0.0f,    0.0f,0.0f,
            -1.0f,-1.0f,1.0f,    0.0f,1.0f,1.0f,        -1.0f,0.0f,0.0f,    1.0f,0.0f,
            
            1.0f,1.0f,-1.0f,    1.0f,0.0f,1.0f,        0.0f,1.0f,0.0f,        1.0f,1.0f,
            -1.0f,1.0f,-1.0f,    1.0f,0.0f,1.0f,        0.0f,1.0f,0.0f,        0.0f,1.0f,
            -1.0f,1.0f,1.0f,    1.0f,0.0f,1.0f,        0.0f,1.0f,0.0f,        0.0f,0.0f,
            1.0f,1.0f,1.0f,        1.0f,0.0f,1.0f,        0.0f,1.0f,0.0f,        1.0f,0.0f,
            
            1.0f,-1.0f,-1.0f,    1.0f,1.0f,0.0f,        0.0f,-1.0f,0.0f,    1.0f,1.0f,
            -1.0f,-1.0f,-1.0f,    1.0f,1.0f,0.0f,        0.0f,-1.0f,0.0f,    0.0f,1.0f,
            -1.0f,-1.0f,1.0f,    1.0f,1.0f,0.0f,        0.0f,-1.0f,0.0f,    0.0f,0.0f,
            1.0f,-1.0f,1.0f,    1.0f,1.0f,0.0f,        0.0f,-1.0f,0.0f,    1.0f,0.0f,
        };
        
        
        
        //rectangle
        //recording start
        
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        
        //posiiotn data
        glGenBuffers(1, &vbo_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(vcntCube), vcntCube, GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat),(void*) (0*sizeof(GLfloat)));
        glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
        
        glVertexAttribPointer(THB_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
        
        glVertexAttribPointer(THB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));
        glEnableVertexAttribArray(THB_ATTRIBUTE_NORMAL);
        
        glVertexAttribPointer(THB_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(9 * sizeof(GLfloat)));
        glEnableVertexAttribArray(THB_ATTRIBUTE_TEXCOORD0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //recording stop
        glBindVertexArray(0);
        //recording stop
        glBindVertexArray(0);

        
        texture_marble=[self loadTextureFromBMPFile:@"marble":@"bmp"];
      
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

-(GLuint)loadTextureFromBMPFile:(NSString* )texFileName :(NSString*)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
   
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"\n can't find %@",texFileName);
        return 0;
    }
    
    CGImageRef cgImage=bmpImage.CGImage;
    
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void *pixels=(void*)CFDataGetBytePtr(imageData);
    
    GLuint bmptexture;
    glGenTextures(1,&bmptexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bmptexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmptexture);
}

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 scaleMatrix;
    
    
    glUseProgram(shaderProgramObject);
    //square
    modelMatrix=vmath::mat4::identity();
    viewMatrix=vmath::mat4::identity();
    translationMatrix=vmath::mat4::identity();
    rotationMatrix=vmath::mat4::identity();
    scaleMatrix=vmath::mat4::identity();
    
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    scaleMatrix = vmath::scale(0.75f,0.75f,0.75f);
    
    
    rotationMatrix=vmath::rotate(angleCube, angleCube, angleCube);
    
    
    //do matrix multipliacation
    modelMatrix = modelMatrix * translationMatrix;
    modelMatrix = modelMatrix * scaleMatrix;
    //modelViewMatrix = modelViewMatrix * rotationMatrixX * rotationMatrixY* rotationMatrixZ;
    modelMatrix = modelMatrix * rotationMatrix;
    
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_marble);
    glUniform1i(samplerUniform, 0);
    
    if (bLight)
    {
        glUniform1i(LKeyIsPressedUniform, 1);
        glUniform3fv(laUniform, 1, light_ambient);        //glLightfv()
        glUniform3fv(ldUniform, 1, light_diffuse);        //glLightfv()
        glUniform3fv(lsUniform, 1, light_specular);        //glLightfv()
        glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position
        
        glUniform3fv(kaUniform, 1, material_ambient);    //glMaterialfv();
        glUniform3fv(kdUniform, 1, material_diffuse);    //glMaterialfv();
        glUniform3fv(ksUniform, 1, material_specular);    //glMaterialfv();
        glUniform1f(materialShininessUniform, material_shininess);    //glMaterialfv();
    }
    else
    {
        glUniform1i(LKeyIsPressedUniform, 0);
    }
    
    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    glBindVertexArray(0);
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    if(bRotation)
    {
        [self update];
    }
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) update
{
    angleCube=angleCube+0.5f;
    if(angleCube>360.0f)
    {
        angleCube=0.0f;
    }
}
-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    if (bLight == false)
    {
        bLight = true;
        //LKeyIsPressed = 1;
    }
    else
    {
        bLight = false;
        //LKeyIsPressed = 0;
    }
    
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    if (bRotation == false)
    {
        bRotation = true;
    }
    else
    {
        bRotation = false;
    }
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


@end
