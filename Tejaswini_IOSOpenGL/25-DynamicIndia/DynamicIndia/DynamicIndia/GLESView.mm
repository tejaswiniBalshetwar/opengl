//
//  GLESView.m
//  DynamicIndia
//
//  Created by Chetan Balshetwar on 09/04/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"




enum
{
    THB_ATTRIBUTE_POSITION=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};
using namespace vmath;

FILE *gpFile=NULL;
float positions[117];


GLuint vao_circle;
GLuint vbo_circle_position;
GLuint vbo_circle_color;

GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;

GLuint vao_square = 0;
GLuint vbo_square_position = 0;
GLuint vbo_square_color = 0;


GLuint vao_triangle = 0;
GLuint vbo_triangle_position = 0;
GLuint vbo_triangle_color = 0;


//dynamic india translations
GLfloat translateIInX = -4.0f;

GLfloat translateNInY = 4.0f;

GLfloat translateD = 1.0f;

GLfloat translateI2InY = -4.0f;

GLfloat translateAInX = 5.0f;

bool ShowPlanes = false;
bool ShowTricolor = false;

GLfloat incAngle = 270.0f;
GLfloat upperPlaneAngle = M_PI;
GLfloat upperPlaneStopAngle = (3.0f*M_PI) / 2.0f;

GLfloat lowerPlaneAngle = M_PI;
GLfloat decAngle = 90.0f;

GLfloat incAngle2 = 0.0f;
GLfloat upperPlaneAngle2 = (3.0f*M_PI) / 2.0f;
GLfloat upperPlaneStopAngle2 = 2.5f*M_PI;

GLfloat lowerPlaneAngle2 = (M_PI / 2.0f);
GLfloat decAngle2 = 360.0f;

GLfloat translateAllInXDirection = -2.499705f;

bool meet = false;
bool crossTheA = false;
GLfloat translateMiddle = -7.1f;

GLfloat XTranslatePointUP = 0.0f;
GLfloat YTranslatePointUP = 0.0f;

GLfloat XTranslatePointDown = 0.0f;
GLfloat YTranslatePointDown = 0.0f;

vmath::mat4 perspectiveProjectionMatrix;

GLuint mvpUniform;

@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    
    
    //opengl
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;



    
    GLuint mvpUniform;
    GLuint colorUniform;
    GLfloat verticesForOuterCircle[2000];
    GLfloat colorForCircle[2000];

    GLfloat verticesForInnerCircle[2000];

    
}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        //Opengl GL shader code
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const char* vertexShaderSourceCode=
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "out vec4 out_color;" \
        "uniform mat4 u_mvp_uniform;" \
        "void main(void)" \
        "{" \
            "gl_Position=u_mvp_uniform*vPosition;" \
            "gl_PointSize=5.0;" \
            "out_color=vColor;" \
        "}";
        
        
        //specify the shader to vertex shader object
        glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength=0;
        GLint iShaderCompileStatus=0;
        char* szInfoLog=NULL;
        
        glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Vertex Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf( "\nVertex shader compiled successfully");
        }
        
        
        //fragment shader
        
        //creating fragment shader
        fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
        
        //writting shader source code
        const char* fragmentShaderSourceCode=
        "#version 300 es " \
        "\n" \
        "precision highp float;" \
        "in vec4 out_color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor=out_color;" \
        "}" ;
        
        //specifying shader source to fragment shader object
        glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
        
        //compiling shader
        glCompileShader(fragmentShaderObject);
        
        //error checking
        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;
        
        glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in fragment Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf("\nFragement shader compiled successfully");
        }
        
        //Shader Program
        
        shaderProgramObject=glCreateProgram();
        
       //attching the sahder to object
        glAttachShader(shaderProgramObject,vertexShaderObject);
        glAttachShader(shaderProgramObject,fragmentShaderObject);

        //pre-linking attrinbuts
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_POSITION,"vPosition");
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_COLOR,"vColor");

        glLinkProgram(shaderProgramObject);

        iInfoLogLength=0;
        GLint iShaderLinkStatus=0;
        szInfoLog=NULL;

        glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
        if(iShaderLinkStatus==GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Linking Shader ....");
                    free(szInfoLog);
                    [self release];
                   
                }
            }
        }
        else
        {
            printf("\n Linking of shader successfull");
        }

        //post linking
        mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_uniform");

        //vertices for triangle

         //actual code for geometry
       //crating VAO for recording
        //1.line
        glGenVertexArrays(1,&vao_line);
        glBindVertexArray(vao_line);
            glGenBuffers(1,&vbo_line_position);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
                glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            glGenBuffers(1,&vbo_line_color);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
                glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
            glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);


        
        //half circle
        
        GLfloat XCenterOfCircle = 0.0f;
        GLfloat YCenterOfCircle = 0.0f;
        GLfloat radius = 1.0f;

        memset(verticesForInnerCircle,0,sizeof(verticesForInnerCircle));
        memset(colorForCircle,0,sizeof(colorForCircle));
        calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);

        //circle
        glGenVertexArrays(1, &vao_circle);
        glBindVertexArray(vao_circle);
            glGenBuffers(1, &vbo_circle_position);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(verticesForInnerCircle), verticesForInnerCircle, GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glGenBuffers(1, &vbo_circle_color);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
                glBufferData(GL_ARRAY_BUFFER, sizeof(colorForCircle), colorForCircle, GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);


        //triangle for plane
        createPlane();

        
        glGenVertexArrays(1,&vao_triangle);
        glBindVertexArray(vao_triangle);
            glGenBuffers(1,&vbo_triangle_position);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_triangle_position);
                glBufferData(GL_ARRAY_BUFFER,sizeof(positions),positions,GL_STATIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            glVertexAttrib4f(THB_ATTRIBUTE_COLOR, 0.0729f, 0.886f, 0.9333f, 1.0f);
        glBindVertexArray(0);


        //square
        GLfloat square_vertices[] = {
            1.0f,1.20f,0.0f,
            -1.0f,1.20f,0.0f,
            -1.0f,-1.20f,0.0f,
            1.0f,-1.20f,0.0f
        };

        GLfloat square_color[] = {
            0.0f,0.0f,0.0f,1.0f,
            0.0f,0.0f,0.0f,1.0f,
            0.0f,0.0f,0.0f,1.0f,
            0.0f,0.0f,0.0f,1.0f,

        };

        glGenVertexArrays(1,&vao_square);
        glBindVertexArray(vao_square);
            glGenBuffers(1,&vbo_square_position);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
                glBufferData(GL_ARRAY_BUFFER,sizeof(square_vertices),square_vertices,GL_STATIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            glGenBuffers(1,&vbo_square_color);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
                glBufferData(GL_ARRAY_BUFFER,sizeof(square_color),square_color,GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
            glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);

        //geometry end here
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix= mat4::identity();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
     mat4 translationMatrix;
        
        mat4 modelViewMatrix;
        mat4 modelViewProjectionMatrix;
        mat4 rotationMatrix;
        
        glUseProgram(shaderProgramObject);
        modelViewMatrix=mat4::identity();
            modelViewProjectionMatrix=mat4::identity();
            translationMatrix=mat4::identity();

            translationMatrix= translate(translateIInX, 0.0f, -6.0f);
            modelViewMatrix=modelViewMatrix*translationMatrix;

            modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
            glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

            glLineWidth(5.0f);
            GLfloat lineVerticesI[]={
                0.0f,1.0f,0.0f,
                0.0f,-1.0f,0.0f
            };
            GLfloat colorForLineI[] = {
                1.0f,0.6f,0.2f,1.0f,
                0.0706f,0.3831f,0.02745f,1.0f
            };
            drawLine(lineVerticesI,sizeof(lineVerticesI),colorForLineI,sizeof(colorForLineI),2);


            //N
            modelViewMatrix = mat4::identity();
            translationMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();

            translationMatrix = translate(-2.0f,translateNInY,-6.0f);
            modelViewMatrix = modelViewMatrix * translationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

            glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);



            GLfloat lineVerticesN[] = {
                1.0f,1.0f,0.0f,
                1.0f,-1.0f,0.0f,
                0.0f,1.0f,0.0f,
                0.0f,-1.0f,0.0f

            };
            GLfloat colorForLineN[] = {
                1.0f,0.6f,0.2f,1.0f,
                0.0706f,0.3831f,0.02745f,1.0f,
                1.0f,0.6f,0.2f,1.0f,
                0.0706f,0.3831f,0.02745f,1.0f,
                1.0f,0.6f,0.2f,1.0f,
                0.0706f,0.3831f,0.02745f,1.0f,

            };
            drawLine(lineVerticesN, sizeof(lineVerticesN), colorForLineN, sizeof(colorForLineN),4);

            //D

            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();
            translationMatrix = mat4::identity();

            translationMatrix = translate(-0.50f, 0.0f, -6.0f);
            modelViewMatrix = modelViewMatrix * translationMatrix;

            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

            
            drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

            //circle
            //loadIdentity for circle
            translationMatrix = mat4::identity();
            rotationMatrix = mat4::identity();
            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();

            //for circle Translations
            translationMatrix = translate(-0.50f, 0.0f,-6.0f);
            rotationMatrix = rotate(-90.0f,0.0f,0.0f,1.0f);
            
            modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

            drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

            //for blendEffect
            translationMatrix = mat4::identity();
            rotationMatrix = mat4::identity();
            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();

            //for circle Translations
            translationMatrix = translate(0.10f, 0.0f, -6.0f);
            modelViewMatrix = modelViewMatrix * translationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

            GLfloat colorForSquare[16];
            colorForSquare[0] = 0.0f;
            colorForSquare[1] = 0.0f;
            colorForSquare[2] = 0.0f;
            colorForSquare[3] = translateD;
            
            colorForSquare[4] = 0.0f;
            colorForSquare[5] = 0.0f;
            colorForSquare[6] = 0.0f;
            colorForSquare[7] = translateD;
            
            colorForSquare[8] = 0.0f;
            colorForSquare[9] = 0.0f;
            colorForSquare[10] = 0.0f;
            colorForSquare[11] = translateD;
            
            colorForSquare[12] = 0.0f;
            colorForSquare[13] = 0.0f;
            colorForSquare[14] = 0.0f;
            colorForSquare[15] = translateD;


            glBindVertexArray(vao_square);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
                    glBufferData(GL_ARRAY_BUFFER,sizeof(colorForSquare),colorForSquare,GL_DYNAMIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER,0);
                glDrawArrays(GL_TRIANGLE_FAN,0,4);
            glBindVertexArray(0);



            //for I

            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();
            translationMatrix = mat4::identity();

            translationMatrix = translate(1.0f, translateI2InY, -6.0f);
            modelViewMatrix = modelViewMatrix * translationMatrix;

            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        
            drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

            //A
            translationMatrix = mat4::identity();
            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();

            translationMatrix = translate(translateAInX,0.0f,-6.0f);
            modelViewMatrix = modelViewMatrix * translationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

            GLfloat verticesForA[] = {
                -0.5f,-1.0f,0.0f,
                0.0f,1.0f,0.0f,
                0.5f,-1.0f,0.0f,
            };

            GLfloat colorForA[] = {
                0.0706f,0.3831f,0.02745f,1.0f,
                1.0f,0.6f,0.2f,1.0f,
                0.0706f,0.3831f,0.02745f,1.0f,
                1.0f,0.6f,0.2f,1.0f,

            };

            drawLine(verticesForA,sizeof(verticesForA),colorForA,sizeof(colorForA),3);
          
            if (ShowTricolor)
            {
                drawTriColor(verticesForA);
            }
                
            if (ShowPlanes)
              {
                  
                  //upperplane
                   drawUpperPlane();

                  //Lower Plane
                  drawLowerPlane();

                  //Middle Plane
                  drawMiddelePlane();

              }

            
            glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [self update];
    
    if (ShowPlanes)
    {
        PlaneTranslations();
    }
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}
-(void) update
{
    if (translateIInX <= -2.5f)
    {
        translateIInX = translateIInX + 0.008;
    }

    if (translateIInX >= -2.500609f && translateAInX >= 2.0f)
    {
        translateAInX = translateAInX - 0.008f;
    }

    if (translateIInX >= -2.500609f && translateAInX <= 2.0f &&  translateNInY >= 0.0f)
    {
        translateNInY = translateNInY - 0.008f;
    }

    if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY <= 0.0f)
    {
        translateI2InY = translateI2InY + 0.008f;
    }

    if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY >= 0.0f &&translateD >=0.0f)
    {
        translateD = translateD - 0.008f;
    }
    if (translateI2InY >= 0.0f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateIInX >= -2.5f &&translateD <= 0.0f)
    {
        ShowPlanes = true;
    }

    if (XTranslatePointUP >= 3.499705 && XTranslatePointDown >= 3.499705 && translateMiddle >= 3.499705)
    {
        ShowTricolor = true;
    }

    
    
}

-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
    
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
    GLfloat x = 0.0f, y = 0.0f;
    GLfloat RColor = 0.0706f;
    GLfloat GColor = 0.3831f;
    GLfloat BColor = 0.02745f;
    //vertices[2000];
    for (int i = 0; i < 2000-3; i=i+3)
    {
        GLfloat angle =(GLfloat)(M_PI*i / 1500);
        x =(GLfloat) (XPointOfCenter + radius * cos(angle));
        y =(GLfloat) (YPointOfCenter + radius * sin(angle));

        vertices[i] = x;
        vertices[i + 1] = y;
        vertices[i + 2] = 0.0f;

        if (RColor <= 1.0)
        {
            RColor += 0.002f;

        }
        if (GColor <= 0.6)
        {
            GColor += 0.002f;
        }
        if (BColor <= 0.2)
        {
            BColor += 0.002f;
        }
        color[i] = RColor;
        color[i + 1] = GColor;
        color[i + 2] = BColor;
    }

}

void drawCircle(GLfloat * vertices,int verticesSize,GLfloat *color,int colorSize)
{
    glBindVertexArray(vao_circle);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
            glBufferData(GL_ARRAY_BUFFER,verticesSize,(vertices),GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER,0);
    
        glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
            glBufferData(GL_ARRAY_BUFFER, colorSize,color, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        glDrawArrays(GL_POINTS,0,500);
    glBindVertexArray(0);
}





void createPlane()
{
    
    positions[0] = 1.0f;
    positions[1] = 0.0f;
    positions[2] = 1.0f;

    positions[3] = 0.0f;
    positions[4] = -0.35f;
    positions[5] = 1.0f;

    positions[6] = 0.0f;
    positions[7] = 0.35f;
    positions[8] = 1.0f;

    //body
    positions[9] = 0.0f;
    positions[10] = 0.35f;
    positions[11] = 1.0f;

    positions[12] = -1.0f;
    positions[13] = 0.35f;
    positions[14] = 1.0f;

    positions[15] = -1.0f;
    positions[16] = -0.35f;
    positions[17] = 1.0f;

    positions[18] = -1.0f;
    positions[19] = -0.35f;
    positions[20] = 1.0f;

    positions[21] = 0.0f;
    positions[22] = -0.35f;
    positions[23] = 1.0f;

    positions[24] = 0.0f;
    positions[25] = 0.35f;
    positions[26] = 1.0f;
    
    //polygon start
    positions[27] = -1.0f;
    positions[28] = -0.35f;
    positions[29] = 1.0f;

    positions[30] = -2.0f;
    positions[31] = -1.75f;
    positions[32] = 1.0f;
    
    positions[33] = -2.5f;
    positions[34] = -1.75f;
    positions[35] = 1.0f;

    positions[36] = -2.5f;
    positions[37] = -1.75f;
    positions[38] = 1.0f;

    positions[39] = -1.8f;
    positions[40] = -0.35f;
    positions[41] = 1.0f;

    positions[42] = -1.0f;
    positions[43] = -0.35f;
    positions[44] = 1.0f;

    
    //upper start half
    positions[45] = -1.0f;
    positions[46] = 0.35f;
    positions[47] = 1.0f;

    positions[48] = -2.0f;
    positions[49] = 1.75f;
    positions[50] = 1.0f;

    positions[51] = -2.5f;
    positions[52] = 1.75f;
    positions[53] = 1.0f;

    positions[54] = -2.5f;
    positions[55] = 1.75f;
    positions[56] = 1.0f;

    positions[57] = -1.8f;
    positions[58] = 0.35f;
    positions[59] = 1.0f;

    positions[60] = -1.0f;
    positions[61] = 0.35f;
    positions[62] = 1.0f;
    
    //quad
    positions[63] = -1.35f;
    positions[64] = 0.35f;
    positions[65] = 1.0f;

    positions[66] = -3.0f;
    positions[67] = 0.35f;
    positions[68] = 1.0f;

    positions[69] = -3.0f;
    positions[70] = -0.35f;
    positions[71] = 1.0f;

    positions[72] = -3.0f;
    positions[73] = -0.35f;
    positions[74] = 1.0f;

    positions[75] = -1.0f;
    positions[76] = -0.35f;
    positions[77] = 1.0f;

    positions[78] = -1.0f;
    positions[79] =0.35f;
    positions[80] = 1.0f;
    
    
    //lower part of planes last
    positions[81] = -3.0f;
    positions[82] = -0.35f;
    positions[83] = 1.0f;

    positions[84] = -3.5f;
    positions[85] = -1.0f;
    positions[86] = 1.0f;

    positions[87] = -3.90f;
    positions[88] = -1.0f;
    positions[89] = 1.0f;
    
    positions[90] = -3.90f;
    positions[91] = -1.0f;
    positions[92] = 1.0f;

    positions[93] = -3.0f;
    positions[94] = 0.35f;
    positions[95] = 1.0f;

    positions[96] = -3.0f;
    positions[97] = -0.35f;
    positions[98] = 1.0f;
    
    //plane upper part of last side
    positions[99] = -3.0f;
    positions[100] = 0.35f;
    positions[101] = 1.0f;

    positions[102] = -3.5f;
    positions[103] = 1.0f;
    positions[104] = 1.0f;
    
    positions[105] = -3.90f;
    positions[106] = 1.0f;
    positions[107] = 1.0f;
    
    positions[108] = -3.90f;
    positions[109] = 1.0f;
    positions[110] = 1.0f;

    positions[111] = -3.0f;
    positions[112] = -0.35f;
    positions[113] = 1.0f;

    positions[114] = -3.0f;
    positions[115] = -0.35f;
    positions[116] = 1.0f;
            
    
}
void drawUpperPlane(void)
{
    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;
   
    modelViewMatrix= mat4::identity();
    modelViewProjectionMatrix=mat4::identity();
    translationMatrix=mat4::identity();
    rotationMatrix = mat4::identity();
    scaleMatrix = mat4::identity();

    if (meet == false)
    {
        XTranslatePointUP = -1.20f + 4.60f*cos(upperPlaneAngle);
        YTranslatePointUP = 4.6f + 4.60f*sin(upperPlaneAngle);
        //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointUP,YTranslatePointUP);
        translationMatrix = translate(XTranslatePointUP, YTranslatePointUP, -6.0f);
        //glTranslatef(XTranslatePointUP, YTranslatePointUP, -10.0f);
        if (incAngle <= 360.0f || incAngle > 0.0f)
        {
            rotationMatrix = rotate(incAngle,0.0f,0.0f,1.0f);
            //glRotatef(incAngle, 0.0f, 0.0f, 1.0f);
        }
    }
    else if (translateAllInXDirection > 3.0f)
    {

        XTranslatePointUP = 3.0f + 4.60f*cos(upperPlaneAngle2);
        YTranslatePointUP = 4.60f + 4.60f*sin(upperPlaneAngle2);
        //fprintf(gpFile, "XTranslatePoint2:%f  YTranslatePoint2%f\n",XTranslatePointUP,YTranslatePointUP);
        
        //glTranslatef(XTranslatePointUP, YTranslatePointUP, -10.0f);
        translationMatrix = translate(XTranslatePointUP, YTranslatePointUP, -6.0f);
        if (incAngle2 <= 90.0f || incAngle2 > 0.0f)
        {

            //glRotatef(incAngle2, 0.0f, 0.0f, 1.0f);
            rotationMatrix = rotate(incAngle2, 0.0f, 0.0f, 1.0f);
        }
    }
    else
    {
       
        translationMatrix = translate(translateAllInXDirection, 0.0f, -6.0f);
        //glTranslatef(translateAllInXDirection, 0.0f, -10.0f);
    }

    

    scaleMatrix = scale(0.350f, 0.350f, 0.350f);
    modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
               
     glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
    drawPlane();


}

void drawLowerPlane(void)
{
    void drawPlane(void);
    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    translationMatrix = mat4::identity();
    scaleMatrix = mat4::identity();
    rotationMatrix = mat4::identity();


    if (meet == false)
    {
        XTranslatePointDown = -1.2f + 4.60f*cos(lowerPlaneAngle);
        YTranslatePointDown = -4.6f + 4.60f*sin(lowerPlaneAngle);
        //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
        //glTranslatef(XTranslatePointDown, YTranslatePointDown, -10.0f);
        translationMatrix = translate(XTranslatePointDown, YTranslatePointDown, -6.0f);
        if (decAngle <= 90.0f || decAngle > 0.0f)
        {
            rotationMatrix = rotate(decAngle, 0.0f, 0.0f, 1.0f);
            //glRotatef(decAngle, 0.0f, 0.0f, 1.0f);
        }
    }

    else if (translateAllInXDirection > 3.0f)
    {
        XTranslatePointDown = 3.0f + 4.60f*cos(lowerPlaneAngle2);
        YTranslatePointDown = -4.60f + 4.60f*sin(lowerPlaneAngle2);
        //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
        translationMatrix = translate(XTranslatePointDown, YTranslatePointDown, -6.0f);
        translate(XTranslatePointDown, YTranslatePointDown, -10.0f);
        if (decAngle2 <= 360.0f || decAngle2 > 270.0f)
        {
            rotationMatrix = rotate(decAngle2, 0.0f, 0.0f, 1.0f);
            //glRotatef(decAngle2, 0.0f, 0.0f, 1.0f);
        }
    }
    else
    {
        //glTranslatef(translateAllInXDirection, 0.0f, -10.0f);
        translationMatrix = translate(translateAllInXDirection, 0.0f, -6.0f);
    }


    scaleMatrix = scale(0.350f, 0.350f, 0.350f);
    modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    drawPlane();

}

void drawMiddelePlane(void)
{
    mat4 translationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    translationMatrix = mat4::identity();
    scaleMatrix = mat4::identity();
    rotationMatrix = mat4::identity();

    //glTranslatef(translateMiddle, 0.0f, -10.0f);
    translationMatrix = translate(translateMiddle, 0.0f, -6.0f);
    
    scaleMatrix = scale(0.350f, 0.350f, 0.350f);
    modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    drawPlane();
}

void drawPlane()
{

     glBindVertexArray(vao_triangle);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDrawArrays(GL_TRIANGLE_FAN, 3, 6);
    glDrawArrays(GL_TRIANGLE_FAN, 9, 6);
    glDrawArrays(GL_TRIANGLE_FAN, 15, 6);
    glDrawArrays(GL_TRIANGLE_FAN, 21, 6);
    glDrawArrays(GL_TRIANGLE_FAN, 27, 6);
    glDrawArrays(GL_TRIANGLE_FAN, 33, 6);
    glBindVertexArray(0);

    GLfloat linePositions[] = {
        -3.3f,0.12f,0.0f,
        -5.0f,0.12f,0.0f,
        -5.0f,0.04f,0.0f,
        -3.3f,0.04f,0.0f,
        -3.3f,-0.04f,0.0f,
        -5.0f,-0.04f,0.0f
    };

    GLfloat color[] = {
        1.0f,0.6f,0.2f,1.0f,
        0.0f,0.0f,0.0f,1.0f,
        0.0f,0.0f,0.0f,1.0f,
        1.0f,1.0f,1.0f,1.0f,
        0.0706f,0.3831f,0.02745f,1.0f,
        0.0f,0.0f,0.0f,1.0f
    };
    drawLine(linePositions, sizeof(linePositions), color, sizeof(color), 6);
}


    
void drawTriColor(GLfloat* verticesForA)
{
    void drawLine(GLfloat*, int, GLfloat*, int, int);

    GLfloat triColorVertex[6];

    GLfloat X1ForLine = (verticesForA[0] + verticesForA[3]) / 2;
    GLfloat Y1ForLine = (verticesForA[1] + verticesForA[4]) / 2;

    GLfloat X2ForLine = (verticesForA[3] + verticesForA[6]) / 2;
    GLfloat Y2ForLine = (verticesForA[1] + verticesForA[4]) / 2;

    GLfloat colorForTricolor[8];

    triColorVertex[0] = X1ForLine;
    triColorVertex[1] = Y1ForLine;
    triColorVertex[2] = 0.0f;

    triColorVertex[3] = X2ForLine;
    triColorVertex[4] = Y2ForLine;
    triColorVertex[5] = 0.0f;

    colorForTricolor[0] = 1.0f;
    colorForTricolor[1] = 0.6f;
    colorForTricolor[2] = 0.2f;
    colorForTricolor[3] = 1.0f;

    colorForTricolor[4] = 1.0f;
    colorForTricolor[5] = 0.6f;
    colorForTricolor[6] = 0.2f;
    colorForTricolor[7] = 1.0f;


    drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

    triColorVertex[0] = X1ForLine - 0.01f;
    triColorVertex[1] = Y1ForLine - 0.04f;
    triColorVertex[2] = 0.0f;

    triColorVertex[3] = X2ForLine;
    triColorVertex[4] = Y2ForLine - 0.04f;
    triColorVertex[5] = 0.0f;

    colorForTricolor[0] = 1.0f;
    colorForTricolor[1] = 1.0f;
    colorForTricolor[2] = 1.0f;
    colorForTricolor[3] = 1.0f;

    colorForTricolor[4] = 1.0f;
    colorForTricolor[5] = 1.0f;
    colorForTricolor[6] = 1.0f;
    colorForTricolor[7] = 1.0f;


    drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

    triColorVertex[0] = X1ForLine;
    triColorVertex[1] = Y1ForLine - 0.07f;
    triColorVertex[2] = 0.0f;

    triColorVertex[3] = X2ForLine;
    triColorVertex[4] = Y2ForLine - 0.07f;
    triColorVertex[5] = 0.0f;

    colorForTricolor[0] = 0.0706f;
    colorForTricolor[1] = 0.3831f;
    colorForTricolor[2] = 0.02745f;
    colorForTricolor[3] = 1.0f;

    colorForTricolor[4] = 0.0706f;
    colorForTricolor[5] = 0.3831f;
    colorForTricolor[6] = 0.02745f;
    colorForTricolor[7] = 1.0f;


    drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);
}
void drawLine(GLfloat *vertices ,int sizeOfVertices, GLfloat * color, int sizeofColor, int numberOfPoints)
{
    glBindVertexArray(vao_line);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
        glBufferData(GL_ARRAY_BUFFER,sizeOfVertices,vertices,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
        glBufferData(GL_ARRAY_BUFFER,sizeofColor,color,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
    glBindVertexArray(0);
}

void PlaneTranslations(void)
{
   
    if (upperPlaneAngle < upperPlaneStopAngle)
    {
        upperPlaneAngle = upperPlaneAngle + 0.002f;
        // upperPlaneAngle=0.0f;
    }
    if (incAngle <= 360.0f)
    {
        incAngle = incAngle + 0.13f;
        // incAngle=0.0f;
    }


    //lower Plane
    if (lowerPlaneAngle > (M_PI / 2))
    {
        lowerPlaneAngle = lowerPlaneAngle - 0.002;
    }
    if (decAngle >= 0.0f)
    {
        decAngle = decAngle - 0.13f;
        // incAngle=0.0f;
    }

    if (translateMiddle <= -2.494244f)
    {
        translateMiddle = translateMiddle + 0.0072f;
    }


    if (translateAllInXDirection <= 3.0f && meet == true)
    {
        translateAllInXDirection = translateAllInXDirection + 0.005f;
        translateMiddle = translateAllInXDirection;
    }


    /**********************************************cheking the meet condition********************************/
    if (XTranslatePointUP >= -2.494244f && XTranslatePointDown >= -2.494244f && translateMiddle >= -2.494244f)
    {
        meet = true;
    }



    /*******************************----------------2Nd Part---------------------*****************************/

    if (translateAllInXDirection >= 3.0f && meet == true && translateMiddle < 11.50f)
    {
        translateMiddle = translateMiddle + 0.008f;

    }
    if (upperPlaneAngle2 < upperPlaneStopAngle2 && translateAllInXDirection >= 3.0f)
    {
        upperPlaneAngle2 = upperPlaneAngle2 + 0.002f;

    }
    if (incAngle2 <= 90.0f && translateAllInXDirection >= 3.0f)
    {
        incAngle2 = incAngle2 + 0.013f;

    }

    if (lowerPlaneAngle2 > -0.5f && translateAllInXDirection >= 3.0f)
    {
        lowerPlaneAngle2 = lowerPlaneAngle2 - 0.002f;

    }
    if (decAngle2 >= 270.0f && translateAllInXDirection >= 2.50f)
    {
        decAngle2 = decAngle2 - 0.013f;

    }
}

@end




