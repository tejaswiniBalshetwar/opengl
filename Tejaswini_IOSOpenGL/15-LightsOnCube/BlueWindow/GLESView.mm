

//
//  GLESView.m
//  02-BlueWindow
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"



enum
{
    THB_ATTRIBUTE_POSITION=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};
using namespace vmath;
@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
   
    mat4 perspectiveProjectionMatrix;
    
    GLuint vao_cube;
    
    GLuint vbo_position_cube;
    
    
    GLuint vbo_normal_cube;
    
    GLuint mvUniform;
    GLuint projectionUniform;
    GLuint LKeyIsPressedUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightPositionUniform;
    
    
    bool bRotation;
    bool bLight;
    
    int LKeyIsPressed;
    
    GLfloat angleCube;
    
}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        //Opengl GL shader code
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const char* vertexShaderSourceCode=
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal; " \
        "uniform mat4 u_mv_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_LKeyIsPressed;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_kd;" \
        "uniform vec4 u_light_position;" \
        "out vec3 out_diffuse_color;" \
        "void main(void)" \
        "{" \
        "if(u_LKeyIsPressed==1)" \
        "{" \
        "vec4 eye_coordinates=u_mv_matrix * vPosition;" \
        "mat3 normal_matrix=mat3(transpose(inverse(u_mv_matrix)));" \
        "vec3 t_normal=normalize(normal_matrix * vNormal);" \
        "vec3 s=vec3(u_light_position - eye_coordinates);" \
        "out_diffuse_color=u_ld * u_kd * max(dot(s,t_normal),0.0);" \
        
        "}" \
        "gl_Position=u_projection_matrix* u_mv_matrix * vPosition ;"  \
        "}";


        
        //specify the shader to vertex shader object
        glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength=0;
        GLint iShaderCompileStatus=0;
        char* szInfoLog=NULL;
        
        glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Vertex Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf( "\nVertex shader compiled successfully");
        }
        
        
        //fragment shader
        
        //creating fragment shader
        fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
        
        //writting shader source code
        const char* fragmentShaderSourceCode=
        "#version 300 es " \
        "\n" \
        "precision highp float;" \
        "precision highp int;" \
        "in vec3 out_diffuse_color;" \
        "uniform int u_LKeyIsPressed;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
        "   if(u_LKeyIsPressed==1)" \
            "{" \
                "fragColor=vec4(out_diffuse_color,1.0);" \
            "}" \
            "else" \
            "{" \
                "fragColor=vec4(1.0,1.0,1.0,1.0);"
            "}"\
        "}";
        
        //specifying shader source to fragment shader object
        glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
        
        //compiling shader
        glCompileShader(fragmentShaderObject);
        
        //error checking
        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;
        
        glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in fragment Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf("\nFragement shader compiled successfully");
        }
        
        //Shader Program
        
        shaderProgramObject=glCreateProgram();
        
        //attching the sahder to object
        glAttachShader(shaderProgramObject,vertexShaderObject);
        glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        //pre-linking attrinbuts
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_POSITION,"vPosition");
         glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_NORMAL,"vNormal");
        
        glLinkProgram(shaderProgramObject);
        
        iInfoLogLength=0;
        GLint iShaderLinkStatus=0;
        szInfoLog=NULL;
        
        glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
        if(iShaderLinkStatus==GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in Linking Shader =%s....",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    // [NSApp terminate];
                }
            }
        }
        else
        {
            printf( "\n Linking of shader successfull");
        }
        
        //post linking
        mvUniform = glGetUniformLocation(shaderProgramObject, "u_mv_matrix");
        projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
        kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
        LKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyIsPressed");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
        
        const GLfloat cubeVertices[] = {
            //front
            1.0f,1.0f,1.0f,
            -1.0f,1.0f,1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            //right
            1.0f,1.0f,-1.0f,
            1.0f,1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            1.0f,-1.0f,-1.0f,
            
            //back
            1.0f,1.0f,-1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,-1.0f,
            
            //left
            -1.0f,1.0f,1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,
            
            //top
            1.0f,1.0f,-1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
            //bottom
            1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f
        };
        
        const GLfloat cubeNormal[] = {
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            0.0f,0.0f,1.0f,
            
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            0.0f,0.0f,-1.0f,
            
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            -1.0f,0.0f,0.0f,
            
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
            
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
            0.0f,-1.0f,0.0f,
        };
        
        
        
        
        //rectangle
        //recording start
        
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        
        //posiiotn data
        glGenBuffers(1, &vbo_position_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices),cubeVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //normal data
        glGenBuffers(1, &vbo_normal_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormal), cubeNormal, GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //recording stop
        glBindVertexArray(0);
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    vmath::mat4 modelViewMatrix;
    
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 scaleMatrix;
    
    
    glUseProgram(shaderProgramObject);
    //square
    modelViewMatrix=vmath::mat4::identity();
    translationMatrix=vmath::mat4::identity();
    rotationMatrix=vmath::mat4::identity();
    scaleMatrix=vmath::mat4::identity();
    
    translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    scaleMatrix = vmath::scale(0.75f,0.75f,0.75f);
    
    
    rotationMatrix=vmath::rotate(angleCube, angleCube, angleCube);
    
    
    //do matrix multipliacation
    modelViewMatrix = modelViewMatrix * translationMatrix;
    modelViewMatrix = modelViewMatrix * scaleMatrix;
    //modelViewMatrix = modelViewMatrix * rotationMatrixX * rotationMatrixY* rotationMatrixZ;
    modelViewMatrix = modelViewMatrix * rotationMatrix;
    
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvUniform, 1, GL_FALSE, modelViewMatrix);
    
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    if (bLight)
    {
        glUniform1i(LKeyIsPressedUniform,1);
        glUniform3f(ldUniform,0.0f,1.0f,1.0f);        //glLightfv()
        glUniform3f(kdUniform,0.5f,0.5f,0.5f);    //glMaterialfv();
        glUniform4f(lightPositionUniform,0.0f,0.0f,2.0f,1.0f); //glLightfv() for position
    }
    else
    {
        glUniform1i(LKeyIsPressedUniform,0);
    }
    
    glBindVertexArray(vao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);

    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    if(bRotation)
    {
        [self update];
    }
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) update
{
    angleCube=angleCube+0.5f;
    if(angleCube>360.0f)
    {
        angleCube=0.0f;
    }

}
-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
    if (bLight == false)
    {
        bLight = true;
       
    }
    else
    {
        bLight = false;

    }
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    if (bRotation == false)
    {
        bRotation = true;
    }
    else
    {
        bRotation = false;
    }

    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


@end
