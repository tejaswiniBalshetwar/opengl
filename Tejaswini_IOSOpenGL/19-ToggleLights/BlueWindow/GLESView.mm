

//
//  GLESView.m
//  02-BlueWindow
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"
#import "Sphere.h"


enum
{
    THB_ATTRIBUTE_POSITION=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};
using namespace vmath;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;

float light_ambient[] = {0.0f,0.0f,0.0f,0.0f};
float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
float lightPosition[] = {100.0f,100.0f,100.0f,1.0f};

float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 50.0f;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShininessUniform;

GLuint gShaderProgramObjectVertex ;
GLuint gShaderProgramObjectFragment ;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;

//vertex lighting
GLuint modelUniformVertex;
GLuint viewUniformVertex;
GLuint projectionUniformVertex;
GLuint LKeyIsPressedUniformVertex;
GLuint laUniformVertex;
GLuint ldUniformVertex;
GLuint lsUniformVertex;
GLuint kaUniformVertex;
GLuint kdUniformVertex;
GLuint ksUniformVertex;
GLuint lightPositionUniformVertex;
GLuint materialShininessUniformVertex;
GLuint vertexShaderObject;
GLuint fragmentShaderObject;
GLuint shaderProgramObject;

mat4 perspectiveProjectionMatrix;
vmath::mat4 modelMatrix;
vmath::mat4 viewMatrix;

int LKeyIsPressed;
bool gbIsVertexShader = true;
bool gbIsFragmentShader = false;

void CreateProgramForPerFragment();
void CreateProgramForPerVertex();

@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    bool bLight;
    
    
}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        CreateProgramForPerFragment();
        CreateProgramForPerVertex();

        getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
        gNumVertices = getNumberOfSphereVertices();
        gNumElements = getNumberOfSphereElements();
        
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
        
        //posiiotn data
        glGenBuffers(1, &vbo_position_sphere);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //normal data
        glGenBuffers(1, &vbo_normal_sphere);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1,&vbo_element_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
        
        //recording stop
        glBindVertexArray(0);
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    vmath::mat4 modelViewMatrix;
    
    vmath::mat4 translationMatrix;
    
    //square
    modelMatrix = vmath::mat4::identity();
     viewMatrix = vmath::mat4::identity();
    //modelViewProjectionMatrix = mat4::identity();
    translationMatrix = vmath::mat4::identity();
    
    //do necessary translation
    
    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    //do matrix multipliacation
    modelMatrix = modelMatrix * translationMatrix;
    
    
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    if (gbIsVertexShader == false)
    {
        glUseProgram(gShaderProgramObjectFragment);
        glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
        
        glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
        
        if (bLight)
        {
            glUniform1i(LKeyIsPressedUniform, 1);
            glUniform3fv(laUniform, 1, light_ambient);        //glLightfv()
            glUniform3fv(ldUniform, 1, light_diffuse);        //glLightfv()
            glUniform3fv(lsUniform, 1, light_specular);        //glLightfv()
            glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position
            
            glUniform3fv(kaUniform, 1, material_ambient);    //glMaterialfv();
            glUniform3fv(kdUniform, 1, material_diffuse);    //glMaterialfv();
            glUniform3fv(ksUniform, 1, material_specular);    //glMaterialfv();
            glUniform1f(materialShininessUniform, material_shininess);    //glMaterialfv();
        }
        else
        {
            glUniform1i(LKeyIsPressedUniform, 0);
        }
    }
    else
    {
        glUseProgram(gShaderProgramObjectVertex);
        
        glUniformMatrix4fv(modelUniformVertex, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniformVertex, 1, GL_FALSE, viewMatrix);
        
        glUniformMatrix4fv(projectionUniformVertex, 1, GL_FALSE, perspectiveProjectionMatrix);
        
        if (bLight)
        {
            glUniform1i(LKeyIsPressedUniformVertex, 1);
            glUniform3fv(laUniformVertex, 1, light_ambient);        //glLightfv()
            glUniform3fv(ldUniformVertex, 1, light_diffuse);        //glLightfv()
            glUniform3fv(lsUniformVertex, 1, light_specular);        //glLightfv()
            glUniform4fv(lightPositionUniformVertex, 1, lightPosition); //glLightfv() for position
            
            glUniform3fv(kaUniformVertex, 1, material_ambient);    //glMaterialfv();
            glUniform3fv(kdUniformVertex, 1, material_diffuse);    //glMaterialfv();
            glUniform3fv(ksUniformVertex, 1, material_specular);    //glMaterialfv();
            glUniform1f(materialShininessUniformVertex, material_shininess);    //glMaterialfv();
        }
        else
        {
            glUniform1i(LKeyIsPressedUniformVertex, 0);
        }
    }
    
    
    
    glBindVertexArray(vao_sphere);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
    glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
    //glDrawArrays(GL_TRIANGLE_STRIP,0,gNumElements);
    glBindVertexArray(0);
    
    
    glUseProgram(0);

    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    
     [self update];
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) update
{

}
-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
    if (bLight == false)
    {
        bLight = true;
       
    }
    else
    {
        bLight = false;

    }
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    if (gbIsVertexShader == false)
    {
        gbIsVertexShader = true;
        
    }
    else
    {
        gbIsVertexShader = false;
        
    }
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


@end
void CreateProgramForPerFragment()
{
    
    GLuint vertexShaderObjectFragment = 0;
    GLuint fragmentShaderObjectFragment = 0;
    vertexShaderObjectFragment = glCreateShader(GL_VERTEX_SHADER);
    
    //writing the source code for the shader
    const GLchar* vertexShaderSourceCodeFragment =
    "#version 300 es " \
    "\n" \
    "in vec4 vPosition;                                \n" \
    "in vec3 vNormal;                                \n" \
    "uniform mat4 u_model_matrix;                    \n" \
    "uniform mat4 u_view_matrix;                    \n" \
    "uniform mat4 u_projection_matrix;                \n" \
    "uniform int u_LKeyIsPressed;                    \n" \
    "uniform vec4 u_light_position;                    \n" \
    "out vec3 out_light_direction;                    \n" \
    "out vec3 out_t_normal;                            \n" \
    "out vec3 out_viewer_vector;                    \n" \
    "void main(void)                                \n" \
    "{                                                \n" \
    "    if(u_LKeyIsPressed==1)                        \n" \
    "    {                                            \n" \
    "        vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;            \n" \
    "        out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;                    \n" \
    "        out_light_direction=vec3(u_light_position - eye_coordinates);                \n" \
    "        out_viewer_vector =vec3(-eye_coordinates);                                    \n" \
    "    }                                                                                \n" \
    "    gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;    \n"  \
    "}";
    
    //specify source code to obeject
    glShaderSource(vertexShaderObjectFragment, 1, (const GLchar**)&vertexShaderSourceCodeFragment, NULL);
    
    
    //compile shader
    glCompileShader(vertexShaderObjectFragment);
    
    //error checking
    int iCompileStatus = 0;
    int iInfoLogLength = 0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
    if (iCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
            if (szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectFragment, iInfoLogLength, &written, szInfoLog);
                
                printf( "\nError in vertex Shader= %s\n", szInfoLog);
                free(szInfoLog);
                
                exit(0);
            }
        }
    }
    
    
    //fragment shader object
    fragmentShaderObjectFragment = glCreateShader(GL_FRAGMENT_SHADER);
    
    //source code for fragment shader
    const GLchar* fragmentShaderSourceCodeFragment =
    "#version 300 es " \
    "\n" \
    "precision highp float;" \
    "precision highp int;" \
    "uniform vec3 u_la;                                \n" \
    "uniform vec3 u_ld;                                \n" \
    "uniform vec3 u_ls;                                \n" \
    "uniform int u_LKeyIsPressed;                    \n" \
    "uniform vec3 u_ka;                                \n" \
    "uniform vec3 u_kd;                                \n" \
    "uniform vec3 u_ks;                                \n" \
    "uniform float u_material_shininess;            \n" \
    "in vec3 out_light_direction;                    \n" \
    "in vec3 out_t_normal;                            \n" \
    "in vec3 out_viewer_vector;                        \n" \
    "out vec4 fragColor;                            \n" \
    "void main(void)                                \n" \
    "{                                                \n" \
    "    vec3 phong_ads_light;                        \n" \
    "    if(u_LKeyIsPressed==1)                        \n" \
    "    {                                            \n"    \
    "        vec3 normalized_light_direction=normalize(out_light_direction);                                                    \n"    \
    "        vec3 normalized_t_norm=normalize(out_t_normal);                                                                    \n"    \
    "        float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);                                        \n" \
    "        vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);                                    \n" \
    "        vec3 normalized_viewer_vector=normalize(out_viewer_vector);                                                            \n" \
    "        vec3 ambient= u_la * u_ka;                                                                                        \n " \
    "        vec3 diffuse=u_ld * u_kd * t_dot_ld;                                                                            \n" \
    "        vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);            \n" \
    "        phong_ads_light=ambient + diffuse + specular;                                                                \n" \
    "    }                                                                                                                    \n" \
    "    else                                                                                                                \n" \
    "    {                                                                                                                    \n" \
    "            phong_ads_light=vec3(1.0,1.0,1.0);                                                                        \n" \
    "    }                                                                                                                    \n" \
    "    fragColor=vec4(phong_ads_light,1.0);                                                                            \n" \
    "}                                                                                                                        \n";
    
    //specify source code to object
    glShaderSource(fragmentShaderObjectFragment, 1, (const GLchar**)&fragmentShaderSourceCodeFragment, NULL);
    
    //compile shader
    glCompileShader(fragmentShaderObjectFragment);
    
    //error checking
    iCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetShaderiv(fragmentShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
    if (iCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
            if (szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectFragment, iInfoLogLength, &written, szInfoLog);
                
                printf( "\nError in fragment shader = %s\n", szInfoLog);
                
                free(szInfoLog);
                
                exit(0);
            }
        }
    }
    
    //create Shader Program object
    gShaderProgramObjectFragment = glCreateProgram();
    
    //attach Shaders to program object
    glAttachShader(gShaderProgramObjectFragment, vertexShaderObjectFragment);
    glAttachShader(gShaderProgramObjectFragment, fragmentShaderObjectFragment);
    
    //prelinking the atttributes
    glBindAttribLocation(gShaderProgramObjectFragment, THB_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObjectFragment, THB_ATTRIBUTE_NORMAL, "vNormal");
    
    //link the code to make it gpu specific shader processor understandble
    glLinkProgram(gShaderProgramObjectFragment);
    
    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetProgramiv(gShaderProgramObjectFragment, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
            if (szInfoLog)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectFragment, iInfoLogLength, &written, szInfoLog);
                
                printf( "Error while Linking the program: %s", szInfoLog);
                
                exit(0);
                
            }
        }
    }
    
    //post linking retriving uniform location
    modelUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_model_matrix");
    viewUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_view_matrix");
    projectionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_projection_matrix");
    laUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_la");
    ldUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ld");
    lsUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ls");
    
    kaUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ka");
    kdUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_kd");
    ksUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ks");
    
    LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_LKeyIsPressed");
    
    lightPositionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_light_position");
    materialShininessUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_material_shininess");
    
}
void CreateProgramForPerVertex()
{
    
    
    GLuint vertexShaderObject = 0;
    GLuint fragmentShaderObject = 0;
    
    //create source code object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //writing the shader source code
    const GLchar *vertexShaderSourceCode =
    "    #version 300 es                                            \n" \
    "    \n                                                            \n" \
    "    in vec4 vPosition1;                                            \n" \
    "    in vec3 vNormal1;                                            \n" \
    "    uniform mat4 u_model_matrix1;                                \n" \
    "    uniform mat4 u_view_matrix1;                                \n" \
    "    uniform mat4 u_projection_matrix1;                            \n" \
    "    uniform vec3 u_la1;                                            \n" \
    "    uniform vec3 u_ld1;                                            \n" \
    "    uniform vec3 u_ls1;                                            \n" \
    "    uniform vec3 u_ka1;                                            \n" \
    "    uniform vec3 u_kd1;                                            \n" \
    "    uniform vec3 u_ks1;                                            \n" \
    "    uniform int u_LKeyIsPressed1;                                                            \n" \
    "    uniform float u_materialShininess1;                                                        \n" \
    "    uniform vec4 u_light_position1;                                                            \n" \
    "    out vec3 out_phong_ads_light1 ;                                                            \n" \
    "    void main(void)                                                                            \n" \
    "    {                                                                                        \n" \
    "        if(u_LKeyIsPressed1==1)                                                                \n" \
    "        {                                                                                    \n" \
    "            vec4 eye_coordinates=u_view_matrix1 * u_model_matrix1 * vPosition1;                                                            \n" \
    "            vec3 t_normal=normalize(mat3(u_view_matrix1*u_model_matrix1) * vNormal1) ;                                                    \n" \
    "            vec3 light_direction=normalize(vec3(u_light_position1 - eye_coordinates));                                                \n" \
    "            float t_dot_ld=max(dot(light_direction,t_normal),0.0);                                                                    \n" \
    "            vec3 reflection_vector=reflect(-light_direction,t_normal);                                                                \n" \
    "            vec3 viewer_vector=normalize(vec3(-eye_coordinates));                                                                    \n" \
    "            vec3 ambient= u_la1 * u_ka1;                                                                                                \n" \
    "            vec3 diffuse= u_ld1 * u_kd1 * t_dot_ld;                                                                                    \n" \
    "            vec3 specular= u_ls1 * u_ks1 * pow(max(dot(reflection_vector,viewer_vector),0.0),u_materialShininess1);                    \n" \
    "            out_phong_ads_light1= ambient + diffuse + specular;                                                                            \n" \
    "        }                                                                                                                            \n" \
    "        else                                                                                                                        \n" \
    "        {                                                                                                                            \n" \
    "            out_phong_ads_light1=vec3(1.0,1.0,1.0);                                                                                    \n" \
    "                                                                                                                                    \n" \
    "        }                                                                                                                            \n" \
    "        gl_Position= u_projection_matrix1 * u_view_matrix1 * u_model_matrix1 * vPosition1;                                                                                                                            \n" \
    "    }                                                                                                                                \n" \
    ;
    
    //specify source code to object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    //compile shader
    glCompileShader(vertexShaderObject);
    
    //error checking
    
    int iCompileStatus = 0;
    int iInfoLogLength = 0;
    char * szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
    if (iCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
            if (szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf( "\n Error while compiling Vertex Shader: %s\n", szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                exit(0);
            }
        }
        
    }
    
    //fragment shader object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    //writting shader source code for the fragment shader
    const GLchar* fragmentShaderSourceCode =
    "    #version 300 es                                                                         " \
    "    \n                                                                                        " \
    "    precision highp float;" \
     "    precision highp int;" \
    "    in vec3 out_phong_ads_light1;                                                            \n" \
    "    out vec4 fragColor;                                                                        \n" \
    "    void main(void)                                                                            \n" \
    "    {                                                                                        \n" \
    "        fragColor=vec4(out_phong_ads_light1,1.0);                                            \n" \
    "    }                                                                                        \n" \
    ;
    
    //specify shader code to shader object
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    //compile shader
    glCompileShader(fragmentShaderObject);
    
    //error cheking for compilation
    iCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
    if (iCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
            if (szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                printf("\nError in Fragment Shader:=%s\n",szInfoLog);
                free(szInfoLog);
                szInfoLog = NULL;
                
                exit(0);
            }
        }
    }
    
    //create shader program object
    gShaderProgramObjectVertex =glCreateProgram();
    
    //cattach shader to program
    glAttachShader(gShaderProgramObjectVertex, vertexShaderObject);
    glAttachShader(gShaderProgramObjectVertex,fragmentShaderObject);
    
    //prelinkg for attributes
    glBindAttribLocation(gShaderProgramObjectVertex,THB_ATTRIBUTE_POSITION,"vPosition1");
    glBindAttribLocation(gShaderProgramObjectVertex, THB_ATTRIBUTE_NORMAL, "vNormal1");
    
    //link the program
    glLinkProgram(gShaderProgramObjectVertex);
    
    //errror checking for linking the program
    
    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetProgramiv(gShaderProgramObjectVertex,GL_LINK_STATUS,&iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectVertex,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            GLsizei written;
            glGetProgramInfoLog(gShaderProgramObjectVertex,iInfoLogLength,&written,szInfoLog);
            printf("\nError while linking the Program:=%s\n",szInfoLog);
            
            free(szInfoLog);
            exit(0);
        }
    }
    
    //post linking binding uniform
    modelUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_model_matrix1");
    viewUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_view_matrix1");
    projectionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_projection_matrix1");
    
    laUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_la1");
    ldUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ld1");
    lsUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ls1");
    
    kaUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ka1");
    kdUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_kd1");
    ksUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ks1");
    
    LKeyIsPressedUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_LKeyIsPressed1");
    
    lightPositionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_light_position1");
    materialShininessUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_materialShininess1");
    
    
}

