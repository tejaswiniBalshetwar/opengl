	//
//  GLESView.m
//  StaticIndia
//
//  Created by Chetan Balshetwar on 09/04/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"



enum
{
    THB_ATTRIBUTE_POSITION=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};
using namespace vmath;

FILE *gpFile=NULL;

GLuint vao_circle;
GLuint vbo_circle_position;
GLuint vbo_circle_color;

GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;

@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    
    
    //opengl
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;



    vmath::mat4 perspectiveProjectionMatrix;

    GLuint mvpUniform;
    GLuint colorUniform;
    GLfloat verticesForOuterCircle[2000];
    GLfloat colorForCircle[2000];

    GLfloat verticesForInnerCircle[2000];

    
}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        //Opengl GL shader code
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const char* vertexShaderSourceCode=
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "out vec4 out_color;" \
        "uniform mat4 u_mvp_uniform;" \
        "void main(void)" \
        "{" \
            "gl_Position=u_mvp_uniform*vPosition;" \
            "gl_PointSize=5.0;" \
            "out_color=vColor;" \
        "}";
        
        
        //specify the shader to vertex shader object
        glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength=0;
        GLint iShaderCompileStatus=0;
        char* szInfoLog=NULL;
        
        glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Vertex Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf( "\nVertex shader compiled successfully");
        }
        
        
        //fragment shader
        
        //creating fragment shader
        fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
        
        //writting shader source code
        const char* fragmentShaderSourceCode=
        "#version 300 es " \
        "\n" \
        "precision highp float;" \
        "in vec4 out_color;" \
        "out vec4 fragColor;" \
        "void main(void)" \
        "{" \
            "fragColor=out_color;" \
        "}" ;
        
        //specifying shader source to fragment shader object
        glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
        
        //compiling shader
        glCompileShader(fragmentShaderObject);
        
        //error checking
        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;
        
        glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in fragment Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf("\nFragement shader compiled successfully");
        }
        
        //Shader Program
        
        shaderProgramObject=glCreateProgram();
        
       //attching the sahder to object
        glAttachShader(shaderProgramObject,vertexShaderObject);
        glAttachShader(shaderProgramObject,fragmentShaderObject);

        //pre-linking attrinbuts
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_POSITION,"vPosition");
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_COLOR,"vColor");

        glLinkProgram(shaderProgramObject);

        iInfoLogLength=0;
        GLint iShaderLinkStatus=0;
        szInfoLog=NULL;

        glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
        if(iShaderLinkStatus==GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Linking Shader ....");
                    free(szInfoLog);
                    [self release];
                   
                }
            }
        }
        else
        {
            printf("\n Linking of shader successfull");
        }

        //post linking
        mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_uniform");

        //vertices for triangle

         //actual code for geometry
        
        //crating VAO for recording
        //1.line
        glGenVertexArrays(1,&vao_line);
        glBindVertexArray(vao_line);
            glGenBuffers(1,&vbo_line_position);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
                glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            glGenBuffers(1,&vbo_line_color);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
                glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
            glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);


        
        //half circle
        
        GLfloat XCenterOfCircle = 0.0f;
        GLfloat YCenterOfCircle = 0.0f;
        GLfloat radius = 1.0f;

        memset(verticesForInnerCircle,0,sizeof(verticesForInnerCircle));
        memset(colorForCircle,0,sizeof(colorForCircle));
        calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);

        //circle
        glGenVertexArrays(1, &vao_circle);
        glBindVertexArray(vao_circle);
            glGenBuffers(1, &vbo_circle_position);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(verticesForInnerCircle), verticesForInnerCircle, GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glGenBuffers(1, &vbo_circle_color);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
                glBufferData(GL_ARRAY_BUFFER, sizeof(colorForCircle), colorForCircle, GL_DYNAMIC_DRAW);
                glVertexAttribPointer(THB_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(THB_ATTRIBUTE_COLOR);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);




        //geometry end here
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix= mat4::identity();
        
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
     mat4 translationMatrix;
       
       mat4 modelViewMatrix;
       mat4 modelViewProjectionMatrix;
       mat4 rotationMatrix;
       
       glUseProgram(shaderProgramObject);

           modelViewMatrix=mat4::identity();
           modelViewProjectionMatrix=mat4::identity();
           translationMatrix=mat4::identity();

           translationMatrix= translate(-2.50f, 0.0f, -6.0f);
           modelViewMatrix=modelViewMatrix*translationMatrix;

           modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
           glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

           glLineWidth(5.0f);
           GLfloat lineVerticesI[]={
               0.0f,1.0f,0.0f,
               0.0f,-1.0f,0.0f
           };
           GLfloat colorForLineI[] = {
               1.0f,0.6f,0.2f,
               0.0706f,0.3831f,0.02745f
           };
           drawLine(lineVerticesI,sizeof(lineVerticesI),colorForLineI,sizeof(colorForLineI),2);


           //N
           modelViewMatrix = mat4::identity();
           translationMatrix = mat4::identity();
           modelViewProjectionMatrix = mat4::identity();

           translationMatrix = translate(-2.0f,0.0f,-6.0f);
           modelViewMatrix = modelViewMatrix * translationMatrix;
           modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

           glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);



           GLfloat lineVerticesN[] = {
               1.0f,1.0f,0.0f,
               1.0f,-1.0f,0.0f,
               0.0f,1.0f,0.0f,
               0.0f,-1.0f,0.0f

           };
           GLfloat colorForLineN[] = {
               1.0f,0.6f,0.2f,
               0.0706f,0.3831f,0.02745f,
               1.0f,0.6f,0.2f,
               0.0706f,0.3831f,0.02745f,
               1.0f,0.6f,0.2f,
               0.0706f,0.3831f,0.02745f
           };
           drawLine(lineVerticesN, sizeof(lineVerticesN), colorForLineN, sizeof(colorForLineN),4);

           //D

           modelViewMatrix = mat4::identity();
           modelViewProjectionMatrix = mat4::identity();
           translationMatrix = mat4::identity();

           translationMatrix = translate(-0.50f, 0.0f, -6.0f);
           modelViewMatrix = modelViewMatrix * translationMatrix;

           modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
           glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

           
           drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

           //circle
           //loadIdentity for circle
           translationMatrix = mat4::identity();
           rotationMatrix = mat4::identity();
           modelViewMatrix = mat4::identity();
           modelViewProjectionMatrix = mat4::identity();

           translationMatrix = translate(-0.50f, 0.0f,-6.0f);
           rotationMatrix = rotate(-90.0f,0.0f,0.0f,1.0f);
           
           modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
           modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
           glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

           //for circle Translations

           drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

           //for I

           modelViewMatrix = mat4::identity();
           modelViewProjectionMatrix = mat4::identity();
           translationMatrix = mat4::identity();

           translationMatrix = translate(1.0f, 0.0f, -6.0f);
           modelViewMatrix = modelViewMatrix * translationMatrix;

           modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
           glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

       
           drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

           //A
           translationMatrix = mat4::identity();
           modelViewMatrix = mat4::identity();
           modelViewProjectionMatrix = mat4::identity();

           translationMatrix = translate(2.0f,0.0f,-6.0f);
           modelViewMatrix = modelViewMatrix * translationMatrix;
           modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
           glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

           GLfloat verticesForA[] = {
               -0.5f,-1.0f,0.0f,
               0.0f,1.0f,0.0f,
               0.5f,-1.0f,0.0f
           };

           GLfloat colorForA[] = {
               0.0706f,0.3831f,0.02745f,
               1.0f,0.6f,0.2f,
               0.0706f,0.3831f,0.02745f,
               1.0f,0.6f,0.2f,

           };

           drawLine(verticesForA,sizeof(verticesForA),colorForA,sizeof(colorForA),3);

           GLfloat triColorVertex[6];

           GLfloat X1ForLine = (verticesForA[0] + verticesForA[3] )/ 2;
           GLfloat Y1ForLine = (verticesForA[1] + verticesForA[4]) / 2;

           GLfloat X2ForLine = (verticesForA[3] + verticesForA[6] )/ 2;
           GLfloat Y2ForLine = (verticesForA[1] + verticesForA[4] )/ 2;
           
           GLfloat colorForTricolor[6];

           triColorVertex[0] = X1ForLine;
           triColorVertex[1] = Y1ForLine;
           triColorVertex[2] = 0.0f;

           triColorVertex[3] = X2ForLine;
           triColorVertex[4] = Y2ForLine;
           triColorVertex[5] = 0.0f;

           colorForTricolor[0] = 1.0f;
           colorForTricolor[1] = 0.6f;
           colorForTricolor[2] = 0.2f;

           colorForTricolor[3] = 1.0f;
           colorForTricolor[4] = 0.6f;
           colorForTricolor[5] = 0.2f;


           drawLine(triColorVertex,sizeof(triColorVertex), colorForTricolor,sizeof(colorForTricolor),2);

           triColorVertex[0] = X1ForLine-0.01f;
           triColorVertex[1] = Y1ForLine-0.04f;
           triColorVertex[2] = 0.0f;

           triColorVertex[3] = X2ForLine;
           triColorVertex[4] = Y2ForLine-0.04f;
           triColorVertex[5] = 0.0f;

           colorForTricolor[0] = 1.0f;
           colorForTricolor[1] = 1.0f;
           colorForTricolor[2] = 1.0f;

           colorForTricolor[3] = 1.0f;
           colorForTricolor[4] = 1.0f;
           colorForTricolor[5] = 1.0f;


           drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

           triColorVertex[0] = X1ForLine;
           triColorVertex[1] = Y1ForLine-0.07f;
           triColorVertex[2] = 0.0f;

           triColorVertex[3] = X2ForLine;
           triColorVertex[4] = Y2ForLine-0.07f;
           triColorVertex[5] = 0.0f;

           colorForTricolor[0] = 0.0706f;
           colorForTricolor[1] = 0.3831f;
           colorForTricolor[2] = 0.02745f;

           colorForTricolor[3] = 0.0706f;
           colorForTricolor[4] = 0.3831f;
           colorForTricolor[5] = 0.02745f;


           drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);
               
       glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
    
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
    GLfloat x = 0.0f, y = 0.0f;
    GLfloat RColor = 0.0706f;
    GLfloat GColor = 0.3831f;
    GLfloat BColor = 0.02745f;
    //vertices[2000];
    for (int i = 0; i < 2000-3; i=i+3)
    {
        GLfloat angle =(GLfloat)(M_PI*i / 1500);
        x =(GLfloat) (XPointOfCenter + radius * cos(angle));
        y =(GLfloat) (YPointOfCenter + radius * sin(angle));

        vertices[i] = x;
        vertices[i + 1] = y;
        vertices[i + 2] = 0.0f;

        if (RColor <= 1.0)
        {
            RColor += 0.002f;

        }
        if (GColor <= 0.6)
        {
            GColor += 0.002f;
        }
        if (BColor <= 0.2)
        {
            BColor += 0.002f;
        }
        color[i] = RColor;
        color[i + 1] = GColor;
        color[i + 2] = BColor;
    }

}

void drawCircle(GLfloat * vertices,int verticesSize,GLfloat *color,int colorSize)
{
    glBindVertexArray(vao_circle);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
            glBufferData(GL_ARRAY_BUFFER,verticesSize,(vertices),GL_DYNAMIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER,0);
    
        glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
            glBufferData(GL_ARRAY_BUFFER, colorSize,color, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        glDrawArrays(GL_POINTS,0,500);
    glBindVertexArray(0);
}



void drawLine(GLfloat *vertices ,int sizeOfVertices, GLfloat * color, int sizeofColor, int numberOfPoints)
{
    glBindVertexArray(vao_line);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
        glBufferData(GL_ARRAY_BUFFER,sizeOfVertices,vertices,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
        glBufferData(GL_ARRAY_BUFFER,sizeofColor,color,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
    glBindVertexArray(0);
}

@end




