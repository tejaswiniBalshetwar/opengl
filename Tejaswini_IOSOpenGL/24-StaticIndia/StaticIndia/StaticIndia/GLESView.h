//
//  GLESView.h
//  StaticIndia
//
//  Created by Chetan Balshetwar on 09/04/20.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end
