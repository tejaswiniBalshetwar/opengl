//
//  AppDelegate.h
//  BlueWindow
//
//  Created by Saurabh Bhalgat on 27/01/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

