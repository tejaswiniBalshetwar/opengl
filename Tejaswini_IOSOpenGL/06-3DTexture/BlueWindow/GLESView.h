//
//  GLESView.h
//  02-BlueWindow
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end
