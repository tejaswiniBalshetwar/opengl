

//
//  GLESView.m
//  02-BlueWindow
//
//  Created by Saurabh Bhalgat on 24/01/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"



enum
{
    THB_ATTRIBUTE_POSITION=0,
    THB_ATTRIBUTE_COLOR,
    THB_ATTRIBUTE_NORMAL,
    THB_ATTRIBUTE_TEXCOORD0,
};
using namespace vmath;
@implementation GLESView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

{
    EAGLContext *eaglContext;
    GLuint defaultFrameBuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    mat4 perspectiveProjectionMatrix;
    
    GLuint vao_pyramid;
    GLuint vao_cube;

    GLuint vbo_position_pyramid ;
    GLuint vbo_texture_pyramid ;
    GLuint vbo_position_cube ;
    GLuint vbo_texture_cube ;
    
    
    GLuint texture_stone;
    GLuint texture_kundali;
    
    GLuint mvpUniform;
    GLuint samplerUniform;
    
    GLfloat anglePyramid;
    GLfloat angleCube;
}
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("\nError in getting context");
            [self release];
            return nil;
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenBuffers(1,&defaultFrameBuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nError in frame bufffer creation=");
            return nil;
        }
        
        printf("\nRenderer: %s | GL Version: %s |GLES Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        //Opengl GL shader code
        vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const char* vertexShaderSourceCode=
        "#version 300 es"\
        "\n" \
        "in vec4 vPosition;                                                     \n" \
        "in vec2 vTexCoord;                                                         \n" \
        "out vec2 out_texCoord;                                                    \n" \
        "uniform mat4 u_mvp_matrix;                                             \n" \
        " void main()                                                            \n" \
        "{                                                                        \n"    \
        "gl_Position=u_mvp_matrix* vPosition;                                \n" \
        "out_texCoord=vTexCoord;                                                    \n" \
        "}                                                                        \n" ;


        
        //specify the shader to vertex shader object
        glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength=0;
        GLint iShaderCompileStatus=0;
        char* szInfoLog=NULL;
        
        glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf("\nError in Vertex Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf( "\nVertex shader compiled successfully");
        }
        
        
        //fragment shader
        
        //creating fragment shader
        fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
        
        //writting shader source code
        const char* fragmentShaderSourceCode=
        "#version 300 es " \
        "\n" \
        "precision highp float;" \
        "out vec4 fragColor;                                    \n" \
        "in vec2 out_texCoord;                                    \n" \
        "uniform sampler2D    u_sampler;                            \n" \
        "void main()                                            \n" \
        "{                                                        \n" \
        "fragColor=texture(u_sampler,out_texCoord);            \n" \
        "}                                                        \n";

        
        //specifying shader source to fragment shader object
        glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
        
        //compiling shader
        glCompileShader(fragmentShaderObject);
        
        //error checking
        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;
        
        glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
        if(iShaderCompileStatus==GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in fragment Shader ....");
                    free(szInfoLog);
                    [self release];
                    
                }
            }
        }
        else
        {
            printf("\nFragement shader compiled successfully");
        }
        
        //Shader Program
        
        shaderProgramObject=glCreateProgram();
        
        //attching the sahder to object
        glAttachShader(shaderProgramObject,vertexShaderObject);
        glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        //pre-linking attrinbuts
        glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_POSITION,"vPosition");
         glBindAttribLocation(shaderProgramObject,THB_ATTRIBUTE_TEXCOORD0,"vTexCoord");
        
        glLinkProgram(shaderProgramObject);
        
        iInfoLogLength=0;
        GLint iShaderLinkStatus=0;
        szInfoLog=NULL;
        
        glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
        if(iShaderLinkStatus==GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog=(char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
                    printf( "\nError in Linking Shader ....");
                    free(szInfoLog);
                    [self release];
                    // [NSApp terminate];
                }
            }
        }
        else
        {
            printf( "\n Linking of shader successfull");
        }
        
        //post linking
        mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
        samplerUniform=glGetUniformLocation(shaderProgramObject,"u_sampler");
        //vertices for triangle
        const GLfloat cubeVertices[] = {
            1.0f,1.0f,1.0f,
            -1.0f,1.0f,1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            
            1.0f,1.0f,-1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,-1.0f,
            
            -1.0f,1.0f,1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,
            
            1.0f,1.0f,-1.0f,
            1.0f,1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            1.0f,-1.0f,-1.0f,
            
            1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            
            1.0f,1.0f,-1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,
        };
        
        const GLfloat texCoordsCube[] = {
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f
            
        };
        
        
        const GLfloat pyramidVertices[] = {
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            
            0.0f,1.0f,0.0f,
            1.0f,-1.0f,1.0f,
            1.0f,-1.0f,-1.0f,
            
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,
            
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,-1.0f,
            
        };
        
        const GLfloat texCoordsPyramid[] = {
            0.5f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            0.5f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            0.5f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            0.5f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
        };
        
        glGenVertexArrays(1, &vao_pyramid);
        glBindVertexArray(vao_pyramid);
        glGenBuffers(1, &vbo_position_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_position_pyramid);
        glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidVertices),pyramidVertices,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        glGenBuffers(1,&vbo_texture_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_pyramid);
        glBufferData(GL_ARRAY_BUFFER,sizeof(texCoordsPyramid),texCoordsPyramid,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_TEXCOORD0);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        glBindVertexArray(0);
        
        
        
        //cube
        //create the vao recoreder for display
        glGenVertexArrays(1,&vao_cube);
        glBindVertexArray(vao_cube);
        
        //create vbo for actual transfering the data
        glGenBuffers(1,&vbo_position_cube);
        //binding for data to the gpu at target
        glBindBuffer(GL_ARRAY_BUFFER,vbo_position_cube);
        //sending the data and its size to gpu
        glBufferData(GL_ARRAY_BUFFER,sizeof(cubeVertices),cubeVertices,GL_STATIC_DRAW);
        //sending the reading info to gpu and accessing info through cpu end
        glVertexAttribPointer(THB_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
        //enableing pur access point for data at cpu
        glEnableVertexAttribArray(THB_ATTRIBUTE_POSITION);
        //for bindng again in display convention
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        
        //texture bufferes;
        glGenBuffers(1,&vbo_texture_cube);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_cube);
        
        glBufferData(GL_ARRAY_BUFFER,sizeof(texCoordsCube),texCoordsCube,GL_STATIC_DRAW);
        glVertexAttribPointer(THB_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(THB_ATTRIBUTE_TEXCOORD0);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
        //recording stop
        glBindVertexArray(0);

        
        texture_stone=[self loadTextureFromBMPFile:@"Stone":@"bmp"];
        texture_kundali=[self loadTextureFromBMPFile:@"Kundali":@"bmp"];
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        //gesture recognition
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //Double Tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press
        UILongPressGestureRecognizer *longPressGestureRecognozer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognozer];
        
    }
    
    return self;
}

-(GLuint)loadTextureFromBMPFile:(NSString* )texFileName :(NSString*)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
   
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"\n can't find %@",texFileName);
        return 0;
    }
    
    CGImageRef cgImage=bmpImage.CGImage;
    
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void *pixels=(void*)CFDataGetBytePtr(imageData);
    
    GLuint bmptexture;
    glGenTextures(1,&bmptexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bmptexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmptexture);
}

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 scaleMatrix;
    
    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    translationMatrix=vmath::mat4::identity();
    rotationMatrix=vmath::mat4::identity();
    
    translationMatrix=vmath::translate(-1.50f,0.0f,-6.0f);
    rotationMatrix=vmath::rotate(anglePyramid,0.0f,1.0f,0.0f);
    modelViewMatrix=modelViewMatrix*translationMatrix * rotationMatrix;
    
    modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;
    
   // glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,texture_stone);
    glUniform1i(samplerUniform,0);
    
    glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
    
    glBindVertexArray(vao_pyramid);
    glDrawArrays(GL_TRIANGLES,0,12);
    glBindVertexArray(0);
    
    
    //square
    modelViewMatrix=vmath::mat4::identity();
    modelViewProjectionMatrix=vmath::mat4::identity();
    translationMatrix=vmath::mat4::identity();
    rotationMatrix=vmath::mat4::identity();
    scaleMatrix=vmath::mat4::identity();
    
    translationMatrix=vmath::translate(1.50f,0.0f,-6.0f);
    rotationMatrix=vmath::rotate(angleCube,angleCube,angleCube);
    scaleMatrix=vmath::scale(0.75f,0.75f,0.75f);
    modelViewMatrix=modelViewMatrix*translationMatrix*scaleMatrix* rotationMatrix;
    modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
    
    
   // glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,texture_kundali);
    glUniform1i(samplerUniform,0);
    
    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    glDrawArrays(GL_TRIANGLE_FAN,4,4);
    glDrawArrays(GL_TRIANGLE_FAN,8,4);
    glDrawArrays(GL_TRIANGLE_FAN,12,4);
    glDrawArrays(GL_TRIANGLE_FAN,16,4);
    glDrawArrays(GL_TRIANGLE_FAN,20,4);
    //glDrawArray(GL_TRIANGLE_FAN,24,4);
    glBindVertexArray(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [self update];
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void) update
{
    anglePyramid=anglePyramid+0.5f;
    
    if(anglePyramid>360.0f)
    {
        anglePyramid=0.0f;
    }
    
    angleCube=angleCube+0.5f;
    if(angleCube>360.0f)
    {
        angleCube=0.0f;
    }
}
-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0,0, width, height);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("\nError in framebuffer setup :%x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    GLfloat fWidth=(GLfloat)width;
    GLfloat fHeight=(GLfloat)height;
    perspectiveProjectionMatrix= vmath::perspective(45.0f,GLfloat(fWidth)/GLfloat(fHeight),0.1f,100.0f);
    
    [self drawView:nil];
}

-(void) startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating=YES;
    }
}
-(void) stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        isAnimating=NO;
    }
}
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer*)gr
{
    
    
}

-(void)onDoubleTap:(UITapGestureRecognizer*)gr
{
    
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)gr
{
    
    
}

-(void)onSwipe:(UISwipeGestureRecognizer*)gr
{
    [self release];
    exit(0);
}

- (void)dealloc
{
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        depthRenderBuffer=0;
    }
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
        
    }
    [eaglContext release];
    eaglContext=nil;
    [super dealloc];
}


@end
