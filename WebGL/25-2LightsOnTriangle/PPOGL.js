var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_normal;



var light_ambientRed=[0.0,0.0,0.0];
var light_diffuseRed=[1.0,0.0,0.0];
var light_specularRed=[1.0,0.0,0.0];
var light_positionRed=[3.0,0.0,0.0,1.0];

var light_ambientBlue=[0.0,0.0,0.0];
var light_diffuseBlue=[0.0,0.0,1.0];
var light_specularBlue=[0.0,0.0,1.0];
var light_positionBlue=[-3.0,0.0,0.0,1.0];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var materialShininess=128.0;

var sphere=null;

var modelUniform;
var viewUnifrom;
var projectionUniform;
var LKeyIsPressedUniform;
var laUniformRed,ldUniformRed,lsUniformRed,kaUniform,kdUniform,ksUniform,materialShininessUniform;
var laUniformBlue,ldUniformBlue,lsUniformBlue,kaUniform,kdUniform,ksUniform,materialShininessUniform;


var lightPositionUniformRed;
var lightPositionUniformBlue;

var bRotation=false;
var bLight=false;
var angleCube=0.0;

var LKeyIsPressed=0;

var perspectiveProjectionMatrix;
var modelMatrix;
var viewMatrix;

const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;								\n" +
		"in vec3 vNormal;								\n" +
		"uniform mat4 u_model_matrix;					\n" +
		"uniform mat4 u_view_matrix;					\n" +
		"uniform mat4 u_projection_matrix;				\n" +
		"uniform mediump int u_LKeyIsPressed;					\n" +
		"uniform vec4 u_light_positionForRedLight;					\n" +
		"uniform vec4 u_light_positionForBlueLight;					\n" +
		"out vec3 out_light_directionForRedLight;					\n" +
		"out vec3 out_light_directionForBlueLight;					\n" +
		"out vec3 out_t_normal;							\n" +
		"out vec3 out_viewer_vector;					\n" +
		"void main(void)								\n" +
		"{												\n" +
		"	if(u_LKeyIsPressed==1)						\n" +
		"	{											\n" +
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" +
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" +
		"		out_light_directionForRedLight=vec3(u_light_positionForRedLight - eye_coordinates);				\n" +
		"		out_light_directionForBlueLight=vec3(u_light_positionForBlueLight - eye_coordinates);				\n" +
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" +
		"	}																				\n" +
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  +
		"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n " +
		"precision highp float;" +
		"uniform vec3 u_laRed;								\n" +
		"uniform vec3 u_ldRed;								\n" +
		"uniform vec3 u_lsRed;								\n" +
		"uniform vec3 u_laBlue;								\n" +
		"uniform vec3 u_ldBlue;								\n" +
		"uniform vec3 u_lsBlue;								\n" +	
		"uniform mediump int u_LKeyIsPressed;					\n" +
		"uniform vec3 u_ka;								\n" +
		"uniform vec3 u_kd;								\n" +
		"uniform vec3 u_ks;								\n" +
		"uniform float u_material_shininess;			\n" +
		"in vec3 out_light_directionForRedLight;					\n" +
		"in vec3 out_light_directionForBlueLight;					\n" +
		"in vec3 out_t_normal;							\n" +
		"in vec3 out_viewer_vector;						\n" +
		"out vec4 fragColor;							\n" +
		"void main(void)								\n" +
		"{												\n" +
		"	vec3 phong_ads_light;						\n" +
		"	if(u_LKeyIsPressed==1)						\n" +
		"	{											\n"	+
		"		vec3 normalized_light_directionForRedLight=normalize(out_light_directionForRedLight);													\n"	+
		"		vec3 normalized_light_directionForBlueLight=normalize(out_light_directionForBlueLight);													\n"	+
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	+
		
		"		float t_dot_ldForRedLight=max(dot(normalized_light_directionForRedLight,normalized_t_norm),0.0);										\n" +
		"		vec3 reflection_vetorForRedLight=reflect(-normalized_light_directionForRedLight ,normalized_t_norm);									\n" +
		
		"		float t_dot_ldForBlueLight=max(dot(normalized_light_directionForBlueLight,normalized_t_norm),0.0);										\n" +
		"		vec3 reflection_vetorForBlueLight=reflect(-normalized_light_directionForBlueLight ,normalized_t_norm);									\n" +
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" +
		"		vec3 ambientRed= u_laRed * u_ka;																						\n " +
		"		vec3 ambientBlue= u_laRed * u_ka;																						\n " +
		
		"		vec3 diffuseRed=u_ldRed * u_kd * t_dot_ldForRedLight;																			\n" +
		"		vec3 diffuseBlue=u_ldBlue * u_kd * t_dot_ldForBlueLight;																			\n" +
		
		"		vec3 specularRed = u_lsRed * u_ks * pow(max(dot(reflection_vetorForRedLight,normalized_viewer_vector),0.0),u_material_shininess);			\n" +
		"		vec3 specularBlue = u_lsBlue * u_ks * pow(max(dot(reflection_vetorForBlueLight,normalized_viewer_vector),0.0),u_material_shininess);			\n" +
		
		"		phong_ads_light=ambientRed + diffuseRed + specularRed+ambientBlue + diffuseBlue + specularBlue;																\n" +
		"	}																													\n" +
		"	else																												\n" +
		"	{																													\n" +
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" +
		"	}																													\n" +
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" +
		"}	";		

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	modelUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
	viewUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
	projectionUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LKeyIsPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyIsPressed");
	laUniformRed=gl.getUniformLocation(shaderProgramObject,"u_laRed");
	ldUniformRed=gl.getUniformLocation(shaderProgramObject,"u_ldRed");
	lsUniformRed=gl.getUniformLocation(shaderProgramObject,"u_lsRed");
	laUniformBlue=gl.getUniformLocation(shaderProgramObject,"u_laBlue");
	ldUniformBlue=gl.getUniformLocation(shaderProgramObject,"u_ldBlue");
	lsUniformBlue=gl.getUniformLocation(shaderProgramObject,"u_lsBlue");

	kaUniform=gl.getUniformLocation(shaderProgramObject,"u_ka");
	kdUniform=gl.getUniformLocation(shaderProgramObject,"u_kd");
	ksUniform=gl.getUniformLocation(shaderProgramObject,"u_ks");
	lightPositionUniformRed=gl.getUniformLocation(shaderProgramObject,"u_light_positionForRedLight");
	lightPositionUniformBlue=gl.getUniformLocation(shaderProgramObject,"u_light_positionForBlueLight");
	materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");


	var triangleVertices=new Float32Array([
		0.0,1.0,0.0,
		-1.0,-1.0,1.0,
		1.0,-1.0,1.0,

		0.0,1.0,0.0,
		1.0,-1.0,1.0,
		1.0,-1.0,-1.0,

		0.0,1.0,0.0,
		1.0,-1.0,-1.0,
		-1.0,-1.0,-1.0,

		0.0,1.0,0.0,
		-1.0,-1.0,-1.0,
		-1.0,-1.0,1.0,
		

	]);

	var triangleNormals=new Float32Array([
			0.0,0.441214,0.894427,
			0.0,0.441214,0.894427,
			0.0,0.441214,0.894427,
			
			0.894427,0.447214,0.0,
			0.894427,0.447214,0.0,
			0.894427,0.447214,0.0,
			
			0.0,0.447214,-0.894427,
			0.0,0.447214,-0.894427,
			0.0,0.447214,-0.894427,
			
			-0.894427,0.447214,0.0,
			-0.894427,0.447214,0.0,
			-0.894427,0.447214,0.0

		]);
	
	vao_pyramid=gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);

	vbo_pyramid_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER,triangleVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_pyramid_normal=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_pyramid_normal);
		gl.bufferData(gl.ARRAY_BUFFER,triangleNormals,gl.STATIC_DRAW);
		gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,0,0);
		gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.disable(gl.CULL_FACE)
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	//gl.enable(gl.FRONT_FACE);

	perspectiveProjectionMatrix=mat4.create();

}

function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		var viewMatrix=mat4.create();
		
		var modelViewProjectionMatrix=mat4.create();

		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-4.0]);
		
		mat4.rotateY(modelViewMatrix,modelViewMatrix,angleCube);
		gl.uniformMatrix4fv(modelUniform,false,modelViewMatrix);
		gl.uniformMatrix4fv(viewUniform,false,viewMatrix);
	
		gl.uniformMatrix4fv(projectionUniform,false,perspectiveProjectionMatrix);

		if (bLight)
		{
			gl.uniform1i(LKeyIsPressedUniform,1);
			gl.uniform3fv(laUniformRed,light_ambientRed);		//glLightfv() 
			gl.uniform3fv(ldUniformRed,light_diffuseRed);
			gl.uniform3fv(lsUniformRed,light_specularRed);
			gl.uniform4fv(lightPositionUniformRed,light_positionRed); //glLightfv() for position

			gl.uniform3fv(laUniformBlue,light_ambientBlue);		//glLightfv() 
			gl.uniform3fv(ldUniformBlue,light_diffuseBlue);
			gl.uniform3fv(lsUniformBlue,light_specularBlue);
			gl.uniform4fv(lightPositionUniformBlue,light_positionBlue); //glLightfv() for position

			gl.uniform3fv(kaUniform,material_ambient);
			gl.uniform3fv(kdUniform,material_diffuse);
			gl.uniform3fv(ksUniform,material_specular);	//glMaterialfv();
			gl.uniform1f(materialShininessUniform,materialShininess);
		}
		else
		{
			gl.uniform1i(LKeyIsPressedUniform,0);
		}


		gl.bindVertexArray(vao_pyramid);
			gl.drawArrays(gl.TRIANGLES,0,12);
		gl.bindVertexArray(null);
	gl.useProgram(null);
	if(bRotation)
	{
		update();
	}
	
	requestAnimationFrame(draw,canvas);
}


function update()
{
	angleCube=angleCube+0.01;
	if(angleCube>360.0)
	{
		angleCube=0.0;
	}
}

function keyDown(event)
{

	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 27:
			uninitialize();
			window.close();
			break;
		case 76:
			if (bLight == false)
			{
				bLight = true;
				LKeyIsPressed = 1;
			}
			else
			{
				bLight = false;
				LKeyIsPressed = 0;
			}
				break;
		case 65:
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;

	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_sphere)
	{
		gl.deleteVertexArray(vao_sphere);
	}
	if(vbo_sphere_position)
	{
		gl.deleteBuffer(vbo_sphere_position);
	}

	if(vbo_sphere_normal)
	{
		gl.deleteBuffer(vbo_sphere_normal);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}



























