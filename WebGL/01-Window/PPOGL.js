function main()
{
	var canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	var context=canvas.getContext("2d");
	if(!context)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}
	context.fillStyle="black"
	context.fillRect(0,0,canvas.width , canvas.height);

	context.textAllign="center";
	context.textBaseline="middle";

	var str="Hello World!!!";

	context.font="48px sans-serif";

	context.fillStyle="white";
	context.fillText(str,canvas.width/2,canvas.height/2);
}