var canvas=null;
var context=null;

function main()
{
	 canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	context=canvas.getContext("2d");
	if(!context)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}
	
	drawText("Hello	World!!");
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
}

function drawText(str)
{
	context.fillStyle="black"
	context.fillRect(0,0,canvas.width , canvas.height);

	context.textAllign="center";
	context.textBaseline="middle";

	context.font="48px sans-serif";

	context.fillStyle="white";
	context.fillText(str,canvas.width/2,canvas.height/2);


}
function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		drawText("Hello World !!");
		break;
	}
}

function mouseDown() {
	alert("Mouse is clicked");
}

function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}

