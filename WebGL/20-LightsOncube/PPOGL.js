var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var angleCube=0.0;

var vao_square;

var vbo_square_position;
var vbo_square_noraml;

var modelViewUniform;
var projectionUniform;
var LKeyIsPressedUniform;
var ldUniform;
var kdUniform;
var lightPositionUniform;

var bRotation=false;
var bLight=false

var LKeyIsPressed=0;

var perspectiveProjectionMatrix;

const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;" +
	"in vec3 vNormal; " +
	"uniform mat4 u_mv_matrix;" +
	"uniform mat4 u_projection_matrix;" +
	"uniform mediump int u_LKeyIsPressed;" +
	"uniform vec3 u_ld;" +
	"uniform vec3 u_kd;" +
	"uniform vec4 u_light_position;" +
	"out vec3 out_diffuse_color;" +
	"void main(void)" +
	"{" +
		"if(u_LKeyIsPressed==1)" +
		"{" +
			"vec4 eye_coordinates=u_mv_matrix * vPosition;" +
			"mat3 normal_matrix=mat3(transpose(inverse(u_mv_matrix)));" +
			"vec3 t_normal=normalize(normal_matrix * vNormal);" +
			"vec3 s=vec3(u_light_position - eye_coordinates);" +
			"out_diffuse_color=u_ld * u_kd * max(dot(s,t_normal),0.0);" +

		"}" +
		"gl_Position=u_projection_matrix* u_mv_matrix * vPosition ;"  +
	"}";

	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"precision highp float;" +
	"in vec3 out_diffuse_color;" +
	"uniform int u_LKeyIsPressed;" +
	"out vec4 fragColor;" +
	"void main(void)" +
	"{" +
		"if(u_LKeyIsPressed==1)" +
		"{" +
			"fragColor=vec4(out_diffuse_color,1.0);" +
		"}" +
		"else" +
		"{" +
			"fragColor=vec4(1.0,1.0,1.0,1.0);" +
		"}"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	modelViewUniform=gl.getUniformLocation(shaderProgramObject,"u_mv_matrix");
	projectionUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LKeyIsPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyIsPressed");
	ldUniform=gl.getUniformLocation(shaderProgramObject,"u_ld");
	kdUniform=gl.getUniformLocation(shaderProgramObject,"u_kd");
	lightPositionUniform=gl.getUniformLocation(shaderProgramObject,"u_light_position");

	
	var squareVertices=new Float32Array([
			1.0,1.0,1.0,
			-1.0,1.0,1.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			1.0,1.0,-1.0,
			1.0,1.0,1.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0,
			
			1.0,1.0,-1.0,
			-1.0,1.0,-1.0,
			-1.0,-1.0,-1.0,
			1.0,-1.0,-1.0,

			-1.0,1.0,1.0,
			-1.0,1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0,

			1.0,1.0,-1.0,
			-1.0,1.0,-1.0,
			-1.0,1.0,1.0,
			1.0,1.0,1.0,

			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,							
	]);


	for(var i=0;i<72;i++)
	{
		if(squareVertices[i]<0.0)
		{
			squareVertices[i]=squareVertices[i]+0.25;
		}
		else if(squareVertices[i]>0.0)
		{
			squareVertices[i]=squareVertices[i]-0.25;
		}
		else
		{
			squareVertices[i]=squareVertices[i];
		}
	}
	
	var sqaureNormal=new Float32Array([
			0.0,0.0,1.0,
			0.0,0.0,1.0,
			0.0,0.0,1.0,
			0.0,0.0,1.0,
			
			1.0,0.0,0.0,
			1.0,0.0,0.0,
			1.0,0.0,0.0,
			1.0,0.0,0.0,

			0.0,0.0,-1.0,
			0.0,0.0,-1.0,
			0.0,0.0,-1.0,
			0.0,0.0,-1.0,
			
			-1.0,0.0,0.0,
			-1.0,0.0,0.0,
			-1.0,0.0,0.0,
			-1.0,0.0,0.0,

			0.0,1.0,0.0,
			0.0,1.0,0.0,
			0.0,1.0,0.0,
			0.0,1.0,0.0,
			
			0.0,-1.0,0.0,
			0.0,-1.0,0.0,
			0.0,-1.0,0.0,
			0.0,-1.0,0.0,
	]);

	

	
	vao_square=gl.createVertexArray();
	gl.bindVertexArray(vao_square);

	vbo_square_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_position);
	gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_square_noraml=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_noraml);
		gl.bufferData(gl.ARRAY_BUFFER,sqaureNormal,gl.STATIC_DRAW);
		gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,0,0);
		gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	

	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.disable(gl.CULL_FACE)
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	//gl.enable(gl.FRONT_FACE);

	perspectiveProjectionMatrix=mat4.create();

}

function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		//var viewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();

		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-4.0]);
		mat4.rotateX(modelViewMatrix,modelViewMatrix,angleCube);
		mat4.rotateY(modelViewMatrix,modelViewMatrix,angleCube);
		mat4.rotateZ(modelViewMatrix,modelViewMatrix,angleCube);
		//mat4.scale(modelViewMatrix,modelViewMatrix,[0.25,0.25,0.25]);

		//mat4.multiply(modelViewMatrix,modelViewMatrix,viewMatrix);
		
		gl.uniformMatrix4fv(modelViewUniform,false,modelViewMatrix);
		//gl.uniformMatrix4fv(projectionUniform,false,viewMatrix);
		gl.uniformMatrix4fv(projectionUniform,false,perspectiveProjectionMatrix);

		if (bLight)
		{
			gl.uniform1i(LKeyIsPressedUniform,1);
			gl.uniform3f(ldUniform,1.0,1.0,0.0);		//glLightfv() 
			gl.uniform3f(kdUniform,0.5,0.5,0.5);	//glMaterialfv();
			gl.uniform4f(lightPositionUniform,0.0,0.0,2.0,1.0); //glLightfv() for position
		}
		else
		{
			gl.uniform1i(LKeyIsPressedUniform,0);
		}


		gl.bindVertexArray(vao_square);
			gl.drawArrays(gl.TRIANGLE_FAN ,0,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,4,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,8,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,12,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,16,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,20,4);
		gl.bindVertexArray(null);
	gl.useProgram(null);
	if(bRotation)
	{
		update();

	}
	requestAnimationFrame(draw,canvas);
}


function update()
{
	angleCube=angleCube+0.01;
	if(angleCube>360.0)
	{
		angleCube=0.0;
	}
}

function keyDown(event)
{
	console.log(event.keyCode)
	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 27:
			uninitialize();
			window.close();
			break;
		case 76:
			if (bLight == false)
			{
				bLight = true;
				LKeyIsPressed = 1;
			}
			else
			{
				bLight = false;
				LKeyIsPressed = 0;
			}
				break;
		case 65:
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;

	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
	}
	if(vbo_square_noraml)
	{
		gl.deleteBuffer(vbo_square_noraml);
	}

	if(vbo_square_position)
	{
		gl.deleteBuffer(vbo_square_position);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}



























