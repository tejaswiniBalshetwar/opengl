var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var shoulder=0;
var elbow=0;

var vao;

var stackedMatrix=new Array(10);
var top=-1;

var vbo_position;
var vbod_color;


var mvpUniform;
var colorUniform;
var scaleUniform;

var perspectiveProjectionMatrix;

const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;" +
	"in vec3 vColor;" +
	"out vec3 out_color;" +
	"uniform mat4 u_mvp_matrix;" +
	"uniform mat4 u_scale_matrix;" +
	"void main(void)" +
	"{" +
		"gl_Position=u_mvp_matrix * vPosition ;" +
		"out_color=vColor;" +
	"}" ; 

	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"precision highp float;" +
	"in vec3 out_color;" +
	"uniform vec3 uColor;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
		"FragColor=vec4(uColor,1.0);" +
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	colorUniform=gl.getUniformLocation(shaderProgramObject,"uColor");
	scaleUniform=gl.getUniformLocation(shaderProgramObject,"u_scale_matrix");

	sphere=new Mesh();
	makeSphere(sphere,2.0,30,30);
		

	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.disable(gl.CULL_FACE)
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	for(var i=0;i<10;i++)
	{
		stackedMatrix[i]=mat4.create();
	}

	perspectiveProjectionMatrix=mat4.create();

}

function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		var viewMatrix=mat4.create();
		

		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-22.0]);
		
		pushMatrix(modelViewMatrix);
		
		mat4.rotateZ(modelViewMatrix,modelViewMatrix,parseFloat(shoulder));
		mat4.translate(modelViewMatrix,modelViewMatrix,[1.0,0.0,0.0]);
		

		pushMatrix(modelViewMatrix);

		var xformMatrix = new Float32Array([
            2.0,  0.0,  0.0,  0.0,
            0.0,  0.5,   0.0,  0.0,
            0.0,  0.0,  1.0,   0.0,
            0.0,  0.0,  0.0,  1.0    
         ]);

		mat4.multiply(modelViewMatrix,modelViewMatrix,xformMatrix);
		
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);

		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
		gl.uniform3f(colorUniform,0.5,0.35,0.05);

		sphere.draw();

		var poppedMatrix=popMatrix();
/*
		mat4.identity(modelViewProjectionMatrix);
		mat4.identity(modelViewMatrix);
	
		mat4.translate(modelViewMatrix,modelViewMatrix,[1.0,0.0,0.0]);
		mat4.rotateZ(modelViewMatrix,modelViewMatrix,parseFloat(elbow));
		mat4.translate(modelViewMatrix,modelViewMatrix,[0.90,0.0,0.0]);
		mat4.multiply(modelViewMatrix,modelViewMatrix,xformMatrix);
		
		mat4.multiply(modelViewMatrix,poppedMatrix,modelViewMatrix);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	
		sphere.draw();*/

		popMatrix();

	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
	update();
}


function update()
{
	//year = (year + 1) % 30;
	/*year = year + 0.008;
	if (year > 360.0)
	{
		year = 0.0;
	}*/
}

function keyDown(event)
{

	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 69:
			elbow = (elbow + 3) % 360;
		break;

		case 83:
		shoulder = (shoulder + 3) % 360;
		
		break;

		case 27:
			uninitialize();
			window.close();
			break;
	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_triangle)
	{
		gl.deleteVertexArray(vao_triangle);
	}
	if(vbo_triangle_color)
	{
		gl.deleteBuffer(vbo_triangle_color);
	}

	if(vbo_triangle_position)
	{
		gl.deleteBuffer(vbo_triangle_position);
	}

	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
	}
	if(vbo_square_color)
	{
		gl.deleteBuffer(vbo_square_color);
	}

	if(vbo_square_position)
	{
		gl.deleteBuffer(vbo_square_position);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}

function pushMatrix(matrix)
{
	top=top+1;
	stackedMatrix[top]=matrix;
}

function popMatrix()
{
	return(stackedMatrix[top--]);
}