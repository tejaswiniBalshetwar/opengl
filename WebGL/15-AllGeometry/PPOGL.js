var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_line;
var vao_circle;
var vao_square;
var vao_triangle;

var vbo_line_position;
var vbo_line_color;

var vbo_square_color;
var vbo_square_position;

var vbo_triangle_color;
var vbo_triangle_position;

var vbo_circle_color;
var vbo_circle_position;

var verticesForOuterCircle=new Float32Array(5000);
var colorForCircle=new Float32Array(5000);

var verticesForInnerCircle=new Float32Array(5000);

var mvpUniform;

var perspectiveProjectionMatrix;
var lineColor=new Float32Array(8);
var lineVertices=new Float32Array(6);

const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"out vec4 out_color;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
		"gl_Position=u_mvp_matrix * vPosition;" +
		"gl_PointSize=3.0;" +
		"out_color=vColor;" +
	"}" ; 

	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
		"FragColor=vec4(out_color);" +
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

	var lengthOfASide = 0.0;
	var lengthOfBSide = 0.0;
	var lengthOfCSide = 0.0;



	vao_line=gl.createVertexArray();
	gl.bindVertexArray(vao_line);

	vbo_line_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_position);
	gl.bufferData(gl.ARRAY_BUFFER,null,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_line_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_color);
		gl.bufferData(gl.ARRAY_BUFFER,null,gl.DYNAMIC_DRAW);
		gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,4,gl.FLOAT,false,0,0);
		gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);


//square

	var verticesForSquare=new Float32Array([
		1.0,1.0,0.0,
		-1.0,1.0,0.0,
		-1.0,-1.0,0.0,
		1.0,-1.0,0.0]);
	var square_color=new Float32Array([
		1.0,1.0,0.0,0.0,
		1.0,1.0,0.0,0.0,
		1.0,1.0,0.0,0.0,
		1.0,1.0,0.0,0.0,]);

	vao_square=gl.createVertexArray();
	gl.bindVertexArray(vao_square);

	vbo_square_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_position);
	gl.bufferData(gl.ARRAY_BUFFER,verticesForSquare,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_square_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_color);
	gl.bufferData(gl.ARRAY_BUFFER,square_color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,4,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);

	//outer circle
	lengthOfASide = calculateLengthByDistanceFormula(verticesForSquare[0], verticesForSquare[1], verticesForSquare[2], verticesForSquare[3], verticesForSquare[4], verticesForSquare[5]);
	lengthOfBSide = calculateLengthByDistanceFormula(verticesForSquare[3], verticesForSquare[4], verticesForSquare[5], verticesForSquare[6], verticesForSquare[7], verticesForSquare[8]);
	
/*
	GLfloat verticesForOuterCircle[2000];
	memset(verticesForOuterCircle,0,sizeof(verticesForOuterCircle));
	GLfloat colorForCircle[2000];
	memset(colorForCircle,0,sizeof(colorForCircle));*/
	var	radius = calculateRadiusOfOuterCircle(lengthOfASide, lengthOfBSide);


	calculateVerticesForCircle(radius, 0.0, 0.0);

	vao_circle=gl.createVertexArray();
	gl.bindVertexArray(vao_circle);
	vbo_circle_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_position);
	gl.bufferData(gl.ARRAY_BUFFER,null,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_circle_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_color);
	gl.bufferData(gl.ARRAY_BUFFER,colorForCircle,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,4,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);


	var  verticesForTriangle=new Float32Array([
		0.0,1.0,0.0,
		-1.0,-1.0,0.0,
		1.0,-1.0,0.0
	]);

	var colorForTriangle =new Float32Array([
		0.0,1.0,1.0,0.0,
		0.0,1.0,1.0,0.0,
		0.0,1.0,1.0,0.0
	]);
	vao_triangle=gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);
	vbo_triangle_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_triangle_position);
	gl.bufferData(gl.ARRAY_BUFFER,verticesForTriangle,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_triangle_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_triangle_color);
	gl.bufferData(gl.ARRAY_BUFFER,colorForTriangle,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,4,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);


	//inner circle
	lengthOfASide = calculateLengthByDistanceFormula(verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2], verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5]);
	lengthOfBSide = calculateLengthByDistanceFormula(verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5], verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8]);
	lengthOfCSide = calculateLengthByDistanceFormula(verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8], verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2]);

	
	var XCenterOfCircle = ((verticesForTriangle[0] * lengthOfBSide) + (verticesForTriangle[3] * lengthOfCSide) + (verticesForTriangle[6] * lengthOfASide))/(lengthOfASide+lengthOfBSide+lengthOfCSide);
	var YCenterOfCircle = ((verticesForTriangle[1] * lengthOfBSide) + (verticesForTriangle[4] * lengthOfCSide) + (verticesForTriangle[7] * lengthOfASide))/ (lengthOfASide + lengthOfBSide + lengthOfCSide);

	radius = calculateInnerCircleRadius(lengthOfASide, lengthOfBSide, lengthOfCSide);
	
	calculateVerticesForCircle(radius, parseFloat(XCenterOfCircle), parseFloat(YCenterOfCircle));


	gl.clearColor(0.0,0.0,0.0,1.0);

	perspectiveProjectionMatrix=mat4.create();
}

function calculateLengthByDistanceFormula( x1,  y1,  z1,  x2,  y2,  z2 )
{
	var length = 0.0;

	length = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2) +Math.pow((z2 - z1), 2));

	return length;

}
function calculateRadiusOfOuterCircle( lengthA,  lengthB)
{
	var radius = 0.0;
	var hypotenious = 0.0;

	hypotenious = Math.sqrt(Math.pow(lengthA, 2) + Math.pow(lengthB, 2));

	radius = hypotenious / 2;
	return radius;

}
function calculateInnerCircleRadius( A,  B,  C)
{
	var semiperimeter = (A + B + C) / 2.0;
	var area = Math.sqrt(semiperimeter*((semiperimeter - A)*(semiperimeter - B)*(semiperimeter - C)));

	var radius = (area / semiperimeter);

	return radius;
}

function calculateVerticesForCircle( radius,  XPointOfCenter,  YPointOfCenter)
{
	var x = 0.0, y = 0.0;
	var angle=0.0;
	//vertices[2000];
		console.log(typeof(XPointOfCenter))
		console.log("YPointOfCenter "+ YPointOfCenter)
	for (var i = 0; i < 5000-3; i=i+3)
	{
		 angle = (4 * Math.PI*i) / 3000;
		if(XPointOfCenter==0.0 && YPointOfCenter<0.0)
		{
			
			x = parseFloat(XPointOfCenter) + radius * Math.cos(angle);
			y =parseFloat(YPointOfCenter) + radius * Math.sin(angle);
		
			verticesForInnerCircle[i] = x;
			verticesForInnerCircle[i + 1] = y;
			verticesForInnerCircle[i + 2] = 0.0;
			//verticesForInnerCircle[i + 3] = 0.0; 

		}
		else
		{	
			x =  radius * Math.cos(angle);
			y = radius * Math.sin(angle);
			
			verticesForOuterCircle[i] = x;
			verticesForOuterCircle[i + 1] = y;
			verticesForOuterCircle[i + 2] = 0.0; 
			//verticesForOuterCircle[i + 3] = 0.0; 
		}
		
	}

	for(var i=0;i<5000-4;i=i+4)
	{
		colorForCircle[i] = 1.0;
		colorForCircle[i + 1] = 0.0;
		colorForCircle[i + 2] = 1.0;
		colorForCircle[i + 3] = 0.0;
	}

}


function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();

		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
		
		
		lineVertices[0] = 8.0;
		lineVertices[1] = 0.0;
		lineVertices[2] = 0.0;
		lineVertices[3] = -8.0;
		lineVertices[4] = 0.0;
		lineVertices[5] = 0.0;
		

		lineColor[0] = 1.0;
		lineColor[1] = 0.0;
		lineColor[2] = 0.0;
		lineColor[3] = 0.0;
		lineColor[4] = 1.0;
		lineColor[5] = 0.0;
		lineColor[6] = 0.0;
		lineColor[7] = 0.0;

		drawLine();

		lineVertices[0] = 0.0;
		lineVertices[1] = 4.0;
		lineVertices[2] = 0.0;
		lineVertices[3] = 0.0;
		lineVertices[4] = -4.0;
		lineVertices[5] = 0.0;
		

		lineColor[0] = 0.0;
		lineColor[1] = 1.0;
		lineColor[2] = 0.0;
		lineColor[3] = 0.0;
		lineColor[4] = 0.0;
		lineColor[5] = 1.0;
		lineColor[6] = 0.0;
		lineColor[7] = 0.0;

		drawLine();


		//vertical lines
		lineColor[0] = 0.0;
		lineColor[1] = 0.0;
		lineColor[2] = 1.0;
		lineColor[3] = 0.0;
		//vertical Lines
		lineColor[4] = 0.0;
		lineColor[5] = 0.0;
		lineColor[6] = 1.0;
		lineColor[7] = 0.0;


			for (var i =1; i < 21; i++)
			{	
				var width = 3.20*i / 20;
				lineVertices[0] = width;
				lineVertices[1] = 4.0;
				lineVertices[2] = 0.0;

				lineVertices[3] = width;
				lineVertices[4] = -4.0;
				lineVertices[5] = 0.0;
				drawLine();

				 width =-3.20*i / 20;
				lineVertices[0] = width;
				lineVertices[1] = 4.0;
				lineVertices[2] = 0.0;

				lineVertices[3] = width;
				lineVertices[4] = -4.0;
				lineVertices[5] = 0.0;

				drawLine();
			}

			for (var i = 1; i < 21; i++)
			{
				var width = 2.20*i / 20;
				lineVertices[0] = 4.2;
				lineVertices[1] = width;
				lineVertices[2] = 0.0;

				lineVertices[3] = -4.2;
				lineVertices[4] = width;
				lineVertices[5] = 0.0;

				drawLine();

				width = -2.20*i / 20;
				lineVertices[0] = 4.2;
				lineVertices[1] = width;
				lineVertices[2] = 0.0;

				lineVertices[3] = -4.20;
				lineVertices[4] = width;
				lineVertices[5] = 0.0;
				
				drawLine();
			}

		drawSquare();

		drawCircleOuterCircle();
		
		drawTriangle();

		drawCircleInnerCircle();
			
	
		gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
}
function drawSquare()
{
	gl.bindVertexArray(vao_square);
	gl.lineWidth(2.0);
	gl.drawArrays(gl.LINE_LOOP,0,4);
	gl.lineWidth(0.5);
	gl.bindVertexArray(null);
}

function drawCircleInnerCircle()
{
	gl.bindVertexArray(vao_circle);
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_position);
			gl.bufferData(gl.ARRAY_BUFFER,verticesForInnerCircle,gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
		/*gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle_color);
			gl.bufferData(gl.ARRAY_BUFFER, colorForCircle, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);*/
	
		//gl.pointSize(3.0);
		gl.drawArrays(gl.POINTS,0,1000);
	gl.bindVertexArray(null);
}

function drawCircleOuterCircle()
{
	gl.bindVertexArray(vao_circle);
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_position);
			gl.bufferData(gl.ARRAY_BUFFER,verticesForOuterCircle,gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
		/*gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle_color);
			gl.bufferData(gl.ARRAY_BUFFER, colorForCircle, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);*/
	
		//gl.pointSize(3.0);
		gl.drawArrays(gl.POINTS,0,500);
	gl.bindVertexArray(null);
}

function drawTriangle()
{
	gl.bindVertexArray(vao_triangle);
	gl.drawArrays(gl.LINE_LOOP,0,3);
	gl.bindVertexArray(null);
}

function drawLine()
{
		gl.bindVertexArray(vao_line);
			gl.bindBuffer(gl.ARRAY_BUFFER, vbo_line_position);
				gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindBuffer(gl.ARRAY_BUFFER, vbo_line_color);
				gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.drawArrays(gl.LINES,0,2);
		gl.bindVertexArray(null);
}
function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 27:
			uninitialize();
			window.close();
			break;
	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_triangle)
	{
		gl.deleteVertexArray(vao_triangle);
	}
	if(vbo_triangle_color)
	{
		gl.deleteBuffer(vbo_triangle_color);
	}

	if(vbo_triangle_position)
	{
		gl.deleteBuffer(vbo_triangle_position);
	}

	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
	}
	if(vbo_square_color)
	{
		gl.deleteBuffer(vbo_square_color);
	}

	if(vbo_square_position)
	{
		gl.deleteBuffer(vbo_square_position);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}

