var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObjectPerVertex;
var shaderProgramObjectPerFragment=0;

var gbIsPerVertex=false;

var vao_sphere;
var vbo_sphere_position;
var vbo_sphere_normal;

var light_ambientRed=[0.0,0.0,0.0];
var light_diffuseRed=[1.0,0.0,0.0];
var light_specularRed=[1.0,0.0,0.0];
var light_positionRed=[0.0,0.0,0.0,1.0];

var light_ambientBlue=[0.0,0.0,0.0];
var light_diffuseBlue=[0.0,0.0,1.0];
var light_specularBlue=[0.0,0.0,1.0];
var light_positionBlue=[0.0,0.0,0.0,1.0];

var light_ambientGreen=[0.0,0.0,0.0];
var light_diffuseGreen=[0.0,1.0,0.0];
var light_specularGreen=[0.0,1.0,0.0];
var light_positionGreen=[0.0,0.0,0.0,1.0];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var materialShininess=128.0;

var sphere=null;

var modelUniform;
var viewUnifrom;
var projectionUniform;
var LKeyIsPressedUniform;
var laUniformRed,ldUniformRed,lsUniformRed,kaUniform,kdUniform,ksUniform,materialShininessUniform;
var laUniformGreen,ldUniformGreen,lsUniformGreen;
var laUniformBlue,ldUniformBlue,lsUniformBlue;
var lightPositionUniformRed;
var lightPositionUniformGreen;
var lightPositionUniformBlue;

var modelUniformFragment;
var viewUniformFragment;
var projectionUniformFragment;
var LKeyIsPressedUniformFragment;
var laUniformRedFragment,ldUniformRedFragment,lsUniformRedFragment,kaUniformFragment,kdUniformFragment,ksUniformFragment,materialShininessUniform;
var laUniformBlueFragment,ldUniformBlueFragment,lsUniformBlueFragment;
var laUniformGreenFragment,ldUniformGreenFragment,lsUniformGreenFragment;


var lightPositionUniformRedFragment;
var lightPositionUniformBlueFragment;
var lightPositionUniformGreenFragment;


var bRotation=false;
var bLight=false

var LKeyIsPressed=0;

var perspectiveProjectionMatrix;
var modelMatrix;
var viewMatrix;


var light_angel_red=0.0;
var light_angel_green=0.0;
var light_angel_blue=0.0;


const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	AMC_ATTRIBUTE_POSITION2:4,
	AMC_ATTRIBUTE_COLOR2:5,
	AMC_ATTRIBUTE_NORMAL2:6,
	AMC_ATTRIBUTE_TEXTURE02:7,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	perFragmentLight();
	perVertexLight();
	sphere=new Mesh();
	makeSphere(sphere,2.0,30,30);
		

	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.disable(gl.CULL_FACE)
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	//gl.enable(gl.FRONT_FACE);

	perspectiveProjectionMatrix=mat4.create();

}
function perVertexLight()
{
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;\n" +
	"in vec3 vNormal;\n " +
	"uniform mat4 u_model_matrix;\n" +
	"uniform mat4 u_view_matrix;\n" +
	"uniform mat4 u_projection_matrix;\n" +
	"uniform int u_LKeyIsPressed;\n" +
	"uniform vec3 u_laRed;								\n" +
	"uniform vec3 u_ldRed;								\n" +
	"uniform vec3 u_lsRed;								\n" +

	"uniform vec3 u_laBlue;								\n" +
	"uniform vec3 u_ldBlue;								\n" +
	"uniform vec3 u_lsBlue;								\n" +

	"uniform vec3 u_laGreen;								\n" +
	"uniform vec3 u_ldGreen;								\n" +
	"uniform vec3 u_lsGreen;								\n" +	

	"uniform vec4 u_light_positionForRedLight;					\n" +
	"uniform vec4 u_light_positionForBlueLight;					\n" +
	"uniform vec4 u_light_positionForGreenLight;					\n" +
	"uniform vec3 u_ka;\n" +
	"uniform vec3 u_kd;\n" +
	"uniform vec3 u_ks;\n" +
	"uniform float u_material_shininess;\n"+
	"out vec3 out_phong_ads_light;\n"+
	"void main(void)\n" +
	"{\n" +
		"if(u_LKeyIsPressed==1)\n" +
		"{\n" +
			"vec4 eye_coordinates= u_view_matrix * u_model_matrix * vPosition;\n" +
			"vec3 t_normal=normalize(mat3(u_view_matrix*u_model_matrix) * vNormal);\n" +
			"vec3 light_directionRed=normalize(vec3(u_light_positionForRedLight - eye_coordinates));\n" +
			"vec3 light_directionGreen=normalize(vec3(u_light_positionForGreenLight - eye_coordinates));\n" +
			"vec3 light_directionBlue=normalize(vec3(u_light_positionForBlueLight - eye_coordinates));\n" +
			"float t_dot_ldRed=max(dot(light_directionRed,t_normal),0.0);\n" +
			"float t_dot_ldGreen=max(dot(light_directionGreen,t_normal),0.0);\n" +
			"float t_dot_ldBlue=max(dot(light_directionBlue,t_normal),0.0);\n" +
			"vec3 reflection_vetorRed=reflect(-light_directionRed ,t_normal);\n" +
			"vec3 reflection_vetorGreen=reflect(-light_directionGreen ,t_normal);\n" +
			"vec3 reflection_vetorBlue=reflect(-light_directionBlue ,t_normal);\n" +
			"vec3 viewer_vector=normalize(vec3(-eye_coordinates));\n" +
			"vec3 ambientRed= u_laRed * u_ka;\n " +
			"vec3 ambientGreen= u_laGreen * u_ka;\n " +
			"vec3 ambientBlue= u_laBlue * u_ka;\n " +
			"vec3 diffuseRed=u_ldRed * u_kd * t_dot_ldRed;\n" +
			"vec3 diffuseGreen=u_ldGreen * u_kd * t_dot_ldGreen;\n" +
			"vec3 diffuseBlue=u_ldBlue * u_kd * t_dot_ldBlue;\n" +
			"vec3 specularRed = u_lsRed * u_ks * pow(max(dot(reflection_vetorRed,viewer_vector),0.0),u_material_shininess);\n" +
			"vec3 specularGreen = u_lsGreen * u_ks * pow(max(dot(reflection_vetorGreen,viewer_vector),0.0),u_material_shininess);\n" +
			"vec3 specularBlue = u_lsBlue * u_ks * pow(max(dot(reflection_vetorBlue,viewer_vector),0.0),u_material_shininess);\n" +
			"out_phong_ads_light=ambientRed + diffuseRed + specularRed+ambientBlue + diffuseBlue + specularBlue+ ambientGreen + diffuseGreen + specularGreen;																\n" +
		"}\n" +
		"else \n" +
		"{ \n" +
			"out_phong_ads_light=vec3(1.0,1.0,1.0);\n" +
		"}\n" +
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;\n"  +
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"precision highp float;" +
	"in vec3 out_phong_ads_light;" +
	"out vec4 fragColor;" +
	"void main(void)" +
	"{" +	
		"fragColor=vec4(out_phong_ads_light,1.0);" +
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObjectPerVertex=gl.createProgram();
	gl.attachShader(shaderProgramObjectPerVertex,vertexShaderObject);
	gl.attachShader(shaderProgramObjectPerVertex,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObjectPerVertex,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObjectPerVertex,webGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(shaderProgramObjectPerVertex);

	if(!gl.getProgramParameter(shaderProgramObjectPerVertex,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObjectPerVertex);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	modelUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_model_matrix");
	viewUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_view_matrix");
	projectionUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_projection_matrix");
	LKeyIsPressedUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_LKeyIsPressed");
	laUniformRed=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_laRed");
	ldUniformRed=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_ldRed");
	lsUniformRed=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_lsRed");

	laUniformGreen=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_laGreen");
	ldUniformGreen=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_ldGreen");
	lsUniformGreen=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_lsGreen");

	laUniformBlue=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_laBlue");
	ldUniformBlue=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_ldBlue");
	lsUniformBlue=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_lsBlue");

	kaUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_ka");
	kdUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_kd");
	ksUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_ks");
	lightPositionUniformRed=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_light_positionForRedLight");
	lightPositionUniformGreen=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_light_positionForBlueLight");
	lightPositionUniformBlue=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_light_positionForGreenLight");
	materialShininessUniform=gl.getUniformLocation(shaderProgramObjectPerVertex,"u_material_shininess");

}
function perFragmentLight()
{
	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;								\n" +
		"in vec3 vNormal;								\n" +
		"uniform mat4 u_model_matrix;					\n" +
		"uniform mat4 u_view_matrix;					\n" +
		"uniform mat4 u_projection_matrix;				\n" +
		"uniform mediump int u_LKeyIsPressed;					\n" +
		"uniform vec4 u_light_positionForRedLight;					\n" +
		"uniform vec4 u_light_positionForBlueLight;					\n" +
		"uniform vec4 u_light_positionForGreenLight;					\n" +
		"out vec3 out_light_directionForRedLight;					\n" +
		"out vec3 out_light_directionForBlueLight;					\n" +
		"out vec3 out_light_directionForGreenLight;					\n" +
		"out vec3 out_t_normal;							\n" +
		"out vec3 out_viewer_vector;					\n" +
		"void main(void)								\n" +
		"{												\n" +
		"	if(u_LKeyIsPressed==1)						\n" +
		"	{											\n" +
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" +
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" +
		"		out_light_directionForRedLight=vec3(u_light_positionForRedLight - eye_coordinates);				\n" +
		"		out_light_directionForGreenLight=vec3(u_light_positionForGreenLight - eye_coordinates);				\n" +
		"		out_light_directionForBlueLight=vec3(u_light_positionForBlueLight - eye_coordinates);				\n" +
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" +
		"	}																				\n" +
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  +
		"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n " +
		"precision highp float;" +
		"uniform vec3 u_laRed;								\n" +
		"uniform vec3 u_ldRed;								\n" +
		"uniform vec3 u_lsRed;								\n" +
		"uniform vec3 u_laBlue;								\n" +
		"uniform vec3 u_ldBlue;								\n" +
		"uniform vec3 u_lsBlue;								\n" +
		"uniform vec3 u_laGreen;								\n" +
		"uniform vec3 u_ldGreen;								\n" +
		"uniform vec3 u_lsGreen;								\n" +	
		"uniform mediump int u_LKeyIsPressed;					\n" +
		"uniform vec3 u_ka;								\n" +
		"uniform vec3 u_kd;								\n" +
		"uniform vec3 u_ks;								\n" +
		"uniform float u_material_shininess;			\n" +
		"in vec3 out_light_directionForRedLight;					\n" +
		"in vec3 out_light_directionForBlueLight;					\n" +
		"in vec3 out_light_directionForGreenLight;					\n" +

		"in vec3 out_t_normal;							\n" +
		"in vec3 out_viewer_vector;						\n" +
		"out vec4 fragColor;							\n" +
		"void main(void)								\n" +
		"{												\n" +
		"	vec3 phong_ads_light;						\n" +
		"	if(u_LKeyIsPressed==1)						\n" +
		"	{											\n"	+
		"		vec3 normalized_light_directionForRedLight=normalize(out_light_directionForRedLight);													\n"	+
		"		vec3 normalized_light_directionForBlueLight=normalize(out_light_directionForBlueLight);													\n"	+
		"		vec3 normalized_light_directionForGreenLight=normalize(out_light_directionForGreenLight);													\n"	+
	
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	+
		
		"		float t_dot_ldForRedLight=max(dot(normalized_light_directionForRedLight,normalized_t_norm),0.0);										\n" +
		"		vec3 reflection_vetorForRedLight=reflect(-normalized_light_directionForRedLight ,normalized_t_norm);									\n" +
		
		"		float t_dot_ldForGreenLight=max(dot(normalized_light_directionForGreenLight,normalized_t_norm),0.0);										\n" +
		"		vec3 reflection_vetorForGreenLight=reflect(-normalized_light_directionForGreenLight ,normalized_t_norm);									\n" +
		

		"		float t_dot_ldForBlueLight=max(dot(normalized_light_directionForBlueLight,normalized_t_norm),0.0);										\n" +
		"		vec3 reflection_vetorForBlueLight=reflect(-normalized_light_directionForBlueLight ,normalized_t_norm);									\n" +
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" +
		"		vec3 ambientRed= u_laRed * u_ka;																						\n " +
		"		vec3 ambientBlue= u_laBlue * u_ka;																						\n " +
		"		vec3 ambientGreen= u_laGreen * u_ka;																						\n " +
		
		"		vec3 diffuseRed=u_ldRed * u_kd * t_dot_ldForRedLight;																			\n" +
		"		vec3 diffuseBlue=u_ldBlue * u_kd * t_dot_ldForBlueLight;																			\n" +
		"		vec3 diffuseGreen=u_ldGreen * u_kd * t_dot_ldForGreenLight;																			\n" +
		
		"		vec3 specularRed = u_lsRed * u_ks * pow(max(dot(reflection_vetorForRedLight,normalized_viewer_vector),0.0),u_material_shininess);			\n" +
		"		vec3 specularBlue = u_lsBlue * u_ks * pow(max(dot(reflection_vetorForBlueLight,normalized_viewer_vector),0.0),u_material_shininess);			\n" +
		"		vec3 specularGreen = u_lsGreen * u_ks * pow(max(dot(reflection_vetorForGreenLight,normalized_viewer_vector),0.0),u_material_shininess);			\n" +
		
		"		phong_ads_light=ambientRed + diffuseRed + specularRed+ambientBlue + diffuseBlue + specularBlue+ ambientGreen + diffuseGreen + specularGreen;																\n" +
		"	}																													\n" +
		"	else																												\n" +
		"	{																													\n" +
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" +
		"	}																													\n" +
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" +
		"}	";		
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObjectPerFragment=gl.createProgram();
	gl.attachShader(shaderProgramObjectPerFragment,vertexShaderObject);
	gl.attachShader(shaderProgramObjectPerFragment,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObjectPerFragment,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObjectPerFragment,webGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(shaderProgramObjectPerFragment);

	if(!gl.getProgramParameter(shaderProgramObjectPerFragment,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObjectPerFragment);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	modelUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_model_matrix");
	viewUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_view_matrix");
	projectionUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_projection_matrix");
	LKeyIsPressedUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_LKeyIsPressed");
	
	laUniformRedFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"	u_laRed");
	ldUniformRedFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_ldRed");
	lsUniformRedFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_lsRed");
	
	laUniformBlueFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_laBlue");
	ldUniformBlueFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_ldBlue");
	lsUniformBlueFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_lsBlue");
	
	laUniformGreenFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_laGreen");
	ldUniformGreenFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_ldGreen");
	lsUniformGreenFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_lsGreen");


	kaUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_ka");
	kdUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_kd");
	ksUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_ks");
	lightPositionUniformRedFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_light_positionForRedLight");
	lightPositionUniformBlueFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_light_positionForBlueLight");
	lightPositionUniformGreenFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_light_positionForGreenLight");
	materialShininessUniformFragment=gl.getUniformLocation(shaderProgramObjectPerFragment,"u_material_shininess");


	//console.log(laUniformFragment);
	
}

function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

		var modelViewMatrix=mat4.create();
		var viewMatrix=mat4.create();
		
		var modelViewProjectionMatrix=mat4.create();


		if(gbIsPerVertex==true)
		{
			gl.useProgram(shaderProgramObjectPerVertex);
			if (bLight)
			{
					
				gl.uniform1i(LKeyIsPressedUniform,1);
				gl.uniform3fv(laUniformRed,light_ambientRed);		//glLightfv() 
				gl.uniform3fv(ldUniformRed,light_diffuseRed);
				gl.uniform3fv(lsUniformRed,light_specularRed);

				light_positionRed[1]=200.0*Math.cos(light_angel_red);
				light_positionRed[2]=200.0*Math.sin(light_angel_red);
				gl.uniform4fv(lightPositionUniformRed,light_positionRed); //glLightfv() for position

				gl.uniform1i(LKeyIsPressedUniform,1);
				gl.uniform3fv(laUniformGreen,light_ambientGreen);		//glLightfv() 
				gl.uniform3fv(ldUniformGreen,light_diffuseGreen);
				gl.uniform3fv(lsUniformGreen,light_specularGreen);
				
				light_positionGreen[2]=200.0*Math.cos(light_angel_green);
				light_positionGreen[0]=200.0*Math.sin(light_angel_green);
				gl.uniform4fv(lightPositionUniformGreen,light_positionGreen); //glLightfv() for position


				gl.uniform3fv(laUniformBlue,light_ambientBlue);		//glLightfv() 
				gl.uniform3fv(ldUniformBlue,light_diffuseBlue);
				gl.uniform3fv(lsUniformBlue,light_specularBlue);

				light_positionBlue[0]=200.0*Math.cos(light_angel_blue);
				light_positionBlue[1]=200.0*Math.sin(light_angel_blue);
				gl.uniform4fv(lightPositionUniformBlue,light_positionBlue); //glLightfv() for position

				gl.uniform3fv(kaUniform,material_ambient);
				gl.uniform3fv(kdUniform,material_diffuse);
				gl.uniform3fv(ksUniform,material_specular);	//glMaterialfv();
				gl.uniform1f(materialShininessUniform,materialShininess);
			}
			else
			{
				gl.uniform1i(LKeyIsPressedUniform,0);
			}

			mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-6.0]);
		
		
			gl.uniformMatrix4fv(modelUniform,false,modelViewMatrix);
			gl.uniformMatrix4fv(viewUniform,false,viewMatrix);
		
			gl.uniformMatrix4fv(projectionUniform,false,perspectiveProjectionMatrix);

		}
		else
		{
			gl.useProgram(shaderProgramObjectPerFragment);
			mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-6.0]);
			
			
			gl.uniformMatrix4fv(modelUniformFragment,false,modelViewMatrix);
			gl.uniformMatrix4fv(viewUniformFragment,false,viewMatrix);
			gl.uniformMatrix4fv(projectionUniformFragment,false,perspectiveProjectionMatrix);
		
			if (bLight)
			{
				gl.uniform1i(LKeyIsPressedUniformFragment,1);
				gl.uniform3fv(laUniformRedFragment,light_ambientRed);		//glLightfv() 
				gl.uniform3fv(ldUniformRedFragment,light_diffuseRed);
				gl.uniform3fv(lsUniformRedFragment,light_specularRed);

				light_positionRed[1]=200.0*Math.cos(light_angel_red);
				light_positionRed[2]=200.0*Math.sin(light_angel_red);

				gl.uniform4fv(lightPositionUniformRedFragment,light_positionRed); //glLightfv() for position

				gl.uniform3fv(laUniformGreenFragment,light_ambientGreen);		//glLightfv() 
				gl.uniform3fv(ldUniformGreenFragment,light_diffuseGreen);
				gl.uniform3fv(lsUniformGreenFragment,light_specularGreen);

				light_positionGreen[2]=200.0*Math.cos(light_angel_green);
				light_positionGreen[0]=200.0*Math.sin(light_angel_green);


				gl.uniform4fv(lightPositionUniformGreenFragment,light_positionGreen); //glLightfv() for position


				gl.uniform3fv(laUniformBlueFragment,light_ambientBlue);		//glLightfv() 
				gl.uniform3fv(ldUniformBlueFragment,light_diffuseBlue);
				gl.uniform3fv(lsUniformBlueFragment,light_specularBlue);
				
				light_positionBlue[0]=200.0*Math.cos(light_angel_blue);
				light_positionBlue[1]=200.0*Math.sin(light_angel_blue);

				gl.uniform4fv(lightPositionUniformBlueFragment,light_positionBlue); //glLightfv() for position

				gl.uniform3fv(kaUniformFragment,material_ambient);
				gl.uniform3fv(kdUniformFragment,material_diffuse);
				gl.uniform3fv(ksUniformFragment,material_specular);	//glMaterialfv();
				gl.uniform1f(materialShininessUniformFragment,materialShininess);
			}
			else
			{
				gl.uniform1i(LKeyIsPressedUniformFragment,0);
			}

		}


		sphere.draw();
	gl.useProgram(null);
	if(bRotation)
	{
		update();
	}
	
	requestAnimationFrame(draw,canvas);
}


function update()
{
	light_angel_red=light_angel_red+0.01;
	if(light_angel_red>360.0)
	{
		light_angel_red=0.0;
	}

	light_angel_green=light_angel_green+0.01;
	if(light_angel_green>360.0)
	{
		light_angel_green=0.0;
	}

	light_angel_blue=light_angel_blue+0.01;
	if(light_angel_blue>360.0)
	{
		light_angel_blue=0.0;
	}
}

function keyDown(event)
{

	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;
		case 86://'G'
			if(gbIsPerVertex==false)
			{
				gbIsPerVertex=true;
			}
			else
			{
				gbIsPerVertex=false;
			}
		break;

		case 27:
			uninitialize();
			window.close();
			break;
		case 76:
			if (bLight == false)
			{
				bLight = true;
				LKeyIsPressed = 1;
			}
			else
			{
				bLight = false;
				LKeyIsPressed = 0;
			}
				break;
		case 65:
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;

	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_sphere)
	{
		gl.deleteVertexArray(vao_sphere);
	}
	if(vbo_sphere_position)
	{
		gl.deleteBuffer(vbo_sphere_position);
	}

	if(vbo_sphere_normal)
	{
		gl.deleteBuffer(vbo_sphere_normal);
	}

	if(shaderProgramObjectPerFragment)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObjectPerFragment,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObjectPerFragment,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObjectPerFragment);
	shaderProgramObjectPerFragment=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}



























