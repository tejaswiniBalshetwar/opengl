var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo_square_position;
var vbo_square_texcoord;
var mvpUniform;
var samplerUniform;

var keyPressed=0;
var texture_smiley;

var texCoordVertices=new Float32Array(8);
var perspectiveProjectionMatrix;

const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;" +
	"in vec2 tex_coords;" +
	"out vec2 out_texCoord;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
		"gl_Position=u_mvp_matrix * vPosition;" +
		"out_texCoord=tex_coords;" +
	"}" ; 

	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"precision highp float;" +
	"in vec2 out_texCoord;" +
	"out vec4 FragColor;" +
	"uniform sampler2D u_sampler;" +
	"void main(void)" +
	"{" +
		"FragColor=texture(u_sampler,out_texCoord);" +
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_TEXTURE0,"tex_coords");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	samplerUniform=gl.getUniformLocation(shaderProgramObject,"u_sampler");

	var squareVertices=new Float32Array([
										1.0,1.0,0.0,
										-1.0,1.0,0.0,
										-1.0,-1.0,0.0,
										1.0,-1.0,0.0
										]);

	/*var squareTexcoords=new Float32Array([
								1.0,1.0,
								0.0,1.0,
								0.0,0.0,
								1.0,0.0							
		]);*/

	vao=gl.createVertexArray();
	gl.bindVertexArray(vao);

	vbo_square_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_position);
	gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_square_texcoord=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_texcoord);
	gl.bufferData(gl.ARRAY_BUFFER,texCoordVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);

	texture_smiley=gl.createTexture();
	texture_smiley.image=new Image();
	texture_smiley.image.src="smiley.png";
	texture_smiley.image.onload=function()
	{
		gl.bindTexture(gl.TEXTURE_2D,texture_smiley);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,texture_smiley.image);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}

	perspectiveProjectionMatrix=mat4.create();
}

function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();

		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
		mat4.rotateZ(modelViewMatrix,modelViewMatrix,179.13);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		

		if(keyPressed==1)
	{
		
		texCoordVertices[0]=0.5;
		texCoordVertices[1]=0.5;
		texCoordVertices[2]=0.0;
		texCoordVertices[3]=0.5;
		texCoordVertices[4]=0.0;
		texCoordVertices[5]=0.0;
		texCoordVertices[6]=0.5;
		texCoordVertices[7]=0.0;
	}
	else if(keyPressed==2)
	{
		
		texCoordVertices[0] =  2.0 ;
		texCoordVertices[1] =  2.0 ;
		texCoordVertices[2] =  0.0 ;
		texCoordVertices[3] =  2.0 ;
		texCoordVertices[4] =  0.0 ;
		texCoordVertices[5] =  0.0 ;
		texCoordVertices[6] =  2.0 ;
		texCoordVertices[7] =  0.0 ;
	}
	else if(keyPressed==3)
	{
		texCoordVertices[0] =  1.0 ;
		texCoordVertices[1] =  1.0 ;
		texCoordVertices[2] =  0.0 ;
		texCoordVertices[3] = 1.0 ;
		texCoordVertices[4] =  0.0 ;
		texCoordVertices[5] =  0.0 ;
		texCoordVertices[6] =  1.0 ;
		texCoordVertices[7] =  0.0 ;

	}
	else 
	{
		texCoordVertices[0] =  0.5 ;
		texCoordVertices[1] =  0.5 ;
		texCoordVertices[2] =  0.5 ;
		texCoordVertices[3] =  0.5 ;
		texCoordVertices[4] =  0.5 ;
		texCoordVertices[5] =  0.5 ;
		texCoordVertices[6] =  0.5 ;
		texCoordVertices[7] =  0.5 ;
	}
	
	gl.bindVertexArray(vao);
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_texcoord);
		gl.bufferData(gl.ARRAY_BUFFER,texCoordVertices,gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
		gl.bindTexture(gl.TEXTURE_2D,texture_smiley);
		gl.uniform1i(samplerUniform,0);
		gl.bindVertexArray(vao);
			gl.drawArrays(gl.TRIANGLE_FAN,0,4);
		gl.bindVertexArray(null);
	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
}
function keyDown(event)
{
	
	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 27:
			uninitialize();
			window.close();
			break;
		case 97:
		case 49:
			keyPressed=1;
			console.log(keyPressed)
			break;
		case 98:
		case 50:
			keyPressed=2;
			console.log(keyPressed)
			break;
		case 99:
		case 51:
			keyPressed=3;
			console.log(keyPressed)
			break;
		default:
		keyPressed=0;
			break;
	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
	}
	if(vbo_square_texcoord)
	{
		gl.deleteBuffer(vbo_square_texcoord);
	}

	if(vbo_square_position)
	{
		gl.deleteBuffer(vbo_square_position);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}

