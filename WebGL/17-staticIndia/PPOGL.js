var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_line;
var vbo_line_position;
var vbo_line_color;

var vao_circle;
var vbo_circle_color;
var vbo_circle_position;

var verticesForOuterCircle=new Float32Array(2000);
var colorForCircle=new Float32Array(2000);

var verticesForInnerCircle=new Float32Array(2000);

var mvpUniform;

var perspectiveProjectionMatrix;
var lineColor=new Float32Array(8);
var lineVertices=new Float32Array(6);


const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"out vec4 out_color;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
		"gl_PointSize=2.0;"	+
		"gl_Position=u_mvp_matrix * vPosition;" +
		"out_color=vColor;" +
	"}" ; 

	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
		"FragColor=vec4(out_color);" +
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	var lineVertices=new Float32Array([
		0.0,1.0,0.0,
		0.0,-1.0,0.0,
		]);
	var lineColor=new Float32Array([
		1.0,0.0,0.0,0.0,
		1.0,0.0,0.0,0.0
		]);

	vao_line=gl.createVertexArray();
	gl.bindVertexArray(vao_line);

	vbo_line_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_position);
	gl.bufferData(gl.ARRAY_BUFFER,lineVertices,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_line_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_color);
		gl.bufferData(gl.ARRAY_BUFFER,lineColor,gl.DYNAMIC_DRAW);
		gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,4,gl.FLOAT,false,0,0);
		gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);

	
	var XCenterOfCircle =0.0;
	var YCenterOfCircle = 0.0;

	radius = 1.0;
	calculateVerticesForCircle(radius, parseFloat(XCenterOfCircle), parseFloat(YCenterOfCircle));

	vao_circle=gl.createVertexArray();
	gl.bindVertexArray(vao_circle);
	vbo_circle_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_position);
	gl.bufferData(gl.ARRAY_BUFFER,verticesForInnerCircle,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_circle_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_color);
	gl.bufferData(gl.ARRAY_BUFFER,colorForCircle,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,4,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);

	perspectiveProjectionMatrix=mat4.create();
}


function calculateVerticesForCircle( radius,  XPointOfCenter,  YPointOfCenter)
{
	var x = 0.0, y = 0.0;
	var angle=0.0;
	var RColor = 0.0706;
	var GColor = 0.3831;
	var BColor = 0.02745;
	//vertices[2000];
	for (var i = 0; i < 2000-3; i=i+3)
	{
		 angle = (4 * Math.PI*i) / 2000;
		
			x = XPointOfCenter + radius * Math.cos(angle);
			y =YPointOfCenter + radius * Math.sin(angle);

			verticesForInnerCircle[i] = x;
			verticesForInnerCircle[i + 1] = y;
			verticesForInnerCircle[i + 2] = 0.0;
			//verticesForInnerCircle[i + 3] = 0.0; 
	
	}

	for(var i=0;i<2000-4;i=i+4)
	{
		if (RColor <= 1.0)
		{
			RColor += 0.002;

		}
		if (GColor <= 0.6)
		{
			GColor += 0.002;
		}
		if (BColor <= 0.2)
		{
			BColor += 0.002;
		}
		colorForCircle[i] = RColor;
		colorForCircle[i + 1] = GColor;
		colorForCircle[i + 2] = BColor;
		colorForCircle[i + 3] = 0.0;
	}

}


function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		var modelViewProjectionMatrix=mat4.create();
		
		mat4.translate(modelViewMatrix,modelViewMatrix,[-2.50,0.0,-6.0]);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
		gl.lineWidth=5.0;

		drawLineI();

		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.translate(modelViewMatrix,modelViewMatrix,[-2.0,0.0,-6.0]);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

		drawLineN();
		
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.translate(modelViewMatrix,modelViewMatrix,[circleXTranslation,circleYTranslation,-5.0]);
		mat4.rotateY(modelViewMatrix,modelViewMatrix,angleCircle);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

		drawCircleInnerCircle();
			
	
		gl.useProgram(null);
	//	update();
	requestAnimationFrame(draw,canvas);
}
function drawLineI()
{

		var lineVerticesI=new Float32Array([
			0.0,1.0,0.0,
			0.0,-1.0,0.0
		]);
		var colorForLineI = new Float32Array ([
			1.0,0.6,0.2,0.0,
			0.0706,0.3831,0.02745,0.0
		]);

		gl.bindVertexArray(vao_line);
			gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_position);
				gl.bufferData(gl.ARRAY_BUFFER,lineVerticesI,gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER,null);
			gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_color);
				gl.bufferData(gl.ARRAY_BUFFER,colorForLineI,gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER,null);
			gl.drawArrays(gl.LINES,0,2);
		gl.bindVertexArray(null);
		
}

function drawLineN()
{
	var lineVerticesN =new Float32Array([
			1.0,1.0,0.0,
			1.0,-1.0,0.0,
			0.0,1.0,0.0,
			0.0,-1.0,0.0
		]);
	var colorForLineN = new Float32Array([
			1.0,0.6,0.2,0.0,
			0.0706,0.3831,0.02745,0.0,
			1.0,0.6,0.2,0.0,
			0.0706,0.3831,0.02745,0.0,
			1.0,0.6,0.2,0.0,
			0.0706,0.3831,0.02745,0.0
		]);
	gl.bindVertexArray(vao_line);
			gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_position);
				gl.bufferData(gl.ARRAY_BUFFER,lineVerticesN,gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER,null);
			gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line_color);
				gl.bufferData(gl.ARRAY_BUFFER,colorForLineN,gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER,null);
			gl.drawArrays(gl.LINE_STRIP,0,4);
		gl.bindVertexArray(null);
}
function update()
{
	if (lineYTranslation >= 0.0)
	{
		lineYTranslation = lineYTranslation - 0.02;
	}

	if (circleXTranslation < 0.0 && circleYTranslation < 0.0)
	{
		circleXTranslation = circleXTranslation + 0.02;
		//circleXTranslation = circleXTranslation - (-3.0f - 0.0f)/100;
		circleYTranslation = circleYTranslation + 0.02;
	}

	if (triangleXTranslation >= 0.0 && triangleYTranslation <= 0.0)
	{
		triangleXTranslation = triangleXTranslation - 0.02;
		triangleYTranslation = triangleYTranslation + 0.02;
	}

	angleCircle = angleCircle + 0.20;
	angleTriangle = angleTriangle + 0.20;
	if (circleXTranslation > 0.0 && circleYTranslation > 0.0 && triangleXTranslation<=0.0 && triangleYTranslation >= 0.0)
	{
		angleTriangle = 0.0;
		angleCircle = 0.0;
	}
}


function drawCircleInnerCircle()
{
	gl.bindVertexArray(vao_circle);
		gl.drawArrays(gl.POINTS,0,500);
	gl.bindVertexArray(null);
}


function drawLine()
{
		gl.bindVertexArray(vao_line);

			gl.drawArrays(gl.LINES,0,2);
		gl.bindVertexArray(null);
}
function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 27:
			uninitialize();
			window.close();
			break;
	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_triangle)
	{
		gl.deleteVertexArray(vao_triangle);
	}
	if(vbo_triangle_color)
	{
		gl.deleteBuffer(vbo_triangle_color);
	}

	if(vbo_triangle_position)
	{
		gl.deleteBuffer(vbo_triangle_position);
	}

	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
	}
	if(vbo_square_color)
	{
		gl.deleteBuffer(vbo_square_color);
	}

	if(vbo_square_position)
	{
		gl.deleteBuffer(vbo_square_position);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}

