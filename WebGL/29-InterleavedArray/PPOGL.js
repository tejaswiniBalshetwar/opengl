var gl=null;
var canvas=null;
var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var vao_sphere;
var vbo_sphere_position;
var vbo_sphere_normal;

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var materialShininess=128.0;


var modelUniform;
var viewUnifrom;
var projectionUniform;
var LKeyIsPressedUniform;
var laUniform,ldUniform,lsUniform,kaUniform,kdUniform,ksUniform,materialShininessUniform;

var lightPositionUniform;
var samplerUniform;

var angleCube=0.0;
var cube_texture=0;

var bRotation=false;
var bLight=false

var LKeyIsPressed=0;

var perspectiveProjectionMatrix;
var modelMatrix;
var viewMatrix;

const webGLMacros=
{
	AMC_ATTRIBUTE_POSITION:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
};

//for starting the animation
var requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequsetAnimationFrame||window.msRequestAnimationFrame;

var cancelAnimationFrame=window.CancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||window.oCancelRequsetAnimationFrame||window.oCancelAnimationFrame||window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

//onload function
function main()
{
	canvas=document.getElementById("AMC");

	if(!canvas)
	{
		console.log("Obtaining canvas Failed");
	}
	else
	{
		console.log("Obtaining canvas Succeded");
	}

	//console.log("canvas Width: "+ canvas.width + " And canvas Height: " + canvas.height);

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);	
	window.addEventListener("resize",resize,false);	

	init();

	resize();

	draw();
}

function init() {
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Obtaining 2D Context Failed");
	}
	else
	{
		console.log("Obtaining 2D context Succeded");
	}

	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;

	//vertex Shader
	var vertexShaderSourceCode=
	"#version 300 es" +
	"\n " +
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	"in vec2 vTexCoord;" +
	"in vec3 vColor;" +
	"uniform mat4 u_model_matrix;"+
	"uniform mat4 u_view_matrix;" +
	"uniform mat4 u_projection_matrix;"+
	"uniform int u_LKeyIsPressed;"  +
	"uniform vec4 u_light_position;"  +
	"out vec3 out_light_direction;" +
	"out vec3 out_t_normal;" +
	"out vec3 out_viewer_vector;" +
	"out vec3 out_color;" +
	"out vec2 out_TexCoord;" +

	"void main(void)" +
	"{" +
		"out_color=vColor;" +
		"out_TexCoord= vTexCoord;" +
		"if(u_LKeyIsPressed==1)" +
		"{" +
			"vec4 eye_coordinates= u_view_matrix* u_model_matrix * vPosition;" +
			"out_t_normal=mat3(u_model_matrix* u_view_matrix)* vNormal;" +
			"out_light_direction=vec3(u_light_position - eye_coordinates);" +
			"out_viewer_vector=vec3(-eye_coordinates);" +
		"}" +
		
		"gl_Position=u_projection_matrix* u_view_matrix *u_model_matrix*vPosition;" +
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n " +
		"precision highp float;" +
		"precision highp int;" +
		"uniform vec3 u_la; " +
		"uniform vec3 u_ld; " +
		"uniform vec3 u_ls; " +
		"uniform vec3 u_ka;"  +
		"uniform vec3 u_kd;"  +
		"uniform vec3 u_ks;"  +
		"uniform float u_material_shininess;" +
		"uniform int u_LKeyIsPressed;" +
		"in vec3 out_light_direction;" +
		"in vec3 out_viewer_vector;" +
		"in vec3 out_t_normal;"+
		"in vec3 out_color;"+
		"in vec2 out_TexCoord;"+
		"uniform sampler2D u_sampler;" +
		"out vec4 fragColor;" +
		
		"void main(void)" +
		"{" +
			"vec3 out_phong_ads_light=vec3(0.0);" +
			"vec4 texSampler=texture(u_sampler,out_TexCoord);" +
			"if(u_LKeyIsPressed==1)" +
			"{" +
				"vec3 normalized_light_direction=normalize(out_light_direction);" +
				"vec3 normalized_t_norm=normalize(out_t_normal);" +
				"float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);" +
				"vec3 reflection_vector=reflect(-normalized_light_direction,normalized_t_norm);"+			
				"vec3 normalized_viewer_vector=normalize(out_viewer_vector);"+
				"vec3 ambient=u_la * u_ka;" +
				"vec3 diffuse=u_ld * u_kd * t_dot_ld;" +
				"vec3 specular=u_ls* u_ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);" +
				"out_phong_ads_light=ambient + diffuse + specular;" +
			"}" +
			"else"+ 
			"{" +
				"out_phong_ads_light=vec3(1.0,1.0,1.0);" +
			"}"+
				"vec3 color=vec3(texSampler) * out_color *out_phong_ads_light;" +
			"fragColor=vec4(color,1.0);" +				
		"}";		

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	//prelinking
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	gl.bindAttribLocation(shaderProgramObject,webGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}	

	modelUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
	viewUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
	projectionUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LKeyIsPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyIsPressed");
	laUniform=gl.getUniformLocation(shaderProgramObject,"u_la");
	ldUniform=gl.getUniformLocation(shaderProgramObject,"u_ld");
	lsUniform=gl.getUniformLocation(shaderProgramObject,"u_ls");

	kaUniform=gl.getUniformLocation(shaderProgramObject,"u_ka");
	kdUniform=gl.getUniformLocation(shaderProgramObject,"u_kd");
	ksUniform=gl.getUniformLocation(shaderProgramObject,"u_ks");
	lightPositionUniform=gl.getUniformLocation(shaderProgramObject,"u_light_position");
	materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");

	samplerUniform=gl.getUniformLocation(shaderProgramObject,"u_sampler");


	
	var squareVCNT=new Float32Array([
				1.0,1.0,1.0,	1.0,0.0,0.0,		0.0,0.0,1.0,  	1.0,1.0,
				-1.0,1.0,1.0,  	1.0,0.0,0.0,		0.0,0.0,1.0,  	0.0,1.0,
				-1.0,-1.0,1.0, 	1.0,0.0,0.0,		0.0,0.0,1.0,	0.0,0.0,
				1.0,-1.0,1.0,  	1.0,0.0,0.0,		0.0,0.0,1.0,	1.0,0.0,

				1.0,1.0,-1.0,	0.0,1.0,0.0,		1.0,0.0,0.0,  	1.0,1.0,
				1.0,1.0,1.0,	0.0,1.0,0.0,		1.0,0.0,0.0,	0.0,1.0,
				1.0,-1.0,1.0,	0.0,1.0,0.0,		1.0,0.0,0.0,	0.0,0.0,
				1.0,-1.0,-1.0,	0.0,1.0,0.0,		1.0,0.0,0.0,	1.0,0.0,

				1.0,1.0,-1.0,	0.0,0.0,1.0,		0.0,0.0,-1.0,	1.0,1.0,
				-1.0,1.0,-1.0,	0.0,0.0,1.0,		0.0,0.0,-1.0,	0.0,1.0,
				-1.0,-1.0,-1.0,	0.0,0.0,1.0,		0.0,0.0,-1.0,	0.0,0.0,
				1.0,-1.0,-1.0,	0.0,0.0,1.0,		0.0,0.0,-1.0,	1.0,0.0,

				-1.0,1.0,1.0,	0.0,1.0,1.0,		-1.0,0.0,0.0,	1.0,1.0,
				-1.0,1.0,-1.0,	0.0,1.0,1.0,		-1.0,0.0,0.0,	0.0,1.0,
				-1.0,-1.0,-1.0,	0.0,1.0,1.0,		-1.0,0.0,0.0,	0.0,0.0,
				-1.0,-1.0,1.0,	0.0,1.0,1.0,		-1.0,0.0,0.0,	1.0,0.0,

				1.0,1.0,-1.0,	1.0,0.0,1.0,		0.0,1.0,0.0,	1.0,1.0,
				-1.0,1.0,-1.0,	1.0,0.0,1.0,		0.0,1.0,0.0,	0.0,1.0,
				-1.0,1.0,1.0,	1.0,0.0,1.0,		0.0,1.0,0.0,	0.0,0.0,
				1.0,1.0,1.0,	1.0,0.0,1.0,		0.0,1.0,0.0,	1.0,0.0,

				1.0,-1.0,-1.0,	1.0,1.0,0.0,		0.0,-1.0,0.0,	1.0,1.0,
				-1.0,-1.0,-1.0,	1.0,1.0,0.0,		0.0,-1.0,0.0,	0.0,1.0,
				-1.0,-1.0,1.0,	1.0,1.0,0.0,		0.0,-1.0,0.0,	0.0,0.0,
				1.0,-1.0,1.0,	1.0,1.0,0.0,		0.0,-1.0,0.0,	1.0,0.0				
	]);

	

	
	vao_square=gl.createVertexArray();
	gl.bindVertexArray(vao_square);

	vbo_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER,squareVCNT,gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,11*4,0*4);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);

	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,11*4,4*3);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);

	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,11*4,4*6);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_NORMAL);

	gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,11*4,4*9);
	gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);

	cube_texture=gl.createTexture();
	cube_texture.image=new Image();
	cube_texture.image.src="marble.png";
	cube_texture.image.onload=function()
	{
		gl.bindTexture(gl.TEXTURE_2D,cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,cube_texture.image);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}

	//gl.enable(gl.TEXTURE_2D);
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.disable(gl.CULL_FACE)

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.clearDepth(1.0);
	//gl.enable(gl.FRONT_FACE);

	perspectiveProjectionMatrix=mat4.create();

}

function resize() {
	if(bFullscreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;		
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);

	mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
		
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
		var modelViewMatrix=mat4.create();
		var viewMatrix=mat4.create();
		
		var modelViewProjectionMatrix=mat4.create();

		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-6.0]);
		mat4.rotateX(modelViewMatrix,modelViewMatrix,angleCube);
		mat4.rotateY(modelViewMatrix,modelViewMatrix,angleCube);
		mat4.rotateZ(modelViewMatrix,modelViewMatrix,angleCube);

		gl.uniformMatrix4fv(modelUniform,false,modelViewMatrix);
		gl.uniformMatrix4fv(viewUniform,false,viewMatrix);
	
		gl.uniformMatrix4fv(projectionUniform,false,perspectiveProjectionMatrix);

		if (bLight)
		{
			gl.uniform1i(LKeyIsPressedUniform,1);
			gl.uniform3fv(laUniform,light_ambient);		//glLightfv() 
			gl.uniform3fv(ldUniform,light_diffuse);
			gl.uniform3fv(lsUniform,light_specular);
			gl.uniform4fv(lightPositionUniform,light_position); //glLightfv() for position

			gl.uniform3fv(kaUniform,material_ambient);
			gl.uniform3fv(kdUniform,material_diffuse);
			gl.uniform3fv(ksUniform,material_specular);	//glMaterialfv();
			gl.uniform1f(materialShininessUniform,materialShininess);
		}
		else
		{
			gl.uniform1i(LKeyIsPressedUniform,0);
		}

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D,cube_texture);
		gl.uniform1i(samplerUniform,0);

		gl.bindVertexArray(vao_square);
			gl.drawArrays(gl.TRIANGLE_FAN ,0,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,4,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,8,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,12,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,16,4);
			gl.drawArrays(gl.TRIANGLE_FAN ,20,4);
		gl.bindVertexArray(null);
	gl.useProgram(null);
	if(bRotation)
	{
		update();

	}
	requestAnimationFrame(draw,canvas);
}


function update()
{
	angleCube=angleCube+0.01;
	if(angleCube>360.0)
	{
		angleCube=0.0;
	}
}

function keyDown(event)
{
	console.log(event.keyCode)
	switch(event.keyCode)
	{
		case 70:
		toggleFullscreen();
		break;

		case 27:
			uninitialize();
			window.close();
			break;
		case 76:
			if (bLight == false)
			{
				bLight = true;
				LKeyIsPressed = 1;
			}
			else
			{
				bLight = false;
				LKeyIsPressed = 0;
			}
				break;
		case 65:
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;

	}
}
function mouseDown() {
	
}

function uninitialize()
{
	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			vertexShaderObject=null;
		}
	}
	gl.deleteProgram(shaderProgramObject);
	shaderProgramObject=null;
}


function toggleFullscreen()
{
	var fullScreen_element=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement||null;

	if(fullScreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullscreen=false;
	}
}



























