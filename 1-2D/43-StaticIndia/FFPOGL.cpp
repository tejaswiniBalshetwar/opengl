#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"user32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

WINDOWPLACEMENT wp_Prev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

bool gbIsFullScreen=false;
bool gbIsActiveWindow=false;

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{   
    //function declarations
    int initialize(void);
    void display(void);
    void ToggleFullscreen(void);


    //variable declarations
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR lpszClassName[]=TEXT("My OPENGL APPLICATION");

    bool bDone=false;
    int iRet=0;

    fopen_s(&gpFile,"log.txt","w");

    if(gpFile==NULL)
    {
        MessageBox(NULL,TEXT("Error while opening File..Exiting"),TEXT("ERROR!!!!!"),MB_OKCANCEL);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"File opened successfully\n");
         fflush(gpFile);
    }

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wndclass.cbClsExtra=0;
    wndclass.cbWndExtra=0;
    wndclass.lpszMenuName=NULL;
    wndclass.lpfnWndProc=WndProc;
    wndclass.hInstance=hInstance;
    wndclass.lpszClassName=lpszClassName;
    wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

    RegisterClassEx(&wndclass);

    hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("FFP OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
    ghwnd=hwnd;

    iRet=initialize();
    if(iRet==-1)
    {
        fprintf(gpFile,"Error in initialize at ChoosePixelFormat\n");
         fflush(gpFile);
        exit(0);
    }
    else if(iRet==-2)
    {
        fprintf(gpFile,"Error in initialize at SetPixelFormat\n");
         fflush(gpFile);
        exit(0);
    }
    else if(iRet==-3)
    {
        fprintf(gpFile,"Error in initialize at wglCreateContext\n");
         fflush(gpFile);
        exit(0);
    }
    else if(iRet==-4)
    {
        fprintf(gpFile,"Error in initilaize at wglMakeCurrent\n");
         fflush(gpFile);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"Initialize Function executed successfully......");
         fflush(gpFile);
    }


    ToggleFullscreen();
   
    SetFocus(hwnd);
    SetForegroundWindow(hwnd);
    ShowWindow(hwnd,iCmdShow);
   

   
    
    //game Loop
    while(bDone==false)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }

        }
        else 
        {
            if(gbIsActiveWindow==true)
            {
                //upddate
            }
            display();
        }

    }

    return ((int)msg.wParam);


}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    //function declarations
    void resize(int,int);
    void uninitialize(void);

    switch(iMsg)
    {
        case WM_SETFOCUS:
            gbIsActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbIsActiveWindow=false;
            break;
        case WM_ERASEBKGND:
            return(0);
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_SIZE:
            resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
            }
            break;
        case WM_DESTROY:
            uninitialize();
            PostQuitMessage(0);
            break;
    } 

    return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
   
    MONITORINFO mi;
    if(gbIsFullScreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi={sizeof(MONITORINFO)};                              
            if(GetWindowPlacement(ghwnd,&wp_Prev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
            {
                SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

            }
            ShowCursor(FALSE);
            gbIsFullScreen=true;
        }
    }
 else
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wp_Prev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

        ShowCursor(TRUE);
        gbIsFullScreen=false;
    }
}

int initialize(void)
{
    void resize(int ,int);
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormat=0;

    memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cGreenBits=8;
    pfd.cBlueBits=8;
    pfd.cAlphaBits=8;

    ghdc=GetDC(ghwnd);

    iPixelFormat=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelFormat==0)
    {
        return -1;
    }

    if(SetPixelFormat(ghdc,iPixelFormat,&pfd)==FALSE)
    {
        return -2;
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
        return -3;
    }

    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
        return -4;
    }

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    resize(WIN_WIDTH,WIN_HEIGHT);

    return 0;

}

void display(void)
{
    //function declaration
    void drawI();
    void drawN();
    void drawD();
   // void drawI();
    void drawA();
    
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-2.5f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-2.0f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawN();
    glEnd();


    //Drawing D's Line
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-10.0f);
    glPointSize(3.0f);
    glBegin(GL_POINTS);
        drawD();
    glEnd();

    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(1.0f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(2.0f,0.0f,-10.0f);
    glLineWidth(4.0f);
    glBegin(GL_LINES);
        drawA();
    glEnd();

    SwapBuffers(ghdc);
}


void resize(int width,int height)
{
    if(height==0)
    {
        height=1;
        
    }

        glViewport(0,0,GLsizei(width),GLsizei(height));

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize(void)
{
    if(gbIsFullScreen)
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wp_Prev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

        ShowCursor(TRUE);
        gbIsFullScreen=false;
    }

    if(wglGetCurrentContext()==ghrc)
    {
        wglMakeCurrent(NULL,NULL);
    }
    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }

    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"Program executed Successfully ....EXITING\n\n\n");
        fflush(gpFile);
        fclose(gpFile);
        gpFile=NULL;
    }
}

void drawI(void)
{
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);
}

void drawN(void)
{
    //|\|
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
  


}

void drawD()
{
    // |) -
    /*glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(-1.0f,1.0f,0.0f);*/

    //|
   /* glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(-1.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-1.0f,-1.0f,0.0f);*/

    //-
    /*glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-1.0f,-1.0f,0.0f);    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
*/
    //|
   /* glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);*/

   GLfloat RColor=0.0706f;
    GLfloat GColor=0.3831f;
    GLfloat BColor=0.02745f;
    for(GLfloat angle=(3*M_PI/2.0f);angle<=(5*M_PI)/2;angle+=0.02f)
    {
        glVertex3f(cos(angle),sin(angle),0.0f);
        glColor3f(RColor,GColor,BColor);
        if(RColor<=1.0)
        {
            RColor+=0.006f;
            
        }
        if(GColor<=0.6)
        {
            GColor+=0.006f;
        }
        if(BColor<=0.2)
        {
            BColor+=0.006f;
        }
    }


}

void drawA()
{
    //variable declarations
    GLfloat x1ForAHorizontalLine=0.0f;
    GLfloat y1ForAHorizontalLine=0.0f;
    
    GLfloat x2ForAHorizontalLine=0.0f;
    GLfloat y2ForAHorizontalLine=0.0f;
    


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.50f,-1.0f,0.0f);

    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-0.50f,-1.0f,0.0f);


      
    x1ForAHorizontalLine=(0.50f+0.0f)/2.0f;
    y1ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;
    
    x2ForAHorizontalLine=(-0.50f+(0.0f))/2.0f;
    y2ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;

    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine,0.0f);
    glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine,0.0f);

    glColor3f(1.0f,1.0f,1.0f);
    glVertex3f(x1ForAHorizontalLine-0.01f,y1ForAHorizontalLine-0.04f,0.0f);
    glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine-0.04f,0.0f);

    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine-0.1f,0.0f);
    glVertex3f(x2ForAHorizontalLine-0.02f,y2ForAHorizontalLine-0.1f,0.0f);
    
}
