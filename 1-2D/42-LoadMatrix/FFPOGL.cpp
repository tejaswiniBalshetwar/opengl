#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<stdio.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"user32.lib")

HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwStyle;

FILE *gpFile=NULL;

bool gbIsFullScreen=false;
bool gbIsActiveWindow=false;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//matrix declarations
GLfloat IdentityMatrix[16];
GLfloat TranslationMatrix[16];
GLfloat RotationMatrix[16];

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{

    //function declarations
    int initialize(void);
    void display(void);


    //variable declarations
    WNDCLASSEX wndclass;
    TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
    MSG msg;
    HWND hwnd;

    int iRet=0;
    bool bDone= false;

    //code
    fopen_s(&gpFile,"log.txt","w");
    if(gpFile==NULL)
    {
        MessageBox(NULL,TEXT("Error while opening File ..EXITING"),TEXT("ERROR!!"),MB_OKCANCEL);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"\nfile opened successfully..");
    }

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wndclass.cbClsExtra=0;
    wndclass.cbWndExtra=0;
    wndclass.lpszMenuName=NULL;
    wndclass.lpszClassName=lpszClassName;
    wndclass.lpfnWndProc=WndProc;
    wndclass.hInstance=hInstance;
    wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

    RegisterClassEx(&wndclass);

    hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY FFP OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
    ghwnd=hwnd;

    iRet=initialize();
    if(iRet==-1)
    {
        fprintf(gpFile,"\nError in initialize at ChoosePixelFormat");
        exit(0);
    }
    else if(iRet==-2)
    {
        fprintf(gpFile,"Error in initialize at SetPixelFormat");
        exit(0);
    }
    else if(iRet==-3)
    {
        fprintf(gpFile,"Error in initialize at wglCreateContext");
        exit(0);
    }
    else if(iRet==-4)
    {
        fprintf(gpFile,"Error in initialize at wglMakeCurrent");
        exit(0);
    }
    else
    {   
         fprintf(gpFile,"initialize function successfully...");
    }

    SetFocus(hwnd);
    SetForegroundWindow(hwnd);
    ShowWindow(hwnd,iCmdShow);
    
    //game loop
    while(bDone==false)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }
            else
            {   
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }   
        }
        else
        {
            if(gbIsActiveWindow==true)
            {
                //update
            }
            display();
        }
    }    

    return ((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    //function declarations
    void uninitialize(void);
    void ToggleFullscreen(void);
    void resize(int,int);

    //code
    switch(iMsg)
    {
        case WM_SETFOCUS:
            gbIsActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbIsActiveWindow=false;
            break;
        case WM_ERASEBKGND:
            return (0);
        case WM_SIZE:
            resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
                case 0x46:
                    ToggleFullscreen();
                    break;
            }
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            uninitialize();
            PostQuitMessage(0);
            break;
    }

    return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
    MONITORINFO mi;
    if(gbIsFullScreen==false)
    {
            tb_dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
            if(tb_dwStyle & WS_OVERLAPPEDWINDOW)
            {
                mi={sizeof(MONITORINFO)};
                if(GetWindowPlacement(ghwnd,&tb_wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
                {
                    SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle &~ WS_OVERLAPPEDWINDOW);
                    SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
                }
                ShowCursor(FALSE);
                gbIsFullScreen=true;
            } 
    }
    else
    {
        SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle|WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&tb_wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

        ShowCursor(TRUE);
        gbIsFullScreen=false;
    }
}

int initialize(void)
{	
	//function declarations

	void resize(int,int);


	//variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex=0;

    memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cGreenBits=8;
    pfd.cBlueBits=8;
    pfd.cAlphaBits=8;

    ghdc=GetDC(ghwnd);

    iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelFormatIndex==0)
    {
    	return -1;
    }

    if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
    {
    	return -2;
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
    	return -3;
    }

    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
    	return -4;
    }

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    resize(WIN_WIDTH,WIN_HEIGHT);


    IdentityMatrix[0]=1.0f;
    IdentityMatrix[1]=0.0f;
    IdentityMatrix[2]=0.0f;
    IdentityMatrix[3]=0.0f;

    IdentityMatrix[4]=0.0f;
    IdentityMatrix[5]=1.0f;
    IdentityMatrix[6]=0.0f;
    IdentityMatrix[7]=0.0f;

    IdentityMatrix[8]=0.0f;
    IdentityMatrix[9]=0.0f;
    IdentityMatrix[10]=1.0f;
    IdentityMatrix[11]=0.0f;

    IdentityMatrix[12]=0.0f;
    IdentityMatrix[13]=0.0f;
    IdentityMatrix[14]=0.0f;
    IdentityMatrix[15]=1.0f;

    TranslationMatrix[0]=1.0f;
    TranslationMatrix[1]=0.0f;
    TranslationMatrix[2]=0.0f;
    TranslationMatrix[3]=0.0f;

    TranslationMatrix[4]=0.0f;
    TranslationMatrix[5]=1.0f;
    TranslationMatrix[6]=0.0f;
    TranslationMatrix[7]=0.0f;

    TranslationMatrix[8]=0.0f;
    TranslationMatrix[9]=0.0f;
    TranslationMatrix[10]=1.0f;
    TranslationMatrix[11]=0.0f;

    TranslationMatrix[12]=0.0f;
    TranslationMatrix[13]=0.0f;
    TranslationMatrix[14]=-6.0f;
    TranslationMatrix[15]=1.0f;

    return 0;
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	
    static GLfloat triangleRotationAngle=0.0f;

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(IdentityMatrix);

    glMultMatrixf(TranslationMatrix);

    RotationMatrix[0]=cos(triangleRotationAngle);
    RotationMatrix[1]=0;
    RotationMatrix[2]=-sin(triangleRotationAngle);
    RotationMatrix[3]=0;

    RotationMatrix[4]=0;
    RotationMatrix[5]=1;
    RotationMatrix[6]=0;
    RotationMatrix[7]=0;

    RotationMatrix[8]=sin(triangleRotationAngle);
    RotationMatrix[9]=0;
    RotationMatrix[10]=cos(triangleRotationAngle);
    RotationMatrix[11]=0;

    RotationMatrix[12]=0;
    RotationMatrix[13]=0;
    RotationMatrix[14]=0;
    RotationMatrix[15]=1;

    glMultMatrixf(RotationMatrix);

    glBegin(GL_TRIANGLES);
        glColor3f(1.0f,0.5f,0.0f);
        glVertex3f(0.0f,1.0f,0.0f);

        glColor3f(0.0f,1.0f,0.5f);
        glVertex3f(-1.0f,-1.0f,0.0f);

        glColor3f(0.5f,0.0f,1.0f);
        glVertex3f(1.0f,-1.0f,0.0f);
    glEnd();

	SwapBuffers(ghdc);

    triangleRotationAngle=triangleRotationAngle+0.008f;
    if(triangleRotationAngle>360.0)
    {
        triangleRotationAngle=0.0f;
    }
}

void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

}

void uninitialize()
{
    if(gbIsFullScreen)
    {
        SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle|WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&tb_wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

        ShowCursor(TRUE);
        gbIsFullScreen=false;
    }

    if(wglGetCurrentContext()==ghrc)
    {
        wglMakeCurrent(NULL,NULL);

    }

    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }

    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"\nProgram Executed Successfully....EXITING THE PROGRAM");
        fclose(gpFile);
        gpFile=NULL;
    }
}