#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>


#define _USE_MATH_DEFINES 1
#include<math.h>

#include "header.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"Winmm.lib")


HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

WINDOWPLACEMENT wp_prev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

FILE *gpFile=NULL;

bool gbIsActiveWindow=false;
bool gbIsFullScreen=false;

GLfloat translateI=-7.0f;

GLfloat translateA=7.0f;
GLfloat translateN=5.0f;
GLfloat translateI2=-5.0f;

GLfloat transperancyOfSquare=1.0f;


bool ShowPlanes=false;
bool ShowTricolor=false;


/*Global variables for plane translation*/
GLfloat incAngle=270.0f;
GLfloat upperPlaneAngle=M_PI;
GLfloat upperPlaneStopAngle=(3.0f*M_PI)/2.0f;

GLfloat lowerPlaneAngle=M_PI;
GLfloat decAngle=90.0f;

GLfloat incAngle2=0.0f;
GLfloat upperPlaneAngle2=(3.0f*M_PI)/2.0f;
GLfloat upperPlaneStopAngle2=2.5f*M_PI;

GLfloat lowerPlaneAngle2=(M_PI/2.0f);
GLfloat decAngle2=360.0f;

GLfloat translateAllInXDirection=-2.499705f;

bool meet=false; 
bool crossTheA=false;
GLfloat translateMiddle=-7.1f;

GLfloat XTranslatePointUP=0.0f;
GLfloat YTranslatePointUP=0.0f;

GLfloat XTranslatePointDown=0.0f;
GLfloat YTranslatePointDown=0.0f;


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{
    //function declarations
    int initialize(void);
    void display(void);
    void ToggleFullscreen(void);

    //variable declarations
    WNDCLASSEX wndclass;
    MSG msg;
    HWND hwnd;
    TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");

    bool bDone=false;
    int iRet=0;

    fopen_s(&gpFile,"log.txt","w");
    if(gpFile==NULL)
    {
        MessageBox(NULL,TEXT("Error while opening file....exiting"),TEXT("ERROR!!!!"),MB_OKCANCEL);
        exit(0);
    }
    else
    {
        fprintf(gpFile,"File opened successfully...\n");
    }

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wndclass.cbClsExtra=0;
    wndclass.cbWndExtra=0;
    wndclass.lpszMenuName=NULL;
    wndclass.lpszClassName=lpszClassName;
    wndclass.hInstance=hInstance;
    wndclass.lpfnWndProc=WndProc;
    wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

    RegisterClassEx(&wndclass);
    hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY FFPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
    ghwnd=hwnd;

    iRet=initialize();
    if(iRet==-1)
    {
        fprintf(gpFile,"Error in initialize at ChoosePixelFormat\n");
        exit(0);
    }
    else if(iRet==-2)
    {
        fprintf(gpFile,"Error in initialize at SetPixelFormat\n");
        exit(0);
    }
    else if(iRet==-3)
    {
        fprintf(gpFile,"Error in initialize at wglCreateContext\n");
        exit(0);
    }
    else if(iRet==-4)
    {
        fprintf(gpFile,"Error in initialize at wglMakeCurrent\n");
        exit(0);
    }
    else
    {
        fprintf(gpFile,"initialize function executed successfully......");
    }

    SetFocus(hwnd);
    SetForegroundWindow(hwnd);
    ShowWindow(hwnd,iCmdShow);

    ToggleFullscreen();
    
    //game loop
    while(bDone==false)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }
            else 
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if(gbIsActiveWindow==true)
            {
                //update
            }
            display();
        }
        
    }

    return((int)msg.wParam);
    
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    //function declarations
    void resize(int,int);
    void uninitialize(void);


    switch(iMsg)
    {

        case WM_CREATE:
            PlaySound(MAKEINTRESOURCE(MYBACKGROUNDSOUND),GetModuleHandle(NULL),SND_ASYNC|SND_RESOURCE);
            break;
        case WM_SETFOCUS:
            gbIsActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbIsActiveWindow=false;
            break;
        case WM_ERASEBKGND:
            return(0);
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_SIZE:
            resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
            }
            break;
        case WM_DESTROY:
            uninitialize();
            PostQuitMessage(0);
            break;

    }

    return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
    MONITORINFO mi;
    if(gbIsFullScreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi={sizeof(MONITORINFO)};
            if(GetWindowPlacement(ghwnd,&wp_prev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
            {
                SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
            }
        }
        
        ShowCursor(FALSE);
        gbIsActiveWindow=true;
       
    }
     else
        {
            SetWindowLong(ghwnd,GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
            SetWindowPlacement(ghwnd,&wp_prev);
            SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE|SWP_FRAMECHANGED);

            ShowCursor(TRUE);
            gbIsFullScreen=false;
        }
        
}

int initialize(void)
{
    void resize(int,int);
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormat=0;

    memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cBlueBits=8;
    pfd.cGreenBits=8;
    pfd.cAlphaBits=8;

    ghdc=GetDC(ghwnd);
    iPixelFormat=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelFormat==0)
    {
        return -1;
    }
    if(SetPixelFormat(ghdc,iPixelFormat,&pfd)==FALSE)
    {
        return -2;
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
        return -3;
    }
    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
        return -4;
    }

    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    resize(WIN_WIDTH,WIN_HEIGHT);

    return 0;
}


void display(void)
{
    //function declaration
    void drawI();
    void drawN();
    void drawD();
   // void drawI();
    void drawA();
    void drawIndia(void);
    void translationsOfIndia(void);
    void PlaneTranslations(void);
    void drawPlane(void);
    void drawUpperPlane(void);
    void drawLowerPlane(void);
    void drawMiddelePlane(void);
     
    //variable declarations
  
    glClear(GL_COLOR_BUFFER_BIT);

    drawIndia();
   
   if(ShowPlanes)
   {
        //upperplane
        drawUpperPlane();

        //Lower Plane
        drawLowerPlane();
       
        //Middle Plane
        drawMiddelePlane();

   }

    SwapBuffers(ghdc);

    translationsOfIndia();
    if(ShowPlanes)
    {
        PlaneTranslations();
    }

   
}




void resize(int width,int height)
{
    if(height==0)
    {
        height=1;
    }
    glViewport(0,0,GLsizei(width),GLsizei(height));

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();


    gluPerspective(45.0f,GLfloat(width)/(GLfloat)height,0.1f,100.0f);

}

void uninitialize(void)
{
    if(gbIsFullScreen)
    {
        SetWindowLong(ghwnd,GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd,&wp_prev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE|SWP_FRAMECHANGED);

        ShowCursor(TRUE);
        gbIsFullScreen=false;
    }

    if(wglGetCurrentContext()==ghrc)
    {
        wglMakeCurrent(NULL,NULL);
    }
    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }
    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }
    \
    if(gpFile)
    {
        fprintf(gpFile,"Program executed successfully....EXITING");
        fclose(gpFile);
        gpFile=NULL;
    }
}





void drawIndia()
{
    void drawI();
    void drawN();
    void drawD();
   // void drawI();
    void drawA();

    //variable dedclarations

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(-2.5f,0.0f,-8.0f);
    glTranslatef(translateI,0.0f,-8.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();


    /*-------N----------*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    /*glTranslatef(-2.0f,0.0f,-8.0f);*/
    glTranslatef(-2.0f,translateN,-8.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawN();
    glEnd();


    /*-------------D---------------------*/
   

    //Drawing D's Line
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-8.0f);

    glLineWidth(5.0f);
    //glColor3f(RColor,GColor,BColor);
    glBegin(GL_LINES);
        drawI();
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-8.0f);
    glPointSize(3.0f);
    glBegin(GL_POINTS);
        drawD();
    glEnd();

     glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-8.0f);
    glBegin(GL_QUADS);
        glColor4f(0.0f,0.0f,0.0f,transperancyOfSquare);
        glVertex3f(-0.30f,1.20f,0.0f);

        glVertex3f(-0.3,-1.20,0.0f);
        glVertex3f(1.20f,-1.20f,0.0f);
        glVertex3f(1.20f,1.20f,0.0f);
    glEnd();


    /*------------------I--------------------*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
   /* glTranslatef(1.0f,0.0f,-8.0f);*/
     glTranslatef(1.0f,translateI2,-8.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();


    /*--------------A-------------*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(2.0f,0.0f,-8.0f);
    glTranslatef(translateA,0.0f,-8.0f);
    
    glLineWidth(4.0f);
    glBegin(GL_LINES);
        drawA();
    glEnd();

}


void translationsOfIndia(void)
{
     if(translateI<=-2.5f)
    {
        translateI=translateI+0.005f;
    }


    if(translateA>=2.0f && translateI>=-2.5f )
    {
       //fprintf(gpFile, "translateA=%f\n",translateA);
        translateA=translateA-0.05f;
    }

    if(translateN>=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        translateN=translateN-0.05f;
    }

    if( translateI2 <= 0.0f && translateN<=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        translateI2=translateI2+0.05f;
    }

    if( transperancyOfSquare>=0.0f && translateI2 >= 0.0f && translateN<=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        transperancyOfSquare=transperancyOfSquare-0.05;
    }

    if(translateI2 >= 0.0f && translateN<=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        ShowPlanes=true;
    }
   
    if(XTranslatePointUP>=3.499705 && XTranslatePointDown>=3.499705 && translateMiddle>=3.499705)
    {
        ShowTricolor=true;
    }

}


void drawI(void)
{
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);
}

void drawN(void)
{
    //|\|
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
  


}

void drawD()
{
   
   GLfloat RColor=0.0706f;
    GLfloat GColor=0.3831f;
    GLfloat BColor=0.02745f;
    for(GLfloat angle=(3*M_PI/2.0f);angle<=(5*M_PI)/2;angle+=0.02f)
    {
        glVertex3f(cos(angle),sin(angle),0.0f);
        glColor3f(RColor,GColor,BColor);
        if(RColor<=1.0)
        {
            RColor+=0.006f;
            
        }
        if(GColor<=0.6)
        {
            GColor+=0.006f;
        }
        if(BColor<=0.2)
        {
            BColor+=0.006f;
        }
    }


}

void drawA()
{
    //variable declarations
    GLfloat x1ForAHorizontalLine=0.0f;
    GLfloat y1ForAHorizontalLine=0.0f;
    
    GLfloat x2ForAHorizontalLine=0.0f;
    GLfloat y2ForAHorizontalLine=0.0f;
    


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.50f,-1.0f,0.0f);

    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-0.50f,-1.0f,0.0f);


      
    x1ForAHorizontalLine=(0.50f+0.0f)/2.0f;
    y1ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;
    
    x2ForAHorizontalLine=(-0.50f+(0.0f))/2.0f;
    y2ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;

    if(ShowTricolor)
   {
       glColor3f(1.0f,0.6f,0.2f);
       glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine,0.0f);
       glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine,0.0f);
   
       glColor3f(1.0f,1.0f,1.0f);
       glVertex3f(x1ForAHorizontalLine-0.01f,y1ForAHorizontalLine-0.04f,0.0f);
       glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine-0.04f,0.0f);
   
       glColor3f(0.0706f,0.3831f,0.02745f);
       glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine-0.08f,0.0f);
       glVertex3f(x2ForAHorizontalLine-0.02f,y2ForAHorizontalLine-0.08f,0.0f);
   }
    
}


void drawUpperPlane(void)
{
    void drawPlane(void);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLineWidth(8.0f); 
     if(meet==false)
     {
        XTranslatePointUP=-2.5f+4.60f*cos(upperPlaneAngle);
        YTranslatePointUP=4.60f+4.60f*sin(upperPlaneAngle);
        //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointUP,YTranslatePointUP);
        glTranslatef(XTranslatePointUP ,YTranslatePointUP,-10.0f);
        if(incAngle<=360.0f || incAngle>0.0f)
        {
            glRotatef(incAngle,0.0f,0.0f,1.0f);
        }
     }
     else if(translateAllInXDirection>3.50f)
     {
       
         XTranslatePointUP=2.5f+4.60f*cos(upperPlaneAngle2);
         YTranslatePointUP=4.60f+4.60f*sin(upperPlaneAngle2);
        // fprintf(gpFile, "XTranslatePoint2:%f  YTranslatePoint2%f\n",XTranslatePointUP2,YTranslatePointUP2);
        glTranslatef(XTranslatePointUP ,YTranslatePointUP,-10.0f);

        if(incAngle2<=90.0f || incAngle2>0.0f)
        {
            glRotatef(incAngle2,0.0f,0.0f,1.0f);
        }
     }
     else 
     {
         glTranslatef(translateAllInXDirection ,0.0f,-10.0f);
     }

    glScalef(0.45f,0.45f,0.45f);
    drawPlane();
    

}

void drawLowerPlane(void)
{
    void drawPlane(void);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLineWidth(6.0f);
   
     if(meet==false)
     {
         XTranslatePointDown=-2.5f+4.60f*cos(lowerPlaneAngle);
         YTranslatePointDown=-4.60f+4.60f*sin(lowerPlaneAngle);
         //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
        glTranslatef(XTranslatePointDown,YTranslatePointDown,-10.0f);
        if(decAngle<=90.0f || decAngle>0.0f)
        {
            glRotatef(decAngle,0.0f,0.0f,1.0f);
        }
     }

     else if(translateAllInXDirection>3.50f)
     {  
        XTranslatePointDown=2.5f+4.60f*cos(lowerPlaneAngle2);
        YTranslatePointDown=-4.60f+4.60f*sin(lowerPlaneAngle2);
         //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
        glTranslatef(XTranslatePointDown,YTranslatePointDown,-10.0f);
        if(decAngle2<=360.0f || decAngle2>270.0f)
        {
            glRotatef(decAngle2,0.0f,0.0f,1.0f);
        }
     }
     else
     {
        glTranslatef(translateAllInXDirection ,0.0f,-10.0f);
     }

    
    
    glScalef(0.45f,0.45f,0.45f);
    drawPlane();

}

void drawMiddelePlane(void)
{
    void drawPlane(void);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor3f(1.0f,0.0f,0.0f);
    if(meet==false)
    {
        glTranslatef(translateMiddle,0.0f,-10.0f);
    }
    else if(translateAllInXDirection>3.50f)
    {
        glTranslatef(translateMiddle,0.0f,-10.0f);
    }
    else
    {
        glTranslatef(translateAllInXDirection ,0.0f,-10.0f);
    }
    glScalef(0.45f,0.45f,0.45f);
    drawPlane();
}

void drawPlane(void)
{
   /* glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_TRIANGLES);
   glColor3f(0.0729f,0.886f,0.9333f);
    glVertex3f(1.0f,0.0f,0.0f);
    glVertex3f(0.0,-0.35f,0.0f);
    glVertex3f(0.0f,0.35f,0.0f);

    glEnd();

   /* glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_QUADS);
        glColor3f(0.0729f,0.886f,0.9333f);
        glVertex3f(0.0f,0.350f,0.0f);
        glVertex3f(-1.0f,0.35f,0.0f);
        glVertex3f(-1.0f,-0.35,0.0f);
        glVertex3f(0.0,-0.35f,0.0f);
    glEnd();

  /*  glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_POLYGON);
        glColor3f(0.0729f,0.886f,0.9333f);
        glVertex3f(-1.0f,0.35f,0.0f);
        //up
        glVertex3f(-2.0f,1.75f,0.0f);
        glVertex3f(-2.4f,1.75f,0.0f);
        glVertex3f(-1.85f,0.35f,0.0f);
        glVertex3f(-3.0f,0.35f,0.0f);
    
        //down
        glVertex3f(-3.0f,-0.35f,0.0f);
        glVertex3f(-1.85f,-0.35f,0.0f);
        glVertex3f(-2.4f,-1.75f,0.0f);
        glVertex3f(-2.0f,-1.75f,0.0f);
        //end at same point
        glVertex3f(-1.0f,-0.35f,0.0f);
    glEnd();

  /*  glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_POLYGON);
        glColor3f(0.0729f,0.886f,0.9333f);
        glVertex3f(-3.0f,0.35f,0.0f);
        glVertex3f(-3.5f,1.0,0.0f);
        glVertex3f(-3.8f,1.0,0.0f);


        glVertex3f(-3.4f,0.0f,0.0f);

        glVertex3f(-3.8f,-1.0,0.0f);
        glVertex3f(-3.5f,-1.0,0.0f);
        glVertex3f(-3.0f,-0.35f,0.0f);

    glEnd();

     glBegin(GL_LINES);
       
        glVertex3f(-2.5f,0.12f,0.0f);
        glVertex3f(-4.5f,0.12f,0.0f);

       
        glVertex3f(-3.44f,0.01f,0.0f);
        glVertex3f(-4.5f,0.01f,0.0f);

       
        glVertex3f(-3.44f,-0.06f,0.0f);
        glVertex3f(-4.5f,-0.06f,0.0f);


    glEnd();


    glBegin(GL_LINES);
        glColor3f(1.0f,0.6f,0.2f);
        glVertex3f(-3.44f,0.12f,0.0f);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-5.0f,0.12f,0.0f);

        glColor3f(1.0f,1.0f,1.0f);
        glVertex3f(-3.44f,0.01f,0.0f);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-5.0f,0.01f,0.0f);

        glColor3f(0.0706f,0.3831f,0.02745f);
        glVertex3f(-3.44f,-0.06f,0.0f);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-5.0f,-0.06f,0.0f);


    glEnd();

    glBegin(GL_LINES);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-2.40f,0.12f,0.0f);
        glVertex3f(-2.40f,-0.3f,0.0f);
    glEnd();

     glBegin(GL_LINES);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-2.0f,0.12f,0.0f);
        glVertex3f(-2.2f,-0.3f,0.0f);

        glVertex3f(-2.0f,0.12f,0.0f);
        glVertex3f(-1.85f,-0.3f,0.0f);

        glVertex3f(-2.10f,-0.09f,0.0f);
        glVertex3f(-1.925f,-0.09f,0.0f);
    glEnd();

     glBegin(GL_LINES);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-1.6f,0.12f,0.0f);
        glVertex3f(-1.6f,-0.3f,0.0f);

        glVertex3f(-1.6f,0.1f,0.0f);
        glVertex3f(-1.4f,0.1f,0.0f);

        glVertex3f(-1.6f,-0.1f,0.0f);
        glVertex3f(-1.4f,-0.1f,0.0f);
    glEnd();


}

void PlaneTranslations(void)
{
    if(upperPlaneAngle<upperPlaneStopAngle)
    {
        upperPlaneAngle=upperPlaneAngle+0.02f;
       // upperPlaneAngle=0.0f;
    }
    if(incAngle<=360.0f)
    {
        incAngle=incAngle+0.13f;
       // incAngle=0.0f;
    }


    //lower Plane
    if(lowerPlaneAngle>(M_PI/2))
    {
        lowerPlaneAngle=lowerPlaneAngle-0.02;
    }
    if(decAngle>=0.0f)
    {
        decAngle=decAngle-0.13f;
       // incAngle=0.0f;
    }

    if(translateMiddle<=-2.499705)
    {
        translateMiddle=translateMiddle+0.058f;
    }

   
    if(translateAllInXDirection<=3.5f && meet==true)
    {
        translateAllInXDirection=translateAllInXDirection+0.05f;
        translateMiddle=translateAllInXDirection;
    }
  

    /**********************************************cheking the meet condition********************************/
     if(XTranslatePointUP>=-2.499705 && XTranslatePointDown>=-2.499705 && translateMiddle>=-2.499705)
    {
        meet=true;
    }



    /*******************************----------------2Nd Part---------------------*****************************/

    if(translateAllInXDirection>=3.5f && meet==true && translateMiddle<11.50f)
    {
        translateMiddle=translateMiddle+0.08f;

    }
    if(upperPlaneAngle2<upperPlaneStopAngle2 && translateAllInXDirection>=3.50f)
    {
        upperPlaneAngle2=upperPlaneAngle2+0.02f;
      
    }
    if(incAngle2<=90.0f && translateAllInXDirection>=3.50f)
    {
        incAngle2=incAngle2+0.13f;
       
    }

    if(lowerPlaneAngle2>-0.5f && translateAllInXDirection>=3.50f)
    {
        lowerPlaneAngle2=lowerPlaneAngle2-0.02f;
      
    }
    if(decAngle2>=270.0f && translateAllInXDirection>=3.50f)
    {
        decAngle2=decAngle2-0.13f;
      
    }
}