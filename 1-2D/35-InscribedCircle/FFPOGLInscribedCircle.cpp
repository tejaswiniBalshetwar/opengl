#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define _USE_MATH_DEFINES 1
#include<math.h>
#include<stdio.h>


#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwStyle;

FILE *gpFile=NULL;

bool gbIsFullscreen=false;
bool gbIsActiveWindow=false;

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
	//function declarations
	void display(void);
	int initialize(void);


	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("My OPENGL Application");

	int iRet=0;
	bool bDone=false;

	//code

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error at File opening"),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\nFile opened successfully");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszMenuName=NULL;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My FFP OPENGL Application"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\nerror in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\n Error in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\nError in initialize at wglCreatecontext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\nError in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\ninitialize function successfully executed");
	}

	ShowWindow(hwnd,iCmdShow);
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow==true)
			{
				//render update
			}
			display();
		}
	}
	return (int(msg.wParam));



}


LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}	
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;

	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if(gbIsFullscreen==false)
	{
		tb_dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(tb_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpPrev) &&GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle& ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
			ShowCursor(FALSE);
			gbIsFullscreen=true;
		}
		
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle|WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}

int initialize(void)
{
	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelDescriptorIndex=0;

	//function declarations
	void resize(int,int);

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);

	iPixelDescriptorIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelDescriptorIndex==0)
	{
		return -1;
	}

	if(SetPixelFormat(ghdc,iPixelDescriptorIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc =wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	resize(WIN_WIDTH,WIN_HEIGHT);

	return 0;


}

void display(void)
{
	//function declarations

	GLfloat CalculateRadiusOfCircle(GLfloat,GLfloat);
	GLfloat CalculateLengthByDistanceFormula(GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat CalculateSemiperimeterOfTriangle(GLfloat,GLfloat,GLfloat);	
	GLfloat CalculateAreaOfTriangle(GLfloat,GLfloat,GLfloat,GLfloat);

	GLfloat XcenterOfTriangle=0.0f;
	GLfloat YcenterOfTriangle=0.0f;

	//variable declarations
	GLfloat x1=0.0f,y1=1.0f,z1=0.0f;
	GLfloat x2=-1.0f,y2=-1.0f,z2=0.0f;
	GLfloat x3=1.0f,y3=-1.0f,z3=0.0f;
	GLfloat lengthOfA=0.0f;
	GLfloat lengthOfB=0.0f;
	GLfloat lengthOfC=0.0f;

	GLfloat semiperimeterOfTriangle=0.0f;
	GLfloat AreaOfTriangle=0.0f;
	GLfloat radius=0.0;


	GLfloat xOfCircle=0.0f;
	GLfloat yOfCircle=0.0f;
	

		//code

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//draw Triangle
	glTranslatef(0.0f,0.0f,-3.0f);
	glBegin(GL_LINE_LOOP);
		glColor3f(1.0f,1.0f,0.0f);
		glVertex3f(x1,y1,z1);
		glVertex3f(x2,y2,z2);
		glVertex3f(x3,y3,z3);

	glEnd();

	lengthOfA=CalculateLengthByDistanceFormula(x1,y1,z1,x2,y2,z2);
	lengthOfB=CalculateLengthByDistanceFormula(x2,y2,z2,x3,y3,z3);
	lengthOfC=CalculateLengthByDistanceFormula(x3,y3,z3,x1,y1,z1);


	semiperimeterOfTriangle=CalculateSemiperimeterOfTriangle(lengthOfA,lengthOfB,lengthOfC);
	AreaOfTriangle=CalculateAreaOfTriangle(semiperimeterOfTriangle,lengthOfA,lengthOfB,lengthOfC);
	radius=CalculateRadiusOfCircle(semiperimeterOfTriangle,AreaOfTriangle);

	//center of triangle
	XcenterOfTriangle=((x1*lengthOfB)+(x2*lengthOfC)+(x3*lengthOfA))/(lengthOfA+lengthOfB+lengthOfC);
	YcenterOfTriangle=((y1*lengthOfB)+(y2*lengthOfC)+(y3*lengthOfA))/(lengthOfA+lengthOfB+lengthOfC);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPointSize(2.0f);
	glTranslatef(0.0f,0.0f,-3.0f);
	glBegin(GL_POINTS);
		for(GLfloat angle=0.0f;angle<=2*M_PI;angle=angle+0.01)
		{
			xOfCircle=XcenterOfTriangle+radius*cos(angle);
			yOfCircle=YcenterOfTriangle+radius*sin(angle);

			glColor3f(xOfCircle,1.0f,0.0f);
			glVertex3f(xOfCircle,yOfCircle,0.0f);
		}
	glEnd();


	SwapBuffers(ghdc);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}


void uninitialize(void)
{
	if(gbIsFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle|WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_FRAMECHANGED|SWP_NOOWNERZORDER|SWP_NOMOVE|SWP_NOSIZE);
		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile, "\nProgram run successfully ...Exiting");
		fclose(gpFile);
		gpFile=NULL;	
	}
}

//My Functions


GLfloat CalculateRadiusOfCircle(GLfloat perimeter,GLfloat area)
{

	GLfloat radius=0.0f;

	radius=(area/perimeter);

	return radius;


}
GLfloat CalculateLengthByDistanceFormula(GLfloat x1,GLfloat y1 , GLfloat z1,GLfloat x2,GLfloat y2 , GLfloat z2)
{
	GLfloat length=0.0f;

	length=sqrt(pow((x2-x1),2) +pow((y2-y1),2) + pow((z2-z1),2));
	
	return(length);
}

GLfloat CalculateSemiperimeterOfTriangle(GLfloat lengthOfA ,GLfloat lengthOfB,GLfloat lengthOfC)
{
	GLfloat semiperimeter=(lengthOfA+lengthOfB+lengthOfC)/2.0f;

	return(semiperimeter);

}

GLfloat CalculateAreaOfTriangle(GLfloat semiperimeter,GLfloat lengthOfA ,GLfloat lengthOfB,GLfloat lengthOfC)
{
	GLfloat area=0.0f;

	area=sqrt(semiperimeter*((semiperimeter-lengthOfA)*(semiperimeter- lengthOfB)*(semiperimeter - lengthOfC)));

	return(area);
}