#include<windows.h>

HRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR cmdLine,int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("My Window: Message Handling");
	HWND hwnd;
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName=lpszClassName;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hInstance=hInstance;

	RegisterClassEx(&wndclass);

	hwnd=CreateWindow(lpszClassName,TEXT("My Window-Message Handling"),WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,NULL,hInstance,NULL);

	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	switch(iMsg)
	{
		case WM_CREATE:
			MessageBox(hwnd,TEXT("In WM_CREATE"),TEXT("My Message"),MB_OK);
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					MessageBox(hwnd,TEXT("In WM_KEYDOWN- ESCAPE key Pressed"),TEXT("My Message"),MB_OK);
					DestroyWindow(hwnd);
					break;
				case 0X46:
					MessageBox(hwnd,TEXT("In WM_KEYDOWN- F key Pressed"),TEXT("My Message"),MB_OK);
					break;
			}
			break;
		case WM_LBUTTONDOWN:
				MessageBox(hwnd,TEXT("In WM_LBUTTONDOWN- Left Button is Pressed"),TEXT("My Message"),MB_OK);
				break;
		case WM_RBUTTONDOWN:
			MessageBox(hwnd,TEXT("In WM_RBUTTONDOWN- Right Button is Pressed"),TEXT("My Message"),MB_OK);
			break;

		case WM_DESTROY :
			MessageBox(hwnd,TEXT("In WM_DESTROY"),TEXT("My Message"),MB_OK);
			PostQuitMessage(0);
			break;

	}	

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}