#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH  800
#define WIN_HEIGHT 600


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);



HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

WINDOWPLACEMENT  tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwStyle;

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

FILE *gpFile=NULL;

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{

	//function declarations
	int initialize(void);
	void display(void);

	//variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("My OPENGL Application");
	bool bDone=false;
	int iRet=0;


	//code

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening File log.txt "),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\nFile opened successfully");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszMenuName=NULL;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;	
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My OpenGL FFP Application"),WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();

	if(iRet==-1)
	{
		fprintf(gpFile,"\nError in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\nError in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\nError in initialize at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\nError in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\ninitialize successfully done");
	}


	ShowWindow(hwnd,iCmdShow);
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow==true)
			{
				//call to update
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//fucntion declarations
	void resize(int,int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_ERASEBKGND:
			return(true);
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));

}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if(gbIsFullscreen==false)
	{
		tb_dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(tb_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};

			if(GetWindowPlacement(ghwnd,&tb_wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

			}
			ShowCursor(FALSE);
			gbIsFullscreen=true;
		}
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_FRAMECHANGED|SWP_NOOWNERZORDER|SWP_NOMOVE|SWP_NOSIZE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}

int initialize(void)
{
	//function Declarations
	void resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex==0)
	{
		return -1;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}	

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}


	glClearColor(0.0f,0.0f,0.0f,1.0f);
	resize(WIN_WIDTH,WIN_HEIGHT);
	return(0);
}

void display(void)
{
	//function declarations
	void ChooseColor(int);
	//triangle variables
	GLfloat x1=0.0f,y1=1.0f,z1=0.0f;
	GLfloat x2=-1.0f,y2=-1.0f,z2=0.0f;
	GLfloat x3=1.0f,y3=-1.0f,z3=0.0f;
	
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

	glBegin(GL_LINES);

		//triangle
		for(int i=0;i<8;i++)
		{
			ChooseColor(i);
			glVertex3f(x1,y1,z1);
			glVertex3f(x2,y2,z2);

			glVertex3f(x2,y2,z2);
			glVertex3f(x3,y3,z3);

			glVertex3f(x3,y3,z3);
			glVertex3f(x1,y1,z1);

			y1=y1-0.08;

			x2=x2+0.08;
			y2=y2+0.08;
		
			x3=x3-0.08;
			y3=y3+0.08;
		}

	glEnd();
	SwapBuffers(ghdc);
}

void ChooseColor(int color)
{
	switch(color)
	{
		case 0:
		glColor3f(1.0f,1.0f,1.0f);
		break;
		case 1:
		glColor3f(1.0f,0.0f,0.0f);
		break;
		case 2:
		glColor3f(0.0f,1.0f,0.0f);
		break;
		case 3:
		glColor3f(0.0f,0.0f,1.0f);
		break;
		case 4:
		glColor3f(1.0f,1.0f,0.0f);
		break;				
		case 5:
		glColor3f(0.0f,1.0f,1.0f);
		break;
		case 6:
		glColor3f(1.0f,0.0f,1.0f);
		break;
		case 7:
		glColor3f(0.5f,0.5f,1.0f);
		break;				
	}
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}
void uninitialize(void)
{
	if(gbIsFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOMOVE|SWP_NOSIZE|SWP_FRAMECHANGED);
		ShowCursor(TRUE);

	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;

	}


	if(gpFile)
	{
		fprintf(gpFile,"\n Program Exiting Successfully");
		fclose(gpFile);
		gpFile=NULL;
	}

}
