#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>


#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL; 

WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwStyle;

FILE *gpFile=NULL;

bool gbIsActiveWindow=false;
bool  gbIsFullScreen=false;

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	//function declarations
		int initialize();
		void display(void);

	//variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
	HWND hwnd;

	int iRet=0;
	bool bDone=false;

	//code

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszMenuName=NULL;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(NULL,IDI_WINLOGO);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor=LoadCursor(NULL,IDC_NO);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY FFP OENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\nError in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\nError in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\nError in initialize at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\nError in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n initialize function executed succeefully");
		
	}

	ShowWindow(hwnd,iCmdShow);
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow==true)
			{
				//update
			}
			display();
		}
	}
	return ((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullScreen(void);
	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullScreen();
					break;

			}
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}	
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if(gbIsFullScreen==false)
	{
		tb_dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(tb_dwStyle& WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle & ~ WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
			ShowCursor(FALSE);
			gbIsFullScreen=true;
		}
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullScreen=false;
	}
}

int initialize(void)
{
	void resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);

	iPixelFormat=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormat==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelFormat,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{	
		return -3;

	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	resize(WIN_WIDTH,WIN_HEIGHT);

	return 0;
}

void display(void)
{
	GLfloat CalculateLength(GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat CalculateAreaOfTriangle(GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat CalculateSemiperimenterOfTriangle(GLfloat,GLfloat,GLfloat);
	GLfloat CalculateRadiusOfCircle(GLfloat,GLfloat);


	//variable declarations

	//translate Variables
	static GLfloat XtriangleTranslate=-3.0f;
	static GLfloat YtriangleTranslate=-3.0f;

	static GLfloat lineTranslate=3.0f;

	static GLfloat XcircleTranslate=3.0f;
	static GLfloat YcircleTranslate=-3.0f;


	//ratations
	static GLfloat triangleRotation=0.0f;
	static GLfloat circleRotation=0.0f;


	//circle vars
	GLfloat radiusOfInCircle=0.0f;
	GLfloat xCenterOfCircle=0.0f;
	GLfloat yCenterOfCircle=0.0f;
	GLfloat AreaOfTriangle=0.0f;
	GLfloat SemiperimeterOfTriangle=0.0f;

	//triangle cords
	GLfloat x1Triangle=0.0f,y1Triangle=1.0f,z1Triangle=0.0f;
	GLfloat x2Triangle=-1.0f,y2Triangle=-1.0f,z2Triangle=0.0f;
	GLfloat x3Triangle=1.0f,y3Triangle=-1.0f,z3Triangle=0.0f;

	//code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(XtriangleTranslate,YtriangleTranslate,-6.0f);
	if(XtriangleTranslate<0.0f && YtriangleTranslate<0.0f)
	{
		glRotatef(triangleRotation,0.0f,1.0f,0.0f);
	}
	glBegin(GL_LINE_LOOP);
		glColor3f(1.0f,0.5f,0.0f);
		
		glVertex3f(x1Triangle,y1Triangle,z1Triangle); 
		//glVertex3f(x2Triangle,y2Triangle,z2Triangle);

		glVertex3f(x2Triangle,y2Triangle,z2Triangle);
		//glVertex3f(x3Triangle,y3Triangle,z3Triangle);

		glVertex3f(x3Triangle,y3Triangle,z3Triangle);
		//glVertex3f(x1Triangle,y1Triangle,z1Triangle);
	glEnd();


	//incircle code
	GLfloat LenghtOfA=CalculateLength(x1Triangle,y1Triangle,z1Triangle,x2Triangle,y2Triangle,z2Triangle);
	GLfloat LenghtOfB=CalculateLength(x2Triangle,y2Triangle,z2Triangle,x3Triangle,y3Triangle,z3Triangle);
	GLfloat LenghtOfC=CalculateLength(x3Triangle,y3Triangle,z3Triangle,x1Triangle,y1Triangle,z1Triangle);


	SemiperimeterOfTriangle=CalculateSemiperimenterOfTriangle(LenghtOfA,LenghtOfB,LenghtOfC);
	AreaOfTriangle=CalculateAreaOfTriangle(SemiperimeterOfTriangle,LenghtOfA,LenghtOfB,LenghtOfC);

	radiusOfInCircle=CalculateRadiusOfCircle(SemiperimeterOfTriangle,AreaOfTriangle);

	xCenterOfCircle=((LenghtOfB*x1Triangle)+(LenghtOfC*x2Triangle)+(LenghtOfA*x3Triangle))/(SemiperimeterOfTriangle*2);
	yCenterOfCircle=((LenghtOfB*y1Triangle)+(LenghtOfC*y2Triangle)+(LenghtOfA*y3Triangle))/(SemiperimeterOfTriangle*2);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(XcircleTranslate,YcircleTranslate,-6.0f);
	if(XcircleTranslate>0.0f && YcircleTranslate<0.0f)
	{
		glRotatef(circleRotation,0.0f,1.0f,0.0f);
	}
	glBegin(GL_POINTS);

	for(GLfloat angle=0.0f;angle<=2*M_PI;angle=angle+0.002)
	{

		glColor3f(0.5f,0.5f,0.0f);
		glVertex3f(xCenterOfCircle+(radiusOfInCircle*cos(angle)),yCenterOfCircle+(radiusOfInCircle*sin(angle)),0.0f);
	}

	glEnd();



	//sword of hollows code
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLineWidth(2.0f);
	glTranslatef(0.0f,lineTranslate,-6.0f);
	
	glBegin(GL_LINES);

		glColor3f(0.0f,1.0f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(0.0f,-1.0f,0.0f);

	glEnd();


	SwapBuffers(ghdc);

	
	//rotation update
	triangleRotation=triangleRotation+0.4f;
	if(triangleRotation>360.0f)
	{
		triangleRotation=0.0f;
	}
	circleRotation=circleRotation+0.8f;
	if(circleRotation>360.0f)
	{
		circleRotation=0.0f;
	}

	//translation calls
	if(lineTranslate>=0.0f)
	{
		lineTranslate=lineTranslate-0.001f;
		
	}
	if(XtriangleTranslate<=0.0f && YtriangleTranslate<=0.0f)
	{
		XtriangleTranslate=XtriangleTranslate+0.001f;
		YtriangleTranslate=YtriangleTranslate+0.001f;
	}

	if(XcircleTranslate>=0.0f && YcircleTranslate<=0.0f)
	{
		XcircleTranslate=XcircleTranslate-0.001f;
		YcircleTranslate=YcircleTranslate+0.001f;
	}


}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize(void)
{
	if(gbIsFullScreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullScreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile,"\nProgram Executed succeefully ....EXITING");
		fclose(gpFile);
		gpFile=NULL;
	}
}

GLfloat CalculateLength(GLfloat x1,GLfloat y1,GLfloat z1,GLfloat x2,GLfloat y2,GLfloat z2)
{
	GLfloat Length=0.0f;

	Length=sqrt(pow((x2-x1),2)+pow((y2-y1),2)+pow((z2-z1),2));

	return Length;
}

GLfloat CalculateSemiperimenterOfTriangle(GLfloat length1,GLfloat length2,GLfloat length3)
{
	GLfloat semiperimeter=0.0f;
	semiperimeter=(length1+length2+length3)/2;

	return semiperimeter;
}

GLfloat CalculateAreaOfTriangle(GLfloat semiperimeter,GLfloat length1,GLfloat length2,GLfloat length3)
{
	GLfloat area=0.0f;
	area=sqrt(semiperimeter*(semiperimeter- length1)*(semiperimeter- length2)*(semiperimeter- length3));

	return area;
}

GLfloat CalculateRadiusOfCircle(GLfloat semiperimeter,GLfloat area)
{
	GLfloat radius=0.0f;
	radius=(area/semiperimeter);
	return radius;
}