#include<windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd;
bool gbFullScreen=false;
DWORD dwStyle;
WINDOWPLACEMENT TB_wpPrev={sizeof(WINDOWPLACEMENT)};


int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstancec,LPSTR iCmdLine,int iCmdShow)
{

	WNDCLASSEX wndclass;
	TCHAR lpszClassName[]=TEXT("MyWindow-WM_PAINT");
	HWND hwnd;
	MSG msg;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_VREDRAW|CS_HREDRAW;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName=lpszClassName;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindow(lpszClassName,TEXT("Window-Painting In WM_PAINT"),WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,NULL,hInstance,NULL);

	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//Function Declarations
	void ToggeleFullscreen(void);



	//variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	TCHAR str[]=TEXT("Hello World");
	RECT rc;



	//code
	switch(iMsg)
	{
		case WM_PAINT:
			GetClientRect(hwnd,&rc);
			hdc=BeginPaint(hwnd,&ps);
			SetBkColor(hdc,RGB(0,0,0));
			SetTextColor(hdc,RGB(0,255,0));
			DrawText(hdc,str,-1,&rc,DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			EndPaint(hwnd,&ps);
		break;

		/*case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
				case 0x46:
					ToggeleFullscreen();
					break;
			}
			break;*/
			
		case WM_DESTROY:
		PostQuitMessage(0);
		break;
		
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

