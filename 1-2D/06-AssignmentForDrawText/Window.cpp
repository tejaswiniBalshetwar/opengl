#include<windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR CmdLine,int iCmdShow)
{

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("My window");

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_VREDRAW|CS_HREDRAW;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hInstance=hInstance;

	RegisterClassEx(&wndclass);

	//hwnd=CreateWindow(lpszClassName,TEXT("My Window-FullScreen"),WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,NULL,hInstance,NULL);
	hwnd=CreateWindow(lpszClassName,TEXT("My Window-WM_CREATE DrawText"),WS_OVERLAPPEDWINDOW,100,100,800,600,NULL,NULL,hInstance,NULL);

	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{

	TCHAR str[]=TEXT("Hello World");
	RECT rc;
	HDC hdc;
	switch(iMsg)
	{
		case WM_CREATE:
			GetClientRect(hwnd,&rc);
			hdc=GetDC(hwnd);
			SetBkMode(hdc,RGB(0,0,0));
			SetTextColor(hdc,RGB(0,255,0));
			DrawText(hdc,str,-1,&rc,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
			ReleaseDC(hwnd,hdc);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
		break;

	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}