
#include<windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd;
bool gbFullScreen=false;
DWORD dwStyle;
WINDOWPLACEMENT TB_wpPrev={sizeof(WINDOWPLACEMENT)};


int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstancec,LPSTR iCmdLine,int iCmdShow)
{

	WNDCLASSEX wndclass;
	TCHAR lpszClassName[]=TEXT("MyWindow-WM_PAINT");
	HWND hwnd;
	HWND hwndDesktop;
	RECT rcForDesktop;
	MSG msg;
	int WindowWidth=800;
	int WindowHeight=600;
	
	int leftOfWindow=0;
	int topOfWindow=0;


	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_VREDRAW|CS_HREDRAW;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName=lpszClassName;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

//	hwndDesktop=GetDesktopWindow();

//	GetWindowRect(hwndDesktop,&rcForDesktop);


	/*2 Method For centering the Window*/
	//SystemParametersInfo(SPI_GETWORKAREA,NULL,(void*)&rcForDesktop,NULL);

	//leftOfWindow=((rcForDesktop.right-rcForDesktop.left)/2)-(WindowWidth/2);
	//topOfWindow=(rcForDesktop.bottom-rcForDesktop.top)/2-(WindowHeight/2);



	hwnd=CreateWindow(lpszClassName,TEXT("Window-Painting In WM_PAINT"),WS_OVERLAPPEDWINDOW,leftOfWindow,topOfWindow,WindowWidth,WindowHeight,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;
	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//Function Declarations
	void ToggeleFullscreen(void);




	//variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	TCHAR str[]=TEXT("Hello World");
    RECT rc;
	int WindowWidth=800;
	int WindowHeight=600;
	
	static int leftOfWindow=0;
	static int topOfWindow=0;

	MONITORINFO  mi={sizeof(MONITORINFO)};
	//code
	switch(iMsg)
	{
		case WM_CREATE:

			GetMonitorInfo(MonitorFromWindow(hwnd,MONITORINFOF_PRIMARY),&mi);
		/*3Rd Way For Centering the Window*/
			leftOfWindow=((mi.rcMonitor.right-mi.rcMonitor.left)/2)-(WindowWidth/2);
			topOfWindow=(mi.rcMonitor.bottom-mi.rcMonitor.top)/2-(WindowHeight/2);
			SetWindowPos(hwnd,HWND_TOP,leftOfWindow	,topOfWindow,WindowWidth,WindowHeight,SWP_NOOWNERZORDER|SWP_DRAWFRAME);
			break;
		case WM_PAINT:
			GetClientRect(hwnd,&rc);
			hdc=BeginPaint(hwnd,&ps);
			SetBkColor(hdc,RGB(0,0,0));
			SetTextColor(hdc,RGB(0,255,0));
			DrawText(hdc,str,-1,&rc,DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			EndPaint(hwnd,&ps);
		break;

		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
				case 0x46:
					ToggeleFullscreen();
					break;
			}
			break;
			
		case WM_DESTROY:
		PostQuitMessage(0);
		break;
		
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggeleFullscreen(void)
{
	MONITORINFO mi;
	if(gbFullScreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&TB_wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE, dwStyle &~WS_OVERLAPPEDWINDOW  );
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right -mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER|SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		gbFullScreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&TB_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOMOVE|SWP_NOSIZE);
		gbFullScreen=false;
		ShowCursor(TRUE);

	}
}