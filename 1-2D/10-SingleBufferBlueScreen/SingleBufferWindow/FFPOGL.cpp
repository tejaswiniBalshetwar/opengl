#include<windows.h>
#include<gl/GL.h>
#include<stdio.h>

#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//initilaization function declarations
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;
bool gbActiveWindow=false;

//Full screen declarations
DWORD dwStyle;
WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbIsFullScreen=false;

//File io Variable
FILE *gpFile=NULL;
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	//function Declarations
	int initialize(void);

	//variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("MY FFP Program");
	bool bDone=false;
	int iRet=0;

	//code

	if(fopen_s(&gpFile,"log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Error while file opening"),TEXT("ERROR"),MB_OK|MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\nFile Opened Successfully");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.lpszClassName=lpszClassName;
	wndclass.cbWndExtra=0;
	wndclass.cbClsExtra=0;
	wndclass.lpszMenuName=NULL;
	wndclass.hInstance=hInstance;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My FFP-Single Buffer Blue Screen"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

	ghwnd=hwnd;
	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile, "\nError at  ChoosePixelFormat" );
		DestroyWindow(hwnd);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile, "\nError at SetPixelFormat ");
		DestroyWindow(hwnd);
	}

	else if(iRet==-3)
	{
		fprintf(gpFile, "\nError at wglCreateContext");
		DestroyWindow(hwnd);
	}

	else if(iRet==-4)
	{
		fprintf(gpFile, "\nError at wglMakeCurrent ");
		DestroyWindow(hwnd);
	}

	else
	{
		fprintf(gpFile, "\n initialization Successfully  Done" );

	}

	ShowWindow(hwnd,iCmdShow);
	//UpdateWindow(NULL);
	//InvalidateRect(hwnd,NULL,TRUE);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Game Loop 
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if(gbActiveWindow==true)
			{
				//Here Call to Update
			}
			//Call To display
			//display();
			
		}
	}
 
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam ,LPARAM lParam)
{
	void resize(int,int);
	void uninitialize(void);
	void display();
	void ToggleFullScreen(void);
	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_PAINT:
			display();
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
				case 0x46:
				ToggleFullScreen();
				break;
			}
			break;
		case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;


	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if(gbIsFullScreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER|SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle|WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbIsFullScreen=false;
	}
}


int initialize(void)
{
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int ipixelFormatIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;

	//getting OS device context to convert to OPENGL device Context 
	ghdc=GetDC(ghwnd);

	//submiting out pixel format to os so that os can give us the nearest available pixel format in her repository
	ipixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(ipixelFormatIndex==0)
	{
		return -1;
	}

	//if it is available then select that for painting
	if(SetPixelFormat(ghdc,ipixelFormatIndex,&pfd)==FALSE)
	{
		return -2;
	}

	//Change the OS DC to our needed opengl dc using opengl and os bridging APIS

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	//now make this my painting machine to paint on window
	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glClearColor(0.0f,0.0f,1.0f,0.0f);

	resize(WIN_WIDTH,WIN_HEIGHT);
	return 0;

}

void display()
{
	//now mother opengl please clear the buffer i am selecting  
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

void resize(int width,int height)
{

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

}

void uninitialize(void)
{

	if(gbIsFullScreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);

	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile, "\nLog file is closed Successfully");
		fclose(gpFile);
		gpFile=NULL;
	}
}