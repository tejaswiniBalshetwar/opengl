#include<windows.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#define _USE_MATH_DEFINES 1
#include<math.h>
#include<stdio.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd=NULL;

HDC ghdc=NULL; 
HGLRC ghrc=NULL;
	
WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwStyle;

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;


FILE *gpFile=NULL;

int WINAPI WinMain( HINSTANCE hInstance,HINSTANCE hPrevInstance, LPSTR iCmdLine,int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);


	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG	msg;
	TCHAR lpszClassName[]=TEXT("My opengl Application");
	int iRet=0;
	bool bDone=false;
	
	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error While opening file..exiting"),TEXT("ERROR!!!!"),MB_OKCANCEL);
		exit(0);
	}
	else{
		fprintf(gpFile,"\n File opened successfull");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My OPENGL Application"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\nerror in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\nError in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\nError in initialize at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\nError in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n initialize function executes successfull");
	}

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd,iCmdShow);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow==true)
			{
				//update()
			}
			
			display();
			
		}
	}

	return((int)msg.wParam);

}



LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void ToggleFullscreen(void);
	void resize(int,int);
	void uninitialize();

	switch(iMsg)
	{
		case WM_SETFOCUS:
 			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;

			}
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if(gbIsFullscreen==false)
	{
		tb_dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(tb_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle& ~ WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER|SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbIsFullscreen=true;
		}
	}
	else
	{

		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle| WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_NOMOVE|SWP_NOSIZE);


		ShowCursor(TRUE);
		gbIsFullscreen=false;

	}
}

int initialize(void)
{
	void resize(int,int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelDescriptor=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);


	iPixelDescriptor=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelDescriptor==0)
	{
		return -1;
	}

	if(SetPixelFormat(ghdc,iPixelDescriptor,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	resize(WIN_WIDTH,WIN_HEIGHT);

	return 0;

}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	//functions declarations
	GLfloat CalculateRadiusOfInCircle(GLfloat,GLfloat);
	GLfloat CalculateRadiusOfOuterCircle(GLfloat,GLfloat);
	GLfloat CalculateLengthByDistanceFormula(GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat CalculateSemiperimeterOfTriangle(GLfloat,GLfloat,GLfloat);	
	GLfloat CalculateAreaOfTriangle(GLfloat,GLfloat,GLfloat,GLfloat);

	void DrawOuterCircle(GLfloat);
	GLfloat lengthARectangle=0.0f;
	GLfloat lengthBRectangle=0.0f;
	GLfloat lengthCRectangle=0.0f;
	GLfloat lengthDRectangle=0.0f;

	GLfloat lengthATriangle=0.0f;
	GLfloat lengthBTriangle=0.0f;
	GLfloat lengthCTriangle=0.0f;
	
	GLfloat InCircleRadius=0.0f;
	GLfloat OutCircleRadius=0.0f;

	GLfloat semiperimeterOfTriangle=0.0f;
	GLfloat AreaOfTriangle=0.0f;

	GLfloat XcenterOfTriangle=0.0f;
	GLfloat YcenterOfTriangle=0.0f;

	//Triangle variables
	GLfloat x1Triangle=0.0f,y1Triangle=1.0f,z1Triangle=0.0f;
	GLfloat x2Triangle=-1.0f,y2Triangle=-1.0f,z2Triangle=0.0f;
	GLfloat x3Triangle=1.0f,y3Triangle=-1.0f,z3Triangle=0.0f;
	
	

	//rectangle variables
	GLfloat x1=1.0f,y1=1.0f,z1=0.0f;
	GLfloat x2=-1.0f,y2=1.0f,z2=0.0f;
	GLfloat x3=-1.0f,y3=-1.0f,z3=0.0f;
	GLfloat x4=1.0f,y4=-1.0f,z4=0.0f;


	//Draw Graph Paper
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

//	glLineWidth(5.0f);
	glBegin(GL_LINES);


	glColor3f(0.0f,0.0f,1.0f);

	GLfloat yLine=0.0f;

	for(int i=0;i<20;i++)
	{
		yLine=yLine+0.05f;
		glVertex3f(-1.0f,yLine,0.0f);
		glVertex3f(1.0f,yLine,0.0f);
		
	}

	glEnd();


	glMatrixMode(GL_MODELVIEW);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

	glBegin(GL_LINES);


	glColor3f(0.0f,0.0f,1.0f);

	GLfloat y1Line=-0.0f;


	for(int i=0;i<20;i++)
	{
		y1Line=y1Line-0.05f;
		glVertex3f(-1.0f,y1Line,0.0f);
		glVertex3f(1.0f,y1Line,0.0f);
		
	}
	glEnd();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

	glBegin(GL_LINES);


	glColor3f(0.0f,0.0f,1.0f);

	GLfloat xLine=0.0f;

	for(int i=0;i<20;i++)
	{
		xLine=xLine+0.05f;
		glVertex3f(xLine,1.0f,0.0f);
		glVertex3f(xLine,-1.0f,0.0f);
		
	}

	glEnd();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);
	glBegin(GL_LINES);


	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(0.0f,1.0f,0.0f);
	glVertex3f(0.0f,-1.0f,0.0f);

	glEnd();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

	glBegin(GL_LINES);


	glColor3f(0.0f,0.0f,1.0f);

	GLfloat x1Line=0.0f;


	for(int i=0;i<20;i++)
	{
		x1Line=x1Line-0.05f;
		glVertex3f(x1Line,-1.0f,0.0f);
		glVertex3f(x1Line,1.0f,0.0f);
		
	}
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);
	glBegin(GL_LINES);


	glColor3f(1.0f,0.0f,0.0f);
	glVertex3f(-1.0f,0.0f,0.0f);
	glVertex3f(1.0f,0.0f,0.0f);

	glEnd();




	lengthARectangle=CalculateLengthByDistanceFormula(x1,y1,z1,x2,y2,z2);
	lengthBRectangle=CalculateLengthByDistanceFormula(x2,y2,z2,x3,y3,z3);


	OutCircleRadius=CalculateRadiusOfOuterCircle(lengthARectangle,lengthBRectangle);

	//Outer Circle for rectangle 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-5.0f);

	glPointSize(2.0f);
	glBegin(GL_POINTS);

	DrawOuterCircle(OutCircleRadius);

	glEnd();


	//Rectangle
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-5.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f,1.0f,0.0f);
	glVertex3f(x1,y1,z1);
	glVertex3f(x2,y2,z2);

	glVertex3f(x2,y2,z2);
	glVertex3f(x3,y3,z3);

	glVertex3f(x3,y3,z3);
	glVertex3f(x4,y4,z4);

	glVertex3f(x4,y4,z4);
	glVertex3f(x1,y1,z1);

	glEnd();


	//triangle

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-5.0f);
	glBegin(GL_LINES);
		glColor3f(0.0f,1.0f,1.0f);
		
		glVertex3f(x1Triangle,y1Triangle,z1Triangle);
		glVertex3f(x2Triangle,y2Triangle,z2Triangle);
		
		glVertex3f(x2Triangle,y2Triangle,z2Triangle);
		glVertex3f(x3Triangle,y3Triangle,z3Triangle);
		
		glVertex3f(x3Triangle,y3Triangle,z3Triangle);
		glVertex3f(x1Triangle,y1Triangle,z1Triangle);
	

	glEnd();

	
	//Incircle
	lengthATriangle=CalculateLengthByDistanceFormula(x1Triangle,y1Triangle,z1Triangle,x2Triangle,y2Triangle,z2Triangle);
	lengthBTriangle=CalculateLengthByDistanceFormula(x2Triangle,y2Triangle,z2Triangle,x3Triangle,y3Triangle,z3Triangle);
	lengthCTriangle=CalculateLengthByDistanceFormula(x3Triangle,y3Triangle,z3Triangle,x1Triangle,y1Triangle,z1Triangle);

	semiperimeterOfTriangle=CalculateSemiperimeterOfTriangle(lengthATriangle,lengthBTriangle,lengthCTriangle);

	AreaOfTriangle=CalculateAreaOfTriangle(semiperimeterOfTriangle,lengthATriangle,lengthBTriangle,lengthCTriangle);

	InCircleRadius=CalculateRadiusOfInCircle(semiperimeterOfTriangle,AreaOfTriangle);

	XcenterOfTriangle=((x1Triangle*lengthBTriangle)+(x2Triangle*lengthCTriangle)+(x3Triangle*lengthATriangle))/(lengthATriangle+lengthBTriangle+lengthCTriangle);
	YcenterOfTriangle=((y1Triangle*lengthBTriangle)+(y2Triangle*lengthCTriangle)+(y3Triangle*lengthATriangle))/(lengthATriangle+lengthBTriangle+lengthCTriangle);
	

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPointSize(2.0f);
	glTranslatef(0.0f,0.0f,-5.0f);
	glBegin(GL_POINTS);
		for(GLfloat angle=0.0f;angle<=2*M_PI;angle=angle+0.002)
		{
			GLfloat xOfCircle=XcenterOfTriangle+InCircleRadius*cos(angle);
			GLfloat yOfCircle=YcenterOfTriangle+InCircleRadius*sin(angle);

			glColor3f(1.0f,0.0f,1.0f);
			glVertex3f(xOfCircle,yOfCircle,0.0f);
		}
	glEnd();




	SwapBuffers(ghdc);

}

void DrawOuterCircle(GLfloat OutCircleRadius)
{

	//outer circle 
	GLfloat xOfCircle=0.0f;
	GLfloat yOfCircle=0.0f;

	for(GLfloat angle=0.0f;angle<2*M_PI;angle=angle+0.002f)
		{

			xOfCircle=OutCircleRadius*cos(angle);
			yOfCircle=OutCircleRadius*sin(angle);

			glColor3f(1.0f,0.50f,0.0f);
			glVertex3f(xOfCircle,yOfCircle,0.0f);
		}

}

void resize(int width ,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

void uninitialize(void)
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle| WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_NOMOVE|SWP_NOSIZE);


		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);

	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;

	}
	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}


	if(gpFile)
	{
		fprintf(gpFile,"\nProgram executed successfully .....EXITING");
		fclose(gpFile);
		gpFile=NULL;
	}


}


GLfloat CalculateRadiusOfInCircle(GLfloat perimeter,GLfloat area)
{

	GLfloat radius=0.0f;

	radius=(area/perimeter);
	return radius;


}

GLfloat CalculateRadiusOfOuterCircle(GLfloat lengthA, GLfloat lengthB)
{
	GLfloat radius=0.0f;
	GLfloat hypotenious=0.0f;

	hypotenious=sqrt(pow(lengthA,2)+pow(lengthB,2));

	radius=hypotenious/2;
	return radius;

}
GLfloat CalculateLengthByDistanceFormula(GLfloat x1,GLfloat y1 , GLfloat z1,GLfloat x2,GLfloat y2 , GLfloat z2)
{
	GLfloat length=0.0f;

	length=sqrt(pow((x2-x1),2) +pow((y2-y1),2) + pow((z2-z1),2));
	
	return(length);
}

GLfloat CalculateSemiperimeterOfTriangle(GLfloat lengthOfA ,GLfloat lengthOfB,GLfloat lengthOfC)
{
	GLfloat semiperimeter=(lengthOfA+lengthOfB+lengthOfC)/2.0f;

	return(semiperimeter);

}

GLfloat CalculateAreaOfTriangle(GLfloat semiperimeter,GLfloat lengthOfA ,GLfloat lengthOfB,GLfloat lengthOfC)
{
	GLfloat area=0.0f;

	area=sqrt(semiperimeter*((semiperimeter-lengthOfA)*(semiperimeter- lengthOfB)*(semiperimeter - lengthOfC)));

	return(area);
}