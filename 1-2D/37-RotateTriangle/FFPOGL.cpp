#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile=NULL;

HWND ghwnd=NULL;
HGLRC ghrc=NULL;
HDC ghdc=NULL;

WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

bool gbIsFullscreen=false;
bool gbIsActiveWindow=false;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{
	//function declaration
	int initialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("My OpenGL Application");
	bool bDone=false;
	int iRet=0;

	//code

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file..exiting"),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n File opened successfully");
	}
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszMenuName=NULL;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My FFP openGL application"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\n Error in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\n Error in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\n Error in initialize at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\n Error in initialize at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n initialize function executed successfully");
	}

	ShowWindow(hwnd,iCmdShow);
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);

	//game loop

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		else
		{
			if(gbIsActiveWindow==true)
			{
				//update
			}
			display();
		}
	}

	
	return ((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void uninitialize(void);
	void ToggeleFullscreen(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_ERASEBKGND:
			return (0);
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggeleFullscreen();
					break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}

	return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggeleFullscreen(void)
{
	MONITORINFO mi;

	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		if(dwStyle& WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpPrev)&& GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

			}
			ShowCursor(FALSE);
			gbIsFullscreen=true;
		}
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}


int initialize()
{
	//function declarations
	void resize(int,int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);
	iPixelFormat=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormat==0)
	{
		return -1;
	}

	if(SetPixelFormat(ghdc,iPixelFormat,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	
	glClearColor(0.0f,0.0f,0.0f,1.0f);

	resize(WIN_WIDTH,WIN_HEIGHT);


	return 0;
}

void display(void)
{
	
	static GLfloat AngleOfRotationForTriangle=0.0f;
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-5.0f);

	glRotatef(AngleOfRotationForTriangle,0.0f,1.0f,0.0f);

	glBegin(GL_TRIANGLES);
		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		
		glColor3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		
		glColor3f(0.0f,0.0f,1.0f);
		glVertex3f(1.0f,-1.0f,0.0f);
	glEnd();

	SwapBuffers(ghdc);

	AngleOfRotationForTriangle=AngleOfRotationForTriangle+2.0f;
	if(AngleOfRotationForTriangle>=360.0f)
	{
		AngleOfRotationForTriangle=0.0f;
	}

}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}



void uninitialize(void)
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile, "\n Program executed successfully..exiting");
		fclose(gpFile);
		gpFile=NULL;
	}
}

