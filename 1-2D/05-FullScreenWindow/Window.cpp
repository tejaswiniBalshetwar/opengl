#include<windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

bool gbIsFullScreen=false;
HWND ghwnd;
WINDOWPLACEMENT tb_wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwStyle;

int WINAPI WinMain(HINSTANCE hInstance , HINSTANCE hPrevInstance, LPSTR cmdLine,int iCmdShow)
{

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("My window");

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_VREDRAW|CS_HREDRAW;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hInstance=hInstance;

	RegisterClassEx(&wndclass);

	//hwnd=CreateWindow(lpszClassName,TEXT("My Window-FullScreen"),WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,NULL,hInstance,NULL);
	hwnd=CreateWindow(lpszClassName,TEXT("My Window-FullScreen"),WS_OVERLAPPEDWINDOW,100,100,800,600,NULL,NULL,hInstance,NULL);

	ghwnd=hwnd;
	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg,WPARAM wParam, LPARAM lParam)
{
	void ToggeleFullScreen(void);
	switch(iMsg)
	{
		case WM_KEYDOWN:
			switch(wParam)
			{
				case 0X46:
					ToggeleFullScreen();
					break;
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;

			}

		break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}


void ToggeleFullScreen(void)
{

	MONITORINFO tb_mi;
	if(gbIsFullScreen==false)
	{
		tb_dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(tb_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			tb_mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&tb_mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE, tb_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,
								tb_mi.rcMonitor.left,
								tb_mi.rcMonitor.top,
								tb_mi.rcMonitor.right-tb_mi.rcMonitor.left,
								tb_mi.rcMonitor.bottom-tb_mi.rcMonitor.top,
								SWP_NOZORDER|SWP_FRAMECHANGED);


			}
		}
		gbIsFullScreen=true;
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwStyle| WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_FRAMECHANGED|SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbIsFullScreen=false;

	}

}

