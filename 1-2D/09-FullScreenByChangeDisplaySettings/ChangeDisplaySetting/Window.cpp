#include<windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HWND ghwnd;
bool gbFullScreen=false;

DWORD dwStyle;

DEVMODE dm;
WINDOWPLACEMENT wp_prev = {sizeof(WINDOWPLACEMENT)};
int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstancec,LPSTR iCmdLine,int iCmdShow)
{

	WNDCLASSEX wndclass;
	TCHAR lpszClassName[]=TEXT("MyWindow-for ChangeDisplaySettings");
	HWND hwnd;
	MSG msg;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_VREDRAW|CS_HREDRAW;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName=lpszClassName;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindow(lpszClassName,TEXT("Window-FullScreen By ChangeDisplaySetting"),WS_OVERLAPPEDWINDOW,0,0,800,600,NULL,NULL,hInstance,NULL);
	ghwnd = hwnd;
	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullScreen(void);
	
	//variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	TCHAR str[]=TEXT("Hello World");
	static RECT rc;

	static MONITORINFO mi;


	//code
	switch(iMsg)
	{
		case WM_PAINT:
			GetClientRect(hwnd,&rc);
			hdc=BeginPaint(hwnd,&ps);
			SetBkColor(hdc,RGB(0,0,0));
			SetTextColor(hdc,RGB(0,255,0));
			DrawText(hdc,str,-1,&rc,DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			EndPaint(hwnd,&ps);
		break;

		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
				case 0x46:
					ToggleFullScreen();
					
					break;
			}
			break;
			
		case WM_DESTROY:
		PostQuitMessage(0);
		break;
		
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullScreen(void)
{
	if (gbFullScreen == false) {

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			GetWindowPlacement(ghwnd, &wp_prev);
			EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);
			//dm.dmSize = sizeof(DEVMODE);
			//dm.dmPosition = pointForFullScreen;
			dm.dmPelsWidth = 800;
			dm.dmPelsHeight = 600;
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle&~WS_OVERLAPPEDWINDOW);

			ChangeDisplaySettings(&dm, CDS_RESET | CDS_FULLSCREEN);
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else {
		//POINTL pointForNormalScreen;
		//pointForNormalScreen.x =wp_prev.ptMaxPosition.x;
		//pointForNormalScreen.y = wp_prev.ptMaxPosition.y;

		EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);

		dm.dmSize = sizeof(DEVMODE);

		dm.dmPelsWidth = 1366;
		dm.dmPelsHeight = 768;
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp_prev);
		ChangeDisplaySettings(&dm, CDS_RESET);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}