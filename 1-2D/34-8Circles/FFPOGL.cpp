#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>


//openGL libs
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//windows libs
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);


FILE *gpFile=NULL;

HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

bool gbIsFullscreen=false;
bool gbIsActiveWindow=false;


WINDOWPLACEMENT tb_wpprev={sizeof(WINDOWPLACEMENT)};
DWORD tb_dwstyle;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{

	//Function Declarations
	void display(void);
	int initialize(void);

	//Variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	int iRet=0;
	bool bDone=false;
	TCHAR lpszClassName[]=TEXT("My OpenGL Application");
	HWND hwnd;
		
	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error While opening Log File"),TEXT("ERROR!!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\nFile Opened Succesfully");

	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_VREDRAW|CS_HREDRAW|CS_OWNDC;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszMenuName=NULL;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);


	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My FFP OPENGL Application"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);

	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"\n initialize failse at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"\ninitialize failed at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"\n initialize failed at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"\n initialize failed at wglMakeCurrent");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"\n initialize Function follows");
	}

	ShowWindow(hwnd,iCmdShow);
	SetFocus(hwnd);
	SetForegroundWindow(hwnd);


	//game Loop 
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow==true)
			{
				//render
			}
			display();
		}
	}

	return ((int)msg.wParam);


}



LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void resize(int,int);
	void uninitialize(void);
	void ToggleFullscreen(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_ERASEBKGND:
			return(0);
			break;

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;

		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;

		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}



void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if(gbIsFullscreen==false)
	{
		tb_dwstyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(tb_dwstyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&tb_wpprev)&&GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,tb_dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER|SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbIsFullscreen=true;
		}
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpprev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOMOVE|SWP_NOOWNERZORDER|SWP_NOSIZE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}

int initialize(void)
{
	void resize(int,int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;


	ghdc=GetDC(ghwnd);

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex==0)
	{
		return -1;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);


	resize(WIN_WIDTH,WIN_HEIGHT);


	return 0;

}

void display(void)
{
	//fucntion Declarations
	void ChooseColor(int);

	GLfloat fRadious=1.0f;
	GLfloat x=0;
	GLfloat y=0;

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glPointSize(2.0f);
	glTranslatef(0.0f,0.0f,-3.0f);
	glBegin(GL_POINTS);

		for(int i=0;i<8;i++)
		{
			ChooseColor(i);

			for(GLfloat angle=0.0f;angle<=(2*M_PI);angle=angle+0.005f)
			{
				//fprintf(gpFile,"\n%f",angle);
				x=fRadious*cos(angle);
				y=fRadious*sin(angle);

				glVertex3f(x,y,0.0f);
			}

			fRadious=fRadious-0.05f;
		}

	glEnd();


	SwapBuffers(ghdc);
}

void ChooseColor(int color)
{
	switch(color)
	{
		case 0:
		glColor3f(1.0f,1.0f,1.0f);
		break;
		case 1:
		glColor3f(1.0f,0.0f,0.0f);
		break;
		case 2:
		glColor3f(0.0f,1.0f,0.0f);
		break;
		case 3:
		glColor3f(0.0f,0.0f,1.0f);
		break;
		case 4:
		glColor3f(1.0f,1.0f,0.0f);
		break;				
		case 5:
		glColor3f(0.0f,1.0f,1.0f);
		break;
		case 6:
		glColor3f(1.0f,0.0f,1.0f);
		break;
		case 7:
		glColor3f(0.5f,0.5f,1.0f);
		break;				
	}
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}


void uninitialize(void)
{
	if(gbIsFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,tb_dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&tb_wpprev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOMOVE|SWP_NOSIZE|SWP_FRAMECHANGED);
		ShowCursor(TRUE);

	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;

	}


	if(gpFile)
	{
		fprintf(gpFile,"\n Program Exiting Successfully");
		fclose(gpFile);
		gpFile=NULL;
	}

}
