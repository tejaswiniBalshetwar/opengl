#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

GLUquadric *quadric[24];

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL;

bool gbIsFullscreen=false;
bool gbIsActiveWindow=false;

GLfloat lightAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat lightPosition[]={0.0f,0.0f,0.0f,1.0f};

GLuint keyPress=0;
GLfloat angleOfXRotation=0.0f;
GLfloat angleOfYRotation=0.0f;
GLfloat angleOfZRotation=0.0f;


bool bLight=false;
GLfloat light_model_ambient[]={0.2f,0.2f,0.2f,1.0f};
GLfloat light_model_local_viewer[]={0.0f};

FILE *gpFile=NULL;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
	//function declarations
	void display(void);
	int initialize(void);
	void update();

	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
	MSG msg;

	int iRet=0;
	bool bDone=false;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File opened successfully\n");
	}

	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.hInstance=hInstance;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszMenuName=NULL;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error ini initialize at ChoosePixelFormat\n\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in  initialize at SetPixelFormat\n\n");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"Error in  initialize at wglCreateContext\n\n");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"Error in  initialize at wglMakeCurrent\n\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"initialize function successfully executed\n\n");
	}

	SetFocus(hwnd);
	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void resize(int,int);
	void ToggleFullscreen(void);
	void uninitialize(void);
	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CHAR:
			switch(wParam)
			{
				case 'l':
				case 'L':
					if(bLight==false)
					{
						bLight=true;
						glEnable(GL_LIGHTING);
					}
					else
					{
						bLight=false;
						glDisable(GL_LIGHTING);
					}
					break;
				case 'x':
				case 'X':
					keyPress=1;
					angleOfXRotation=0.0f;
					break;
				case 'y':
				case 'Y':
					keyPress=2;
					angleOfYRotation=0.0f;
					break;
				case 'z':
				case 'Z':
					keyPress=3;
					angleOfZRotation=0.0f;
					break;

			}
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbIsFullscreen=true;

	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		gbIsFullscreen=false;
		ShowCursor(TRUE);

	}
}

int initialize()
{
	void resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);
	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
	{
		return -2;
	}
	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}
	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuse);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);	

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);

	glEnable(GL_LIGHT0);

	for(int i=0;i<24;i++)
	{
		quadric[i]=gluNewQuadric();
	}

	resize(WIN_WIDTH,WIN_HEIGHT);
	return 0;
}

void display(void)
{
	void draw24Spheres(void);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	if(keyPress==1)
	{
		glRotatef(angleOfXRotation,1.0f,0.0f,0.0f);
		lightPosition[1]=angleOfXRotation;
	}
	else if(keyPress==2)
	{
		glRotatef(angleOfYRotation,0.0f,1.0f,0.0f);
		lightPosition[2]=angleOfYRotation;
	}
	else if(keyPress==3)
	{
		glRotatef(angleOfZRotation,0.0f,0.0f,1.0f);
		lightPosition[0]=angleOfZRotation;
	}

	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);
	glPopMatrix();
	draw24Spheres();

	SwapBuffers(ghdc);
}

void draw24Spheres()
{
	GLfloat glMaterialAmbient[4];
	GLfloat glMaterialDiffuse[4];
	GLfloat glMaterialSpecular[4];
	GLfloat glMaterialShininess;

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	glMaterialAmbient[0]=0.0215;
	glMaterialAmbient[1]=0.1745;
	glMaterialAmbient[2]=0.0215;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.07568;
	glMaterialDiffuse[1]=0.61424;
	glMaterialDiffuse[2]=0.07568;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.633;
	glMaterialSpecular[1]=0.727811;
	glMaterialSpecular[2]=0.633;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.6 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,14.0f,0.0f);

	gluSphere(quadric[0],1.0f,30,30);

	
	//2nd row 1st col
	glMaterialAmbient[0]=0.135f;
	glMaterialAmbient[1]=0.2225f;
	glMaterialAmbient[2]=0.1575f;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.54f;
	glMaterialDiffuse[1]=0.89f;
	glMaterialDiffuse[2]=0.63f;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.316228;
	glMaterialSpecular[1]=0.316228;
	glMaterialSpecular[2]=0.316228;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.1 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,11.50f,0.0f);

	gluSphere(quadric[1],1.0f,30,30);


	//3rd row 1 col
	glMaterialAmbient[0]=0.05375;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.06625;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.18275;
	glMaterialDiffuse[1]=0.17;
	glMaterialDiffuse[2]=0.22525;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.332741;
	glMaterialSpecular[1]=0.328634;
	glMaterialSpecular[2]=0.346435;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.3 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,9.0f,0.0f);

	gluSphere(quadric[2],1.0f,30,30);

	//4th row 1st col
	glMaterialAmbient[0]=0.25;
	glMaterialAmbient[1]=0.20725;
	glMaterialAmbient[2]=0.20725;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=1.0;
	glMaterialDiffuse[1]=0.829;
	glMaterialDiffuse[2]=0.829;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.296648;
	glMaterialSpecular[1]=0.296648;
	glMaterialSpecular[2]=0.296648;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.088 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,6.50f,0.0f);

	gluSphere(quadric[3],1.0f,30,30);

	//5th row and 1st col
	glMaterialAmbient[0]=0.1745;
	glMaterialAmbient[1]=0.01175;
	glMaterialAmbient[2]=0.01175;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.61424;
	glMaterialDiffuse[1]=0.04136;
	glMaterialDiffuse[2]=0.04136;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.727811;
	glMaterialSpecular[1]=0.626959;
	glMaterialSpecular[2]=0.626959;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.6 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,4.0f,0.0f);

	gluSphere(quadric[4],1.0f,30,30);

	//6th row 1st col
	glMaterialAmbient[0]=0.1;
	glMaterialAmbient[1]=0.18725;
	glMaterialAmbient[2]=0.1745;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.396;
	glMaterialDiffuse[1]=0.74151;
	glMaterialDiffuse[2]=0.69102;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.297254;
	glMaterialSpecular[1]=0.30829;
	glMaterialSpecular[2]=0.306678;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.1 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,1.50f,0.0f);

	gluSphere(quadric[5],1.0f,30,30);




	//2nd column

	glMaterialAmbient[0]=0.329412;
	glMaterialAmbient[1]=0.223529;
	glMaterialAmbient[2]=0.027451;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.780392;
	glMaterialDiffuse[1]=0.568627;
	glMaterialDiffuse[2]=0.113725;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.992157;
	glMaterialSpecular[1]=0.941176;
	glMaterialSpecular[2]=0.807843;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.21794872 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,14.0f,0.0f);

	gluSphere(quadric[6],1.0f,30,30);

	
	//2nd row 2 col
	glMaterialAmbient[0]=0.2125;
	glMaterialAmbient[1]=0.1275f;
	glMaterialAmbient[2]=0.054f;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.714;
	glMaterialDiffuse[1]=0.4284;
	glMaterialDiffuse[2]=0.18144;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.393548;
	glMaterialSpecular[1]=0.271906;
	glMaterialSpecular[2]=0.166721;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.2 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,11.50f,0.0f);

	gluSphere(quadric[7],1.0f,30,30);


	//3rd row 2 col
	glMaterialAmbient[0]=0.25;
	glMaterialAmbient[1]=0.25;
	glMaterialAmbient[2]=0.25;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.4;
	glMaterialDiffuse[1]=0.4;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.774597;
	glMaterialSpecular[1]=0.774597;
	glMaterialSpecular[2]=0.774597;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.6 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,9.0f,0.0f);

	gluSphere(quadric[8],1.0f,30,30);

	//4th row 2nd col
	glMaterialAmbient[0]=0.19125;
	glMaterialAmbient[1]=0.0735;
	glMaterialAmbient[2]=0.0225;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.7038;
	glMaterialDiffuse[1]=0.27048;
	glMaterialDiffuse[2]=0.0828;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.256777;
	glMaterialSpecular[1]=0.137622;
	glMaterialSpecular[2]=0.086014;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.1 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,6.50f,0.0f);

	gluSphere(quadric[9],1.0f,30,30);

	//5th row and 2nd col
	glMaterialAmbient[0]=0.24725;
	glMaterialAmbient[1]=0.1995;
	glMaterialAmbient[2]=0.0745;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.75164;
	glMaterialDiffuse[1]=0.60648;
	glMaterialDiffuse[2]=0.22648;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.628281;
	glMaterialSpecular[1]=0.555802;
	glMaterialSpecular[2]=0.366065;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.4 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,4.0f,0.0f);

	gluSphere(quadric[10],1.0f,30,30);

	//6th row 2nd col
	glMaterialAmbient[0]=0.19225;
	glMaterialAmbient[1]=0.19225;
	glMaterialAmbient[2]=0.19225;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.50754;
	glMaterialDiffuse[1]=0.50754;
	glMaterialDiffuse[2]=0.50754;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.508273;
	glMaterialSpecular[1]=0.508273;
	glMaterialSpecular[2]=0.508273;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.4 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,1.50f,0.0f);

	gluSphere(quadric[11],1.0f,30,30);






	//3rd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.01;
	glMaterialDiffuse[1]=0.01;
	glMaterialDiffuse[2]=0.01;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.50;
	glMaterialSpecular[1]=0.50;
	glMaterialSpecular[2]=0.50;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,14.0f,0.0f);

	gluSphere(quadric[12],1.0f,30,30);

	
	//2nd row 3 col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.1f;
	glMaterialAmbient[2]=0.06f;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.0;
	glMaterialDiffuse[1]=0.50980392;
	glMaterialDiffuse[2]=0.50980392;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.50196078;
	glMaterialSpecular[1]=0.50196078;
	glMaterialSpecular[2]=0.50195078;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,11.50f,0.0f);

	gluSphere(quadric[13],1.0f,30,30);


	//3rd row 3 col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.1;
	glMaterialDiffuse[1]=0.35;
	glMaterialDiffuse[2]=0.1;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.45;
	glMaterialSpecular[1]=0.55;
	glMaterialSpecular[2]=0.45;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,9.0f,0.0f);

	gluSphere(quadric[14],1.0f,30,30);

	//4th row 3nd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.0;
	glMaterialDiffuse[2]=0.0;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.6;
	glMaterialSpecular[2]=0.6;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,6.50f,0.0f);

	gluSphere(quadric[15],1.0f,30,30);

	//5th row and 3nd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.55;
	glMaterialDiffuse[1]=0.55;
	glMaterialDiffuse[2]=0.55;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.70;
	glMaterialSpecular[1]=0.70;
	glMaterialSpecular[2]=0.70;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,4.0f,0.0f);

	gluSphere(quadric[16],1.0f,30,30);

	//6th row 2nd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.0;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.60;
	glMaterialSpecular[1]=0.60;
	glMaterialSpecular[2]=0.50;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,1.50f,0.0f);

	gluSphere(quadric[17],1.0f,30,30);





	//4th column
	glMaterialAmbient[0]=0.02;
	glMaterialAmbient[1]=0.02;
	glMaterialAmbient[2]=0.02;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.01;
	glMaterialDiffuse[1]=0.01;
	glMaterialDiffuse[2]=0.01;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.4;
	glMaterialSpecular[1]=0.4;
	glMaterialSpecular[2]=0.4;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,14.0f,0.0f);

	gluSphere(quadric[18],1.0f,30,30);

	
	//2nd row 2 col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.05;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.4;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.5;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.04;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.7;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,11.50f,0.0f);

	gluSphere(quadric[19],1.0f,30,30);


	//3rd row 4col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.4;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.04;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.04;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,9.0f,0.0f);

	gluSphere(quadric[20],1.0f,30,30);

	//4th row 2nd col
	glMaterialAmbient[0]=0.05;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.4;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.04;
	glMaterialSpecular[2]=0.04;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,6.50f,0.0f);

	gluSphere(quadric[21],1.0f,30,30);

	//5th row and 4nd col
	glMaterialAmbient[0]=0.05;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.05;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.5;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.7;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,4.0f,0.0f);

	gluSphere(quadric[22],1.0f,30,30);

	//6th row 4nd col
	glMaterialAmbient[0]=0.05;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.04;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,1.50f,0.0f);

	gluSphere(quadric[23],1.0f,30,30);

}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if(width<height)
	{
		glOrtho(0.0f,15.5f,0.0f,15.5f* ((GLfloat)height/(GLfloat)width),-10.0f,10.0f);
	}
	else
	{
		glOrtho(0.0f,15.5f*(GLfloat(width)/GLfloat(height)),0.0f,15.5f,-10.0f,10.0f);	
	}
}
void uninitialize(void)
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		gbIsFullscreen=false;
		ShowCursor(TRUE);
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile,"Program executed successfully\n\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}
 void update()
 {
 	angleOfXRotation=angleOfXRotation+0.2f;
 	if(angleOfXRotation>360.0f)
 	{
 		angleOfXRotation=0.0f;
 	}
 	angleOfYRotation=angleOfYRotation+0.2f;
 	if(angleOfYRotation>360.0f)
 	{
 		angleOfYRotation=0.0f;
 	}
 	angleOfZRotation=angleOfZRotation+0.2f;
 	if(angleOfZRotation>360.0f)
 	{
 		angleOfZRotation=0.0f;
 	}
 }