#include<windows.h>
#include<gl/GL.h>
#include<gl/GLu.h>
#include<stdio.h>
#define _USE_DEFINES_MATH 1
#include<math.h>
#include "header.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

HDC ghdc=NULL;
HGLRC ghrc=NULL;
HWND ghwnd=NULL;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;


bool gbIsFullscreen=false;
bool gbIsActiveWindow=false;

FILE *gpFile=NULL;

GLuint texture_smily;
int keyPressed=0;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR CmdLine,int iCmdShow)
{
	int initialize(void);
	void display();
	void update();

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
	
	bool bDone=false;
	int iRet=0;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening File"),TEXT("ERROR!!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"File opened successfully\n");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.hInstance=hInstance;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY FFPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error in initialize at ChoosePixelFormat\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in initialize at SetPixelFormat\n");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"Error in initialize at wglCreateContext\n");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"Error in initialize at wglMakeurrent\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"initialize function executed successfully\n");
	}

	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd,iCmdShow);

	//game loop

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				//update
			}
			display();
		}
	}

	return((int)msg.wParam);

}


LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void resize(int,int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_ERASEBKGND :
			return(0);
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
				case 0x49:
				case VK_NUMPAD1:
					keyPressed=1;
					break;
				case 0x50:	
				case VK_NUMPAD2:
					keyPressed=2;
					break;
				case 0x51:
				case VK_NUMPAD3:
					keyPressed=3;
					break;
				case 0x52:
				case VK_NUMPAD4:
					keyPressed=4;
					break;
				
				default:
					keyPressed=0;
					break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			uninitialize();;
			break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
	
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbIsFullscreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle |WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullscreen=TRUE;
	}
}

int initialize(void)
{
	void resize(int,int);
	BOOL LoadTextures(GLuint *,TCHAR []);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);
	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}
	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_DEPTH_TEST);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//texures
	glEnable(GL_TEXTURE_2D);
	LoadTextures(&texture_smily , MAKEINTRESOURCE(IDBITMAP_SMILY));

	resize(WIN_WIDTH,WIN_HEIGHT);
	return(0);
}

BOOL LoadTextures(GLuint *texture,TCHAR imageResourceID[] )
{
	BOOL bStatus=FALSE;
	HBITMAP hBitmap=NULL;
	BITMAP bmp;
	hBitmap=(HBITMAP)LoadImage(GetModuleHandle(NULL),imageResourceID,IMAGE_BITMAP,0,0,LR_CREATEDIBSECTION);
	if(hBitmap)
	{
		bStatus=TRUE;
		GetObject(hBitmap,sizeof(BITMAP),&bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glGenTextures(1,texture);
		glBindTexture(GL_TEXTURE_2D,*texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D,3,bmp.bmWidth,bmp.bmHeight,GL_BGR_EXT,GL_UNSIGNED_BYTE,bmp.bmBits);
		
		DeleteObject(hBitmap);
	}
	return(bStatus);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);
	if(keyPressed==1)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smily);
		glBegin(GL_QUADS);
		glTexCoord2f(0.5f,0.5f);
		glVertex3f(1.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.5f);
		glVertex3f(-1.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glTexCoord2f(0.5f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);	
		glEnd();
	}
	 else if(keyPressed==2)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smily);
		glBegin(GL_QUADS);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-1.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);	
		glEnd();
	}
	 else if(keyPressed==3)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smily);
		glBegin(GL_QUADS);
		glTexCoord2f(2.0f,2.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,2.0f);
		glVertex3f(-1.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glTexCoord2f(2.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);	
		glEnd();
	}
	 else if(keyPressed==4)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smily);
		glBegin(GL_QUADS);
		glTexCoord2f(0.50f,0.50f);
		glVertex3f(1.0f,1.0f,0.0f);
		glTexCoord2f(0.50f,0.50f);
		glVertex3f(-1.0f,1.0f,0.0f);
		glTexCoord2f(0.50f,0.50f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glTexCoord2f(0.50f,0.50f);
		glVertex3f(1.0f,-1.0f,0.0f);	
		glEnd();
	}
	else
	{

		glBindTexture(GL_TEXTURE_2D,0);
		glBegin(GL_QUADS);
		glColor3f(1.0f,1.0f,1.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		glVertex3f(-1.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);
		glEnd();

	}

	SwapBuffers(ghdc);

}

void resize(int width,int height)
{
	if(height==0)
		height=1;
	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,GLfloat(width)/(GLfloat)height,0.1f,100.0f);

}
void uninitialize()
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle |WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullscreen=TRUE;
	}
	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}
	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(gpFile)
	{
		fprintf(gpFile,"Program executed successfully\n\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}