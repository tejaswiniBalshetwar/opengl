#include<windows.h>
#include<stdio.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")

HDC ghdc=NULL;
HGLRC ghrc=NULL;
HWND ghwnd=NULL;

GLUquadric *glquadric=NULL;

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

FILE *gpFile=NULL;
GLfloat glLightAmbientZero[]={0.0f,0.0f,0.0f,1.0f};
GLfloat glLightDiffuseZero[]={1.0f,0.0f,0.0f,1.0f};
GLfloat glLightSpecularZero[]={1.0f,0.0f,0.0f,1.0f};
GLfloat glLightPositionZero[]={0.0f,0.0f,0.0f,1.0f};

GLfloat glLightAmbientOne[]={0.0f,0.0f,0.0f,1.0f};
GLfloat glLightDiffuseOne[]={0.0f,1.0f,0.0f,1.0f};
GLfloat glLightSpecularOne[]={0.0f,1.0f,0.0f,1.0f};
GLfloat glLightPositionOne[]={0.0f,0.0f,0.0f,1.0f};

GLfloat glLightAmbientTwo[]={0.0f,0.0f,0.0f,1.0f};
GLfloat glLightDiffuseTwo[]={0.0f,0.0f,1.0f,1.0f};
GLfloat glLightSpecularTwo[]={0.0f,0.0f,1.0f,1.0f};
GLfloat glLightPositionTwo[]={0.0f,0.0f,0.0f,1.0f};

GLfloat glMaterialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat glMaterialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat glMaterialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat glMaterialShininess[]={100.0f};


GLfloat lightAngleZero=0.0f;
GLfloat lightAngleOne=0.0f;
GLfloat lightAngleTwo=0.0f;

bool bLight=false;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR iCmdLine,int iCmdShow)
{
	//functional declarations
	int initialize(void);
	void display(void);
	void update();

	//variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");

	bool bDone=false;
	int iRet=0;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error While opening file"),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	else 
	{
		fprintf(gpFile,"Fileopened Successfully");
	}
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszMenuName=NULL;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY FFPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet= initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error in initialize at ChoosePixelFormat");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in initialize at SetPixelFormat");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"Error in initialize at wglCreateContext");
		exit(0);
	}
	else if(iRet==-4)
	{
		fprintf(gpFile,"Error in initialize at wglMakeCurrent");
		exit(0);
	}
	else 
	{
		fprintf(gpFile,"initialize function executed Successfully");
	}

	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd,iCmdShow);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	//function declarations
	void uninitialize(void);
	void resize(int,int);
	void ToggleFullscreen();

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;
		case WM_CHAR:
			switch(wParam)
			{
				case 'l':
				case 'L':
					if(bLight==false)
					{
						bLight=true;
						glEnable(GL_LIGHTING);
					}
					else
					{
						bLight=false;
						glDisable(GL_LIGHTING);
					}
				break;
			}
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);

			}

		}	
		ShowCursor(FALSE);
		gbIsFullscreen=true;
	
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}


int initialize(void)
{
	void resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex==0)
	{
		return -1;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glClearDepth(1.0f);
	glClearColor(0.0f,0.0f,0.0f,1.0f);


	glLightfv(GL_LIGHT0,GL_AMBIENT,glLightAmbientZero);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,glLightDiffuseZero);
	glLightfv(GL_LIGHT0,GL_SPECULAR,glLightSpecularZero);
	glEnable(GL_LIGHT0);


	glLightfv(GL_LIGHT1,GL_AMBIENT,glLightAmbientOne);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,glLightDiffuseOne);
	glLightfv(GL_LIGHT1,GL_SPECULAR,glLightSpecularOne);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2,GL_AMBIENT,glLightAmbientTwo);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,glLightDiffuseTwo);
	glLightfv(GL_LIGHT2,GL_SPECULAR,glLightSpecularTwo);
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,glMaterialShininess);

	resize(WIN_WIDTH,WIN_HEIGHT);	

	return (0);

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	gluLookAt(0.0f,0.0f,3.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);

	//zero light
	glPushMatrix();
	glRotatef(lightAngleZero,1.0f,0.0f,0.0f);
	glLightPositionZero[1]=lightAngleZero;
	glLightfv(GL_LIGHT0,GL_POSITION,glLightPositionZero);
	glPopMatrix();

	//one light
	glPushMatrix();
	glRotatef(lightAngleOne,0.0f,1.0f,0.0f);
	glLightPositionOne[0]=lightAngleOne;
	glLightfv(GL_LIGHT1,GL_POSITION,glLightPositionOne);
	glPopMatrix();

	glPushMatrix();
	glRotatef(lightAngleTwo,0.0f,0.0f,1.0f);
	glLightPositionTwo[0]=lightAngleTwo;
	glLightfv(GL_LIGHT2,GL_POSITION,glLightPositionTwo);
	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glquadric=gluNewQuadric();
	gluSphere(glquadric,0.5f,30,20);

	glPopMatrix();

	SwapBuffers(ghdc);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/(GLfloat)height,0.1f,100.0f);

}
void uninitialize(void)
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}
	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(gpFile)
	{
		fprintf(gpFile, "APPLICATION executed successfully\n");
		fclose(gpFile);
		gpFile=NULL;
	}
}

void update(void)
{
	lightAngleZero=lightAngleZero+0.5f;
	if(lightAngleZero>360.0f)
	{
		lightAngleZero=0.0f;
	}

	lightAngleOne=lightAngleOne+0.5f;
	if(lightAngleOne>360.0f)
	{
		lightAngleOne=0.0f;
	}

	lightAngleTwo=lightAngleTwo+0.5f;
	if(lightAngleTwo>360.0f)
	{
		lightAngleTwo=0.0f;
	}

}