#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

bool gbIsFullScreen=false;
bool gbIsActiveWindow=false;
bool bLight=false;

GLfloat lightAmbientZero[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseZero[]={1.0f,0.0f,0.0f,1.0f};
GLfloat lightSpecularZero[]={1.0f,0.0f,0.0f,1.0f};
GLfloat lightPositionZero[]={-2.0f,0.0f,0.0f,1.0f};


GLfloat lightAmbientOne[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseOne[]={0.0f,0.0f,1.0f,1.0f};
GLfloat lightSpecularOne[]={0.0f,0.0f,1.0f,1.0f};
GLfloat lightPositionOne[]={2.0f,0.0f,0.0f,1.0f};


GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.};
GLfloat materialShininess[]={255.0f};

GLfloat anglePyramid=0.0f;

FILE *gpFile=NULL;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
	//function declarations
	void display(void);
	int initialize(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("MY FFPOGP APPLICATION");
	MSG msg;

	bool bDone=false;
	int iRet=0;


	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening File"),TEXT("ERROR!!!"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"File opened su");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error in initialize at ChoosePixelFormat\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in initialize at SetPixelFormat\n");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile,"Error in initialize at wglCreateContext\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in initialize at wglMakeCurrent\n");
		exit(0);

	}
	else
	{
		fprintf(gpFile,"Initialize executed successfully\n\n");
	}

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declarations
	void resize(int,int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_ERASEBKGND:
			return 0;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;
		case WM_CHAR:
			switch(wParam)
			{
				case 'l':
				case 'L':
					if(bLight==false)
					{
						glEnable(GL_LIGHTING);
						bLight=true;
					}
					else
					{
						glDisable(GL_LIGHTING);
						bLight=false;
					}
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			uninitialize();
			break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if(gbIsFullScreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);

		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}

		}
		ShowCursor(FALSE);
		gbIsFullScreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullScreen=false;
	}
}

int initialize(void)
{
	void resize(int,int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);
	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}
	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}


	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbientZero);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuseZero);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecularZero);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPositionZero);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1,GL_AMBIENT,lightAmbientOne);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,lightDiffuseOne);
	glLightfv(GL_LIGHT1,GL_SPECULAR,lightSpecularOne);
	glLightfv(GL_LIGHT1,GL_POSITION,lightPositionOne);
	glEnable(GL_LIGHT1);

	

	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materialDiffuse);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular	);

	resize(WIN_WIDTH,WIN_HEIGHT);

	return (0);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-6.0f);
	glRotatef(anglePyramid,0.0f,1.0f,0.0f);

	glBegin(GL_TRIANGLES);

		//front
		glNormal3f(0.0f,0.441214,0.894427f);
		glVertex3f(0.0f,1.0f,0.0f);
		//glNormal3f();
		glVertex3f(-1.0f,-1.0f,1.0f);
		//glNormal3f();
		glVertex3f(1.0f,-1.0f,1.0f);

		//back face
		glNormal3f(0.0f,0.447214f,-0.894427f);
		glVertex3f(0.0f,1.0f,0.0f);
		//glNormal3f();
		glVertex3f(1.0f,-1.0f,-1.0f);
		//glNormal3f();
		glVertex3f(-1.0f,-1.0f,-1.0f);

		//right
		glNormal3f(0.894427f,0.447214f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		//glNormal3f();
		glVertex3f(1.0f,-1.0f,1.0f);
		//glNormal3f();
		glVertex3f(1.0f,-1.0f,-1.0f);

		//left
		glNormal3f(-0.894427f,0.447214f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		//glNormal3f();
		glVertex3f(-1.0f,-1.0f,-1.0f);
		//glNormal3f();
		glVertex3f(-1.0f,-1.0f,1.0f);


	glEnd();

	SwapBuffers(ghdc);
}


void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize(void)
{
	if(gbIsFullScreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullScreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}
	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}
	if(gpFile)
	{
		fprintf(gpFile,"APPLICATION executed successfully\n\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}

void update()
{
	anglePyramid=anglePyramid+0.2f;

	if(anglePyramid>360.0f)
	{
		anglePyramid=0.0f;
	}
}