#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")


LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

struct lights
{
	GLfloat lightAmbient[4];
	GLfloat lightDiffuse[4];
	GLfloat lightSpecular[4];
	GLfloat lightPosition[4];
	GLfloat angle;
};


lights light[3];


GLfloat glMaterialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat glMaterialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat glMaterialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat glMaterialShininess[]={128.0f};

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL;


bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

bool bLight=false;

FILE *gpFile=NULL;

GLUquadric *quadric=NULL;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
	//function declaration
	int initialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR lpszClassName[]=TEXT("MY FFPOGL APPLICATION");
	HWND hwnd;

	bool bDone=false;
	int iRet=0;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR!!"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"File opened successfully\n");
	}

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY OPENGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error in initialize at ChoosePixelFormat\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile,"Error in  initialize at SetPixelFormat\n");
		exit(0);
	}
		else if(iRet==-3)
	{
		fprintf(gpFile,"Error in  initialize at wglCreatecontext\n");
		exit(0);
	}
		else if(iRet==-4)
	{
		fprintf(gpFile,"Error in  initialize at wglMakeCurrent\n");
		exit(0);
	}
	else 
	{
		fprintf(gpFile,"initialize function executed successfully\n");
	}

	SetFocus(hwnd);
	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int,int);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CHAR:
			switch(wParam)
			{
				case 'l':
				case 'L':
					if(bLight==false)
					{
						glEnable(GL_LIGHTING);
						bLight=true;
					}
					else
					{
						glDisable(GL_LIGHTING);
						bLight=false;
					}
					break;
			}
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			uninitialize();
			break;

	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}



void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbIsFullscreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
}


int initialize(void)
{
	void resize(int,int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;
	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cBlueBits=8;
	pfd.cGreenBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=8;

	ghdc=GetDC(ghwnd);

	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}
	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glShadeModel(GL_SMOOTH);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	//initialitionof light
	light[0].lightAmbient[0]=0.0f;
	light[0].lightAmbient[1]=0.0f;
	light[0].lightAmbient[2]=0.0f;
	light[0].lightAmbient[3]=1.0f;

	light[0].lightDiffuse[0]=1.0f;
	light[0].lightDiffuse[1]=0.0f;
	light[0].lightDiffuse[2]=0.0f;
	light[0].lightDiffuse[3]=1.0f;

	light[0].lightSpecular[0]=1.0f;
	light[0].lightSpecular[1]=0.0f;
	light[0].lightSpecular[2]=0.0f;
	light[0].lightSpecular[3]=1.0f;


	light[0].lightPosition[0]=0.0f;
	light[0].lightPosition[1]=0.0f;
	light[0].lightPosition[2]=0.0f;
	light[0].lightPosition[3]=1.0f;

	light[0].angle=0.0f;


	//second light

	light[1].lightAmbient[0]=0.0f;
	light[1].lightAmbient[1]=0.0f;
	light[1].lightAmbient[2]=0.0f;
	light[1].lightAmbient[3]=1.0f;

	light[1].lightDiffuse[0]=0.0f;
	light[1].lightDiffuse[1]=1.0f;
	light[1].lightDiffuse[2]=0.0f;
	light[1].lightDiffuse[3]=1.0f;

	light[1].lightSpecular[0]=0.0f;
	light[1].lightSpecular[1]=1.0f;
	light[1].lightSpecular[2]=0.0f;
	light[1].lightSpecular[3]=1.0f;


	light[1].lightPosition[0]=0.0f;
	light[1].lightPosition[1]=0.0f;
	light[1].lightPosition[2]=0.0f;
	light[1].lightPosition[3]=1.0f;

	light[1].angle=0.0f;


	//third angle

	light[2].lightAmbient[0]=0.0f;
	light[2].lightAmbient[1]=0.0f;
	light[2].lightAmbient[2]=0.0f;
	light[2].lightAmbient[3]=1.0f;

	light[2].lightDiffuse[0]=0.0f;
	light[2].lightDiffuse[1]=0.0f;
	light[2].lightDiffuse[2]=1.0f;
	light[2].lightDiffuse[3]=1.0f;

	light[2].lightSpecular[0]=0.0f;
	light[2].lightSpecular[1]=0.0f;
	light[2].lightSpecular[2]=1.0f;
	light[2].lightSpecular[3]=1.0f;


	light[2].lightPosition[0]=0.0f;
	light[2].lightPosition[1]=0.0f;
	light[2].lightPosition[2]=0.0f;
	light[2].lightPosition[3]=1.0f;

	light[2].angle=0.0f;


	glLightfv(GL_LIGHT0,GL_AMBIENT,light[0].lightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light[0].lightDiffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light[0].lightSpecular);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1,GL_AMBIENT,light[1].lightAmbient);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,light[1].lightDiffuse);
	glLightfv(GL_LIGHT1,GL_SPECULAR,light[1].lightSpecular);
	glEnable(GL_LIGHT1);


	glLightfv(GL_LIGHT2,GL_AMBIENT,light[2].lightAmbient);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,light[2].lightDiffuse);
	glLightfv(GL_LIGHT2,GL_SPECULAR,light[2].lightSpecular);
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,glMaterialShininess);

	resize(WIN_WIDTH,WIN_HEIGHT);
	return(0);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	gluLookAt(0.0f,0.0f,4.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);

	glPushMatrix();
	glRotatef(light[0].angle,1.0f,0.0f,0.0f);
	light[0].lightPosition[1]=light[0].angle;
	glLightfv(GL_LIGHT0,GL_POSITION,light[0].lightPosition);
	glPopMatrix();

	glPushMatrix();
	glRotatef(light[1].angle,0.0f,1.0f,0.0f);
	light[1].lightPosition[0]=light[1].angle;
	glLightfv(GL_LIGHT1,GL_POSITION,light[1].lightPosition);
	glPopMatrix();

	glPushMatrix();
	glRotatef(light[2].angle,0.0f,0.0f,1.0f);
	light[2].lightPosition[1]=light[2].angle;
	glLightfv(GL_LIGHT2,GL_POSITION,light[2].lightPosition);
	glPopMatrix();

	quadric=gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	gluSphere(quadric,0.6f,20,20);
	
	glPopMatrix();
	SwapBuffers(ghdc);
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/(GLsizei)height,0.1f,100.0f);
}

void uninitialize(void)
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile,"APPLICATION executed successfully\n\n");
		fclose(gpFile);
		gpFile=NULL;
	}
}



void update()
{
	light[0].angle=light[0].angle+0.2f;
	if(light[0].angle>360.0f)
	{
		light[0].angle=360.0f;
	}

	light[1].angle=light[1].angle+0.2f;
	if(light[1].angle>360.0f)
	{
		light[1].angle=360.0f;
	}

	light[2].angle=light[2].angle+0.2f;
	if(light[2].angle>360.0f)
	{
		light[2].angle=360.0f;
	}
}