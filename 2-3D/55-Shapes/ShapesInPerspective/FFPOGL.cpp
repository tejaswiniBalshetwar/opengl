#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define _USE_MATH_DEFINES 1
#include<math.h>
#include<stdio.h>


#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

HWND ghwnd = NULL;
HGLRC ghrc = NULL;
HDC ghdc = NULL;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;

bool gbIsActivewindow = false;
bool gbIsFullscreen = false;

FILE *gpFile = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpstrCmdLine, int iCmdShow)
{
	//function declarations
	void display(void);
	int initialize(void);
	void ToggleFullscreen();
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR lpszClassName[] = TEXT("My Opengl Application");

	int iRet = 0;
	bool bDone = false;

	fopen_s(&gpFile, "log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Error while opening File."), TEXT("ERROR!!"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File opened successfully..\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hInstance = hInstance;

	RegisterClassEx(&wndclass);
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("MY FFPOGL APPLICATION"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
	ghwnd = hwnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "Error in initialize at ChoosePixelFormat \n");
		exit(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "Error in initialize at SetPixelFormat\n");
		exit(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "Error in initialize at wglCreateContext\n");
		exit(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Error in initialize at wglMakeCurrent\n");
		exit(0);
	}

	else
	{
		fprintf(gpFile, "initialize executed successfully\n");
	}

	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd, iCmdShow);
	ToggleFullscreen();

	//game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActivewindow)
			{
				//update
			}
			display();
		}
	}

	return((int)msg.wParam);


}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declarations
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsActivewindow = true;
		break;
	case WM_KILLFOCUS:
		gbIsActivewindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if (gbIsFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);

			}
		}
		ShowCursor(FALSE);
		gbIsFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen = false;

	}


}

int initialize()
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat = 0;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormat = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormat == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormat, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	//glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glClearDepth(1.0f);
	//glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	GLfloat xOfPoint = -4.0f;
	GLfloat yOfPoint = 2.0f;
	GLfloat xOfGrid = 4.0f;
	GLfloat yOfGrid = 2.0f;



	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor3f(1.0f,1.0f,1.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glPointSize(5.0f);
	glBegin(GL_POINTS);

	for (int i = 0; i < 4; i++)
	{
		xOfPoint = -4.0f;
		for (int j = 0; j < 4; j++)
		{
			glVertex3f(xOfPoint, yOfPoint, 0.0f);
			xOfPoint = xOfPoint + 0.5f;
		}
		yOfPoint = yOfPoint - 0.5f;
	}

	glEnd();

	//2nd shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -6.0f);
	glBegin(GL_LINES);
	xOfGrid = -0.50f;
	for (int j = 0; j < 3; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid, yOfGrid - 1.5f, 0.0f);
		xOfGrid = xOfGrid + 0.5f;
	}

	xOfGrid = -0.50f;
	yOfGrid = 2.0f;
	for (int j = 0; j < 3; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid + 1.5f, yOfGrid, 0.0f);
		yOfGrid = yOfGrid - 0.5f;
	}

	/*xOfGrid = -0.50f;
	
	for (int j = 0; j < 3; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		xOfGrid = xOfGrid - 0.5f;
		yOfGrid = yOfGrid - 0.5f;

	}*/
	
	glVertex3f(xOfGrid, yOfGrid, 0.0f);
	glVertex3f(xOfGrid + 1.5f, yOfGrid + 1.5f, 0.0f);
	
	glVertex3f(xOfGrid , yOfGrid+0.50f, 0.0f);
	glVertex3f(xOfGrid + 1.0f, yOfGrid + 1.5f, 0.0f);
	
	glVertex3f(xOfGrid , yOfGrid+1.00f, 0.0f);
	glVertex3f(xOfGrid + 0.50f, yOfGrid + 1.5f, 0.0f);

	glVertex3f(xOfGrid+0.5f, yOfGrid , 0.0f);
	glVertex3f(xOfGrid + 1.5f, yOfGrid + 1.0f, 0.0f);
	
	glVertex3f(xOfGrid + 1.0f, yOfGrid, 0.0f);
	glVertex3f(xOfGrid + 1.5f, yOfGrid + 0.5f, 0.0f);
	
	glEnd();

	// 3 rd shape for grid
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);

	xOfGrid = 4.0f;
	yOfGrid = 2.0f;
	for (int j = 0; j < 4; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid, yOfGrid - 1.5f, 0.0f);
		xOfGrid = xOfGrid - 0.5f;
	}

	yOfGrid = 2.0f;
	xOfGrid = 4.0f;
	for (int j = 0; j < 4; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid - 1.5f, yOfGrid, 0.0f);
		yOfGrid = yOfGrid - 0.5f;
	}


	//4th shape



	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-6.0f);



	glBegin(GL_LINES);

	xOfGrid = -4.0f;
	yOfGrid = -2.0f;
	for (int j = 0; j < 4; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid, yOfGrid + 1.5f, 0.0f);
		xOfGrid = xOfGrid + 0.5f;
	}

	yOfGrid = -2.0f;
	xOfGrid = -4.0f;
	for (int j = 0; j < 4; j++)
	{
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		glVertex3f(xOfGrid + 1.5f, yOfGrid, 0.0f);
		yOfGrid = yOfGrid + 0.5f;
	}

	xOfGrid = -4.0f;
	yOfGrid = -2.0f;

	glVertex3f(xOfGrid, yOfGrid, 0.0f);
	glVertex3f(xOfGrid + 1.5f, yOfGrid + 1.5f, 0.0f);

	glVertex3f(xOfGrid, yOfGrid + 0.50f, 0.0f);
	glVertex3f(xOfGrid + 1.0f, yOfGrid + 1.5f, 0.0f);

	glVertex3f(xOfGrid, yOfGrid + 1.00f, 0.0f);
	glVertex3f(xOfGrid + 0.50f, yOfGrid + 1.5f, 0.0f);

	glVertex3f(xOfGrid + 0.5f, yOfGrid, 0.0f);
	glVertex3f(xOfGrid + 1.5f, yOfGrid + 1.0f, 0.0f);

	glVertex3f(xOfGrid + 1.0f, yOfGrid, 0.0f);
	glVertex3f(xOfGrid + 1.5f, yOfGrid + 0.5f, 0.0f);
	glEnd();



	//5th shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -6.0f);
	glBegin(GL_LINES);
	//quad
	glVertex3f(-0.50f, -0.50f, 0.0f);
	glVertex3f(1.0f, -0.50f, 0.0f);

	glVertex3f(-0.5f, -0.50f, 0.0f);
	glVertex3f(-0.50f, -2.0f, 0.0f);

	glVertex3f(-0.50f, -2.0f, 0.0f);
	glVertex3f(1.0f, -2.0f, 0.0f);

	glVertex3f(1.0f, -2.0f, 0.0f);
	glVertex3f(1.0f, -0.50f, 0.0f);
	//end quad
	//lines from start point

	xOfGrid = 1.0f;
	yOfGrid = -2.0f;
	for (int i = 0; i < 3; i++)
	{
		glVertex3f(-0.50f, -0.50f, 0.0f);
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		yOfGrid = yOfGrid + 0.5f;
	}

	xOfGrid = 1.0f;
	yOfGrid = -2.0f;
	for (int i = 0; i < 3; i++)
	{
		glVertex3f(-0.50f, -0.50f, 0.0f);
		glVertex3f(xOfGrid, yOfGrid, 0.0f);
		xOfGrid = xOfGrid - 0.5f;
	}

	glEnd();


	//6th shape
	//for colored grid
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-6.0f);

	glBegin(GL_QUADS);
	glColor3f(1.0f,0.0f,0.0f);
	glVertex3f(2.50f, -0.5f, 0.0f);
	glVertex3f(3.0f, -0.50f, 0.0f);
	glVertex3f(3.0f, -2.0f, 0.0f);
	glVertex3f(2.50f, -2.0f, 0.0f); 
	glEnd();


	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(3.0f, -0.5f, 0.0f);
	glVertex3f(3.50f, -0.5f, 0.0f);
	glVertex3f(3.50f, -2.0f, 0.0f);
	glVertex3f(3.0f, -2.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(3.50f, -0.5f, 0.0f);
	glVertex3f(4.0f, -0.5f, 0.0f);
	glVertex3f(4.0f, -2.0f, 0.0f);
	glVertex3f(3.50f, -2.0f, 0.0f);
	
	glEnd();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(0.0f, 0.0f, -6.0f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(2.50f,-0.5f,0.0f);
	glVertex3f(4.0f,-0.5f,0.0f);

	glVertex3f(2.50f, -2.0f, 0.0f);
	glVertex3f(4.0f, -2.0f, 0.0f);


	for (int i = 0; i < 4; i++)
	{
		xOfGrid = 4.0f;
		yOfGrid = -2.0;
		for (int j = 0; j < 4; j++)
		{

			glVertex3f(xOfGrid, yOfGrid, 0.0f);
			glVertex3f(xOfGrid, yOfGrid + 1.5f, 0.0f);
			xOfGrid = xOfGrid - 0.5f;
		}

	}

	glEnd();

	SwapBuffers(ghdc);



}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, GLsizei(width), GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, GLfloat(width) / GLfloat(height), 0.1f, 100.0f);

}
void uninitialize()
{
	if (gbIsFullscreen)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen = FALSE;
	}

	if (ghrc == wglGetCurrentContext())
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Program executed successfully..EXITIN");
		fclose(gpFile);
		gpFile = NULL;
	}

}