#include<GL/freeglut.h>


int year = 0;
int day = 0;
bool bIsFullscreen = false;
int main(int argc, char * argv[])
{
	//function Declaration
	void initialize();
	void uninitialize();
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);

	//code

	glutInit(&argc, argv);
	glutInitDisplayMode, (GLUT_DOUBLE|GLUT_DEPTH| GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First OpenGL Program-Tejaswini Balshetwar");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;

}


void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_FLAT);

}

void uninitialize(void)
{

}

void reshape(int width, int height)
{
	if (height == 0)
		height = 1;
	
	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f,GLfloat(width)/GLfloat(height),1.0f,20.0f);

}
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0f,0.0f,5.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
	glPushMatrix();

	glutWireSphere(1.0f,20.0f,16.0f);

	glRotatef(GLfloat(year),0.0f,-1.0f,0.0f);
	
	glTranslatef(2.0f,0.0f,0.0f);
	glRotatef(GLfloat(day),0.0f,1.0f,0.0f);
	
	glutWireSphere(0.2f,10.0f,8.0f);

	glPopMatrix();

	glutSwapBuffers();


}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullscreen == false)
		{
			glutFullScreen();
			bIsFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullscreen = false;
		}
		break;

	case 'Y':
		year = (year + 5) % 360;
		glutPostRedisplay();
		break;
	case 'y':
		year = (year - 5) % 360;
		glutPostRedisplay();
		break;

	case 'd':
		day = (day + 10) % 360;
		glutPostRedisplay();
		break;
	case 'D':
		day = (day - 10) % 360;
		glutPostRedisplay();
		break;

	}

}


void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}