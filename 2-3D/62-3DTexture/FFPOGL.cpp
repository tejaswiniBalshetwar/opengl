#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include "header.h"
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")

HDC ghdc=NULL;
HWND ghwnd=NULL;
HGLRC ghrc=NULL;

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

FILE *gpFile=NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

GLfloat anglePyramid=0.0f;
GLfloat angleCube=0.0f;

GLuint texture_stone;
GLuint texture_Kundali;

int WINAPI WinMain(HINSTANCE hInstance ,HINSTANCE hPrevInstance,LPSTR lpstrCmdLine,int iCmdShow)
{
	int initialize(void);
	void display(void);
	void update();
	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR lpszClassName[]=TEXT("MY OPENGL APPLICATION");
	MSG msg;

	bool bDone=false;
	int iRet=0;

	fopen_s(&gpFile,"log.txt","w");
	if(gpFile==NULL)
	{
		MessageBox(NULL,TEXT("Error while opening file"),TEXT("ERROR"),MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"File opened successfully");
	}
	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpszClassName=lpszClassName;
	wndclass.lpszMenuName=NULL;
	wndclass.hInstance=hInstance;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("MY FFPOGL APPLICATION"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,100,100,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
	ghwnd=hwnd;

	iRet=initialize();
	if(iRet==-1)
	{
		fprintf(gpFile,"Error in initialize at ChoosePixelFormat\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile, "Error in initialize at SetPixelFormat\n");
		exit(0);
	}
	else if(iRet==-3)
	{
		fprintf(gpFile, "Error in initialize at wglCreateContext\n");
		exit(0);
	}
	else if(iRet==-2)
	{
		fprintf(gpFile, "Error in initialize at wglMakeCurrent\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"initialize function executed successfully\n\n");
	}

	SetFocus(hwnd);
	SetForegroundWindow(hwnd);
	ShowWindow(hwnd,iCmdShow);

	//game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbIsActiveWindow)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);

}	

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void uninitialize();
	void ToggleFullscreen();
	void resize(int,int);

	switch(iMsg)
	{
		case WM_SETFOCUS:
			gbIsActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbIsActiveWindow=false;
			break;
		case WM_ERASEBKGND:
			return(0);
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
					ToggleFullscreen();
					break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_DESTROY:
			uninitialize();
			PostQuitMessage(0);
			break;

	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if(gbIsFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi={sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
	
			}
		}
		ShowCursor(FALSE);
		gbIsFullscreen=true;
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,WS_OVERLAPPEDWINDOW|dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;

	}
}

int initialize()
{
	void resize(int,int);
	BOOL LoadTexture(GLuint *,TCHAR []);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelIndex=0;

	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	ghdc=GetDC(ghwnd);
	iPixelIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelIndex==0)
	{
		return -1;
	}
	if(SetPixelFormat(ghdc,iPixelIndex,&pfd)==FALSE)
	{
		return -2;
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		return -3;
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		return -4;
	}

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//texture code
	glEnable(GL_TEXTURE_2D);
	LoadTexture(&texture_stone,MAKEINTRESOURCE(IDBITMAP_STONE));
	LoadTexture(&texture_Kundali,MAKEINTRESOURCE(IDBITMAP_KUNDALI));


	resize(WIN_WIDTH,WIN_HEIGHT);
	return(0);


}

BOOL LoadTexture(GLuint *texture,TCHAR imageResourceId[])
{
	HBITMAP hBitmap=NULL;
	BITMAP bmp;
	BOOL bStatus=FALSE;
	hBitmap=(HBITMAP)LoadImage(GetModuleHandle(NULL),imageResourceId,IMAGE_BITMAP,0,0,LR_CREATEDIBSECTION);
	if(hBitmap)
	{
		bStatus=TRUE;

		GetObject(hBitmap,sizeof(BITMAP),&bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glGenTextures(1,texture);
		glBindTexture(GL_TEXTURE_2D,*texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		gluBuild2DMipmaps(GL_TEXTURE_2D,3,bmp.bmWidth,bmp.bmHeight,GL_BGR_EXT,GL_UNSIGNED_BYTE,bmp.bmBits);

		DeleteObject(hBitmap);
	}

	return bStatus;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.0f,0.0f,-7.0f);
	glRotatef(anglePyramid,0.0f,1.0f,0.0f);
	glBindTexture(GL_TEXTURE_2D,texture_stone);

	glBegin(GL_TRIANGLES);
		glTexCoord2f(0.50f, 1.0f);
		glVertex3f(0.0f,1.0f,0.0f);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

		glTexCoord2f(0.50f, 1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);


		glTexCoord2f(0.50f, 1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		glTexCoord2f(0.50f, 1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBindTexture(GL_TEXTURE_2D,texture_Kundali);
	glTranslatef(2.0f,0.0f,-7.0f);
	glRotatef(angleCube,1.0f,1.0f,1.0f);
	glScalef(0.75f,0.75f,0.75f);
	glBegin(GL_QUADS);
	//top face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f,1.0f,-1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f,1.0f,-1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f,1.0f,1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f,1.0f,1.0f);

	//bottom face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f,-1.0f,-1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f,-1.0f,1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f,-1.0f,1.0f);

	//front face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f,1.0f,1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f,1.0f,1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f,-1.0f,1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f,-1.0f,1.0f);

	//back face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f,1.0f,-1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f,1.0f,-1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f,-1.0f,-1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);

	//right face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f,1.0f,-1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f,1.0f,1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f,-1.0f,1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f,-1.0f,-1.0f);

	//left face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f,1.0f,1.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f,1.0f,-1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f,-1.0f,1.0f);

	glEnd();
	SwapBuffers(ghdc);
}

void resize(int width,int height)
{
	if(height==0)
		height=1;

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}
void uninitialize()
{
	if(gbIsFullscreen)
	{
		SetWindowLong(ghwnd,GWL_STYLE,WS_OVERLAPPEDWINDOW|dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_FRAMECHANGED|SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOMOVE);

		ShowCursor(TRUE);
		gbIsFullscreen=false;
	}
	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}
	if(ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc=NULL;
	}
	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc=NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile,"Program Executed successfully\n\n");
		fclose(gpFile);
		gpFile=NULL;
	}

}
void update()
{
	anglePyramid=anglePyramid+0.2f;
	if(anglePyramid>360.0f)
	{
		anglePyramid=0.0f;
	}
	
	angleCube=angleCube+0.2f;
	if(angleCube>360.0f)
	{
		angleCube=0.0f;
	}	
}