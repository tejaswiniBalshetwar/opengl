#include <Windows.h>
#include <d3d11.h>
#include <stdio.h>
#include <d3dcompiler.h>


#pragma warning(disable:4838)
#include "XNAMATH\xnamath.h"


#pragma comment(lib,"D3dcompiler.lib")
#pragma comment(lib,"d3d11.lib")

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile;
char szLogFileName[] = "Log.txt";

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

HWND ghwnd;

bool gbIsWindowActive = false;
bool gbFullScreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11HullShader *gpID3D11HullShader = NULL;
ID3D11DomainShader *gpID3D11DomainShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer = NULL;
ID3D11Buffer *gpID3D11Buffer_ColorBuffer = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_HullShader = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;

struct CBUFFER_HULL_SHADER
{
	XMVECTOR Hull_Constant_Function_Params;
};

struct CBUFFER_DOMAIN_SHADER
{
	XMMATRIX WorldViewProjectionMatrix;
};

struct CBUFFER_PIXEL_SHADER
{
	XMVECTOR LineColor;
};

unsigned int guiNumberOfLineSegments = 1;

XMMATRIX gPerspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT initialize();
	void uninitialize();
	void display();

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[] = TEXT("MY DirectX11 Window");
	bool bDone = false;

	if (fopen_s(&gpFile, szLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Error while Opening file"), TEXT("ERROR!!!"), MB_OK | MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Opened successfully");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("My D3D11 Window"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nError in initialize()");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nSuccessfull  initialize()");
		fclose(gpFile);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		}
		else
		{
			if (gbIsWindowActive)
			{
				display();
			}
		}
	}


	return((int)msg.lParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullscreen();
	void uninitialize();

	HRESULT hr;

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsWindowActive = true;
		break;
	case WM_KILLFOCUS:
		gbIsWindowActive = false;
		break;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, szLogFileName, "a+");
				fprintf(gpFile, "\nError in resize()");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, szLogFileName, "a+");
				fprintf(gpFile, "\n Successfull resize()");
				fclose(gpFile);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		case VK_UP:
			guiNumberOfLineSegments++;
			if (guiNumberOfLineSegments >= 50)
			{
				guiNumberOfLineSegments = 50;
			}
			break;
		case VK_DOWN:
			guiNumberOfLineSegments--;
			if (guiNumberOfLineSegments <= 0)
			{
				guiNumberOfLineSegments = 1;
			}
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, ~WS_OVERLAPPEDWINDOW&dwStyle);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | dwStyle);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);

		gbFullScreen = false;
	}
}

HRESULT initialize()
{
	void uninitialize();

	HRESULT resize(int, int);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_aquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverType = 0;
	UINT numFeatureLevels = 1;
	numDriverType = sizeof(d3dDriverTypes) / d3dDriverTypes[0];

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WINDOW_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WINDOW_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverType; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_aquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapchain() Failed ");
		fclose(gpFile);

		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");

		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapchain() Succeded ");
		fprintf(gpFile, "\n Chose Drive Version is:=  ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "\n Hardware Typr");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\n WARP Type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\n Reference Type");
		}

		fprintf(gpFile, "\n The Supported Highest Feature Level:= ");
		if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_11_0 ");
		}
		else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_10_1 ");
		}
		else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_10_0 ");
		}
		else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_9_1)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_9_1 ");
		}
		else
		{
			fprintf(gpFile, "\n Unknows ");
		}

		fclose(gpFile);
	}

	//code for vertex shader
	const char* vertexShaderSourceCode =
		
		"struct vertex_output" \
		"{" \
			"float4 position:POSITION;" \
			
		"};" \

		"vertex_output main(float2 pos:POSITION)" \
		"{"	\
			"vertex_output output;" \
			"output.position=float4(pos,0.0f,1.0f);"\
			"return(output);"\
		"}";

	ID3DBlob* pID3DBlob_vertexShaderCode = NULL;
	ID3DBlob* Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",								//Source code string
		NULL,								//D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,	//Include file ahet ka refer automation midl file
		"main",								//Entry point function address
		"vs_5_0",							//Version/ model number for shader
		0,									// shader compile kasa karava 5 ways ahet fxc.exe 
		0,									//Effect constant
		&pID3DBlob_vertexShaderCode,		//compiled code thevnya sathi
		&Error								//Error for verstex shader
	);

	if (FAILED(hr))
	{
		if (Error != NULL)
		{
			fopen_s(&gpFile, szLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for vertex shader:%s\n", (char*)Error->GetBufferPointer());
			fclose(gpFile);
			Error->Release();
			Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nD3DCompile() vertex shader successfull \n");
		fclose(gpFile);
	}
	//Creating shader and attaching shader to the shader object
	///create function always called on device
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_vertexShaderCode->GetBufferPointer(),
		pID3DBlob_vertexShaderCode->GetBufferSize(),
		NULL,				//Class linkage parameter across shader variable sathi
		&gpID3D11VertexShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CrteateVertexShader() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CrteateVertexShader() succeded \n");
		fclose(gpFile);
	}

	//Device always set the tings to pipeline which contros the pipelines
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, NULL, NULL);
	
	//HULL Shader
	const char* hullShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4 hull_constant_function_params; " \
		"}" \
		"struct vertex_output" \
		"{" \
			"float4 position:POSITION;" \
		"};" \
		"struct hull_constant_output" \
		"{" \
		"float edges[2] : SV_TESSFACTOR;" \
		"};" \
		"hull_constant_output hull_constant_function(void)" \
		"{" \
			"hull_constant_output output;" \
			"float numberOfStrips=hull_constant_function_params[0];" \
			"float numberOfSegments=hull_constant_function_params[1];" \
			"output.edges[0]=numberOfStrips;" \
			"output.edges[1]=numberOfSegments;" \
			"return(output);" \
		"}" \
		"struct hull_output" \
		"{" \
			"float4 position : POSITION;" \
		"};" \
		"[domain (\"isoline\")]" \
		"[partitioning(\"integer\")]" \
		"[outputtopology(\"line\")]" \
		"[outputcontrolpoints(4)]" \
		"[patchconstantfunc(\"hull_constant_function\")]" \
		"hull_output main(InputPatch< vertex_output, 4> input_patch,uint i: SV_OUTPUTCONTROLPOINTID)" \
		"{"	\
			"hull_output output1;" \
			"output1.position=input_patch[i].position;"\
			"return(output1);" \
		"}";

	ID3DBlob* pID3DBlob_hullShaderCode = NULL;
	Error = NULL;

	hr = D3DCompile(hullShaderSourceCode,
		lstrlenA(hullShaderSourceCode) + 1,
		"HS",								//Source code string
		NULL,								//D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,	//Include file ahet ka refer automation midl file
		"main",								//Entry point function address
		"hs_5_0",							//Version/ model number for shader
		0,									// shader compile kasa karava 5 ways ahet fxc.exe 
		0,									//Effect constant
		&pID3DBlob_hullShaderCode,		//compiled code thevnya sathi
		&Error								//Error for verstex shader
	);

	if (FAILED(hr))
	{
		if (Error != NULL)
		{
			fopen_s(&gpFile, szLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for HULL shader:%s\n", (char*)Error->GetBufferPointer());
			fclose(gpFile);
			Error->Release();
			Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nD3DCompile() Hull shader successfull \n");
		fclose(gpFile);
	}
	//Creating shader and attaching shader to the shader object
	///create function always called on device
	hr = gpID3D11Device->CreateHullShader(pID3DBlob_hullShaderCode->GetBufferPointer(),
		pID3DBlob_hullShaderCode->GetBufferSize(),
		NULL,				//Class linkage parameter across shader variable sathi
		&gpID3D11HullShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CrteateGeometryShader() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CrteateGeometryShader() succeded \n");
		fclose(gpFile);
	}

	//Device always set the tings to pipeline which contros the pipelines
	gpID3D11DeviceContext->HSSetShader(gpID3D11HullShader, NULL, NULL);

	pID3DBlob_hullShaderCode->Release();
	pID3DBlob_hullShaderCode = NULL;


	//Domain Shader
	const char* domainShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix; " \
		"}" \

		"struct hull_constant_output" \
		"{" \
		"float edges[2] : SV_TESSFACTOR;" \
		"};" \
		
		"struct hull_output" \
		"{" \
		"float4 position : POSITION;" \
		"};" \

		"struct domain_output" \
		"{" \
		"float4 position : SV_POSITION;" \
		"};" \

		"[domain (\"isoline\")]" \
		
		
		"domain_output main(hull_constant_output input, OutputPatch<hull_output,4> output_patch,float2 tessCoord : SV_DOMAINLOCATION )" \
		"{"	\
			"domain_output output;" \
			"float u= tessCoord.x;" \
			"float3 p0=output_patch[0].position.xyz;" \
			"float3 p1=output_patch[1].position.xyz;" \
			"float3 p2=output_patch[2].position.xyz;" \
			"float3 p3=output_patch[3].position.xyz;" \
			"float u1= (1.0f- u);" \
			"float u2= (u*u);" \
			"float b3= u2*u;" \
			"float b2= 3.0f * u2 * u1;" \
			"float b1= 3.0f * u *  u1 * u1;" \
			"float b0= u1 *  u1 * u1;" \
			"float3 p= p0 * b0 +p1 * b1 + p2 * b2 +p3 * b3 ;" \
		"output.position=mul(worldViewProjectionMatrix,float4(p,1.0f));"\
		"return(output);" \
		"}";

	ID3DBlob* pID3DBlob_domainShaderCode = NULL;
	Error = NULL;

	hr = D3DCompile(domainShaderSourceCode,
		lstrlenA(domainShaderSourceCode) + 1,
		"DS",								//Source code string
		NULL,								//D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,	//Include file ahet ka refer automation midl file
		"main",								//Entry point function address
		"ds_5_0",							//Version/ model number for shader
		0,									// shader compile kasa karava 5 ways ahet fxc.exe 
		0,									//Effect constant
		&pID3DBlob_domainShaderCode,		//compiled code thevnya sathi
		&Error								//Error for verstex shader
	);

	if (FAILED(hr))
	{
		if (Error != NULL)
		{
			fopen_s(&gpFile, szLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for Domain shader:%s\n", (char*)Error->GetBufferPointer());
			fclose(gpFile);
			Error->Release();
			Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nD3DCompile() Domain shader successfull \n");
		fclose(gpFile);
	}
	//Creating shader and attaching shader to the shader object
	///create function always called on device
	hr = gpID3D11Device->CreateDomainShader(pID3DBlob_domainShaderCode->GetBufferPointer(),
		pID3DBlob_domainShaderCode->GetBufferSize(),
		NULL,				//Class linkage parameter across shader variable sathi
		&gpID3D11DomainShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CrteateGeometryShader() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CrteateGeometryShader() succeded \n");
		fclose(gpFile);
	}

	//Device always set the tings to pipeline which contros the pipelines
	gpID3D11DeviceContext->DSSetShader(gpID3D11DomainShader, NULL, NULL);

	pID3DBlob_domainShaderCode->Release();
	pID3DBlob_domainShaderCode = NULL;

	



	//pixel shader start
	const char* pixelShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4 lineColor; " \
		"}" \
		"float4 main(void): SV_TARGET" \
		"{" \
		"float4 color=lineColor;" \
		"return(color);" \
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&Error
	);

	if (FAILED(hr))
	{
		if (Error != NULL)
		{
			fopen_s(&gpFile, szLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader:%s\n", (char*)Error->GetBufferPointer());
			fclose(gpFile);
			Error->Release();
			Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nD3DCompile() pixel shader successfull \n");
		fclose(gpFile);
	}
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreatePixelShader() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreatePixelShader() succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, NULL);


	//create and set input layout je pan glVertexAttribPointer() sarakha in opengl
	D3D11_INPUT_ELEMENT_DESC inputElementDesc;
	ZeroMemory(&inputElementDesc,sizeof(D3D11_INPUT_ELEMENT_DESC));
	inputElementDesc.SemanticName = "POSITION";	//shader madhala float4 pos:POSITION same pahije
	inputElementDesc.SemanticIndex = 0;		//mi position 0 la takat ahe
	inputElementDesc.Format = DXGI_FORMAT_R32G32_FLOAT;	//how many per vertex point we are passing glVertexAttribPointer(AMC,3); 
	inputElementDesc.InputSlot = 0;								// layout in opengl is input slot here
	inputElementDesc.AlignedByteOffset = 0;						//if we have multiple entries for per vertex data then offset of the data 
	inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;	//Per vertex data ahe ha
	inputElementDesc.InstanceDataStepRate = 0;	 //per instance input slot asata tar

	
	hr = gpID3D11Device->CreateInputLayout(&inputElementDesc, 1, pID3DBlob_vertexShaderCode->GetBufferPointer(), pID3DBlob_vertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateInputLayout() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateInputLayout() succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_vertexShaderCode->Release();
	pID3DBlob_vertexShaderCode = NULL;


	//second stage
	//create vertex buffer
	

	//create vertex buffer
	float vertices[] = {
			-1.0f,-1.0f,-0.5f,1.0f,0.5f,-1.0f,1.0f,1.0f
	};

	//glBufferData() la data pathavnyasathi
	D3D11_BUFFER_DESC bufferDesc_vertexBuffer;
	ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));

	bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;	//static vaprala tar createBuffer() la second parameter madhe 
	bufferDesc_vertexBuffer.ByteWidth = sizeof(float)*_ARRAYSIZE(vertices);
	bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_VertexBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateBuffer() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateBuffer() succeded \n");
		fclose(gpFile);
	}

	//copy vertices into abouve buffer

	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, vertices, sizeof(vertices));	//memory mapped IO lihita 
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer, 0); //jo loack kela tyala unlock kara so that to pudhe set karatana successfull hoil bach theory 




	//Uniform sathi glGetUniformLocation()
	//defibe and set constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(bufferDesc_ConstantBuffer));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_HULL_SHADER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, 0, &gpID3D11Buffer_ConstantBuffer_HullShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateBuffer() for constant buffer Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateBuffer() for constant buffer succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->HSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_HullShader);
	

	//domain Shader
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(bufferDesc_ConstantBuffer));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_DOMAIN_SHADER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, 0, &gpID3D11Buffer_ConstantBuffer_DomainShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateBuffer() for constant buffer Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateBuffer() for constant buffer succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->DSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_DomainShader);


	//Pixel Shader constant Buffer
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(bufferDesc_ConstantBuffer));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_PIXEL_SHADER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, 0, &gpID3D11Buffer_ConstantBuffer_PixelShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateBuffer() for constant buffer Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateBuffer() for constant buffer succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PixelShader);




	//d3d clear color
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	//matrix la identity
	gPerspectiveProjectionMatrix = XMMatrixIdentity();
	hr = resize(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n resize() Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n resize() successfull ");
		fclose(gpFile);
	}
	return(S_OK);

}


HRESULT resize(int width, int height)
{
	HRESULT hr=S_OK;
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	ID3D11Texture2D *pID3D11Textuer2D_BackBuffer;

	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Textuer2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Textuer2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateRenderTargetView Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateRenderViewTarget() succeded ");
		fclose(gpFile);
	}
	pID3D11Textuer2D_BackBuffer->Release();
	pID3D11Textuer2D_BackBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport);
	//set orthographic projection matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),float(width)/float(height),0.1f,100.0f);
	return(hr);
}
void display()
{
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	UINT stride = sizeof(float) * 2;
	UINT offset = 0;

	//select verte buffer to display
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer, &stride, &offset);
	
	//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

	//translation is concerened with world matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix=XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(0.0f,0.0f,4.0f);
	worldMatrix = worldMatrix * translationMatrix;
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	CBUFFER_DOMAIN_SHADER constantBuffer;
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_DomainShader, 0, NULL, &constantBuffer, 0, 0);
	
	
	CBUFFER_HULL_SHADER constantBuffer_HullShader;
	constantBuffer_HullShader.Hull_Constant_Function_Params = XMVectorSet(1.0f,(FLOAT)guiNumberOfLineSegments,0.0f,0.0f);
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_HullShader, 0, NULL, &constantBuffer_HullShader, 0, 0);
		
	TCHAR str[255];
	wsprintf(str,TEXT("Direct3D11 Window [Segments = %2d]"),guiNumberOfLineSegments);
	SetWindowText(ghwnd,str);

	//Pixel Buffer
	CBUFFER_PIXEL_SHADER cBuffer_PixelShader;
	cBuffer_PixelShader.LineColor = XMVectorSet(0.0f,1.0f,1.0f,1.0f);
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PixelShader, 0, NULL, &cBuffer_PixelShader, 0, 0);

	
	gpID3D11DeviceContext->Draw(4, 0);

	gpIDXGISwapChain->Present(0, 0);
}

void uninitialize()
{
	if (gpID3D11Buffer_ConstantBuffer_DomainShader)
	{
		gpID3D11Buffer_ConstantBuffer_DomainShader->Release();
		gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer_HullShader)
	{
		gpID3D11Buffer_ConstantBuffer_HullShader->Release();
		gpID3D11Buffer_ConstantBuffer_HullShader = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer_PixelShader)
	{
		gpID3D11Buffer_ConstantBuffer_PixelShader->Release();
		gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer)
	{
		gpID3D11Buffer_VertexBuffer->Release();
		gpID3D11Buffer_VertexBuffer = NULL;
	}
	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}
	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;

	}
	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}
	if (gpFile)
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n Uninitialize succeded");
		fprintf(gpFile, "\n File Closing\n\n");
		fclose(gpFile);
	}
}