#include <Windows.h>
#include <d3d11.h>
#include <stdio.h>
#include <d3dcompiler.h>


#pragma warning(disable:4838)
#include "XNAMATH\xnamath.h"


#pragma comment(lib,"D3dcompiler.lib")
#pragma comment(lib,"d3d11.lib")

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile;
char szLogFileName[] = "Log.txt";

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

HWND ghwnd;

bool gbIsWindowActive = false;
bool gbFullScreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

//Depth Enabling

ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;


struct CBUFFER
{
	XMMATRIX worldViewProjectionMatrix;
};

XMMATRIX gPerspectiveProjectionMatrix;


//mesh loading code
struct vec_float {
	float *p;
	int size;
};


struct vec_int {
	int *p;
	int size;
};
vec_float *gpVertex;
vec_float *gpNormal;
vec_float *gpTexCoords;

vec_int *gpTextureIndices;
vec_int *gpNormalIndices;
vec_int *gpVertexIndices;

vec_float *gpVertexSorted;
vec_float *gpNormalSorted;
vec_float *gpTextureSorted;
FILE *gpFile_Mesh = NULL;
char buffer[1024];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT initialize();
	void uninitialize();
	void display();

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[] = TEXT("MY DirectX11 Window");
	bool bDone = false;

	if (fopen_s(&gpFile, szLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Error while Opening file"), TEXT("ERROR!!!"), MB_OK | MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Opened successfully");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, lpszClassName, TEXT("My D3D11 Window"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, NULL, NULL, hInstance, NULL);

	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nError in initialize()");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nSuccessfull  initialize()");
		fclose(gpFile);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		}
		else
		{
			if (gbIsWindowActive)
			{
				display();
			}
		}
	}


	return((int)msg.lParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullscreen();
	void uninitialize();

	HRESULT hr;

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsWindowActive = true;
		break;
	case WM_KILLFOCUS:
		gbIsWindowActive = false;
		break;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, szLogFileName, "a+");
				fprintf(gpFile, "\nError in resize()");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, szLogFileName, "a+");
				fprintf(gpFile, "\n Successfull resize()");
				fclose(gpFile);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, ~WS_OVERLAPPEDWINDOW&dwStyle);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_FRAMECHANGED | SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | dwStyle);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);

		gbFullScreen = false;
	}
}

HRESULT initialize()
{
	void uninitialize();
	void loadMesh();
	HRESULT resize(int, int);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_aquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverType = 0;
	UINT numFeatureLevels = 1;
	numDriverType = sizeof(d3dDriverTypes) / d3dDriverTypes[0];

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WINDOW_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WINDOW_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverType; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_aquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapchain() Failed ");
		fclose(gpFile);

		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");

		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapchain() Succeded ");
		fprintf(gpFile, "\n Chose Drive Version is:=  ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "\n Hardware Typr");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\n WARP Type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\n Reference Type");
		}

		fprintf(gpFile, "\n The Supported Highest Feature Level:= ");
		if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_11_0 ");
		}
		else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_10_1 ");
		}
		else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_10_0 ");
		}
		else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_9_1)
		{
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_9_1 ");
		}
		else
		{
			fprintf(gpFile, "\n Unknows ");
		}

		fclose(gpFile);
	}

	//code for vertex shader
	const char* vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
			"float4x4 worldViewProjectionMatrix; " \
		"}" \
		"float4 main(float4 pos:POSITION) : SV_POSITION" \
		"{"	\
		"float4 position=mul(worldViewProjectionMatrix,pos);"\
		"return(position);"\
		"}";

	ID3DBlob* pID3DBlob_vertexShaderCode = NULL;
	ID3DBlob* Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",								//Source code string
		NULL,								//D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,	//Include file ahet ka refer automation midl file
		"main",								//Entry point function address
		"vs_5_0",							//Version/ model number for shader
		0,									// shader compile kasa karava 5 ways ahet fxc.exe 
		0,									//Effect constant
		&pID3DBlob_vertexShaderCode,		//compiled code thevnya sathi
		&Error								//Error for verstex shader
	);

	if (FAILED(hr))
	{
		if (Error != NULL)
		{
			fopen_s(&gpFile, szLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for vertex shader:%s\n", (char*)Error->GetBufferPointer());
			fclose(gpFile);
			Error->Release();
			Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nD3DCompile() vertex shader successfull \n");
		fclose(gpFile);
	}
	//Creating shader and attaching shader to the shader object
	///create function always called on device
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_vertexShaderCode->GetBufferPointer(),
		pID3DBlob_vertexShaderCode->GetBufferSize(),
		NULL,				//Class linkage parameter across shader variable sathi
		&gpID3D11VertexShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CrteateVertexShader() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CrteateVertexShader() succeded \n");
		fclose(gpFile);
	}

	//Device always set the tings to pipeline which contros the pipelines
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, NULL, NULL);


	//pixel shader start
	const char* pixelShaderSourceCode =
		"float4 main(void): SV_TARGET" \
		"{" \
		"return(float4(1.0f,1.0f,1.0f,1.0f));" \
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&Error
	);

	if (FAILED(hr))
	{
		if (Error != NULL)
		{
			fopen_s(&gpFile, szLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader:%s\n", (char*)Error->GetBufferPointer());
			fclose(gpFile);
			Error->Release();
			Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nD3DCompile() pixel shader successfull \n");
		fclose(gpFile);
	}
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreatePixelShader() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreatePixelShader() succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, NULL);


	//create and set input layout je pan glVertexAttribPointer() sarakha in opengl
	D3D11_INPUT_ELEMENT_DESC inputElementDesc;
	ZeroMemory(&inputElementDesc,sizeof(D3D11_INPUT_ELEMENT_DESC));
	inputElementDesc.SemanticName = "POSITION";	//shader madhala float4 pos:POSITION same pahije
	inputElementDesc.SemanticIndex = 0;		//mi position 0 la takat ahe
	inputElementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT;	//how many per vertex point we are passing glVertexAttribPointer(AMC,3); 
	inputElementDesc.InputSlot = 0;								// layout in opengl is input slot here
	inputElementDesc.AlignedByteOffset = 0;						//if we have multiple entries for per vertex data then offset of the data 
	inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;	//Per vertex data ahe ha
	inputElementDesc.InstanceDataStepRate = 0;	 //per instance input slot asata tar

	hr = gpID3D11Device->CreateInputLayout(&inputElementDesc, 1, pID3DBlob_vertexShaderCode->GetBufferPointer(), pID3DBlob_vertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateInputLayout() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateInputLayout() succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_vertexShaderCode->Release();
	pID3DBlob_vertexShaderCode = NULL;


	//second stage
	//create vertex buffer
	
	float vertices[] = {
			0.0f,1.0f,0.0f,
			1.0f,-1.0f,0.0f,
			-1.0f,-1.0f,0.0f
	};
	loadMesh();

	//glBufferData() la data pathavnyasathi
	D3D11_BUFFER_DESC bufferDesc_vertexBuffer;
	ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));

	bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;	//static vaprala tar createBuffer() la second parameter madhe 
	bufferDesc_vertexBuffer.ByteWidth = sizeof(float)*(gpVertexSorted->size);
	bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER; 
	bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_VertexBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateBuffer() Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateBuffer() succeded \n");
		fclose(gpFile);
	}

	//copy vertices into abouve buffer

	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy(mappedSubResource.pData, gpVertexSorted->p, sizeof(gpVertexSorted->p));	//memory mapped IO lihita 
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer, 0); //jo loack kela tyala unlock kara so that to pudhe set karatana successfull hoil bach theory 


	//Uniform sathi glGetUniformLocation()
	//defibe and set constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(bufferDesc_ConstantBuffer));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, 0, &gpID3D11Buffer_ConstantBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateBuffer() for constant buffer Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateBuffer() for constant buffer succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);


	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void*)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\nID3D11Device::CreateRasterizerState()  Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf_s(gpFile, "\n D3D11Device::CreateRasterizerState()  succeded \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);



	//d3d clear color
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	//matrix la identity
	gPerspectiveProjectionMatrix = XMMatrixIdentity();
	hr = resize(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n resize() Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n resize() successfull ");
		fclose(gpFile);
	}
	return(S_OK);

}
void loadMesh(void)
{
	struct vec_float* create_vec_float(void);
	struct vec_int* create_vec_int(void);
	int push_back_vec_float(struct vec_float*, float);
	int push_back_vec_int(struct vec_int*, int);

	gpVertex = create_vec_float();
	gpNormal = create_vec_float();
	gpTexCoords = create_vec_float();

	gpVertexIndices = create_vec_int();
	gpTextureIndices = create_vec_int();
	gpNormalIndices = create_vec_int();

	gpVertexSorted = create_vec_float();
	gpNormalSorted = create_vec_float();
	gpTextureSorted = create_vec_float();

	gpFile_Mesh = fopen("MonkeyHead.obj", "r");
	if (gpFile_Mesh == NULL)
	{
		fprintf(gpFile, "\n\n\n::Error while opening OBJ file");
		exit(0);
	}
	else
	{
		
		char* firstToken = NULL;
		char* token = NULL;
		const char* space = " ";
		int num = 0;
		const char* slash = "/";
		char* fEntries[] = { NULL,NULL,NULL };
		while ((fgets(buffer, 1024, gpFile_Mesh)) != NULL)
		{
			firstToken = strtok(buffer, space);
			if (strcmp(firstToken, "v") == 0)
			{
				token = strtok(NULL, space);
				float x = atof(token);
				push_back_vec_float(gpVertex, x);


				token = strtok(NULL, space);
				float y = atof(token);
				push_back_vec_float(gpVertex, y);
				num++;

				token = strtok(NULL, space);
				float z = atof(token);
				push_back_vec_float(gpVertex, z);
				num++;
			}
			else if (strcmp(firstToken, "vt") == 0)
			{
				token = strtok(NULL, space);
				float s = atof(token);
				push_back_vec_float(gpTexCoords, s);

				token = strtok(NULL, space);
				float t = atof(token);
				push_back_vec_float(gpTexCoords, t);
			}
			else if (strcmp(firstToken, "vn") == 0)
			{
				token = strtok(NULL, space);
				float u = atof(token);
				push_back_vec_float(gpNormal, u);

				token = strtok(NULL, space);
				float v = atof(token);
				push_back_vec_float(gpNormal, v);

				token = strtok(NULL, space);
				float w = atof(token);
				push_back_vec_float(gpNormal, w);
			}
			else if (strcmp(firstToken, "f") == 0)
			{
				for (int i = 0; i < 3; i++)
				{
					fEntries[i] = strtok(NULL, space);
				}
				for (int i = 0; i < 3; i++)
				{
					token = strtok(fEntries[i], slash);
					push_back_vec_int(gpVertexIndices, atoi(token) - 1);

					token = strtok(NULL, slash);
					push_back_vec_int(gpTextureIndices, atoi(token) - 1);

					token = strtok(NULL, slash);
					push_back_vec_int(gpNormalIndices, atoi(token) - 1);

				}
			}
		}

	}
	fclose(gpFile_Mesh);

	for (unsigned int i = 0; i < gpVertexIndices->size; i++)
	{

		push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i] * 3 + 0]);
		push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i] * 3 + 1]);
		push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i] * 3 + 2]);
	}

	if (gpNormalIndices->size > 0)
	{
		for (int i = 0; i < gpNormalIndices->size; i = i + 3)
		{
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i] * 3 + 0]);
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i] * 3 + 1]);
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i] * 3 + 2]);

		}


	}

	if (gpTextureIndices->size > 0)
	{
		for (int i = 0; i < gpTextureIndices->size; i = i + 2)
		{
			push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i] * 2 + 0]);
			push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i] * 2 + 1]);
		}

	}
}
struct vec_int* create_vec_int()
{
	struct vec_int* p_new = NULL;

	p_new = (struct vec_int*)malloc(sizeof(struct vec_int));
	if (p_new == NULL)
	{
		printf("Error in malloc");
		exit(0);
	}

	memset(p_new, 0, sizeof(struct vec_int));

	return p_new;
}


int push_back_vec_int(struct vec_int* p_vec, int new_data)
{
	p_vec->p = (int*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(int));

	if (p_vec->p == NULL)
	{
		printf("Errorr  realloc");
		exit(0);
	}

	p_vec->size = p_vec->size + 1;

	p_vec->p[p_vec->size - 1] = new_data;


	return TRUE;
}

struct vec_float* create_vec_float()
{
	struct vec_float* p_new = NULL;

	p_new = (struct vec_float*)malloc(sizeof(struct vec_float));
	if (p_new == NULL)
	{
		printf("Error in malloc");
		exit(0);
	}

	memset(p_new, 0, sizeof(struct vec_float));

	return p_new;
}


int push_back_vec_float(struct vec_float* p_vec, float new_data)
{
	p_vec->p = (float*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(float));


	if (p_vec->p == NULL)
	{
		printf("Errorr  realloc");
		exit(0);
	}

	p_vec->size = p_vec->size + 1;

	p_vec->p[p_vec->size - 1] = new_data;


	return TRUE;
}


HRESULT resize(int width, int height)
{
	HRESULT hr=S_OK;
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	ID3D11Texture2D *pID3D11Textuer2D_BackBuffer;

	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Textuer2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Textuer2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateRenderTargetView Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateRenderViewTarget() succeded ");
		fclose(gpFile);
	}
	pID3D11Textuer2D_BackBuffer->Release();
	pID3D11Textuer2D_BackBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);



	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;


	ID3D11Texture2D* pID3D11Texture2D_DepthBuffer;
	gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateDepthStencilView Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateDepthStencilView() succeded ");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;


	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);


	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport);
	//set orthographic projection matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),float(width)/float(height),0.1f,100.0f);
	return(hr);
}
void display()
{
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	//select verte buffer to display
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer, &stride, &offset);

	//select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//translation is concerened with world matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix=XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(0.0f,0.0f,6.0f);
	worldMatrix = worldMatrix * translationMatrix;
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	CBUFFER constantBuffer;
	constantBuffer.worldViewProjectionMatrix = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->Draw(gpVertexSorted->size, 0);

	gpIDXGISwapChain->Present(0, 0);
}

void uninitialize()
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer)
	{
		gpID3D11Buffer_VertexBuffer->Release();
		gpID3D11Buffer_VertexBuffer = NULL;
	}
	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}
	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;

	}
	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}
	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}
	if (gpFile)
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n Uninitialize succeded");
		fprintf(gpFile, "\n File Closing\n\n");
		fclose(gpFile);
	}
}