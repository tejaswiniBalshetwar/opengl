#include <Windows.h>
#include <d3d11.h>
#include <stdio.h>

#pragma comment(lib,"d3d11.lib")

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE *gpFile;
char szLogFileName[] = "Log.txt";

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

HWND ghwnd;

bool gbIsWindowActive = false;
bool gbFullScreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext=NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT initialize();
	void uninitialize();
	void display();

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR lpszClassName[] = TEXT("MY DirectX11 Window");
	bool bDone = false;

	if (fopen_s(&gpFile, szLogFileName, "w") != 0)
	{
		MessageBox(NULL,TEXT("Error while Opening file"),TEXT("ERROR!!!"),MB_OK|MB_OKCANCEL);
		exit(0);
	}
	else
	{
		fprintf(gpFile,"File Opened successfully");
		fclose(gpFile);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = lpszClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,lpszClassName,TEXT("My D3D11 Window"),WS_OVERLAPPEDWINDOW|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,0,0,WINDOW_WIDTH,WINDOW_HEIGHT,NULL,NULL,hInstance,NULL);

	ghwnd = hwnd;
	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile,szLogFileName,"a+");
		fprintf(gpFile,"\nError in initialize()");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\nSuccessfull  initialize()");
		fclose(gpFile);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);

			}
		}
		else
		{
			if (gbIsWindowActive)
			{
				display();
			}
		}
	}


	return((int)msg.lParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullscreen();
	void uninitialize();

	HRESULT hr;

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbIsWindowActive = true;
		break;
	case WM_KILLFOCUS:
		gbIsWindowActive = false;
		break;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, szLogFileName, "a+");
				fprintf(gpFile, "\nError in resize()");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, szLogFileName, "a+");
				fprintf(gpFile, "\n Successfull resize()");
				fclose(gpFile);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,~WS_OVERLAPPEDWINDOW&dwStyle);
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_FRAMECHANGED|SWP_NOZORDER);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | dwStyle);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0,0,0,0, SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER|SWP_FRAMECHANGED | SWP_NOZORDER);
		ShowCursor(TRUE);

		gbFullScreen = false;
	}
}

HRESULT initialize()
{
	void uninitialize();

	HRESULT resize(int, int);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required =D3D_FEATURE_LEVEL_11_0 ;
	D3D_FEATURE_LEVEL d3dFeatureLevel_aquired =D3D_FEATURE_LEVEL_10_0 ;

	UINT createDeviceFlags = 0;
	UINT numDriverType = 0;
	UINT numFeatureLevels = 1;
	numDriverType = sizeof(d3dDriverTypes) / d3dDriverTypes[0];

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)&dxgiSwapChainDesc,sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WINDOW_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WINDOW_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format =DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator=60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator=1;
	dxgiSwapChainDesc.BufferUsage=DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverType; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_aquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile,szLogFileName,"a+");
		fprintf(gpFile,"\nD3D11CreateDeviceAndSwapchain() Failed ");
		fclose(gpFile);
		
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapchain() Succeded ");
		fprintf(gpFile, "\n Chose Drive Version is:=  ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile,"\n Hardware Typr");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\n WARP Type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\n Reference Type");
		}

		fprintf(gpFile,"\n The Supported Highest Feature Level:= ");
		 if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_11_0)
		 {
			fprintf(gpFile, "\n D3D_FEATURE_LEVEL_11_0 ");
		 }
		 else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_10_1)
		 {
			 fprintf(gpFile, "\n D3D_FEATURE_LEVEL_10_1 ");
		 }
		 else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_10_0)
		 {
			 fprintf(gpFile, "\n D3D_FEATURE_LEVEL_10_0 ");
		 }
		 else if (d3dFeatureLevel_aquired == D3D_FEATURE_LEVEL_9_1)
		 {
			 fprintf(gpFile, "\n D3D_FEATURE_LEVEL_9_1 ");
		 }
		 else
		 {
			 fprintf(gpFile, "\n Unknows ");
		 }

		fclose(gpFile);
	}

	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;

	hr = resize(WINDOW_WIDTH,WINDOW_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n resize() Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n resize() successfull ");
		fclose(gpFile);
	}
	return(S_OK);

}


HRESULT resize(int width,int height)
{
	HRESULT hr;
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1,width,height,DXGI_FORMAT_R8G8B8A8_UNORM,0);

	ID3D11Texture2D *pID3D11Textuer2D_BackBuffer;

	gpIDXGISwapChain->GetBuffer(0,__uuidof(ID3D11Texture2D),(LPVOID*)&pID3D11Textuer2D_BackBuffer);

	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Textuer2D_BackBuffer,NULL,&gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateRenderTargetView Failed ");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, szLogFileName, "a+");
		fprintf(gpFile, "\n CreateRenderViewTarget() succeded ");
		fclose(gpFile);
	}
	pID3D11Textuer2D_BackBuffer->Release();
	pID3D11Textuer2D_BackBuffer = NULL;

	gpID3D11DeviceContext->OMSetRenderTargets(1,&gpID3D11RenderTargetView,NULL);

	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MaxDepth = 1.0f;
	d3dViewport.MinDepth = 0.0f;

	gpID3D11DeviceContext->RSSetViewports(1,&d3dViewport);

	return(hr);
}
void display()
{
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,gClearColor);

	gpIDXGISwapChain->Present(0,0);
}

void uninitialize()
{
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device=NULL;
	}
	if (gpFile)
	{
		fopen_s(&gpFile,szLogFileName,"a+");
		fprintf(gpFile,"\n Uninitialize succeded");
		fprintf(gpFile,"\n File Closing\n\n" );
		fclose(gpFile);
	}
}