//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"

void loadMesh();

struct vec_float* create_vec_float(void);
struct vec_int* create_vec_int(void);
int push_back_vec_float(struct vec_float* , float );
int push_back_vec_int(struct vec_int* , int );


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};

using namespace vmath;

//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;
FILE *gpFile_Mesh=NULL;



float light_ambient[] = { 0.250f,0.250f,0.250f,0.250f };
float light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[] = { 0.0f,0.0f,10.0f,1.0f };

float material_ambient[] = { 0.250f,0.250f,0.250f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;


struct vec_float {
    float *p;
    int size;
};


struct vec_int {
    int *p;
    int size;
};
vec_float *gpVertex;
vec_float *gpNormal;
vec_float *gpTexCoords;

vec_int *gpTextureIndices;
vec_int *gpNormalIndices;
vec_int *gpVertexIndices;

vec_float *gpVertexSorted;
vec_float *gpNormalSorted;
vec_float *gpTextureSorted;

char buffer[1024];


//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		
}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");
   
	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;


	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	GLuint vao;

	GLuint vbo_position;
    GLuint vbo_normal;
    GLuint vbo_element;
    GLuint vbo_texture;


	GLuint modelUniform;
	GLuint viewUniform;
	GLuint projectionUniform;
	GLuint LKeyIsPressedUniform;
	GLuint laUniform;
	GLuint ldUniform;
	GLuint lsUniform;
	GLuint kaUniform;
	GLuint kdUniform;
	GLuint ksUniform;
	GLuint lightPositionUniform;
	GLuint materialShininessUniform;


	GLuint samplerUniform;
	GLuint texture_marble;
    GLuint mvpUniform;

	bool bLight ;
	bool bRotation ;



	GLfloat angleCube;
	vmath::mat4 perspectiveProjectionMatrix;;


}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
    
    loadMesh();

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode=
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vColor; " \
		"in vec2 vTexCoord;" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec3 out_color;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
			"out_color=vColor;								\n" \
			"out_TexCoord=vTexCoord;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";
    /*"in vec4 vPosition;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position= u_mvp_matrix * vPosition ;"  \
    "}";*/


	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	const GLchar* fragmentShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec3 out_color;								\n" \
		"in vec2 out_TexCoord;							\n" \
		"uniform sampler2D u_sampler;					\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"	vec4 texSampler=texture(u_sampler,out_TexCoord);" \
			"	vec3 phong_ads_light;						\n" \
			"	if(u_LKeyIsPressed==1)						\n" \
			"	{											\n"	\
			"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
			"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
			"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
			"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
			"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
			"		vec3 ambient= u_la * u_ka;																							\n " \
			"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																				\n" \
			"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);	\n" \
			"		phong_ads_light=ambient + diffuse + specular;																		\n" \
			"	}																													\n" \
			"	else																												\n" \
			"	{																													\n" \
			"			phong_ads_light=vec3(1.0,1.0,1.0);																			\n" \
			"	}																													\n" \
			/*"	fragColor=vec4((vec3(texSampler)*phong_ads_light),1.0);													\n" \*/
    "    fragColor=vec4((phong_ads_light),1.0);                                                    \n" \
		"}";
    /*"#version 410 core" \
    "\n" \
    "out vec4 fragColor;" \
    "void main(void)" \
    "{" \
    "fragColor=vec4(0.0, 1.0, 0.0, 1.0);" \
    "}";
*/
	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	
	modelUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");

	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");

	LKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyIsPressed");

	lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");


	samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
    
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");


	//rectangle
	//recording start

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//posiiotn data
        glGenBuffers(1, &vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpVertexSorted->size, gpVertexSorted->p, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
        glGenBuffers(1, &vbo_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpNormalSorted->size, gpNormalSorted->p, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
        glGenBuffers(1, &vbo_texture);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpTextureSorted->size, gpTextureSorted->p, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    
        glGenBuffers(1,&vbo_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,gpVertexIndices->size*sizeof(float),gpVertexIndices->p,GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    
    glVertexAttrib3f(AMC_ATTRIBUTE_COLOR,0.0f,0.0f,0.0f);
	//recording stop
	glBindVertexArray(0);
    
    /*glGenVertexArrays(1, &vao);
    
        //bind with cassate  and start recording
        glBindVertexArray(vao);
    
        //gen buffers
        glGenBuffers(1, &vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
    
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpVertexSorted->size, gpVertexSorted->p, GL_STATIC_DRAW);
    
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        glGenBuffers(1,&vbo_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,gpVertexIndices->size*sizeof(float),gpVertexIndices->p,GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    
    
        //recording stop
        glBindVertexArray(0);
*/


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,0.0f,1.0f);

    //glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glFrontFace(GL_CCW);
	perspectiveProjectionMatrix=vmath::mat4::identity();

	texture_marble=[self loadTextureFromBMPFile:"marble.bmp"];

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

void loadMesh(void)
{
    

    gpVertex = create_vec_float();
    gpNormal = create_vec_float();
    gpTexCoords = create_vec_float();
    
    gpVertexIndices = create_vec_int();
    gpTextureIndices = create_vec_int();
    gpNormalIndices = create_vec_int();

    gpVertexSorted = create_vec_float();
    gpNormalSorted = create_vec_float();
    gpTextureSorted = create_vec_float();
    
    
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/MonkeyHead.OBJ",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile_Mesh=fopen(pszLogFileNameWithPath,"r");

   // gpFile_Mesh = fopen("MonkeyHead.OBJ","r");
    if (gpFile_Mesh==NULL)
    {
        fprintf(gpFile,"\n\n\n::Error while opening OBJ file");
        exit(0);
    }
    else
    {
        char* firstToken = NULL;
        char* token = NULL;
        const char* space =" ";
        int num = 0;
        const char* slash = "/";
        char* fEntries[] = {NULL,NULL,NULL};
        while ((fgets(buffer, 1024, gpFile_Mesh)) != NULL)
        {
            firstToken = strtok(buffer,space);
            if (strcmp(firstToken, "v") == 0)
            {
                token = strtok(NULL,space);
                float x = atof(token);
                push_back_vec_float(gpVertex,x);
                

                token = strtok(NULL, space);
                float y = atof(token);
                push_back_vec_float(gpVertex,y);
                num++;
            
                token = strtok(NULL, space);
                float z = atof(token);
                push_back_vec_float(gpVertex,z);
                num++;
            }
            else if (strcmp(firstToken,"vt") == 0)
            {
                token = strtok(NULL, space);
                float s = atof(token);
                push_back_vec_float(gpTexCoords, s);

                token = strtok(NULL, space);
                float t = atof(token);
                push_back_vec_float(gpTexCoords, t);
            }
            else if (strcmp(firstToken, "vn") == 0)
            {
                token = strtok(NULL, space);
                float u = atof(token);
                push_back_vec_float(gpNormal, u);

                token = strtok(NULL, space);
                float v = atof(token);
                push_back_vec_float(gpNormal, v);

                token = strtok(NULL, space);
                float w = atof(token);
                push_back_vec_float(gpNormal, w);
            }
            else if (strcmp(firstToken, "f") == 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    fEntries[i] = strtok(NULL,space);
                }
                for (int i = 0; i < 3; i++)
                {
                    token = strtok(fEntries[i],slash);
                    push_back_vec_int(gpVertexIndices,atoi(token)-1);
                    
                    token = strtok(NULL, slash);
                    push_back_vec_int(gpTextureIndices, atoi(token)-1);
                
                    token = strtok(NULL, slash);
                    push_back_vec_int(gpNormalIndices, atoi(token)-1);
                    
                }
            }
        }

    }
    fclose(gpFile_Mesh);
    
        for (unsigned int i = 0; i < gpVertexIndices->size; i++)
        {
                      
            push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+0]);
            push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+1]);
            push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+2]);
        }

        for (int i = 0; i < gpNormalIndices->size; i=i+3)
        {
            push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+0]);
            push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+1]);
            push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+2]);
            
        }

        for (int i = 0; i < gpTextureIndices->size; i=i+2)
        {
            push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i]*2+0]);
            push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i]*2+1]);
        }
}
struct vec_int* create_vec_int()
{
    struct vec_int* p_new = NULL;

    p_new = (struct vec_int*)malloc(sizeof(struct vec_int));
    if (p_new == NULL)
    {
        printf("Error in malloc");
        exit(0);
    }

    memset(p_new, 0, sizeof(struct vec_int));

    return p_new;
}


int push_back_vec_int(struct vec_int* p_vec, int new_data)
{
    p_vec->p = (int*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(int));

    if (p_vec->p == NULL)
    {
        printf("Errorr  realloc");
        exit(0);
    }

    p_vec->size = p_vec->size + 1;

    p_vec->p[p_vec->size - 1] = new_data;


    return TRUE;
}

struct vec_float* create_vec_float()
{
    struct vec_float* p_new = NULL;

    p_new = (struct vec_float*)malloc(sizeof(struct vec_float));
    if (p_new == NULL)
    {
        printf("Error in malloc");
        exit(0);
    }

    memset(p_new, 0, sizeof(struct vec_float));

    return p_new;
}


int push_back_vec_float(struct vec_float* p_vec, float new_data)
{
    p_vec->p = (float*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(float));


    if (p_vec->p == NULL)
    {
        printf("Errorr  realloc");
        exit(0);
    }

    p_vec->size = p_vec->size + 1;

    p_vec->p[p_vec->size - 1] = new_data;
    

    return TRUE;
}


-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

-(GLuint)loadTextureFromBMPFile:(const char* )texFile
{
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath,texFile];

	NSImage *bmpImage=[[NSImage alloc] initWithContentsOfFile:textureFileNameWithPath];
	if(!bmpImage)
	{
		NSLog(@"can't find %@",texFile);
		fprintf(gpFile, "\nError in File opening\n" );
		return 0;
	}
	else
	{
		fprintf(gpFile, "\nTeture Loaded successfully" );
	}

	CGImageRef cgImage=[bmpImage CGImageForProposedRect:nil context:nil hints:nil];

	int w=(int)CGImageGetWidth(cgImage);
	int h=(int)CGImageGetHeight(cgImage);

	CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));

	void *pixels=(void*)CFDataGetBytePtr(imageData);

	GLuint bmptexture;
	glGenTextures(1,&bmptexture);

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glBindTexture(GL_TEXTURE_2D,bmptexture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);

	glGenerateMipmap(GL_TEXTURE_2D);

	CFRelease(imageData);
	return(bmptexture);
}	


- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		vmath::mat4 modelMatrix;
		vmath::mat4 viewMatrix;
		
		vmath::mat4 translationMatrix;
		vmath::mat4 rotationMatrix;
		vmath::mat4 scaleMatrix;
        mat4 modelViewProjectionMatrix;


		glUseProgram(shaderProgramObject);
			//square
			modelMatrix=vmath::mat4::identity();
			viewMatrix=vmath::mat4::identity();
			translationMatrix=vmath::mat4::identity();
			rotationMatrix=vmath::mat4::identity();
            
			scaleMatrix=vmath::mat4::identity();

    modelViewProjectionMatrix=mat4::identity();
    
			translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
			//scaleMatrix = vmath::scale(0.75f,0.75f,0.75f);
			

			//rotationMatrix=vmath::rotate(angleCube, angleCube, angleCube);


			//do matrix multipliacation
			modelMatrix = modelMatrix * translationMatrix;
			//modelMatrix = modelMatrix * scaleMatrix;
			//modelViewMatrix = modelViewMatrix * rotationMatrixX * rotationMatrixY* rotationMatrixZ;
			//modelMatrix = modelMatrix * rotationMatrix;

			modelViewProjectionMatrix = perspectiveProjectionMatrix * modelMatrix;

			
		glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

		/*glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_marble);
		glUniform1i(samplerUniform, 0);*/

		if (bLight)
		{
			glUniform1i(LKeyIsPressedUniform, 1);
			glUniform3fv(laUniform, 1, light_ambient);		//glLightfv() 
			glUniform3fv(ldUniform, 1, light_diffuse);		//glLightfv() 
			glUniform3fv(lsUniform, 1, light_specular);		//glLightfv() 
			glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position

			glUniform3fv(kaUniform, 1, material_ambient);	//glMaterialfv();
			glUniform3fv(kdUniform, 1, material_diffuse);	//glMaterialfv();
			glUniform3fv(ksUniform, 1, material_specular);	//glMaterialfv();
			glUniform1f(materialShininessUniform, material_shininess);	//glMaterialfv();
            
            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        
        }
		else
		{
			glUniform1i(LKeyIsPressedUniform, 0);
            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		}

    
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glBindVertexArray(vao);
            glDrawArrays(GL_TRIANGLES,0,gpVertexSorted->size);
    
    /*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
    glDrawElements(GL_TRIANGLES,gpVertexIndices->size*sizeof(int), GL_UNSIGNED_SHORT,NULL);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);**/
        glBindVertexArray(0);
    
		glUseProgram(0);

		if(bRotation)
		{
			[self update];	
		}
		
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) update
{

	angleCube=angleCube+0.5f;
	if(angleCube>360.0f)
	{
		angleCube=0.0f;
	}
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
					//LKeyIsPressed = 1;
				}
				else
				{
					bLight = false;
					//LKeyIsPressed = 0;
				}
				break;
		case 'A':
		case 'a':
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;
		default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}


