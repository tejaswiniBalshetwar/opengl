//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import<math.h>

#import "vmath.h"
#import "Sphere.h"


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};


float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;

int screenWidth = 0;
int screenHeight = 0;

//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);
void drawSpheres();
void drawSphere();

//FILE IO
FILE *gpFile=NULL;

struct light
{
	float light_ambient[4];
	float light_diffuse[4];
	float light_specular[4];
	float light_position[4];
};

light lights;

vmath::vec4 lightPosition;
float glMaterialAmbient[4];
float glMaterialDiffuse[4];
float glMaterialSpecular[4];
float glMaterialShininess;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShininessUniform;
GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;
//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		
}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;


	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	


	GLuint modelUniform;
	GLuint viewUniform;
	GLuint projectionUniform;
	GLuint LKeyIsPressedUniform;
	GLuint laUniform;
	GLuint ldUniform;
	GLuint lsUniform;


	GLuint lightPositionUniform;
	

	bool bRotation;
	bool bLight;

	int keyPress;

	GLfloat angleCube;
	vmath::mat4 perspectiveProjectionMatrix;
	vmath::mat4 modelMatrix;
	vmath::mat4 viewMatrix;

	GLfloat lightAngleForX ;
	GLfloat lightAngleForY ;
	GLfloat lightAngleForZ ;


}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode=
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		
		"out vec3 out_light_direction;					\n" \
		
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	GLchar* fragmentShaderSourceCode=
		"#version 410 core" \
		"\n" \
		"uniform vec3 u_la;								\n" \
		
		"uniform vec3 u_ld;								\n" \
		
		"uniform vec3 u_ls;								\n" \
		
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
	
		"in vec3 out_t_normal;							\n" \
		
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		
		"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
		
		"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		
		"		vec3 ambient= u_la * u_ka;																						\n " \
		
		"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																			\n" \
		
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		
		"		phong_ads_light=ambient + diffuse + specular;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";


	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
	
	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	
	LKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyIsPressed");
	
	lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");

	
	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	fprintf(gpFile, "\n%d\n",gNumElements );
	fprintf(gpFile, "\n%d\n",gNumVertices );


	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//posiiotn data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1,&vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,0.0f,1.0f);

//initializeing lights
	lights.light_ambient[0] = 0.0f;
	lights.light_ambient[1] = 0.0f;
	lights.light_ambient[2] = 0.0f;
	lights.light_ambient[3] = 0.0f;

	lights.light_diffuse[0] = 1.0f;
	lights.light_diffuse[1] = 1.0f;
	lights.light_diffuse[2] = 1.0f;
	lights.light_diffuse[3] = 0.0f;

	lights.light_specular[0] = 1.0f;
	lights.light_specular[1] = 1.0f;
	lights.light_specular[2] = 1.0f;
	lights.light_specular[3] = 0.0f;


	lights.light_position[0] = 0.0f;
	lights.light_position[1] = 0.0f;
	lights.light_position[2] = 0.0f;
	lights.light_position[3] = 1.0f;




	perspectiveProjectionMatrix=vmath::mat4::identity();
	modelMatrix=vmath::mat4::identity();
	viewMatrix=vmath::mat4::identity();

	keyPress=0;

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	screenWidth=(GLsizei)rect.size.width;
	screenHeight=(GLsizei)rect.size.height;
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		vmath::mat4 modelViewMatrix;
		
		vmath::mat4 translationMatrix;


		glUseProgram(shaderProgramObject);
			//square
			modelMatrix = vmath::mat4::identity();
			//modelViewProjectionMatrix = mat4::identity();
			translationMatrix = vmath::mat4::identity();
			
			//do necessary translation

			translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

			//do matrix multipliacation
			modelMatrix = modelMatrix * translationMatrix;
			

			//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
			
			glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

			if (bLight)
			{
				glUniform1i(LKeyIsPressedUniform,1);
				if (keyPress == 1)
				{
					lights.light_position[1] = 200.0 * cos(lightAngleForX);
					lights.light_position[2] = 200.0 * sin(lightAngleForX);
				}
				else if (keyPress == 2)
				{
					lights.light_position[2] = 200.0 * cos(lightAngleForY);
					lights.light_position[0] = 200.0 * sin(lightAngleForY);
				}
				else
				{
					lights.light_position[0] = 200.0 * cos(lightAngleForZ);
					lights.light_position[1] = 200.0 * sin(lightAngleForZ);
				}
				glUniform3fv(laUniform,1,lights.light_ambient);		//glLightfv() 
				glUniform3fv(ldUniform,1, lights.light_diffuse);		//glLightfv() 
				glUniform3fv(lsUniform,1, lights.light_specular);		//glLightfv() 
				glUniform4fv(lightPositionUniform,1, lights.light_position); //glLightfv() for position


			}
			else
			{
				glUniform1i(LKeyIsPressedUniform,0);
			}


			drawSpheres();
			


		glUseProgram(0);

		if(bRotation)
		{
			[self update];	
		}
		
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) update
{

	lightAngleForX = lightAngleForX + 0.020f;
	if (lightAngleForX >= 360.0f)
	{
		lightAngleForX = 0.0f;
	}

	lightAngleForY = lightAngleForY + 0.020f;
	if (lightAngleForY > 360.0f)
	{
		lightAngleForY = 0.0f;
	}

	lightAngleForZ = lightAngleForZ + 0.020f;
	if (lightAngleForZ > 360.0f)
	{
		lightAngleForZ = 0.0f;
	}
	
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
				}
				else
				{
					bLight = false;
					
				}
				break;
		case 'A':
		case 'a':
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;
		case 'x':
		case 'X':

			keyPress = 1;
			break;
		case 'y':
		case 'Y':
			keyPress = 2;
			break;
		case 'z':
		case 'Z':
			keyPress = 3;
			break;
		default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

void drawSpheres()
{
		//1 row 1st col
		glViewport(screenWidth/9,(screenHeight/5)*4 ,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0215;
		glMaterialAmbient[1] = 0.1745;
		glMaterialAmbient[2] = 0.0215;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1,glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.07568;
		glMaterialDiffuse[1] = 0.61424;
		glMaterialDiffuse[2] = 0.07568;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.633;
		glMaterialSpecular[1] = 0.727811;
		glMaterialSpecular[2] = 0.633;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.6 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*2,(screenHeight/5)*4,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.135;
		glMaterialAmbient[1] = 0.2225;
		glMaterialAmbient[2] = 0.1575;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.54;
		glMaterialDiffuse[1] = 0.89;
		glMaterialDiffuse[2] = 0.63;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.316228;
		glMaterialSpecular[1] = 0.316228;
		glMaterialSpecular[2] = 0.316228;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess =  0.1 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//3rd sphere 1st column
		glViewport((screenWidth/9)*3,(screenHeight/5)*4,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.05375;
		glMaterialAmbient[1] = 0.05;
		glMaterialAmbient[2] = 0.06625;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.18275;
		glMaterialDiffuse[1] = 0.17;
		glMaterialDiffuse[2] = 0.22525;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.332741;
		glMaterialSpecular[1] = 0.328634;
		glMaterialSpecular[2] = 0.346435;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.3 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//4rd sphere 1st column
		glViewport((screenWidth/9)*4,(screenHeight/5)*4,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.25;
		glMaterialAmbient[1] = 0.20725;
		glMaterialAmbient[2] = 0.20725;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform, 1,glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 1.0;
		glMaterialDiffuse[1] = 0.829;
		glMaterialDiffuse[2] = 0.829;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.296648;
		glMaterialSpecular[1] = 0.296648;
		glMaterialSpecular[2] = 0.296648;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.088 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*5,(screenHeight/5)*4,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.1745;
		glMaterialAmbient[1] = 0.01175;
		glMaterialAmbient[2] = 0.01175;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.61424;
		glMaterialDiffuse[1] = 0.04136;
		glMaterialDiffuse[2] = 0.04136;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.727811;
		glMaterialSpecular[1] = 0.626959;
		glMaterialSpecular[2] = 0.626959;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.6 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*6,(screenHeight/5)*4,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.1;
		glMaterialAmbient[1] = 0.18725;
		glMaterialAmbient[2] = 0.1745;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.396;
		glMaterialDiffuse[1] = 0.74151;
		glMaterialDiffuse[2] = 0.69102;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.297254;
		glMaterialSpecular[1] = 0.30829;
		glMaterialSpecular[2] = 0.306678;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.1 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//2nd row
		//1 row 1st col
		glViewport((screenWidth/9),(screenHeight/5)*3,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.329412;
		glMaterialAmbient[1] = 0.223529;
		glMaterialAmbient[2] = 0.027451;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.780392;
		glMaterialDiffuse[1] = 0.568627;
		glMaterialDiffuse[2] = 0.113725;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.992157;
		glMaterialSpecular[1] = 0.941176;
		glMaterialSpecular[2] = 0.807843;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.21794872 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//2 row 2nd col
		glViewport((screenWidth/9)*2,(screenHeight/5)*3,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.2125;
		glMaterialAmbient[1] = 0.1275;
		glMaterialAmbient[2] = 0.054;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.714;
		glMaterialDiffuse[1] = 0.4284;
		glMaterialDiffuse[2] = 0.18144;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform, 1,glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.393548;
		glMaterialSpecular[1] = 0.271906;
		glMaterialSpecular[2] = 0.166721;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.2*128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//3rd sphere 1st column
		glViewport((screenWidth/9)*3,(screenHeight/5)*3,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.25;
		glMaterialAmbient[1] = 0.25;
		glMaterialAmbient[2] = 0.25;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.4;
		glMaterialDiffuse[1] = 0.4;
		glMaterialDiffuse[2] = 0.4;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.774597;
		glMaterialSpecular[1] = 0.774597;
		glMaterialSpecular[2] = 0.774597;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform, 1,glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.6 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//4rd sphere 1st column
		glViewport((screenWidth/9)*4,(screenHeight/5)*3,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.19125;
		glMaterialAmbient[1] = 0.0735;
		glMaterialAmbient[2] = 0.0225;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.7038;
		glMaterialDiffuse[1] = 0.27048;
		glMaterialDiffuse[2] = 0.0828;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.256777;
		glMaterialSpecular[1] = 0.137622;
		glMaterialSpecular[2] = 0.086014;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.1 *128.0;
		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*5,(screenHeight/5)*3,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.24725;
		glMaterialAmbient[1] = 0.1995;
		glMaterialAmbient[2] = 0.0745;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.75164;
		glMaterialDiffuse[1] = 0.60648;
		glMaterialDiffuse[2] = 0.22648;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.628281;
		glMaterialSpecular[1] = 0.555802;
		glMaterialSpecular[2] = 0.366065;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.4 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*6,(screenHeight/5)*3,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.19225;
		glMaterialAmbient[1] = 0.19225;
		glMaterialAmbient[2] = 0.19225;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.50754;
		glMaterialDiffuse[1] = 0.50754;
		glMaterialDiffuse[2] = 0.50754;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.508273;
		glMaterialSpecular[1] = 0.508273;
		glMaterialSpecular[2] = 0.508273;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.4 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();


		//3rd row
			//1 row 1st col
		glViewport((screenWidth/9),(screenHeight/5)*2,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.0;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.01;
		glMaterialDiffuse[1] = 0.01;
		glMaterialDiffuse[2] = 0.01;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform, 1,glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.50;
		glMaterialSpecular[1] = 0.50;
		glMaterialSpecular[2] = 0.50;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform, 1,glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.25 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//2 row 2nd col
		glViewport((screenWidth/9)*2,(screenHeight/5)*2,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.2125;
		glMaterialAmbient[1] = 0.1275;
		glMaterialAmbient[2] = 0.054;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.714;
		glMaterialDiffuse[1] = 0.4284;
		glMaterialDiffuse[2] = 0.18144;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.393548;
		glMaterialSpecular[1] = 0.271906;
		glMaterialSpecular[2] = 0.166721;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform, 1,glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.2*128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//3rd sphere 1st column
		glViewport((screenWidth/9)*3,(screenHeight/5)*2,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.0;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.1;
		glMaterialDiffuse[1] = 0.35;
		glMaterialDiffuse[2] = 0.1;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.45;
		glMaterialSpecular[1] = 0.55;
		glMaterialSpecular[2] = 0.45;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform, 1,glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.25 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//4rd sphere 1st column
		glViewport((screenWidth/9)*4,(screenHeight/5)*2,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.0;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform, 1,glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.5;
		glMaterialDiffuse[1] = 0.0;
		glMaterialDiffuse[2] = 0.0;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.7;
		glMaterialSpecular[1] = 0.6;
		glMaterialSpecular[2] = 0.6;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.25 *128.0;
		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*5,(screenHeight/5)*2,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.0;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.55;
		glMaterialDiffuse[1] = 0.55;
		glMaterialDiffuse[2] = 0.55;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.70;
		glMaterialSpecular[1] = 0.70;
		glMaterialSpecular[2] = 0.70;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.25 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*6,(screenHeight/5)*2,screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.0;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.5;
		glMaterialDiffuse[1] = 0.5;
		glMaterialDiffuse[2] = 0.0;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.60;
		glMaterialSpecular[1] = 0.60;
		glMaterialSpecular[2] = 0.50;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.25 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();


		//4th column
		glViewport((screenWidth/9),(screenHeight/5),screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.02;
		glMaterialAmbient[1] = 0.02;
		glMaterialAmbient[2] = 0.02;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.01;
		glMaterialDiffuse[1] = 0.01;
		glMaterialDiffuse[2] = 0.01;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.4;
		glMaterialSpecular[1] = 0.4;
		glMaterialSpecular[2] = 0.4;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.078125 *128.0;
		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//2 row 2nd col
		glViewport((screenWidth/9)*2,(screenHeight/5),screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.05;
		glMaterialAmbient[2] = 0.05;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.4;
		glMaterialDiffuse[1] = 0.5;
		glMaterialDiffuse[2] = 0.5;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.04;
		glMaterialSpecular[1] = 0.7;
		glMaterialSpecular[2] = 0.7;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.078125 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//3rd sphere 1st column
		glViewport((screenWidth/9)*3,(screenHeight/5),screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.0;
		glMaterialAmbient[1] = 0.05;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.4;
		glMaterialDiffuse[1] = 0.5;
		glMaterialDiffuse[2] = 0.4;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.04;
		glMaterialSpecular[1] = 0.7;
		glMaterialSpecular[2] = 0.04;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.078125 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		//4rd sphere 1st column
		glViewport((screenWidth/9)*4,(screenHeight/5),screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.05;
		glMaterialAmbient[1] = 0.0;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.5;
		glMaterialDiffuse[1] = 0.4;
		glMaterialDiffuse[2] = 0.4;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.7;
		glMaterialSpecular[1] = 0.04;
		glMaterialSpecular[2] = 0.04;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.078125 *128.0;
		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*5,(screenHeight/5),screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.05;
		glMaterialAmbient[1] = 0.05;
		glMaterialAmbient[2] = 0.05;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.5;
		glMaterialDiffuse[1] = 0.5;
		glMaterialDiffuse[2] = 0.5;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.7;
		glMaterialSpecular[1] = 0.7;
		glMaterialSpecular[2] = 0.7;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.078125 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

		glViewport((screenWidth/9)*6,(screenHeight/5),screenWidth/6,screenHeight/6);
		glMaterialAmbient[0] = 0.05;
		glMaterialAmbient[1] = 0.05;
		glMaterialAmbient[2] = 0.0;
		glMaterialAmbient[3] = 1.0;
		glUniform3fv(kaUniform,1, glMaterialAmbient);	//glMaterialfv();
		glMaterialDiffuse[0] = 0.5;
		glMaterialDiffuse[1] = 0.5;
		glMaterialDiffuse[2] = 0.4;
		glMaterialDiffuse[3] = 1.0;
		glUniform3fv(kdUniform,1, glMaterialDiffuse);	//glMaterialfv();
		glMaterialSpecular[0] = 0.7;
		glMaterialSpecular[1] = 0.7;
		glMaterialSpecular[2] = 0.04;
		glMaterialSpecular[3] = 1.0;
		glUniform3fv(ksUniform,1, glMaterialSpecular);	//glMaterialfv();
		glMaterialShininess = 0.078125 *128.0;

		glUniform1f(materialShininessUniform, glMaterialShininess);	//glMaterialfv();

		drawSphere();

}

void drawSphere()
{
	glBindVertexArray(vao_sphere);
				
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
	glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
	//glDrawArrays(GL_TRIANGLE_STRIP,0,gNumElements);
	glBindVertexArray(0);
}

