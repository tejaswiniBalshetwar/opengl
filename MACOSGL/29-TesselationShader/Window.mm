//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};


//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;


//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		

}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint tessellationControlShaderObject;
    GLuint tessellationEvaluationShaderObject;


    GLuint shaderProgramObject;

		GLuint vao;
		GLuint vbo;
		GLuint mvpUniform;
    GLuint numberSegMentsUniform;
    GLuint numberOfStripsUniform;
    GLuint lineColorUniform;
    int gNumberLineSegments;

		vmath::mat4 perspectiveProjectionMatrix;


}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode=
	"#version 410" \
	"\n" \
	"in vec2 vPosition; 													\n" \
	"uniform mat4 u_mvp_matrix; 											\n" \
	" void main()															\n" \
	"{																		\n"	\
		"gl_Position=vec4(vPosition,0.0,1.0);								\n" \
	"}																		\n" ;


	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}

    
    //Tessellation Control
    
    tessellationControlShaderObject=glCreateShader(GL_TESS_CONTROL_SHADER);

    //provide source code to shader
    const GLchar* tesselationControlShaderSourceCode=
    "#version 410" \
    "\n" \
    "layout(vertices=4)out;                                                     \n" \
    "uniform int numberOfSegments;                                             \n" \
    "uniform int numberOfStrips;                                             \n" \
    " void main()                                                            \n" \
    "{                                                                        \n"    \
        "gl_out[gl_InvocationID].gl_Position=gl_in[gl_InvocationID].gl_Position;                                \n" \
    "   gl_TessLevelOuter[0]=float(numberOfStrips);                                                                        \n" \
    "   gl_TessLevelOuter[1]=float(numberOfSegments);                                                                     \n" \
    "}                                                                        \n" ;


    //specify the shader to vertex shader object
    glShaderSource(tessellationControlShaderObject,1,(const GLchar**)&tesselationControlShaderSourceCode,NULL);

    //compile shader
    glCompileShader(tessellationControlShaderObject);

     iInfoLogLength=0;
     iShaderCompileStatus=0;
     szInfoLog=NULL;

    glGetShaderiv(tessellationControlShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
    if(iShaderCompileStatus==GL_FALSE)
    {
        glGetShaderiv(tessellationControlShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
        if(iInfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iInfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(tessellationControlShaderObject,iInfoLogLength,&written,szInfoLog);
                fprintf(gpFile, "\nError in tessellationControl Shader ....");
                free(szInfoLog);
                [self release];
                [NSApp terminate];
            }
        }
    }
    else
    {
        fprintf(gpFile, "\tessellationControl shader compiled successfully");
    }

    
    //TessellationEvaluation Shader
    tessellationEvaluationShaderObject=glCreateShader(GL_TESS_EVALUATION_SHADER);

       //provide source code to shader
       const GLchar* tesselationEvalShaderSourceCode=
       "#version 410" \
       "\n" \
       "layout(isolines)in;                                                     \n" \
       "uniform mat4 u_mvp_matrix;                                             \n" \
       "uniform int numberOfStrips;                                             \n" \
       " void main()                                                            \n" \
       "{                                                                        \n"    \
        "  float u=gl_TessCoord.x ;                                \n" \
        "   vec3 p0=gl_in[0].gl_Position.xyz;                                                                      \n" \
        "   vec3 p1=gl_in[1].gl_Position.xyz;                                                                      \n" \
        "   vec3 p2=gl_in[2].gl_Position.xyz;                                                                      \n" \
        "   vec3 p3=gl_in[3].gl_Position.xyz;                                                                      \n" \
        "    float u1=(1.0 - u);                                                                        \n" \
        "    float u2=u* u;                                                                             \n" \
        "    float b3=u2 * u;                                                                           \n"\
        "    float b2=3.0 * u2 * u1 ;                                                                   \n" \
        "    float b1=3.0 * u * u1 * u1;                                                                \n" \
        "    float b0= u1 *u1 *u1;                                                                      \n" \
        "    vec3 p= p0 *b0 + p1* b1 + p2* b2 + p3*b3; \n"\
        "    gl_Position=u_mvp_matrix*vec4(p,1.0);\n" \
    "}                                                                        \n";


       //specify the shader to vertex shader object
       glShaderSource(tessellationEvaluationShaderObject,1,(const GLchar**)&tesselationEvalShaderSourceCode,NULL);

       //compile shader
       glCompileShader(tessellationEvaluationShaderObject);

        iInfoLogLength=0;
        iShaderCompileStatus=0;
        szInfoLog=NULL;

       glGetShaderiv(tessellationEvaluationShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
       if(iShaderCompileStatus==GL_FALSE)
       {
           glGetShaderiv(tessellationEvaluationShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
           if(iInfoLogLength>0)
           {
               szInfoLog=(char*)malloc(iInfoLogLength);
               if(szInfoLog!=NULL)
               {
                   GLsizei written;
                   glGetShaderInfoLog(tessellationEvaluationShaderObject,iInfoLogLength,&written,szInfoLog);
                   fprintf(gpFile, "\nError in tessellationEvaluation Shader ....");
                   free(szInfoLog);
                   [self release];
                   [NSApp terminate];
               }
           }
       }
       else
       {
           fprintf(gpFile, "\tessellationEvaluation shader compiled successfully");
       }
    
    
    
	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	GLchar* fragmentShaderSourceCode=
	"#version 410 " \
	"\n" \
	"uniform vec4 lineColor;                    \n" \
    "out vec4 fragColor;					\n" \
	"void main()							\n" \
	"{										\n" \
		"fragColor=lineColor;	\n" \
	"}										\n";


	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,tessellationControlShaderObject);
    glAttachShader(shaderProgramObject,tessellationEvaluationShaderObject);
    glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	 szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	//post linking 
	mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
    numberSegMentsUniform=glGetUniformLocation(shaderProgramObject,"numberOfSegments");
    numberOfStripsUniform=glGetUniformLocation(shaderProgramObject,"numberOfStrips");
    lineColorUniform=glGetUniformLocation(shaderProgramObject,"lineColor");


	//vertices for triangle
	const GLfloat Vertices[]=
	{
		-1.0f,-1.0f, -0.5f, 1.0f , 0.5f, -1.0f, 1.0f, 1.0f
	};


	//vertex array
	glGenVertexArrays(1,&vao);

	glBindVertexArray(vao);
		glGenBuffers(1,&vbo);
		glBindBuffer(GL_ARRAY_BUFFER,vbo);
			glBufferData(GL_ARRAY_BUFFER,sizeof(Vertices),Vertices,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,2,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);



	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,0.0f,0.0f);
    glLineWidth(3.0f);


	perspectiveProjectionMatrix=vmath::mat4::identity();

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		glUseProgram(shaderProgramObject);
			vmath::mat4 modelViewMatrix;
			vmath::mat4 modelViewProjectionMatrix;
			vmath::mat4 translationMatrix;

			modelViewMatrix=vmath::mat4::identity();
			modelViewProjectionMatrix=vmath::mat4::identity();
			translationMatrix=vmath::mat4::identity();

			translationMatrix=vmath::translate(0.5f,0.5f,-4.0f);
			modelViewMatrix=modelViewMatrix*translationMatrix;
			modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
            
            glUniform1i(numberSegMentsUniform,gNumberLineSegments);
            glUniform1i(numberOfStripsUniform,1);
                
            glUniform4fv(lineColorUniform,1,vmath::vec4(1.0,1.0,0.0,1.0));

			glBindVertexArray(vao);
    glPatchParameteri(GL_PATCH_VERTICES,4);
                glDrawArrays(GL_PATCHES,0,4);
			glBindVertexArray(0);
		glUseProgram(0);
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		
        case 'u':
        case 'U':
            fprintf(gpFile,"\n 125");
            gNumberLineSegments++;
            if(gNumberLineSegments >=50 )
            {
                gNumberLineSegments=50;
            }
        break;
        case 'd':
        case 'D':
            gNumberLineSegments--;
           if(gNumberLineSegments <=0 )
           {
               gNumberLineSegments=1;
           }
            break;
		default:
            break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}


