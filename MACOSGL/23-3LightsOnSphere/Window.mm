//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import<math.h>

#import "vmath.h"
#import "Sphere.h"


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};


float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;

//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;

struct light
{
	float light_ambient[4];
	float light_diffuse[4];
	float light_specular[4];
	float light_position[4];
};

light lights[3];

vmath::vec4 lightPosition;
float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;


//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		
}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;


	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	GLuint vao_sphere;
	GLuint vbo_position_sphere;
	GLuint vbo_normal_sphere;
	GLuint vbo_element_sphere;


	GLuint modelUniform;
	GLuint viewUniform;
	GLuint projectionUniform;
	GLuint LKeyIsPressedUniform;
	GLuint laUniformForRed;
	GLuint laUniformForBlue;
	GLuint laUniformForGreen;
	GLuint ldUniformForRed;
	GLuint ldUniformForBlue;
	GLuint ldUniformForGreen;
	GLuint lsUniformForRed;
	GLuint lsUniformForBlue;
	GLuint lsUniformForGreen;
	GLuint kaUniform;
	GLuint kdUniform;
	GLuint ksUniform;
	GLuint lightPositionUniformForRed;
	GLuint lightPositionUniformForBlue;
	GLuint lightPositionUniformForGreen;
	GLuint materialShininessUniform;


	bool bRotation;
	bool bLight;

	int LKeyIsPressed;

	GLfloat angleCube;
	vmath::mat4 perspectiveProjectionMatrix;
	vmath::mat4 modelMatrix;
	vmath::mat4 viewMatrix;

	GLfloat lightAngleForOne;
	GLfloat lightAngleForTwo ;
	GLfloat lightAngleForThree ;


}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode=
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_positionForRedLight;					\n" \
		"uniform vec4 u_light_positionForBlueLight;					\n" \
		"uniform vec4 u_light_positionForGreenLight;					\n" \
		
		"out vec3 out_light_directionForRedLight;					\n" \
		"out vec3 out_light_directionForBlueLight;					\n" \
		"out vec3 out_light_directionForGreenLight;					\n" \
		
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_directionForRedLight=vec3(u_light_positionForRedLight - eye_coordinates);				\n" \
		"		out_light_directionForBlueLight=vec3(u_light_positionForBlueLight - eye_coordinates);				\n" \
		"		out_light_directionForGreenLight=vec3(u_light_positionForGreenLight - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	GLchar* fragmentShaderSourceCode=
		"#version 410 core" \
		"\n" \
		"uniform vec3 u_laForRedLight;								\n" \
		"uniform vec3 u_laForBlueLight;								\n" \
		"uniform vec3 u_laForGreenLight;								\n" \
		
		"uniform vec3 u_ldForRedLight;								\n" \
		"uniform vec3 u_ldForBlueLight;								\n" \
		"uniform vec3 u_ldForGreenLight;								\n" \
		
		"uniform vec3 u_lsForRedLight;								\n" \
		"uniform vec3 u_lsForBlueLight;								\n" \
		"uniform vec3 u_lsForGreenLight;								\n" \
		
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_directionForRedLight;					\n" \
		"in vec3 out_light_directionForBlueLight;					\n" \
		"in vec3 out_light_directionForGreenLight;					\n" \
		
		"in vec3 out_t_normal;							\n" \
		
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_directionForRedLight=normalize(out_light_directionForRedLight);													\n"	\
		"		vec3 normalized_light_directionForBlueLight=normalize(out_light_directionForBlueLight);													\n"	\
		"		vec3 normalized_light_directionForGreenLight=normalize(out_light_directionForGreenLight);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		
		"		float t_dot_ldForRed=max(dot(normalized_light_directionForRedLight,normalized_t_norm),0.0);										\n" \
		"		float t_dot_ldForBlue=max(dot(normalized_light_directionForBlueLight,normalized_t_norm),0.0);										\n" \
		"		float t_dot_ldForGreen=max(dot(normalized_light_directionForGreenLight,normalized_t_norm),0.0);										\n" \
		
		"		vec3 reflection_vetorForRed=reflect(-normalized_light_directionForRedLight ,normalized_t_norm);									\n" \
		"		vec3 reflection_vetorForBlue=reflect(-normalized_light_directionForBlueLight ,normalized_t_norm);									\n" \
		"		vec3 reflection_vetorForGreen=reflect(-normalized_light_directionForGreenLight ,normalized_t_norm);									\n" \
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		
		"		vec3 ambientRed= u_laForRedLight * u_ka;																						\n " \
		"		vec3 ambientBlue= u_laForBlueLight * u_ka;																						\n " \
		"		vec3 ambientGreen= u_laForGreenLight * u_ka;																						\n " \
		
		"		vec3 diffuseForRed=u_ldForRedLight * u_kd * t_dot_ldForRed;																			\n" \
		"		vec3 diffuseForBlue=u_ldForBlueLight * u_kd * t_dot_ldForBlue;																			\n" \
		"		vec3 diffuseForGreen=u_ldForGreenLight * u_kd * t_dot_ldForGreen;																			\n" \
	
		"		vec3 specularRed = u_lsForRedLight * u_ks * pow(max(dot(reflection_vetorForRed,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		vec3 specularBlue = u_lsForBlueLight * u_ks * pow(max(dot(reflection_vetorForBlue,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		vec3 specularGreen = u_lsForGreenLight * u_ks * pow(max(dot(reflection_vetorForGreen,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		
		"		phong_ads_light=ambientRed + ambientBlue + ambientGreen + diffuseForRed + diffuseForBlue + diffuseForGreen + specularRed + specularBlue + specularGreen;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";


	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	modelUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniformForRed = glGetUniformLocation(shaderProgramObject, "u_laForRedLight");
	laUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_laForBlueLight");
	laUniformForGreen = glGetUniformLocation(shaderProgramObject, "u_laForGreenLight");
	ldUniformForRed = glGetUniformLocation(shaderProgramObject, "u_ldForRedLight");
	ldUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_ldForBlueLight");
	ldUniformForGreen = glGetUniformLocation(shaderProgramObject, "u_ldForGreenLight");
	lsUniformForRed = glGetUniformLocation(shaderProgramObject, "u_lsForRedLight");
	lsUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_lsForBlueLight");
	lsUniformForGreen = glGetUniformLocation(shaderProgramObject, "u_lsForGreenLight");

	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	
	LKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyIsPressed");
	
	lightPositionUniformForRed = glGetUniformLocation(shaderProgramObject, "u_light_positionForRedLight");
	lightPositionUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_light_positionForBlueLight");
	lightPositionUniformForGreen = glGetUniformLocation(shaderProgramObject, "u_light_positionForGreenLight");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");

	
	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	fprintf(gpFile, "\n%d\n",gNumElements );
	fprintf(gpFile, "\n%d\n",gNumVertices );


	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//posiiotn data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1,&vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,0.0f,1.0f);


	lights[0].light_ambient[0] = 0.0f;
	lights[0].light_ambient[1] = 0.0f;
	lights[0].light_ambient[2] = 0.0f;
	lights[0].light_ambient[3] = 0.0f;

	lights[0].light_diffuse[0] = 1.0f;
	lights[0].light_diffuse[1] = 0.0f;
	lights[0].light_diffuse[2] = 0.0f;
	lights[0].light_diffuse[3] = 0.0f;

	lights[0].light_specular[0] = 1.0f;
	lights[0].light_specular[1] = 0.0f;
	lights[0].light_specular[2] = 0.0f;
	lights[0].light_specular[3] = 0.0f;


	lights[0].light_position[0] = 0.0f;
	lights[0].light_position[1] = 0.0f;
	lights[0].light_position[2] = 0.0f;
	lights[0].light_position[3] = 1.0f;

	// lights for 2nd light
	lights[1].light_ambient[0] = 0.0f;
	lights[1].light_ambient[1] = 0.0f;
	lights[1].light_ambient[2] = 0.0f;
	lights[1].light_ambient[3] = 0.0f;

	lights[1].light_diffuse[0] = 0.0f;
	lights[1].light_diffuse[1] = 1.0f;
	lights[1].light_diffuse[2] = 0.0f;
	lights[1].light_diffuse[3] = 0.0f;

	lights[1].light_specular[0] = 0.0f;
	lights[1].light_specular[1] = 1.0f;
	lights[1].light_specular[2] = 0.0f;
	lights[1].light_specular[3] = 0.0f;


	lights[1].light_position[0] = 0.0f;
	lights[1].light_position[1] = 0.0f;
	lights[1].light_position[2] = 0.0f;
	lights[1].light_position[3] = 1.0f;

	//3 rd light
	lights[2].light_ambient[0] = 0.0f;
	lights[2].light_ambient[1] = 0.0f;
	lights[2].light_ambient[2] = 0.0f;
	lights[2].light_ambient[3] = 0.0f;

	lights[2].light_diffuse[0] = 0.0f;
	lights[2].light_diffuse[1] = 0.0f;
	lights[2].light_diffuse[2] = 1.0f;
	lights[2].light_diffuse[3] = 0.0f;

	lights[2].light_specular[0] = 0.0f;
	lights[2].light_specular[1] = 0.0f;
	lights[2].light_specular[2] = 1.0f;
	lights[2].light_specular[3] = 0.0f;


	lights[2].light_position[0] = 0.0f;
	lights[2].light_position[1] = 0.0f;
	lights[2].light_position[2] = 0.0f;
	lights[2].light_position[3] = 1.0f;




	perspectiveProjectionMatrix=vmath::mat4::identity();
	modelMatrix=vmath::mat4::identity();
	viewMatrix=vmath::mat4::identity();

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		vmath::mat4 modelViewMatrix;
		
		vmath::mat4 translationMatrix;


		glUseProgram(shaderProgramObject);
			//square
			modelMatrix = vmath::mat4::identity();
			//modelViewProjectionMatrix = mat4::identity();
			translationMatrix = vmath::mat4::identity();
			
			//do necessary translation

			translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

			//do matrix multipliacation
			modelMatrix = modelMatrix * translationMatrix;
			

			//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
			
			glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

			if (bLight)
			{
				glUniform1i(LKeyIsPressedUniform,1);

				lights[0].light_position[1] = 200.0 * cos(lightAngleForOne);
				lights[0].light_position[2] = 200.0 * sin(lightAngleForOne);
				
				glUniform3fv(laUniformForRed,1,lights[0].light_ambient);		//glLightfv() 
				glUniform3fv(ldUniformForRed,1, lights[0].light_diffuse);		//glLightfv() 
				glUniform3fv(lsUniformForRed,1, lights[0].light_specular);		//glLightfv() 
				glUniform4fv(lightPositionUniformForRed,1, lights[0].light_position); //glLightfv() for position


				//FOR GREEN LIGHT
				
				lights[1].light_position[2] = 200.0f * cos(lightAngleForTwo);
				lights[1].light_position[0] = 200.0f * sin(lightAngleForTwo);

				glUniform3fv(laUniformForGreen, 1, lights[1].light_ambient);		//glLightfv() 
				glUniform3fv(ldUniformForGreen, 1, lights[1].light_diffuse);		//glLightfv() 
				glUniform3fv(lsUniformForGreen, 1, lights[1].light_specular);		//glLightfv() 
				glUniform4fv(lightPositionUniformForGreen, 1, lights[1].light_position); //glLightfv() for position
				

				//FOR BLUE LIGHT
				
				lights[2].light_position[0] = 200.0f * cos(lightAngleForThree);
				lights[2].light_position[1] = 200.0f * sin(lightAngleForThree);


				glUniform3fv(laUniformForBlue, 1, lights[2].light_ambient);		//glLightfv() 
				glUniform3fv(ldUniformForBlue, 1, lights[2].light_diffuse);		//glLightfv() 
				glUniform3fv(lsUniformForBlue, 1, lights[2].light_specular);		//glLightfv() 
				glUniform4fv(lightPositionUniformForBlue, 1, lights[2].light_position); //glLightfv() for position



				glUniform3fv(kaUniform,1,material_ambient);	//glMaterialfv();
				glUniform3fv(kdUniform,1,material_diffuse);	//glMaterialfv();
				glUniform3fv(ksUniform,1,material_specular);	//glMaterialfv();
				glUniform1f(materialShininessUniform,material_shininess);	//glMaterialfv();
			}
			else
			{
				glUniform1i(LKeyIsPressedUniform,0);
			}

			glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

			glBindVertexArray(vao_sphere);
				
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
			glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
			//glDrawArrays(GL_TRIANGLE_STRIP,0,gNumElements);
			glBindVertexArray(0);


		glUseProgram(0);

		if(bRotation)
		{
			[self update];	
		}
		
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) update
{

	lightAngleForOne = lightAngleForOne + 0.020f;
	if (lightAngleForOne >= 360.0f)
	{
		lightAngleForOne = 0.0f;
	}

	lightAngleForTwo = lightAngleForTwo + 0.020f;
	if (lightAngleForTwo > 360.0f)
	{
		lightAngleForTwo = 0.0f;
	}

	lightAngleForThree = lightAngleForThree + 0.020f;
	if (lightAngleForThree > 360.0f)
	{
		lightAngleForThree = 0.0f;
	}
	
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
					LKeyIsPressed = 1;
				}
				else
				{
					bLight = false;
					LKeyIsPressed = 0;
				}
				break;
		case 'A':
		case 'a':
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;
		default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}


