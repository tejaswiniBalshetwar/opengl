//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};

struct light
{
	float light_ambient[4];
	float light_diffuse[4];
	float light_specular[4];
	float light_position[4];
};

light lights[2];

float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;




//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;


//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		
}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;


	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	GLuint vao_pyramid;
	

	GLuint vbo_pyramid_position,vbo_pyramid_normal;
	
	GLuint modelUniform;
	GLuint viewUniform;
	GLuint projectionUniform;
	GLuint LKeyIsPressedUniform;
	GLuint laUniformForRed;
	GLuint laUniformForBlue;
	GLuint ldUniformForRed;
	GLuint ldUniformForBlue;
	GLuint lsUniformForRed;
	GLuint lsUniformForBlue;
	GLuint kaUniform;
	GLuint kdUniform;
	GLuint ksUniform;
	GLuint lightPositionUniformForRed;
	GLuint lightPositionUniformForBlue;
	GLuint materialShininessUniform;

	bool bRotation;
	bool bLight;

	int LKeyIsPressed;

	vmath::mat4 perspectiveProjectionMatrix;
	vmath::mat4 modelMatrix;
	vmath::mat4 viewMatrix;

	GLfloat anglePyramid;



}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode=
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_positionForRedLight;					\n" \
		"uniform vec4 u_light_positionForBlueLight;					\n" \
		
		"out vec3 out_light_directionForRedLight;					\n" \
		"out vec3 out_light_directionForBlueLight;					\n" \
		
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_directionForRedLight=vec3(u_light_positionForRedLight - eye_coordinates);				\n" \
		"		out_light_directionForBlueLight=vec3(u_light_positionForBlueLight - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";


	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	GLchar* fragmentShaderSourceCode=
	"#version 410 core" \
		"\n" \
		"uniform vec3 u_laForRedLight;								\n" \
		"uniform vec3 u_laForBlueLight;								\n" \
		
		"uniform vec3 u_ldForRedLight;								\n" \
		"uniform vec3 u_ldForBlueLight;								\n" \
		
		"uniform vec3 u_lsForRedLight;								\n" \
		"uniform vec3 u_lsForBlueLight;								\n" \
		
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_directionForRedLight;					\n" \
		"in vec3 out_light_directionForBlueLight;					\n" \
		
		"in vec3 out_t_normal;							\n" \
		
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_directionForRedLight=normalize(out_light_directionForRedLight);													\n"	\
		"		vec3 normalized_light_directionForBlueLight=normalize(out_light_directionForBlueLight);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		
		"		float t_dot_ldForRed=max(dot(normalized_light_directionForRedLight,normalized_t_norm),0.0);										\n" \
		"		float t_dot_ldForBlue=max(dot(normalized_light_directionForBlueLight,normalized_t_norm),0.0);										\n" \
		
		"		vec3 reflection_vetorForRed=reflect(-normalized_light_directionForRedLight ,normalized_t_norm);									\n" \
		"		vec3 reflection_vetorForBlue=reflect(-normalized_light_directionForBlueLight ,normalized_t_norm);									\n" \
		
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		
		"		vec3 ambientRed= u_laForRedLight * u_ka;																						\n " \
		"		vec3 ambientBlue= u_laForBlueLight * u_ka;																						\n " \
		
		"		vec3 diffuseForRed=u_ldForRedLight * u_kd * t_dot_ldForRed;																			\n" \
		"		vec3 diffuseForBlue=u_ldForBlueLight * u_kd * t_dot_ldForBlue;																			\n" \
	
		"		vec3 specularRed = u_lsForRedLight * u_ks * pow(max(dot(reflection_vetorForRed,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		vec3 specularBlue = u_lsForBlueLight * u_ks * pow(max(dot(reflection_vetorForBlue,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		
		"		phong_ads_light=ambientRed + ambientBlue + diffuseForRed + diffuseForBlue + specularRed + specularBlue;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";


	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	laUniformForRed = glGetUniformLocation(shaderProgramObject, "u_laForRedLight");
	laUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_laForBlueLight");
	ldUniformForRed = glGetUniformLocation(shaderProgramObject, "u_ldForRedLight");
	ldUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_ldForBlueLight");
	lsUniformForRed = glGetUniformLocation(shaderProgramObject, "u_lsForRedLight");
	lsUniformForBlue = glGetUniformLocation(shaderProgramObject, "u_lsForBlueLight");

	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	
	LKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyIsPressed");
	
	lightPositionUniformForRed = glGetUniformLocation(shaderProgramObject, "u_light_positionForRedLight");
	lightPositionUniformForBlue = glGetUniformLocation(shaderProgramObject, "out_light_directionForBlueLight");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");

	//vertices for pyramid

	GLfloat pyramidVertices[] = {
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,1.0f,
		1.0f,-1.0f,1.0f,

		0.0f,1.0f,0.0f,
		1.0f,-1.0f,1.0f,
		1.0f,-1.0f,-1.0f,

		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,

		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,1.0f,

	};

	GLfloat pyramidNormals[] = {
		0.0f,0.441214,0.894427f,
		0.0f,0.441214,0.894427f,
		0.0f,0.441214,0.894427f,
		
		0.894427f,0.447214f,0.0f,
		0.894427f,0.447214f,0.0f,
		0.894427f,0.447214f,0.0f,
		
		0.0f,0.447214f,-0.894427f,
		0.0f,0.447214f,-0.894427f,
		0.0f,0.447214f,-0.894427f,
		
		-0.894427f,0.447214f,0.0f,
		-0.894427f,0.447214f,0.0f,
		-0.894427f,0.447214f,0.0f
	};

	//recording start

	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);

	//posiiotn data
	glGenBuffers(1, &vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal data
	glGenBuffers(1, &vbo_pyramid_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//recording stop
	glBindVertexArray(0);


	//initializeing lights
	lights[0].light_ambient[0] = 0.0f;
	lights[0].light_ambient[1] = 0.0f;
	lights[0].light_ambient[2] = 0.0f;
	lights[0].light_ambient[3] = 0.0f;

	lights[0].light_diffuse[0] = 1.0f;
	lights[0].light_diffuse[1] = 0.0f;
	lights[0].light_diffuse[2] = 0.0f;
	lights[0].light_diffuse[3] = 0.0f;

	lights[0].light_specular[0] = 1.0f;
	lights[0].light_specular[1] = 0.0f;
	lights[0].light_specular[2] = 0.0f;
	lights[0].light_specular[3] = 0.0f;


	lights[0].light_position[0] = 3.0f;
	lights[0].light_position[1] = 0.0f;
	lights[0].light_position[2] = 0.0f;
	lights[0].light_position[3] = 1.0f;

	// lights for 2nd light
	lights[1].light_ambient[0] = 0.0f;
	lights[1].light_ambient[1] = 0.0f;
	lights[1].light_ambient[2] = 0.0f;
	lights[1].light_ambient[3] = 0.0f;

	lights[1].light_diffuse[0] = 0.0f;
	lights[1].light_diffuse[1] = 0.0f;
	lights[1].light_diffuse[2] = 1.0f;
	lights[1].light_diffuse[3] = 0.0f;

	lights[1].light_specular[0] = 0.0f;
	lights[1].light_specular[1] = 0.0f;
	lights[1].light_specular[2] = 1.0f;
	lights[1].light_specular[3] = 0.0f;


	lights[1].light_position[0] = -3.0f;
	lights[1].light_position[1] = 0.0f;
	lights[1].light_position[2] = 0.0f;
	lights[1].light_position[3] = 1.0f;


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,0.0f,1.0f);


	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();

	perspectiveProjectionMatrix=vmath::mat4::identity();
	bLight=false;

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		glUseProgram(shaderProgramObject);
			
			vmath::mat4 translationMatrix;
			vmath::mat4 rotationMatrix;
			vmath::mat4 scaleMatrix;

			modelMatrix=vmath::mat4::identity();
			
			translationMatrix=vmath::mat4::identity();
			rotationMatrix=vmath::mat4::identity();

			translationMatrix=vmath::translate(0.0f,0.0f,-6.0f);
			rotationMatrix=vmath::rotate(anglePyramid,0.0f,1.0f,0.0f);
			modelMatrix=modelMatrix * translationMatrix * rotationMatrix;

			
			glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
			
			glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

			if (bLight)
			{
				glUniform1i(LKeyIsPressedUniform,1);
				glUniform3fv(laUniformForRed,1,lights[0].light_ambient);		//glLightfv() 
				glUniform3fv(ldUniformForRed,1, lights[0].light_diffuse);		//glLightfv() 
				glUniform3fv(lsUniformForRed,1, lights[0].light_specular);		//glLightfv() 
				glUniform4fv(lightPositionUniformForRed,1, lights[0].light_position); //glLightfv() for position

				glUniform3fv(laUniformForBlue, 1, lights[1].light_ambient);		//glLightfv() 
				glUniform3fv(ldUniformForBlue, 1, lights[1].light_diffuse);		//glLightfv() 
				glUniform3fv(lsUniformForBlue, 1, lights[1].light_specular);		//glLightfv() 
				glUniform4fv(lightPositionUniformForBlue, 1, lights[1].light_position); //glLightfv() for position
				
				glUniform3fv(kaUniform,1,material_ambient);	//glMaterialfv();
				glUniform3fv(kdUniform,1,material_diffuse);	//glMaterialfv();
				glUniform3fv(ksUniform,1,material_specular);	//glMaterialfv();
				glUniform1f(materialShininessUniform,material_shininess);	//glMaterialfv();
			}
			else
			{
				glUniform1i(LKeyIsPressedUniform,0);
			}

			glBindVertexArray(vao_pyramid);
				glDrawArrays(GL_TRIANGLES,0,12);
			glBindVertexArray(0);


		

		glUseProgram(0);

		[self update];
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) update
{
	anglePyramid=anglePyramid+0.5f;

	if(anglePyramid>360.0f)
	{
		anglePyramid=0.0f;
	}

}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;

		case 'L':
		case 'l':
			if(bLight==false)
			{
				bLight=true;
			}
			else
			{
				bLight=false;
			}
			break;
		default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}


