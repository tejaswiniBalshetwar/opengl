//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"
#import "Sphere.h"


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};


float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;

//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

void CreateProgramForPerFragment();
void CreateProgramForPerVertex();

//FILE IO
FILE *gpFile=NULL;

float light_ambient[] = {0.0f,0.0f,0.0f,0.0f};
float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
float lightPosition[] = {100.0f,100.0f,100.0f,1.0f};

float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 50.0f;

GLuint gShaderProgramObjectVertex ;
GLuint gShaderProgramObjectFragment ;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;


GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShininessUniform;


//vertex lighting
GLuint modelUniformVertex;
GLuint viewUniformVertex;
GLuint projectionUniformVertex;
GLuint LKeyIsPressedUniformVertex;
GLuint laUniformVertex;
GLuint ldUniformVertex;
GLuint lsUniformVertex;
GLuint kaUniformVertex;
GLuint kdUniformVertex;
GLuint ksUniformVertex;
GLuint lightPositionUniformVertex;
GLuint materialShininessUniformVertex;


int LKeyIsPressed;
bool gbIsVertexShader = true;
bool gbIsFragmentShader = false;



//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		
}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	bool bRotation;
	bool bLight;

	
	GLfloat angleCube;
	vmath::mat4 perspectiveProjectionMatrix;;
	vmath::mat4 modelMatrix;;
	vmath::mat4 viewMatrix;;


}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


	CreateProgramForPerFragment();
	CreateProgramForPerVertex();
	
	
	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	fprintf(gpFile, "\n%d\n",gNumElements );
	fprintf(gpFile, "\n%d\n",gNumVertices );


	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//posiiotn data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1,&vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,0.0f,1.0f);


	perspectiveProjectionMatrix=vmath::mat4::identity();
	modelMatrix=vmath::mat4::identity();
	viewMatrix=vmath::mat4::identity();

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		vmath::mat4 modelViewMatrix;
		
		vmath::mat4 translationMatrix;

			//square
			modelMatrix = vmath::mat4::identity();
			//modelViewProjectionMatrix = mat4::identity();
			translationMatrix = vmath::mat4::identity();
			
			//do necessary translation

			translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

			//do matrix multipliacation
			modelMatrix = modelMatrix * translationMatrix;
			

			//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			if (gbIsVertexShader == false)
			{
				glUseProgram(gShaderProgramObjectFragment);
				glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
				glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);

				glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

				if (bLight)
				{
					glUniform1i(LKeyIsPressedUniform, 1);
					glUniform3fv(laUniform, 1, light_ambient);		//glLightfv() 
					glUniform3fv(ldUniform, 1, light_diffuse);		//glLightfv() 
					glUniform3fv(lsUniform, 1, light_specular);		//glLightfv() 
					glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position

					glUniform3fv(kaUniform, 1, material_ambient);	//glMaterialfv();
					glUniform3fv(kdUniform, 1, material_diffuse);	//glMaterialfv();
					glUniform3fv(ksUniform, 1, material_specular);	//glMaterialfv();
					glUniform1f(materialShininessUniform, material_shininess);	//glMaterialfv();
				}
				else
				{
					glUniform1i(LKeyIsPressedUniform, 0);
				}
			}
			else
			{
				glUseProgram(gShaderProgramObjectVertex);

				glUniformMatrix4fv(modelUniformVertex, 1, GL_FALSE, modelMatrix);
				glUniformMatrix4fv(viewUniformVertex, 1, GL_FALSE, viewMatrix);

				glUniformMatrix4fv(projectionUniformVertex, 1, GL_FALSE, perspectiveProjectionMatrix);

				if (bLight)
				{
					glUniform1i(LKeyIsPressedUniformVertex, 1);
					glUniform3fv(laUniformVertex, 1, light_ambient);		//glLightfv() 
					glUniform3fv(ldUniformVertex, 1, light_diffuse);		//glLightfv() 
					glUniform3fv(lsUniformVertex, 1, light_specular);		//glLightfv() 
					glUniform4fv(lightPositionUniformVertex, 1, lightPosition); //glLightfv() for position

					glUniform3fv(kaUniformVertex, 1, material_ambient);	//glMaterialfv();
					glUniform3fv(kdUniformVertex, 1, material_diffuse);	//glMaterialfv();
					glUniform3fv(ksUniformVertex, 1, material_specular);	//glMaterialfv();
					glUniform1f(materialShininessUniformVertex, material_shininess);	//glMaterialfv();
				}
				else
				{
					glUniform1i(LKeyIsPressedUniformVertex, 0);
				}
			}

	

			glBindVertexArray(vao_sphere);
				
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
			glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
			//glDrawArrays(GL_TRIANGLE_STRIP,0,gNumElements);
			glBindVertexArray(0);


		glUseProgram(0);

		if(bRotation)
		{
			[self update];	
		}
		
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) update
{

	angleCube=angleCube+0.5f;
	if(angleCube>360.0f)
	{
		angleCube=0.0f;
	}
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		case 'l':
			case 'L':
				if (bLight == false)
				{
					bLight = true;
					LKeyIsPressed = 1;
				}
				else
				{
					bLight = false;
					LKeyIsPressed = 0;
				}
				break;
		case 'A':
		case 'a':
			if (bRotation == false)
			{
				bRotation = true;
			}
			else
			{
				bRotation = false;
			}
			break;
		case 'v':
		case 'V':
			if (gbIsVertexShader==false)
			{
				gbIsVertexShader = true;
			}
			else
			{
				gbIsVertexShader = false;
			}
		break;
	default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

void CreateProgramForPerFragment()
{
	
	GLuint vertexShaderObjectFragment = 0;
	GLuint fragmentShaderObjectFragment = 0;
	vertexShaderObjectFragment = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCodeFragment =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObjectFragment, 1, (const GLchar**)&vertexShaderSourceCodeFragment, NULL);


	//compile shader
	glCompileShader(vertexShaderObjectFragment);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObjectFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObjectFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCodeFragment =
		"#version 410 core" \
		"\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
		"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		"		vec3 ambient= u_la * u_ka;																						\n " \
		"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																			\n" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		phong_ads_light=ambient + diffuse + specular;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";

	//specify source code to object
	glShaderSource(fragmentShaderObjectFragment, 1, (const GLchar**)&fragmentShaderSourceCodeFragment, NULL);

	//compile shader
	glCompileShader(fragmentShaderObjectFragment);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObjectFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObjectFragment = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObjectFragment, vertexShaderObjectFragment);
	glAttachShader(gShaderProgramObjectFragment, fragmentShaderObjectFragment);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObjectFragment, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectFragment, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObjectFragment);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectFragment, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectFragment, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Error while Linking the program: %s", szInfoLog);
			
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_projection_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ks");

	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_LKeyIsPressed");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_material_shininess");

}
void CreateProgramForPerVertex()
{
	

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	//create source code object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the shader source code
	const GLchar *vertexShaderSourceCode =
		"	#version 410 core											\n" \
		"	\n															\n" \
		"	in vec4 vPosition1;											\n" \
		"	in vec3 vNormal1;											\n" \
		"	uniform mat4 u_model_matrix1;								\n" \
		"	uniform mat4 u_view_matrix1;								\n" \
		"	uniform mat4 u_projection_matrix1;							\n" \
		"	uniform vec3 u_la1;											\n" \
		"	uniform vec3 u_ld1;											\n" \
		"	uniform vec3 u_ls1;											\n" \
		"	uniform vec3 u_ka1;											\n" \
		"	uniform vec3 u_kd1;											\n" \
		"	uniform vec3 u_ks1;											\n" \
		"	uniform int u_LKeyIsPressed1;															\n" \
		"	uniform float u_materialShininess1;														\n" \
		"	uniform vec4 u_light_position1;															\n" \
		"	out vec3 out_phong_ads_light1 ;															\n" \
		"	void main(void)																			\n" \
		"	{																						\n" \
		"		if(u_LKeyIsPressed1==1)																\n" \
		"		{																					\n" \
		"			vec4 eye_coordinates=u_view_matrix1 * u_model_matrix1 * vPosition1;															\n" \
		"			vec3 t_normal=normalize(mat3(u_view_matrix1*u_model_matrix1) * vNormal1) ;													\n" \
		"			vec3 light_direction=normalize(vec3(u_light_position1 - eye_coordinates));												\n" \
		"			float t_dot_ld=max(dot(light_direction,t_normal),0.0);																	\n" \
		"			vec3 reflection_vector=reflect(-light_direction,t_normal);																\n" \
		"			vec3 viewer_vector=normalize(vec3(-eye_coordinates));																	\n" \
		"			vec3 ambient= u_la1 * u_ka1;																								\n" \
		"			vec3 diffuse= u_ld1 * u_kd1 * t_dot_ld;																					\n" \
		"			vec3 specular= u_ls1 * u_ks1 * pow(max(dot(reflection_vector,viewer_vector),0.0),u_materialShininess1);					\n" \
		"			out_phong_ads_light1= ambient + diffuse + specular;																			\n" \
		"		}																															\n" \
		"		else																														\n" \
		"		{																															\n" \
		"			out_phong_ads_light1=vec3(1.0,1.0,1.0);																					\n" \
		"																																	\n" \
		"		}																															\n" \
		"		gl_Position= u_projection_matrix1 * u_view_matrix1 * u_model_matrix1 * vPosition1;																															\n" \
		"	}																																\n" \
		;

	//specify source code to object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking

	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char * szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n Error while compiling Vertex Shader: %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				exit(0);
			}
		}

	}

	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code for the fragment shader
	const GLchar* fragmentShaderSourceCode =
		"	#version 410 core																		" \
		"	\n																						" \
		"	in vec3 out_phong_ads_light1;															\n" \
		"	out vec4 fragColor;																		\n" \
		"	void main(void)																			\n" \
		"	{																						\n" \
		"		fragColor=vec4(out_phong_ads_light1,1.0);											\n" \
		"	}																						\n" \
		;

	//specify shader code to shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error cheking for compilation
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"\nError in Fragment Shader:=%s\n",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObjectVertex =glCreateProgram();
	
	//cattach shader to program
	glAttachShader(gShaderProgramObjectVertex, vertexShaderObject);
	glAttachShader(gShaderProgramObjectVertex,fragmentShaderObject);
	
	//prelinkg for attributes
	glBindAttribLocation(gShaderProgramObjectVertex,AMC_ATTRIBUTE_POSITION,"vPosition1");
	glBindAttribLocation(gShaderProgramObjectVertex, AMC_ATTRIBUTE_NORMAL, "vNormal1");

	//link the program
	glLinkProgram(gShaderProgramObjectVertex);

	//errror checking for linking the program

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectVertex,GL_LINK_STATUS,&iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectVertex,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			GLsizei written;
			glGetProgramInfoLog(gShaderProgramObjectVertex,iInfoLogLength,&written,szInfoLog);
			fprintf(gpFile,"\nError while linking the Program:=%s\n",szInfoLog);

			free(szInfoLog);
			exit(0);
		}	
	}

	//post linking binding uniform
	modelUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_model_matrix1");
	viewUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_view_matrix1");
	projectionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_projection_matrix1");
	
	laUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_la1");
	ldUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ld1");
	lsUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ls1");

	kaUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ka1");
	kdUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_kd1");
	ksUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ks1");

	LKeyIsPressedUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_LKeyIsPressed1");

	lightPositionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_light_position1");
	materialShininessUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_materialShininess1");


}

