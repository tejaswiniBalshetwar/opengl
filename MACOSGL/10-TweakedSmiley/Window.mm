//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};



//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;


//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		
}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;


	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	GLuint vao_square;


	GLuint vbo_position_square ;
	GLuint vbo_texture_square;


	GLuint mvpUniform;
	GLuint samplerUniform;


	GLuint texture_smiley;

	

	vmath::mat4 perspectiveProjectionMatrix;

	int keyPressed;



}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode=
	"#version 410" \
	"\n" \
	"in vec4 vPosition; 													\n" \
	"in vec2 vTexCoord;	 													\n" \
	"out vec2 out_texCoord;													\n" \
	"uniform mat4 u_mvp_matrix; 											\n" \
	" void main()															\n" \
	"{																		\n"	\
		"gl_Position=u_mvp_matrix* vPosition;								\n" \
		"out_texCoord=vTexCoord;													\n" \
	"}																		\n" ;


	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	GLchar* fragmentShaderSourceCode=
	"#version 410 " \
	"\n" \
	"out vec4 fragColor;									\n" \
	"in vec2 out_texCoord;									\n" \
	"uniform sampler2D	u_sampler;							\n" \
	"void main()											\n" \
	"{														\n" \
		"fragColor=texture(u_sampler,out_texCoord);			\n" \
	"}														\n";


	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	 szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	//post linking 
	mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
	samplerUniform=glGetUniformLocation(shaderProgramObject,"u_sampler");

	//vertices for triangle
	//specifying the data for drawing;
	const GLfloat squareVertices[] = {
										 1.0f,1.0f,0.0f,
										-1.0f,1.0f,0.0f,
										-1.0f,-1.0f,0.0f,
										 1.0f,-1.0f,0.0f
									};

	
	//create the vao recoreder for display
	glGenVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		
		//create vbo for actual transfering the data
		glGenBuffers(1,&vbo_position_square);
		//binding for data to the gpu at target
		glBindBuffer(GL_ARRAY_BUFFER,vbo_position_square);
		//sending the data and its size to gpu
		glBufferData(GL_ARRAY_BUFFER,sizeof(squareVertices),squareVertices,GL_STATIC_DRAW);
		//sending the reading info to gpu and accessing info through cpu end
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		//enableing pur access point for data at cpu
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//for bindng again in display convention
		glBindBuffer(GL_ARRAY_BUFFER,0);


		//texture bufferes;
		glGenBuffers(1,&vbo_texture_square);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_square);
		glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GL_FLOAT),NULL,GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
		glBindBuffer(GL_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glEnable(GL_TEXTURE_2D);

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	

	texture_smiley=[self loadTextureFromBMPFile:"Smiley.bmp"];
	

	perspectiveProjectionMatrix=vmath::mat4::identity();

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

-(GLuint)loadTextureFromBMPFile:(const char* )texFile
{
	
	
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath,texFile];

	NSImage *bmpImage=[[NSImage alloc] initWithContentsOfFile:textureFileNameWithPath];
	if(!bmpImage)
	{
		NSLog(@"can't find %@",texFile);
		fprintf(gpFile, "\nError in File opening\n" );
		return 0;
	}
	else
	{
		fprintf(gpFile, "\nTeture Loaded successfully" );
	}

	CGImageRef cgImage=[bmpImage CGImageForProposedRect:nil context:nil hints:nil];

	int w=(int)CGImageGetWidth(cgImage);
	int h=(int)CGImageGetHeight(cgImage);

	CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));

	void *pixels=(void*)CFDataGetBytePtr(imageData);

	GLuint bmptexture;
	glGenTextures(1,&bmptexture);

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glBindTexture(GL_TEXTURE_2D,bmptexture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);

	glGenerateMipmap(GL_TEXTURE_2D);

	CFRelease(imageData);
	return(bmptexture);
}	

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{

	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		glUseProgram(shaderProgramObject);
				//declaration of matrices

			GLfloat texCoordVertices[8]; 
			vmath::mat4 modelViewMatrix;
			vmath::mat4 modelViewProjectionMatrix;
			vmath::mat4 translationMatrix;
			vmath::mat4 rotationMatrix;
			//making them identity glLaodIdentity()
			modelViewMatrix = vmath::mat4::identity();
			modelViewProjectionMatrix = vmath::mat4::identity();
			translationMatrix = vmath::mat4::identity();
			rotationMatrix=vmath::mat4::identity();
			//for transpformation multiplication
			translationMatrix = vmath::translate(0.0f,0.0f,-4.0f);
			rotationMatrix=vmath::rotate(180.0f,0.0f,0.0f,1.0f);
			modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;


			//do necesasry rotation

			modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			//creating the projection for viewport and sending is to the gpu using uniform dynamically data transpher
			glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D,texture_smiley);
			glUniform1i(samplerUniform,0);
			
	if(keyPressed==1)
	{
		fprintf(gpFile,"\n In keypressed1");
		texCoordVertices[0]=0.5f;
		texCoordVertices[1]=0.5f;
		texCoordVertices[2]=0.0f;
		texCoordVertices[3]=0.5f;
		texCoordVertices[4]=0.0f;
		texCoordVertices[5]=0.0f;
		texCoordVertices[6]=0.5f;
		texCoordVertices[7]=0.0f;
	}
	else if(keyPressed==2)
	{
		fprintf(gpFile, "\n In keypressed2");
		texCoordVertices[0] =  2.0f ;
		texCoordVertices[1] =  2.0f ;
		texCoordVertices[2] =  0.0f ;
		texCoordVertices[3] =  2.0f ;
		texCoordVertices[4] =  0.0f ;
		texCoordVertices[5] =  0.0f ;
		texCoordVertices[6] =  2.0f ;
		texCoordVertices[7] =  0.0f ;
	}
	else if(keyPressed==3)
	{
		texCoordVertices[0] =  1.0f ;
		texCoordVertices[1] =  1.0f ;
		texCoordVertices[2] =  0.0f ;
		texCoordVertices[3] =  1.0f ;
		texCoordVertices[4] =  0.0f ;
		texCoordVertices[5] =  0.0f ;
		texCoordVertices[6] =  1.0f ;
		texCoordVertices[7] =  0.0f ;

	}
	else 
	{
		fprintf(gpFile, "\n In keypressed default");
		texCoordVertices[0] =  0.5f ;
		texCoordVertices[1] =  0.5f ;
		texCoordVertices[2] =  0.5f ;
		texCoordVertices[3] =  0.5f ;
		texCoordVertices[4] =  0.5f ;
		texCoordVertices[5] =  0.5f ;
		texCoordVertices[6] =  0.5f ;
		texCoordVertices[7] =  0.5f ;
	}
	
			//actual drawing start
			glBindVertexArray(vao_square);
				glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_square);
					glBufferData(GL_ARRAY_BUFFER,sizeof(texCoordVertices),texCoordVertices,GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER,0);
				glDrawArrays(GL_TRIANGLE_FAN,0,4);
			glBindVertexArray(0);
		glUseProgram(0);

	
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}



-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		case '1':
			fprintf(gpFile, "\nIn case 18");
			keyPressed = 1;
			break;
		case '2':
			keyPressed = 2;
			break;
		case '3':
			keyPressed = 3;
			break;
		case '4':
			keyPressed = 4;
			break;
		default:
			keyPressed = 0;
			break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}





