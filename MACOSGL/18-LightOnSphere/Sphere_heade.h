
#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

@interface Sphere : NSObject
    -(void) getSphereVertexData:(float[]) spherePositionCoords: (float[]) sphereNormalCoords: (float[]) sphereTexCoords: (short[]) sphereElements;
    
    -(int) getNumberOfSphereVertices;

    -(int) getNumberOfSphereElements;

    -(void) processSphereData;

    -(void) addTriangle :(float**) single_vertex : (float** ) single_normal : (float**) single_texture;

    -(void) normalizeVector:(float[] )v;

    -(BOOL) isFoundIdentical: ( float) val1 :(float) val2 :(float) diff;
@end
