//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"

using namespace vmath;


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};


//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;
GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;
GLuint vao_circle;
GLuint vbo_circle_position;
GLuint vbo_circle_color;


//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		

}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;


	GLuint vao_square;
	GLuint vbo_square_position ;
	GLuint vbo_square_color;

	GLuint vao_triangle;
	GLuint vbo_triangle_position;
	GLuint vbo_triangle_color;


		

	vmath::mat4 perspectiveProjectionMatrix;

	GLuint mvpUniform;
	GLuint colorUniform;
	GLfloat verticesForOuterCircle[2000];
	GLfloat colorForCircle[2000];

	GLfloat verticesForInnerCircle[2000];

}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	GLfloat lengthOfASide = 0.0f;
	GLfloat lengthOfBSide = 0.0f;
	GLfloat lengthOfCSide = 0.0f;


	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
			"gl_PointSize=50.0;" \
			"out_color=vColor;" \
		"}";
	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader :%s ....",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	const GLchar *fragmentShaderSourceCode=
	{ "#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor=out_color;" \
		"}" \
	};

	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader: %s ....",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	//post linking 
	mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_uniform");

	//vertices for triangle

	//crating VAO for recording 
    //crating VAO for recording 
    glGenVertexArrays(1,&vao_line);
    glBindVertexArray(vao_line);
        glGenBuffers(1,&vbo_line_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
            glBufferData(GL_ARRAY_BUFFER,3*2*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);


	GLfloat verticesForSquare[] = {
		1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f
	};

	GLfloat colorForSquare[] = {
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
	};

    
	//for square
	glGenVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		glGenBuffers(1,&vbo_square_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
		glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForSquare), verticesForSquare,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		//for color
		glGenBuffers(1,&vbo_square_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
		glBufferData(GL_ARRAY_BUFFER,sizeof(colorForSquare),colorForSquare,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);

	//for outer circle
	lengthOfASide = calculateLengthByDistanceFormula(verticesForSquare[0], verticesForSquare[1], verticesForSquare[2], verticesForSquare[3], verticesForSquare[4], verticesForSquare[5]);
	lengthOfBSide = calculateLengthByDistanceFormula(verticesForSquare[3], verticesForSquare[4], verticesForSquare[5], verticesForSquare[6], verticesForSquare[7], verticesForSquare[8]);
	
/*
	GLfloat verticesForOuterCircle[2000];
	memset(verticesForOuterCircle,0,sizeof(verticesForOuterCircle));
	GLfloat colorForCircle[2000];
	memset(colorForCircle,0,sizeof(colorForCircle));*/
	GLfloat radius = 0.0f;
	radius = calculateRadiusOfOuterCircle(lengthOfASide, lengthOfBSide);

	calculateVerticesForCircle(radius, 0.0f, 0.0f, verticesForOuterCircle,colorForCircle);


	glGenVertexArrays(1,&vao_circle);
		glBindVertexArray(vao_circle);
		glGenBuffers(1,&vbo_circle_position);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForOuterCircle),verticesForOuterCircle,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_circle_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(colorForCircle),colorForCircle,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);

	//trinagle
	//triangle
	GLfloat verticesForTriangle[] = {
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f
	};

	GLfloat colorForTriangle[] = {
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f
	};

	glGenVertexArrays(1,&vao_triangle);
	glBindVertexArray(vao_triangle);
		glGenBuffers(1,&vbo_triangle_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_triangle_position);
		glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForTriangle),verticesForTriangle,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_triangle_color);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(colorForTriangle),colorForTriangle,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//inner circle
	lengthOfASide = calculateLengthByDistanceFormula(verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2], verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5]);
	lengthOfBSide = calculateLengthByDistanceFormula(verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5], verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8]);
	lengthOfCSide = calculateLengthByDistanceFormula(verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8], verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2]);

	
	GLfloat XCenterOfCircle = ((verticesForTriangle[0] * lengthOfBSide) + (verticesForTriangle[3] * lengthOfCSide) + (verticesForTriangle[6] * lengthOfASide))/(lengthOfASide+lengthOfBSide+lengthOfCSide);
	GLfloat YCenterOfCircle = ((verticesForTriangle[1] * lengthOfBSide) + (verticesForTriangle[4] * lengthOfCSide) + (verticesForTriangle[7] * lengthOfASide))/ (lengthOfASide + lengthOfBSide + lengthOfCSide);

	radius = calculateInnerCircleRadius(lengthOfASide, lengthOfBSide, lengthOfCSide);

	calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);



	//geometry end here
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    perspectiveProjectionMatrix= mat4::identity();


	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

GLfloat calculateLengthByDistanceFormula(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2 )
{
	GLfloat length = 0.0f;

	length = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2) + pow((z2 - z1), 2));

	return length;

}
GLfloat calculateRadiusOfOuterCircle(GLfloat lengthA, GLfloat lengthB)
{
	GLfloat radius = 0.0f;
	GLfloat hypotenious = 0.0f;

	hypotenious = sqrt(pow(lengthA, 2) + pow(lengthB, 2));

	radius = hypotenious / 2;
	return radius;

}
GLfloat calculateInnerCircleRadius(GLfloat A, GLfloat B, GLfloat C)
{
	GLfloat semiperimeter = (A + B + C) / 2.0f;
	GLfloat area = (GLfloat)sqrt(semiperimeter*((semiperimeter - A)*(semiperimeter - B)*(semiperimeter - C)));

	GLfloat radius =GLfloat (area / semiperimeter);

	return radius;
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
	GLfloat x = 0.0f, y = 0.0f;
	//vertices[2000];
	for (int i = 0; i < 2000-3; i=i+3)
	{
		GLfloat angle = (2 * M_PI*i) / 2000;
		x =(GLfloat) XPointOfCenter + radius * cos(angle);
		y =(GLfloat) YPointOfCenter + radius * sin(angle);

		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = 0.0f; 

		color[i] = 1.0f;
		color[i + 1] = 0.0f;
		color[i + 2] = 1.0f;
	}

}

-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{


	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	    mat4 translationMatrix;
	    mat4 modelViewMatrix;
	    mat4 modelViewProjectionMatrix;
		
	    glUseProgram(shaderProgramObject);

	        modelViewMatrix=mat4::identity();
	        modelViewProjectionMatrix=mat4::identity();
	        translationMatrix=mat4::identity();

			translationMatrix= translate(0.0f, 0.0f, -5.0f);
	        modelViewMatrix=modelViewMatrix*translationMatrix;

	        modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
			glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

			//glLineWidth(5.0f);
			[self drawGraph];

			[self drawSquare];

			drawCircle(verticesForOuterCircle,sizeof(verticesForOuterCircle),colorForCircle,sizeof(colorForCircle));
			
			[self drawTriangle];

			drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));
				
	    glUseProgram(0);
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);


	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) drawGraph
{
	void drawLine(GLfloat*, int, GLfloat*, int);
	GLfloat lineColor[8];

	GLfloat lineColorForVerticalLine[3];
	GLfloat horizontalLines[6];
	GLfloat verticalLines[6];

	GLfloat lineVerticesHorizontal[] = {
			8.0f,0.0f,0.0f,
			-8.0f,0.0f,0.0f,
	};

	lineColor[0] = 1.0f;
	lineColor[1] = 0.0f;
	lineColor[2] = 0.0f;
	lineColor[3] = 0.0f;
	lineColor[4] = 1.0f;
	lineColor[5] = 0.0f;
	lineColor[6] = 0.0f;
	lineColor[7] = 0.0f;

	//glUniform4fv(colorUniform,1,lineColor);

	//X Axis
	drawLine(lineVerticesHorizontal, sizeof(lineVerticesHorizontal), lineColor, sizeof(lineColor));


	//Y Axix
	GLfloat lineVerticesVertical[] = {
		0.0f,4.0f,0.0f,
		0.0f,-4.0f,0.0f
	};

	lineColor[0] = 0.0f;
	lineColor[1] = 1.0f;
	lineColor[2] = 0.0f;
	lineColor[3] = 0.0f;
	lineColor[4] = 0.0f;
	lineColor[5] = 1.0f;
	lineColor[6] = 0.0f;
	lineColor[7] = 0.0f;

	//glUniform4fv(colorUniform,1,lineColor);

	drawLine(lineVerticesVertical, sizeof(lineVerticesVertical), lineColor, sizeof(lineColor));


	//vertical Lines
	lineColor[0] = 0.0f;
	lineColor[1] = 0.0f;
	lineColor[2] = 1.0f;
	lineColor[3] = 0.0f;
	//vertical Lines
	lineColor[4] = 0.0f;
	lineColor[5] = 0.0f;
	lineColor[6] = 1.0f;
	lineColor[7] = 0.0f;


	//glUniform4fv(colorUniform,1,lineColor);


	glBindVertexArray(vao_line);
	for (int i = 1; i < 21; i++)
	{
		GLfloat width = 3.53f*i / 20;
		verticalLines[0] = width;
		verticalLines[1] = 4.0f;
		verticalLines[2] = 0.0f;

		verticalLines[3] = width;
		verticalLines[4] = -4.0f;
		verticalLines[5] = 0.0f;
		drawLine(verticalLines, sizeof(verticalLines), lineColor, sizeof(lineColor));

		width = -3.53f*i / 20;
		verticalLines[0] = width;
		verticalLines[1] = 4.0f;
		verticalLines[2] = 0.0f;

		verticalLines[3] = width;
		verticalLines[4] = -4.0f;
		verticalLines[5] = 0.0f;
		drawLine(verticalLines, sizeof(verticalLines), lineColor, sizeof(lineColor));

	}
	glBindVertexArray(0);


	glBindVertexArray(vao_line);
	for (int i = 1; i < 21; i++)
	{
		GLfloat width = 2.0f*i / 20;
		horizontalLines[0] = 3.53f;
		horizontalLines[1] = width;
		horizontalLines[2] = 0.0f;

		horizontalLines[3] = -3.5f;
		horizontalLines[4] = width;
		horizontalLines[5] = 0.0f;
		drawLine(horizontalLines, sizeof(horizontalLines), lineColor, sizeof(lineColor));

		width = -2.0f*i / 20;
		horizontalLines[0] = 3.53f;
		horizontalLines[1] = width;
		horizontalLines[2] = 0.0f;

		horizontalLines[3] = -3.53f;
		horizontalLines[4] = width;
		horizontalLines[5] = 0.0f;
		drawLine(horizontalLines, sizeof(horizontalLines), lineColor, sizeof(lineColor));

	}
	glBindVertexArray(0);
}


-(void) drawSquare
{
	glBindVertexArray(vao_square);
	glLineWidth(2.0f);
	glDrawArrays(GL_LINE_LOOP,0,4);
	glLineWidth(0.0f);
	glBindVertexArray(0);
}

void drawCircle(GLfloat * vertices,GLfloat verticesSize,GLfloat *color,GLfloat colorSize)
{
	glBindVertexArray(vao_circle);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,verticesSize,vertices,GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, colorSize, color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		glPointSize(3.0f);
		glDrawArrays(GL_POINTS,0,2000);
	glBindVertexArray(0);
}

-(void) drawTriangle
{
	glBindVertexArray(vao_triangle);
	glDrawArrays(GL_LINE_LOOP,0,3);
	glBindVertexArray(0);
}

void drawLine(GLfloat* lineVertices,int lineArraySize,GLfloat* lineColor,int lineColorSize)
{
	glBindVertexArray(vao_line);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line_position);
	glBufferData(GL_ARRAY_BUFFER, lineArraySize, lineVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_line_color);
	glBufferData(GL_ARRAY_BUFFER,lineColorSize, lineColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}


