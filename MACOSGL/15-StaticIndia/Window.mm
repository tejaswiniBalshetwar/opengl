//headers

#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>


#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>


#import "vmath.h"

using namespace vmath;


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0,
};


//'C' style global function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp * ,CVOptionFlags,CVOptionFlags *,void *);

//FILE IO
FILE *gpFile=NULL;

GLuint vao_circle;
GLuint vbo_circle_position;
GLuint vbo_circle_color;

GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;

//interface declaration

@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end


int main(int argc,char *argv[])
{

	//code

	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return 0;
}

//interface implementaions
@implementation AppDelegate
{
	@private
		NSWindow *window;
		GLView *glview;

		

}

-(void) applicationDidFinishLaunching: (NSNotification *)aNotification
{
	//code
	//window

	//log file code
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	if(gpFile==NULL)
	{
		printf("Error while creting file\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile,"\nFile OPened successfully\n");

	NSRect win_rect;

	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window
	window=[[NSWindow alloc] initWithContentRect: win_rect
												styleMask:NSWindowStyleMaskTitled|
														  NSWindowStyleMaskClosable|
														  NSWindowStyleMaskMiniaturizable|
														  NSWindowStyleMaskResizable backing:NSBackingStoreBuffered
														  defer:NO];

	[window setTitle: @"mac OS OPENGL Window"];
	[window center];
	glview=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glview];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];

}

-(void)applicationWillTerminate:(NSNotification *)Notification
{
	fprintf(gpFile,"\nProgram terminated successfully");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}
}

-(void)windowWillClose:(NSNotification *)Notification
{
	[self release];

	[NSApp terminate:self];
}

-(void)dealloc
{
	[glview release];
	[window release];
	[super dealloc];
}

@end


@implementation GLView
{
	CVDisplayLinkRef displayLink;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;



	vmath::mat4 perspectiveProjectionMatrix;

	GLuint mvpUniform;
	GLuint colorUniform;
	GLfloat verticesForOuterCircle[2000];
	GLfloat colorForCircle[2000];

	GLfloat verticesForInnerCircle[2000];

}

-(id)initWithFrame:(NSRect)frame
{
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,0
		};

		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "\nNo Valid OpenGL PixelFormat Is Available\n");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext* glContext=[[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];
		[self setOpenGLContext:glContext];

	}

	return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp*)pOutputTime
{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//opengl Info

	fprintf(gpFile, "\nOpenGL Version: %s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "\nGLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;

	GLfloat lengthOfASide = 0.0f;
	GLfloat lengthOfBSide = 0.0f;
	GLfloat lengthOfCSide = 0.0f;


	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//VERTEX SHADER Code
	//Create Vertexx Shader
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_uniform;" \
		"void main(void)" \
		"{" \
			"gl_Position=u_mvp_uniform*vPosition;" \
			"gl_PointSize=50.0;" \
			"out_color=vColor;" \
		"}";
	//specify the shader to vertex shader object
	glShaderSource(vertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Vertex Shader :%s ....",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nVertex shader compiled successfully");
	}


	//fragment shader

	//creating fragment shader
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code
	const GLchar *fragmentShaderSourceCode=
	{ "#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"fragColor=out_color;" \
		"}" \
	};

	//specifying shader source to fragment shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compiling shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	 szInfoLog=NULL;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in fragment Shader: %s ....",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\nFragement shader compiled successfully");
	}

	//Shader Program

	shaderProgramObject=glCreateProgram();

	//attching the sahder to object
	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);

	//pre-linking attrinbuts
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");

	glLinkProgram(shaderProgramObject);

	iInfoLogLength=0;
	GLint iShaderLinkStatus=0;
	szInfoLog=NULL;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus==GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(iInfoLogLength);
			if(szInfoLog!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile, "\nError in Linking Shader ....");
				free(szInfoLog);
				[self release];
				[NSApp terminate];
			}
		}
	}
	else
	{
		fprintf(gpFile, "\n Linking of shader successfull");
	}

	//post linking 
	mvpUniform=glGetUniformLocation(shaderProgramObject,"u_mvp_uniform");

	//vertices for triangle

	 //actual code for geometry
	
	//crating VAO for recording 
	//1.line
    glGenVertexArrays(1,&vao_line);
    glBindVertexArray(vao_line);
        glGenBuffers(1,&vbo_line_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
            glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);


	
	//half circle
	
	GLfloat XCenterOfCircle = 0.0f;
	GLfloat YCenterOfCircle = 0.0f;
	GLfloat radius = 1.0f;

	memset(verticesForInnerCircle,0,sizeof(verticesForInnerCircle));
	memset(colorForCircle,0,sizeof(colorForCircle));
	calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);

	//circle
	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);
		glGenBuffers(1, &vbo_circle_position);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(verticesForInnerCircle), verticesForInnerCircle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &vbo_circle_color);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, sizeof(colorForCircle), colorForCircle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);




	//geometry end here
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    perspectiveProjectionMatrix= mat4::identity();

	//display shi link for refresh rate

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext] CGLContextObj];

	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

	CVDisplayLinkStart(displayLink);

}

void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
	GLfloat x = 0.0f, y = 0.0f;
	GLfloat RColor = 0.0706f;
	GLfloat GColor = 0.3831f;
	GLfloat BColor = 0.02745f;
	//vertices[2000];
	for (int i = 0; i < 2000-3; i=i+3)
	{
		GLfloat angle =(GLfloat)(M_PI*i / 2000);
		x =(GLfloat) (XPointOfCenter + radius * cos(angle));
		y =(GLfloat) (YPointOfCenter + radius * sin(angle));

		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = 0.0f; 

		if (RColor <= 1.0)
		{
			RColor += 0.002f;

		}
		if (GColor <= 0.6)
		{
			GColor += 0.002f;
		}
		if (BColor <= 0.2)
		{
			BColor += 0.002f;
		}
		color[i] = RColor;
		color[i + 1] = GColor;
		color[i + 2] = BColor;
	}

}


-(void)reshape
{

	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect=[self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);


	perspectiveProjectionMatrix=vmath::perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[self drawView];

}

-(void)drawView
{


	[[self openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    mat4 translationMatrix;
	
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	
    glUseProgram(shaderProgramObject);

        modelViewMatrix=mat4::identity();
        modelViewProjectionMatrix=mat4::identity();
        translationMatrix=mat4::identity();

		translationMatrix= translate(-2.50f, 0.0f, -6.0f);
        modelViewMatrix=modelViewMatrix*translationMatrix;

        modelViewProjectionMatrix=perspectiveProjectionMatrix* modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		glLineWidth(5.0f);
		GLfloat lineVerticesI[]={
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};
		GLfloat colorForLineI[] = {
			1.0f,0.6f,0.2f,
			0.0706f,0.3831f,0.02745f
		};
		drawLine(lineVerticesI,sizeof(lineVerticesI),colorForLineI,sizeof(colorForLineI),2);


		//N
		modelViewMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(-2.0f,0.0f,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);



		GLfloat lineVerticesN[] = {
			1.0f,1.0f,0.0f,
			1.0f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f

		};
		GLfloat colorForLineN[] = {
			1.0f,0.6f,0.2f,
			0.0706f,0.3831f,0.02745f,
			1.0f,0.6f,0.2f,
			0.0706f,0.3831f,0.02745f,
			1.0f,0.6f,0.2f,
			0.0706f,0.3831f,0.02745f
		};
		drawLine(lineVerticesN, sizeof(lineVerticesN), colorForLineN, sizeof(colorForLineN),4);

		//D

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(-0.50f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//circle
		//loadIdentity for circle
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(-0.50f, 0.0f,-6.0f);
		rotationMatrix = rotate(-90.0f,0.0f,0.0f,1.0f);
		
		modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		//for circle Translations

		drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

		//for I

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(1.0f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//A
		translationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(2.0f,0.0f,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		GLfloat verticesForA[] = {
			-0.5f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.5f,-1.0f,0.0f
		};

		GLfloat colorForA[] = {
			0.0706f,0.3831f,0.02745f,
			1.0f,0.6f,0.2f,
			0.0706f,0.3831f,0.02745f,
			1.0f,0.6f,0.2f,

		};

		drawLine(verticesForA,sizeof(verticesForA),colorForA,sizeof(colorForA),3);

		GLfloat triColorVertex[6];

		GLfloat X1ForLine = (verticesForA[0] + verticesForA[3] )/ 2;
		GLfloat Y1ForLine = (verticesForA[1] + verticesForA[4]) / 2;

		GLfloat X2ForLine = (verticesForA[3] + verticesForA[6] )/ 2;
		GLfloat Y2ForLine = (verticesForA[1] + verticesForA[4] )/ 2;
		
		GLfloat colorForTricolor[6];

		triColorVertex[0] = X1ForLine;
		triColorVertex[1] = Y1ForLine;
		triColorVertex[2] = 0.0f;

		triColorVertex[3] = X2ForLine;
		triColorVertex[4] = Y2ForLine;
		triColorVertex[5] = 0.0f;

		colorForTricolor[0] = 1.0f;
		colorForTricolor[1] = 0.6f;
		colorForTricolor[2] = 0.2f;

		colorForTricolor[3] = 1.0f;
		colorForTricolor[4] = 0.6f;
		colorForTricolor[5] = 0.2f;


		drawLine(triColorVertex,sizeof(triColorVertex), colorForTricolor,sizeof(colorForTricolor),2);

		triColorVertex[0] = X1ForLine-0.01f;
		triColorVertex[1] = Y1ForLine-0.04f;
		triColorVertex[2] = 0.0f;

		triColorVertex[3] = X2ForLine;
		triColorVertex[4] = Y2ForLine-0.04f;
		triColorVertex[5] = 0.0f;

		colorForTricolor[0] = 1.0f;
		colorForTricolor[1] = 1.0f;
		colorForTricolor[2] = 1.0f;

		colorForTricolor[3] = 1.0f;
		colorForTricolor[4] = 1.0f;
		colorForTricolor[5] = 1.0f;


		drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

		triColorVertex[0] = X1ForLine;
		triColorVertex[1] = Y1ForLine-0.07f;
		triColorVertex[2] = 0.0f;

		triColorVertex[3] = X2ForLine;
		triColorVertex[4] = Y2ForLine-0.07f;
		triColorVertex[5] = 0.0f;

		colorForTricolor[0] = 0.0706f;
		colorForTricolor[1] = 0.3831f;
		colorForTricolor[2] = 0.02745f;

		colorForTricolor[3] = 0.0706f;
		colorForTricolor[4] = 0.3831f;
		colorForTricolor[5] = 0.02745f;


		drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);
			
    glUseProgram(0);
		CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);

		[self update];
	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}
-(void) update
{
	
	
}
void drawCircle(GLfloat * vertices,int verticesSize,GLfloat *color,int colorSize)
{
	glBindVertexArray(vao_circle);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,verticesSize,(vertices),GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, colorSize,color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		glPointSize(3.0f);
		glDrawArrays(GL_POINTS,0,2000);
	glBindVertexArray(0);
}



void drawLine(GLfloat *vertices ,int sizeOfVertices, GLfloat * color, int sizeofColor, int numberOfPoints)
{
	glBindVertexArray(vao_line);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER,sizeOfVertices,vertices,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
		glBufferData(GL_ARRAY_BUFFER,sizeofColor,color,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
	glBindVertexArray(0);
}


-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent*)theEvent
{
	//code
	int key=(int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27:
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
		break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent*)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent* )theEvent
{
	
}

-(void)dealloc
{

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime ,CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{

	CVReturn result=[(GLView*) pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}


