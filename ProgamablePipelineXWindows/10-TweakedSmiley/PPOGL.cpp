#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include <memory.h>

#include<X11/Xutil.h>
#include <X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include<SOIL/SOIL.h>

#include"./../vmath.h"


using namespace std;
using namespace vmath;


int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;
GLXContext gGLXContext;

Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbIsFullscreen=false;

GLuint gShaderProgramObject;

int keyPressed=0;

enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_square;
GLuint vbo_position_square;
GLuint vbo_texture_square;

GLuint mvpUniform;
GLuint samplerUniform;

GLuint texture_square;

mat4 perspectiveProjectionMatrix;

int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void resize(int,int);
	void display(void);
	void ToggleFullscreen(void);
	void CreateWindow(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;
	bool bDone=false;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
						bDone=true;
						break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							

							break;
                        case '1':
                            keyPressed=1;
                            break;
                        case '2':
                            keyPressed=2;
                            break;
                        case '3':
                            keyPressed=3;
                            break;
                        case '4':
                            keyPressed=4;
                            break;
                        default: 
                            keyPressed=0;
                            break;

					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;    
					}
					break;
				case MotionNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		//update()
		display();
	}

	uninitialize();
	return 0;
}


void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestFBConfig;
	XVisualInfo *ptempXVisualInfo=NULL;

	int numberOfFBConfigs=0;


	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_STENCIL_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in gpDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	//retrive all the FBConfigdriver has
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&numberOfFBConfigs);
	printf("numberOfFBConfigs: %d\n",numberOfFBConfigs);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for(int i=0;i<numberOfFBConfigs;i++)
	{
		ptempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		//for each obtained FBConfig get temporary XVisualInfo its use to check capability of doing two following calls

		if(ptempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//get sample buffers from respective fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
		
			//get number of sample buffers from fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

			//more number of samples and samplebuffers more eligible fbConfig id
			if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNumberOfSamples )
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			if(worstFrameBufferConfig<0 ||!sampleBuffers ||samples<worstNumberOfSamples)
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}
		XFree(ptempXVisualInfo);
	}

	bestFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestFBConfig;

	printf("bestFrameBufferConfig: %d\n",bestFrameBufferConfig );
	printf("bestNumberOfSamples: %d\n",bestNumberOfSamples );
	printf("worstFrameBufferConfig: %d\n",worstFrameBufferConfig);
	printf("worstNumberOfSamples: %d\n",worstNumberOfSamples );

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestFBConfig);


	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error at XCreateWindow()\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My PP OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();

    bool LoadTextures(GLuint*,const char*);
	
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	GLenum status;

	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXCreateContextAttribsARB is not obtained\n");
		uninitialize();
		exit(0);
	}

	const int Attribs[7]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	if(!gGLXContext)
	{
		printf("Best gGLXContext is not available\n");
		const int Attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		
		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Context is not hardware context\n");
	}
	else
	{
		printf("context is hardware rendering context\n"); 
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	status=glewInit();
	if(status!=GLEW_OK)
	{
		printf("\n Error in glewInit()\n");
		uninitialize();
		exit(0);
	}

	//creating shader object
	gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//write source code of vertex shader obejct
	const GLchar *vertexShaderSourceCode={
		"#version 450 core " \
		"\n" \
		"in vec4 vPosition;" \
        " in vec2 vTexCoord;" \
        "out vec2 out_texCoord;" \
 		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{"  \
			"gl_Position=u_mvp_matrix* vPosition;" \
            "out_texCoord=vTexCoord;" \
		"}" \
	};

	//specify vertex shader object to source code
	glShaderSource(gVertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	//error checking for vertex shader
	GLint iShaderCompileStatus=0;
	GLint iInfoLogLength=0;
	GLchar* szInfoLog=NULL;

	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus=GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				 GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("Vertex shader compilation error = %s\n",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader 
	//create fragment shader object
	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//write shder source code
	const GLchar* fragmentShaderSourceCode=
	{
		"#version 450 core" \
		"\n " \
        "in vec2 out_texCoord;" \
		"out vec4 fragColor;" \
        "uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
			"fragColor= texture(u_sampler,out_texCoord);" \
		"}" \
	};

	//specify shader source code to object
	glShaderSource(gFragmentShaderObject,1,(GLchar**)&fragmentShaderSourceCode,NULL);

	//compile Shader
	glCompileShader(gFragmentShaderObject);

	//error checking for fragment shader
	iShaderCompileStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;

	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("Fragment shader compilation error: %s\n",szInfoLog );

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create shader progrma object
	gShaderProgramObject=glCreateProgram();

	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);


	//starts the prelinking code for attributes linkage gpu with cpu
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");

	//link the program
	glLinkProgram(gShaderProgramObject);

	
	//error checking for linking of program
	GLint iShaderProgramLinkStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
	if(iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
				printf("Error in linking the gShaderProgramObject:= %s\n",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

    //post linking for uniform
	mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");
    samplerUniform=glGetUniformLocation(gShaderProgramObject,"u_sampler");


	const GLfloat squareVertices[]={
		1.0f,1.0f,0.0f,
		-1.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
        1.0f,-1.0f,0.0f
	}; 
    // const GLfloat squareTexCoord[]={
    //     1.0f,1.0f,
    //     0.0f,1.0f,
    //     0.0f,0.0f,
    //     1.0f,0.0f
    // };

	glGenVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		glGenBuffers(1,&vbo_position_square);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_position_square);
		glBufferData(GL_ARRAY_BUFFER,sizeof(squareVertices),squareVertices,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER,0);

        //texture vbo creation and data attching
        glGenBuffers(1,&vbo_texture_square);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_square);
            glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,2,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);

	//projection matrix to load identity
	perspectiveProjectionMatrix=mat4::identity();


	glClearColor(0.0f,0.0f,0.0,1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

    glEnable(GL_TEXTURE_2D);
    LoadTextures(&texture_square,"Smiley.bmp");
	resize(giWindowWidth,giWindowHeight);
}

bool LoadTextures(GLuint *texture,const char* imagePath)
{
    bool bStatus;
    int imageWidth;
    int imageHeight;

    unsigned char* imageData=NULL;

    imageData=SOIL_load_image(imagePath,&imageWidth,&imageHeight,0,SOIL_LOAD_RGB);
    if(imageData==NULL)
    {
        bStatus=false;
        return bStatus;
    }
    else
    {
        bStatus=true;

        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glGenTextures(1,texture);
        glBindTexture(GL_TEXTURE_2D,*texture);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,imageWidth,imageHeight,0,GL_RGB,GL_UNSIGNED_BYTE,imageData);

        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,0);
        SOIL_free_image_data(imageData);
        return bStatus;
    }
    




}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

		mat4 modelViewMatrix;
		mat4 modelViewProjectionMatrix;
		mat4 translationMatrix;
        mat4 rotationMatrix;

		//initialize to identity
		modelViewMatrix=mat4::identity();
		modelViewProjectionMatrix=mat4::identity();
		translationMatrix=mat4::identity();
        rotationMatrix=mat4::identity();


		//do necessary transformation
		translationMatrix=translate(0.0f,0.0f,-3.0f);
		//do necessary transformation matrix multiplication
		modelViewMatrix=modelViewMatrix* translationMatrix;

        //do necessary rotation
        rotationMatrix=rotate(180.0f,1.0f,0.0f,0.0f);
        modelViewMatrix=modelViewMatrix*rotationMatrix;
        
		modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;

		//send matrices to necessary uniform 
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

        GLfloat texCoord[8];
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D,texture_square);
        glUniform1i(samplerUniform,0);
        
        //for GL_DYNAMIC_DRAW
        
        if(keyPressed==1)
        {
            texCoord[0]=0.5f;
            texCoord[1]=0.5f;
            texCoord[2]=0.0f;
            texCoord[3]=0.5f;
            texCoord[4]=0.0f;
            texCoord[5]=0.0f;
            texCoord[6]=0.5f;
            texCoord[7]=0.0f;
            
        }
        else if(keyPressed==2)
        {
            texCoord[0]=1.0f;
            texCoord[1]=1.0f;
            texCoord[2]=0.0f;
            texCoord[3]=1.0f;
            texCoord[4]=0.0f;
            texCoord[5]=0.0f;
            texCoord[6]=1.0f;
            texCoord[7]=0.0f;
        }
        else if(keyPressed==3)
        {
            texCoord[0]=2.0f;
            texCoord[1]=2.0f;
            texCoord[2]=0.0f;
            texCoord[3]=2.0f;
            texCoord[4]=0.0f;
            texCoord[5]=0.0f;
            texCoord[6]=2.0f;
            texCoord[7]=0.0f;
            
        }
        else if(keyPressed==4)
        {
            texCoord[0]=0.5f;
            texCoord[1]=0.5f;
            texCoord[2]=0.5f;
            texCoord[3]=0.5f;
            texCoord[4]=0.5f;
            texCoord[5]=0.5f;
            texCoord[6]=0.5f;
            texCoord[7]=0.5f;
            
        }
        else
        {
            texCoord[0]=0.5f;
            texCoord[1]=0.5f;
            texCoord[2]=0.5f;
            texCoord[3]=0.5f;
            texCoord[4]=0.5f;
            texCoord[5]=0.5f;
            texCoord[6]=0.5f;
            texCoord[7]=0.5f;
        }
        

		//bind with recoreder
		glBindVertexArray(vao_square);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_texture_square);
                glBufferData(GL_ARRAY_BUFFER,sizeof(texCoord),texCoord,GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER,0);
			
            glDrawArrays(GL_TRIANGLE_FAN,0,4);
            glBindTexture(GL_TEXTURE_2D,0);
		glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
	
}

void uninitialize()
{
	if(vbo_position_square)
	{
		glDeleteBuffers(1,&vbo_position_square);
		vbo_position_square=0;
	}
    if(vbo_texture_square)
	{
		glDeleteBuffers(1,&vbo_texture_square);
		vbo_texture_square=0;
	}
	if(vao_square)
	{
		glDeleteVertexArrays(1,&vao_square);
		vao_square=0;
	}

	GLsizei shaderCount;
	GLsizei shaderNumber;
	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		GLuint* pShaders=(GLuint*)malloc(shaderCount*sizeof(GLuint));
		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&shaderCount,pShaders);
			for(shaderNumber=0;shaderNumber<shaderCount;shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber]=0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;
		glUseProgram(0);
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();
	if(!currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	/*if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}*/

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
