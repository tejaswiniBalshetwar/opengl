#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include <memory.h>

#include<X11/Xutil.h>
#include <X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include"./../vmath.h"


using namespace std;
using namespace vmath;


int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;
GLXContext gGLXContext;

Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbIsFullscreen=false;

GLuint gShaderProgramObject;

struct vertex
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;

GLuint vao_circle=0;
GLuint vbo_circle_position;
GLuint vbo_circle_color;

GLuint vao_square = 0;
GLuint vbo_square_position = 0;
GLuint vbo_square_color = 0;

GLfloat colorForCircle[4000];
GLfloat verticesForInnerCircle[4000];


//dynamic india translations
GLfloat translateIInX = -4.0f;

GLfloat translateNInY = 4.0f;

GLfloat translateD = 1.0f;

GLfloat translateI2InY = -4.0f;

GLfloat translateAInX = 5.0f;

bool ShowPlanes = false;
bool ShowTricolor = false;
GLfloat incAngle = 270.0f;
GLfloat upperPlaneAngle = M_PI;
GLfloat upperPlaneStopAngle = (3.0f*M_PI) / 2.0f;

GLfloat lowerPlaneAngle = M_PI;
GLfloat decAngle = 90.0f;

GLfloat incAngle2 = 0.0f;
GLfloat upperPlaneAngle2 = (3.0f*M_PI) / 2.0f;
GLfloat upperPlaneStopAngle2 = 2.5f*M_PI;

GLfloat lowerPlaneAngle2 = (M_PI / 2.0f);
GLfloat decAngle2 = 360.0f;

GLfloat translateAllInXDirection = -2.499705f;

bool meet = false;
bool crossTheA = false;
GLfloat translateMiddle = -7.1f;

GLfloat XTranslatePointUP = 0.0f;
GLfloat YTranslatePointUP = 0.0f;

GLfloat XTranslatePointDown = 0.0f;
GLfloat YTranslatePointDown = 0.0f;

//for plane

vertex positions[39];

GLuint vao_triangle = 0;
GLuint vbo_triangle_position = 0;
GLuint vbo_triangle_color = 0;

GLuint mvpUniform;

mat4 perspectiveProjectionMatrix;

int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void resize(int,int);
	void display(void);
	void ToggleFullscreen(void);
	void CreateWindow(void);
	void update();
	void PlaneTranslations();

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;
	bool bDone=false;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
						bDone=true;
						break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							

							break;


					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;    
					}
					break;
				case MotionNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		if (ShowPlanes)
		{
			PlaneTranslations();
		}
		display();
	}

	uninitialize();
	return 0;
}


void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestFBConfig;
	XVisualInfo *ptempXVisualInfo=NULL;

	int numberOfFBConfigs=0;


	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_STENCIL_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in gpDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	//retrive all the FBConfigdriver has
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&numberOfFBConfigs);
	printf("numberOfFBConfigs: %d\n",numberOfFBConfigs);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for(int i=0;i<numberOfFBConfigs;i++)
	{
		ptempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		//for each obtained FBConfig get temporary XVisualInfo its use to check capability of doing two following calls

		if(ptempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//get sample buffers from respective fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
		
			//get number of sample buffers from fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

			//more number of samples and samplebuffers more eligible fbConfig id
			if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNumberOfSamples )
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			if(worstFrameBufferConfig<0 ||!sampleBuffers ||samples<worstNumberOfSamples)
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}
		XFree(ptempXVisualInfo);
	}

	bestFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestFBConfig;

	printf("bestFrameBufferConfig: %d\n",bestFrameBufferConfig );
	printf("bestNumberOfSamples: %d\n",bestNumberOfSamples );
	printf("worstFrameBufferConfig: %d\n",worstFrameBufferConfig);
	printf("worstNumberOfSamples: %d\n",worstNumberOfSamples );

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestFBConfig);


	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error at XCreateWindow()\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My PP OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();
	void createPlane();
  	void calculateVerticesForCircle(GLfloat, GLfloat, GLfloat, GLfloat*,GLfloat*);
	
	
	
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	GLenum status;

	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXCreateContextAttribsARB is not obtained\n");
		uninitialize();
		exit(0);
	}

	const int Attribs[7]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	if(!gGLXContext)
	{
		printf("Best gGLXContext is not available\n");
		const int Attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		
		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Context is not hardware context\n");
	}
	else
	{
		printf("context is hardware rendering context\n"); 
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	status=glewInit();
	if(status!=GLEW_OK)
	{
		printf("\n Error in glewInit()\n");
		uninitialize();
		exit(0);
	}

	//creating shader object
	gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//write source code of vertex shader obejct
	const GLchar *vertexShaderSourceCode={
		"#version 450 core " \
		"\n" \
		"in vec4 vPosition;" \
        "in vec4 vColor;"   \
		"uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;"
		"void main(void)" \
		"{"  \
			"gl_Position=u_mvp_matrix* vPosition;" \
            "out_color=vColor;" \
        "}" \
	};

	//specify vertex shader object to source code
	glShaderSource(gVertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	//error checking for vertex shader
	GLint iShaderCompileStatus=0;
	GLint iInfoLogLength=0;
	GLchar* szInfoLog=NULL;

	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus=GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				 GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("Vertex shader compilation error = %s\n",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader 
	//create fragment shader object
	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//write shder source code
	const GLchar* fragmentShaderSourceCode=
	{
		"#version 450 core" \
		"\n " \
		"out vec4 fragColor;" \
        "in vec4 out_color;" \
		"void main(void)" \
		"{" \
			"fragColor= vec4(out_color);"
		"}" \
	};

	//specify shader source code to object
	glShaderSource(gFragmentShaderObject,1,(GLchar**)&fragmentShaderSourceCode,NULL);

	//compile Shader
	glCompileShader(gFragmentShaderObject);

	//error checking for fragment shader
	iShaderCompileStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;

	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("Fragment shader compilation error: %s\n",szInfoLog );

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create shader progrma object
	gShaderProgramObject=glCreateProgram();

	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);


	//starts the prelinking code for attributes linkage gpu with cpu
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");
	//link the program
	glLinkProgram(gShaderProgramObject);

	
	//error checking for linking of program
	GLint iShaderProgramLinkStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
	if(iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
				printf("Error in linking the gShaderProgramObject:= %s\n",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

    //post linking for uniform
	mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    
    
	glGenVertexArrays(1,&vao_line);
	glBindVertexArray(vao_line);
		glGenBuffers(1,&vbo_line_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER,2*3*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,4*2*sizeof(GLfloat),NULL,GL_DYNAMIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);


	//half circle
	GLfloat XCenterOfCircle =0.0f;
    GLfloat YCenterOfCircle = 0.0f;
    GLfloat radius = 1.0f;

	calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);


    glGenVertexArrays(1,&vao_circle);
		glBindVertexArray(vao_circle);
		glGenBuffers(1,&vbo_circle_position);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForInnerCircle),verticesForInnerCircle,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_circle_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(colorForCircle),colorForCircle,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);


    //square
	GLfloat square_vertices[] = {
		1.0f,1.20f,0.0f,
		-1.0f,1.20f,0.0f,
		-1.0f,-1.20f,0.0f,
		1.0f,-1.20f,0.0f
	};

	GLfloat square_color[] = {
		0.0f,0.0f,0.0f,0.0f,
		0.0f,0.0f,0.0f,0.0f,
		0.0f,0.0f,0.0f,0.0f,
		0.0f,0.0f,0.0f,0.0f,
	};

	glGenVertexArrays(1,&vao_square);
	glBindVertexArray(vao_square);
		glGenBuffers(1,&vbo_square_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(square_vertices),square_vertices,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_square_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(square_color),square_color,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,4,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	

	createPlane();
	glGenVertexArrays(1,&vao_triangle);
	glBindVertexArray(vao_triangle);
		glGenBuffers(1,&vbo_triangle_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_triangle_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(positions),positions,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glVertexAttrib4f(AMC_ATTRIBUTE_COLOR, 0.0729f, 0.886f, 0.9333f, 1.0f);
	glBindVertexArray(0);

	//projection matrix to load identity
	perspectiveProjectionMatrix=mat4::identity();

	glClearColor(0.0f,0.0f,0.0,1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

    glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


	resize(giWindowWidth,giWindowHeight);
}
void createPlane()
{
	
	positions[0].x = 1.0f;
	positions[0].y = 0.0f;
	positions[0].z = 0.0f;

	positions[1].x = 0.0f;
	positions[1].y = -0.35f;
	positions[1].z = 0.0f;

	positions[2].x = 0.0f;
	positions[2].y = 0.35f;
	positions[2].z = 0.0f;

	//body
	positions[3].x = 0.0f;
	positions[3].y = 0.35f;
	positions[3].z = 0.0f;

	positions[4].x = -1.0f;
	positions[4].y = 0.35f;
	positions[4].z = 0.0f;

	positions[5].x = -1.0f;
	positions[5].y = -0.35f;
	positions[5].z = 0.0f;

	positions[6].x = -1.0f;
	positions[6].y = -0.35f;
	positions[6].z = 0.0f;

	positions[7].x = 0.0f;
	positions[7].y = -0.35f;
	positions[7].z = 0.0f;

	positions[8].x = 0.0f;
	positions[8].y = 0.35f;
	positions[8].z = 0.0f;
	
	//polygon start
	positions[9].x = -1.0f;
	positions[9].y = -0.35f;
	positions[9].z = 0.0f;

	positions[10].x = -2.0f;
	positions[10].y = -1.75f;
	positions[10].z = 0.0f;
	
	positions[11].x = -2.5f;
	positions[11].y = -1.75f;
	positions[11].z = 0.0f;

	positions[12].x = -2.5f;
	positions[12].y = -1.75f;
	positions[12].z = 0.0f;

	positions[13].x = -1.8f;
	positions[13].y = -0.35f;
	positions[13].z = 0.0f;

	positions[14].x = -1.0f;
	positions[14].y = -0.35f;
	positions[14].z = 0.0f;

	
	//upper start half
	positions[15].x = -1.0f;
	positions[15].y = 0.35f;
	positions[15].z = 0.0f;

	positions[16].x = -2.0f;
	positions[16].y = 1.75f;
	positions[16].z = 0.0f;

	positions[17].x = -2.5f;
	positions[17].y = 1.75f;
	positions[17].z = 0.0f;

	positions[18].x = -2.5f;
	positions[18].y = 1.75f;
	positions[18].z = 0.0f;

	positions[19].x = -1.8f;
	positions[19].y = 0.35f;
	positions[19].z = 0.0f;

	positions[20].x = -1.0f;
	positions[20].y = 0.35f;
	positions[20].z = 0.0f;
	
	//quad
	positions[21].x = -1.35f;
	positions[21].y = 0.35f;
	positions[21].z = 0.0f;

	positions[22].x = -3.0f;
	positions[22].y = 0.35f;
	positions[22].z = 0.0f;

	positions[23].x = -3.0f;
	positions[23].y = -0.35f;
	positions[23].z = 0.0f;

	positions[24].x = -3.0f;
	positions[24].y = -0.35f;
	positions[24].z = 0.0f;

	positions[25].x = -1.0f;
	positions[25].y = -0.35f;
	positions[25].z = 0.0f;

	positions[26].x = -1.0f;
	positions[26].y =0.35f;
	positions[26].z = 0.0f;
	
	
	//lower part of planes last
	positions[27].x = -3.0f;
	positions[27].y = -0.35f;
	positions[27].z = 0.0f;

	positions[28].x = -3.5f;
	positions[28].y = -1.0f;
	positions[28].z = 0.0f;

	positions[29].x = -3.90f;
	positions[29].y = -1.0f;
	positions[29].z = 0.0f;
	
	positions[30].x = -3.90f;
	positions[30].y = -1.0f;
	positions[30].z = 0.0f;

	positions[31].x = -3.0f;
	positions[31].y = 0.35f;
	positions[31].z = 0.0f;

	positions[32].x = -3.0f;
	positions[32].y = -0.35f;
	positions[32].z = 0.0f;
	
	//plane upper part of last side
	positions[33].x = -3.0f;
	positions[33].y = 0.35f;
	positions[33].z = 0.0f;

	positions[34].x = -3.5f;
	positions[34].y = 1.0f;
	positions[34].z = 0.0f;
	
	positions[35].x = -3.90f;
	positions[35].y = 1.0f;
	positions[35].z = 0.0f;
	
	positions[36].x = -3.90f;
	positions[36].y = 1.0f;
	positions[36].z = 0.0f;

	positions[37].x = -3.0f;
	positions[37].y = -0.35f;
	positions[37].z = 0.0f;

	positions[38].x = -3.0f;
	positions[38].y = -0.35f;
	positions[38].z = 0.0f;
	
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
	GLfloat x = 0.0f, y = 0.0f;
	GLfloat RColor = 0.0706f;
	GLfloat GColor = 0.3831f;
	GLfloat BColor = 0.02745f;
	//vertices[2000];
	for (int i = 0; i < 2000-3; i=i+3)
	{
		GLfloat angle =GLfloat(((M_PI)*i) / 2000);
		x =GLfloat (XPointOfCenter + radius * cos(angle));
		y =GLfloat (YPointOfCenter + radius * sin(angle));

		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = 0.0f; 
	}


	for (int i = 0; i < 2000-3; i=i+3)
	{
	
		if (RColor <= 1.0)
		{
			RColor += 0.002f;

		}
		if (GColor <= 0.6)
		{
			GColor += 0.002f;
		}
		if (BColor <= 0.2)
		{
			BColor += 0.002f;
		}
		color[i] = RColor;
		color[i + 1] = GColor;
		color[i + 2] = BColor;
		//color[i + 3] = 1.0f;
	}

}



void display()
{
    void drawLine(GLfloat*,int,GLfloat*,int,int);
	void drawTriColor(GLfloat*);
	void drawPlane();
	void drawUpperPlane(void);
	void drawLowerPlane(void);
	void drawMiddelePlane(void);
	
	void drawCircle(GLfloat *,int,GLfloat*,int);
   
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

		mat4 modelViewMatrix;
		mat4 modelViewProjectionMatrix;
		mat4 translationMatrix;
		mat4 rotationMatrix;

		//initialize to identity
		modelViewMatrix=mat4::identity();
		modelViewProjectionMatrix=mat4::identity();
		translationMatrix=mat4::identity();

		//do necessary transformation
		translationMatrix= translate(translateIInX, 0.0f, -6.0f);
        //do necessary transformation matrix multiplication
		modelViewMatrix=modelViewMatrix* translationMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;

		//send matrices to necessary uniform 
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		//bind with recoreder related code
		glLineWidth(5.0f);
		GLfloat lineVerticesI[]={
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};
		GLfloat colorForLineI[] = {
			1.0f,0.6f,0.2f,1.0,
			0.0706f,0.3831f,0.02745f,1.0
		};
		drawLine(lineVerticesI,sizeof(lineVerticesI),colorForLineI,sizeof(colorForLineI),2);


        //N
		modelViewMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(-2.0f,translateNInY,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);



		GLfloat lineVerticesN[] = {
			1.0f,1.0f,0.0f,
			1.0f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f

		};
		GLfloat colorForLineN[] = {
			1.0f,0.6f,0.2f,1.0,
			0.0706f,0.3831f,0.02745f,1.0,
			1.0f,0.6f,0.2f,1.0,
			0.0706f,0.3831f,0.02745f,1.0,
			1.0f,0.6f,0.2f,1.0,
			0.0706f,0.3831f,0.02745f,1.0,
		};
		drawLine(lineVerticesN, sizeof(lineVerticesN), colorForLineN, sizeof(colorForLineN),4);


        //D

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(-0.50f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//circle
		//loadIdentity for circle
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(-0.50f, 0.0f,-6.0f);
		rotationMatrix = rotate(-90.0f,0.0f,0.0f,1.0f);
		
		modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		//for circle Translations

		drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));

        	//for blendEffect
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		//for circle Translations 
		translationMatrix = translate(0.10f, 0.0f, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		 GLfloat colorForSquare[16];
		colorForSquare[0] = 0.0f;
		colorForSquare[1] = 0.0f;
		colorForSquare[2] = 0.0f;
		colorForSquare[3] = translateD;
		
		colorForSquare[4] = 0.0f;
		colorForSquare[5] = 0.0f;
		colorForSquare[6] = 0.0f;
		colorForSquare[7] = translateD;
		
		colorForSquare[8] = 0.0f;
		colorForSquare[9] = 0.0f;
		colorForSquare[10] = 0.0f;
		colorForSquare[11] = translateD;
		
		colorForSquare[12] = 0.0f;
		colorForSquare[13] = 0.0f;
		colorForSquare[14] = 0.0f;
		colorForSquare[15] = translateD;


		glBindVertexArray(vao_square);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_square_color);
				glBufferData(GL_ARRAY_BUFFER,sizeof(colorForSquare),colorForSquare,GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER,0);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
		glBindVertexArray(0);



		//for I

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		translationMatrix = translate(1.0f, translateI2InY, -6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	
		drawLine(lineVerticesI, sizeof(lineVerticesI), colorForLineI, sizeof(colorForLineI), 2);

		//A
		translationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(translateAInX,0.0f,-6.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		GLfloat verticesForA[] = {
			-0.5f,-1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.5f,-1.0f,0.0f
		};

		GLfloat colorForA[] = {
			0.0706f,0.3831f,0.02745f,1.0,
			1.0f,0.6f,0.2f,1.0,
			0.0706f,0.3831f,0.02745f,1.0,
			1.0f,0.6f,0.2f,1.0,

		};

		drawLine(verticesForA,sizeof(verticesForA),colorForA,sizeof(colorForA),3);
        if (ShowTricolor)
		{
			drawTriColor(verticesForA);
		}
			
		

		if (ShowPlanes)
		{
			//upperplane
			drawUpperPlane();

			//Lower Plane
			drawLowerPlane();

			//Middle Plane
			drawMiddelePlane();

		}     
		
	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void drawUpperPlane(void)
{
	void drawPlane(void);
	mat4 translationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
	modelViewMatrix= mat4::identity();
	modelViewProjectionMatrix=mat4::identity();
	translationMatrix=mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glLineWidth(8.0f);
	if (meet == false)
	{
		XTranslatePointUP = -1.20f + 4.60f*cos(upperPlaneAngle);
		YTranslatePointUP = 4.6f + 4.60f*sin(upperPlaneAngle);
		//fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointUP,YTranslatePointUP);
		translationMatrix = translate(XTranslatePointUP, YTranslatePointUP, -6.0f);
		//glTranslatef(XTranslatePointUP, YTranslatePointUP, -10.0f);
		if (incAngle <= 360.0f || incAngle > 0.0f)
		{
			rotationMatrix = rotate(incAngle,0.0f,0.0f,1.0f);
			//glRotatef(incAngle, 0.0f, 0.0f, 1.0f);
		}
	}
	else if (translateAllInXDirection > 3.0f)
	{

		XTranslatePointUP = 3.0f + 4.60f*cos(upperPlaneAngle2);
		YTranslatePointUP = 4.60f + 4.60f*sin(upperPlaneAngle2);
		//fprintf(gpFile, "XTranslatePoint2:%f  YTranslatePoint2%f\n",XTranslatePointUP,YTranslatePointUP);
		
		//glTranslatef(XTranslatePointUP, YTranslatePointUP, -10.0f);
		translationMatrix = translate(XTranslatePointUP, YTranslatePointUP, -6.0f);
		if (incAngle2 <= 90.0f || incAngle2 > 0.0f)
		{

			//glRotatef(incAngle2, 0.0f, 0.0f, 1.0f);
			rotationMatrix = rotate(incAngle2, 0.0f, 0.0f, 1.0f);
		}
	}
	//else
	//{
	//	fprintf(gpFile, "translateAllInXDirection %f\n", translateAllInXDirection);
	//	translationMatrix = translate(translateAllInXDirection, 0.0f, -6.0f);
	//	//glTranslatef(translateAllInXDirection, 0.0f, -10.0f);
	//}

	

	scaleMatrix = scale(0.350f, 0.350f, 0.350f);
	modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	drawPlane();


}

void drawLowerPlane(void)
{
	void drawPlane(void);
	mat4 translationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
	
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();


	if (meet == false)
	{
		XTranslatePointDown = -1.2f + 4.60f*cos(lowerPlaneAngle);
		YTranslatePointDown = -4.6f + 4.60f*sin(lowerPlaneAngle);
		//fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
		//glTranslatef(XTranslatePointDown, YTranslatePointDown, -10.0f);
		translationMatrix = translate(XTranslatePointDown, YTranslatePointDown, -6.0f);
		if (decAngle <= 90.0f || decAngle > 0.0f)
		{
			rotationMatrix = rotate(decAngle, 0.0f, 0.0f, 1.0f);
			//glRotatef(decAngle, 0.0f, 0.0f, 1.0f);
		}
	}

	else if (translateAllInXDirection > 3.0f)
	{
		XTranslatePointDown = 3.0f + 4.60f*cos(lowerPlaneAngle2);
		YTranslatePointDown = -4.60f + 4.60f*sin(lowerPlaneAngle2);
		//fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
		translationMatrix = translate(XTranslatePointDown, YTranslatePointDown, -6.0f);
		glTranslatef(XTranslatePointDown, YTranslatePointDown, -10.0f);
		if (decAngle2 <= 360.0f || decAngle2 > 270.0f)
		{
			rotationMatrix = rotate(decAngle2, 0.0f, 0.0f, 1.0f);
			//glRotatef(decAngle2, 0.0f, 0.0f, 1.0f);
		}
	}
	else
	{
		//glTranslatef(translateAllInXDirection, 0.0f, -10.0f);
		translationMatrix = translate(translateAllInXDirection, 0.0f, -6.0f);
	}


	scaleMatrix = scale(0.350f, 0.350f, 0.350f);
	modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	drawPlane();

}

void drawMiddelePlane(void)
{
	void drawPlane(void);
	mat4 translationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	//glTranslatef(translateMiddle, 0.0f, -10.0f);
	translationMatrix = translate(translateMiddle, 0.0f, -6.0f);
	
	scaleMatrix = scale(0.350f, 0.350f, 0.350f);
	modelViewMatrix = modelViewMatrix * translationMatrix *scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	drawPlane();
}

void drawPlane()
{
	void drawLine(GLfloat*,int,GLfloat*,int,int);


	glBindVertexArray(vao_triangle);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawArrays(GL_TRIANGLE_FAN, 3, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 9, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 15, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 21, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 27, 6);
	glDrawArrays(GL_TRIANGLE_FAN, 33, 6);
	glBindVertexArray(0);

	GLfloat linePositions[] = {
		-3.3f,0.12f,0.0f,
		-5.0f,0.12f,0.0f,
		-5.0f,0.04f,0.0f,
		-3.3f,0.04f,0.0f,
		-3.3f,-0.04f,0.0f,
		-5.0f,-0.04f,0.0f
	};

	GLfloat color[] = {
		1.0f,0.6f,0.2f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f,
		1.0f,1.0f,1.0f,1.0f,
		0.0706f,0.3831f,0.02745f,1.0f,
		0.0f,0.0f,0.0f,1.0f
	};
	drawLine(linePositions, sizeof(linePositions), color, sizeof(color), 6);
}
void drawTriColor(GLfloat* verticesForA)
{
	void drawLine(GLfloat*, int, GLfloat*, int, int);

	GLfloat triColorVertex[6];

	GLfloat X1ForLine = (verticesForA[0] + verticesForA[3]) / 2;
	GLfloat Y1ForLine = (verticesForA[1] + verticesForA[4]) / 2;

	GLfloat X2ForLine = (verticesForA[3] + verticesForA[6]) / 2;
	GLfloat Y2ForLine = (verticesForA[1] + verticesForA[4]) / 2;

	GLfloat colorForTricolor[8];

	triColorVertex[0] = X1ForLine;
	triColorVertex[1] = Y1ForLine;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 1.0f;
	colorForTricolor[1] = 0.6f;
	colorForTricolor[2] = 0.2f;
	colorForTricolor[3] = 1.0f;

	colorForTricolor[4] = 1.0f;
	colorForTricolor[5] = 0.6f;
	colorForTricolor[6] = 0.2f;
	colorForTricolor[7] = 1.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

	triColorVertex[0] = X1ForLine - 0.01f;
	triColorVertex[1] = Y1ForLine - 0.04f;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine - 0.04f;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 1.0f;
	colorForTricolor[1] = 1.0f;
	colorForTricolor[2] = 1.0f;
	colorForTricolor[3] = 1.0f;

	colorForTricolor[4] = 1.0f;
	colorForTricolor[5] = 1.0f;
	colorForTricolor[6] = 1.0f;
	colorForTricolor[7] = 1.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);

	triColorVertex[0] = X1ForLine;
	triColorVertex[1] = Y1ForLine - 0.07f;
	triColorVertex[2] = 0.0f;

	triColorVertex[3] = X2ForLine;
	triColorVertex[4] = Y2ForLine - 0.07f;
	triColorVertex[5] = 0.0f;

	colorForTricolor[0] = 0.0706f;
	colorForTricolor[1] = 0.3831f;
	colorForTricolor[2] = 0.02745f;
	colorForTricolor[3] = 1.0f;

	colorForTricolor[4] = 0.0706f;
	colorForTricolor[5] = 0.3831f;
	colorForTricolor[6] = 0.02745f;
	colorForTricolor[7] = 1.0f;


	drawLine(triColorVertex, sizeof(triColorVertex), colorForTricolor, sizeof(colorForTricolor), 2);
}


void drawCircle(GLfloat * vertices,int verticesSize,GLfloat *color,int colorSize)
{
	glBindVertexArray(vao_circle);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,verticesSize,vertices,GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, colorSize, color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		glPointSize(3.0f);
		glDrawArrays(GL_POINTS,0,2000);
	glBindVertexArray(0);
}

void drawLine(GLfloat *vertices ,int sizeOfVertices, GLfloat * color, int sizeofColor, int numberOfPoints)
{
	glBindVertexArray(vao_line);
	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER,sizeOfVertices,vertices,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
		glBufferData(GL_ARRAY_BUFFER,sizeofColor,color,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
	glBindVertexArray(0);
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
	
}

void uninitialize()
{
	if(vbo_line_position)
	{
		glDeleteBuffers(1,&vbo_line_position);
		vbo_line_position=0;
	}

	if(vbo_line_color)
	{
		glDeleteBuffers(1,&vbo_line_color);
		vbo_line_color=0;
	}
	if(vao_line)
	{
		glDeleteVertexArrays(1,&vao_line);
		vao_line=0;
	}

   
    if(vbo_circle_position)
	{
		glDeleteBuffers(1,&vbo_circle_position);
		vbo_circle_position=0;
	}

	if(vbo_circle_color)
	{
		glDeleteBuffers(1,&vbo_circle_color);
		vbo_circle_color=0;
	}
	if(vao_circle)
	{
		glDeleteVertexArrays(1,&vao_circle);
		vao_circle=0;
	}


	GLsizei shaderCount;
	GLsizei shaderNumber;
	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		GLuint* pShaders=(GLuint*)malloc(shaderCount*sizeof(GLuint));
		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&shaderCount,pShaders);
			for(shaderNumber=0;shaderNumber<shaderCount;shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber]=0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;
		glUseProgram(0);
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();
	if(!currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void update()
{
	if (translateIInX <= -2.5f)
	{
		translateIInX = translateIInX + 0.005;
	}

	if (translateIInX >= -2.500609f && translateAInX >= 2.0f)
	{
		translateAInX = translateAInX - 0.005f;
	}

	if (translateIInX >= -2.500609f && translateAInX <= 2.0f &&  translateNInY >= 0.0f)
	{
		translateNInY = translateNInY - 0.005f;
	}

	if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY <= 0.0f)
	{
		translateI2InY = translateI2InY + 0.005f;
	}

	if (translateIInX >= -2.500609f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateI2InY >= 0.0f &&translateD >=0.0f)
	{
		translateD = translateD - 0.005f;
	}
	if (translateI2InY >= 0.0f && translateNInY <= 0.0f && translateAInX <= 2.0f && translateIInX >= -2.5f &&translateD <= 0.0f)
	{
		ShowPlanes = true;
	}

	if (XTranslatePointUP >= 3.499705 && XTranslatePointDown >= 3.499705 && translateMiddle >= 3.499705)
	{
		ShowTricolor = true;
	}
}

void PlaneTranslations(void)
{
	if (upperPlaneAngle < upperPlaneStopAngle)
	{
		upperPlaneAngle = upperPlaneAngle + 0.002f;
		// upperPlaneAngle=0.0f;
	}
	if (incAngle <= 360.0f)
	{
		incAngle = incAngle + 0.13f;
		// incAngle=0.0f;
	}


	//lower Plane
	if (lowerPlaneAngle > (M_PI / 2))
	{
		lowerPlaneAngle = lowerPlaneAngle - 0.002;
	}
	if (decAngle >= 0.0f)
	{
		decAngle = decAngle - 0.13f;
		// incAngle=0.0f;
	}

	if (translateMiddle <= -2.494244f)
	{
		translateMiddle = translateMiddle + 0.0072f;
	}


	if (translateAllInXDirection <= 3.0f && meet == true)
	{
		translateAllInXDirection = translateAllInXDirection + 0.005f;
		translateMiddle = translateAllInXDirection;
	}


	/**********************************************cheking the meet condition********************************/
	if (XTranslatePointUP >= -2.494244f && XTranslatePointDown >= -2.494244f && translateMiddle >= -2.494244f)
	{
		meet = true;
	}



	/*******************************----------------2Nd Part---------------------*****************************/

	if (translateAllInXDirection >= 3.0f && meet == true && translateMiddle < 11.50f)
	{
		translateMiddle = translateMiddle + 0.008f;

	}
	if (upperPlaneAngle2 < upperPlaneStopAngle2 && translateAllInXDirection >= 3.0f)
	{
		upperPlaneAngle2 = upperPlaneAngle2 + 0.002f;

	}
	if (incAngle2 <= 90.0f && translateAllInXDirection >= 3.0f)
	{
		incAngle2 = incAngle2 + 0.013f;

	}

	if (lowerPlaneAngle2 > -0.5f && translateAllInXDirection >= 3.0f)
	{
		lowerPlaneAngle2 = lowerPlaneAngle2 - 0.002f;

	}
	if (decAngle2 >= 270.0f && translateAllInXDirection >= 2.50f)
	{
		decAngle2 = decAngle2 - 0.013f;

	}
}