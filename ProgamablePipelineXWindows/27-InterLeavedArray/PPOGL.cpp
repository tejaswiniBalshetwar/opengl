#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include <memory.h>

#include<X11/Xutil.h>
#include <X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include<SOIL/SOIL.h>

#include"./../vmath.h"


using namespace std;
using namespace vmath;


int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;
GLXContext gGLXContext;

Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbIsFullscreen=false;


GLfloat angleCube = 0.0f;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject = 0;

GLuint vao_cube = 0;
GLuint vbo_cube = 0;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint LKeyIsPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShininessUniform;


GLuint samplerUniform;
GLuint texture_marble;

bool bLight = false;
bool bRotation = false;


float light_ambient[] = { 0.250f,0.250f,0.250f,0.250f };
float light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[] = { 10.0f,10.0f,10.0f,1.0f };

float material_ambient[] = { 0.250f,0.250f,0.250f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;

mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void resize(int,int);
	void display(void);
	void ToggleFullscreen(void);
	void CreateWindow(void);
	void update();
	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;
	bool bDone=false;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
						bDone=true;
						break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							

							break;
						case 'l':
						case 'L':
							if(bLight==false)
							{
								bLight=true;
							}
							else
							{
								bLight=false;
							}
							break;


					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;    
					}
					break;
				case MotionNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return 0;
}


void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestFBConfig;
	XVisualInfo *ptempXVisualInfo=NULL;

	int numberOfFBConfigs=0;


	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_STENCIL_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in gpDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	//retrive all the FBConfigdriver has
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&numberOfFBConfigs);
	printf("numberOfFBConfigs: %d\n",numberOfFBConfigs);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for(int i=0;i<numberOfFBConfigs;i++)
	{
		ptempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		//for each obtained FBConfig get temporary XVisualInfo its use to check capability of doing two following calls

		if(ptempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//get sample buffers from respective fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
		
			//get number of sample buffers from fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

			//more number of samples and samplebuffers more eligible fbConfig id
			if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNumberOfSamples )
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			if(worstFrameBufferConfig<0 ||!sampleBuffers ||samples<worstNumberOfSamples)
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}
		XFree(ptempXVisualInfo);
	}

	bestFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestFBConfig;

	printf("bestFrameBufferConfig: %d\n",bestFrameBufferConfig );
	printf("bestNumberOfSamples: %d\n",bestNumberOfSamples );
	printf("worstFrameBufferConfig: %d\n",worstFrameBufferConfig);
	printf("worstNumberOfSamples: %d\n",worstNumberOfSamples );

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestFBConfig);


	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error at XCreateWindow()\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My PP OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();
	bool LoadTextures(GLuint *texture,const char* imagePath);
	
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;

	GLenum status;

	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXCreateContextAttribsARB is not obtained\n");
		uninitialize();
		exit(0);
	}

	const int Attribs[7]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	if(!gGLXContext)
	{
		printf("Best gGLXContext is not available\n");
		const int Attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		
		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Context is not hardware context\n");
	}
	else
	{
		printf("context is hardware rendering context\n"); 
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	status=glewInit();
	if(status!=GLEW_OK)
	{
		printf("\n Error in glewInit()\n");
		uninitialize();
		exit(0);
	}

	//creating the shader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vColor; " \
		"in vec2 vTexCoord;" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec3 out_color;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
			"out_color=vColor;								\n" \
			"out_TexCoord=vTexCoord;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				printf( "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 out_color;								\n" \
		"in vec2 out_TexCoord;							\n" \
		"uniform sampler2D u_sampler;					\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
			"	vec4 texSampler=texture(u_sampler,out_TexCoord);" \
			"	vec3 phong_ads_light;						\n" \
			"	if(u_LKeyIsPressed==1)						\n" \
			"	{											\n"	\
			"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
			"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
			"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
			"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
			"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
			"		vec3 ambient= u_la * u_ka;																							\n " \
			"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																				\n" \
			"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);	\n" \
			"		phong_ads_light=ambient + diffuse + specular;																		\n" \
			"	}																													\n" \
			"	else																												\n" \
			"	{																													\n" \
			"			phong_ads_light=vec3(1.0,1.0,1.0);																			\n" \
			"	}																													\n" \
			"	fragColor=vec4((vec3(texSampler)*vec3(out_color)*phong_ads_light),1.0);													\n" \
		"}";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				printf( "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				printf( "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyIsPressed");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");


	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");


	GLfloat vcntCube[]=
	{
				1.0f,1.0f,1.0f,		1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,  	1.0f,1.0f,
				-1.0f,1.0f,1.0f,  	1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,  	0.0f,1.0f,
				-1.0f,-1.0f,1.0f, 	1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,	  	0.0f,0.0f,
				1.0f,-1.0f,1.0f,  	1.0f,0.0f,0.0f,		0.0f,0.0f,1.0f,	  	1.0f,0.0f,

				1.0f,1.0f,-1.0f,	0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,  	1.0f,1.0f,
				1.0f,1.0f,1.0f,		0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,		0.0f,1.0f,
				1.0f,-1.0f,1.0f,	0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,		0.0f,0.0f,
				1.0f,-1.0f,-1.0f,	0.0f,1.0f,0.0f,		1.0f,0.0f,0.0f,		1.0f,0.0f,

				1.0f,1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	1.0f,1.0f,
				-1.0f,1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	0.0f,1.0f,
				-1.0f,-1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	0.0f,0.0f,
				1.0f,-1.0f,-1.0f,	0.0f,0.0f,1.0f,		0.0f,0.0f,-1.0f,	1.0f,0.0f,

				-1.0f,1.0f,1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	1.0f,1.0f,
				-1.0f,1.0f,-1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	0.0f,1.0f,
				-1.0f,-1.0f,-1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	0.0f,0.0f,
				-1.0f,-1.0f,1.0f,	0.0f,1.0f,1.0f,		-1.0f,0.0f,0.0f,	1.0f,0.0f,

				1.0f,1.0f,-1.0f,	1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		1.0f,1.0f,
				-1.0f,1.0f,-1.0f,	1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		0.0f,1.0f,
				-1.0f,1.0f,1.0f,	1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		0.0f,0.0f,
				1.0f,1.0f,1.0f,		1.0f,0.0f,1.0f,		0.0f,1.0f,0.0f,		1.0f,0.0f,

				1.0f,-1.0f,-1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	1.0f,1.0f,
				-1.0f,-1.0f,-1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	0.0f,1.0f,
				-1.0f,-1.0f,1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	0.0f,0.0f,
				1.0f,-1.0f,1.0f,	1.0f,1.0f,0.0f,		0.0f,-1.0f,0.0f,	1.0f,0.0f,
	};



	//rectangle
	//recording start

	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);

	//posiiotn data
	glGenBuffers(1, &vbo_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vcntCube), vcntCube, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat),(void*) (0*sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(9 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//recording stop
	glBindVertexArray(0);


	//projection matrix to load identity
	perspectiveProjectionMatrix=mat4::identity();

	glEnable(GL_TEXTURE_2D);

	 LoadTextures(&texture_marble,"marble.bmp");
	 printf("%d\n",texture_marble );

	glClearColor(0.0f,0.0f,0.0,1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);
	glClearDepth(1.0f);

	resize(giWindowWidth,giWindowHeight);
}
bool LoadTextures(GLuint *texture,const char* imagePath)
{
    
    bool bStatus;
    int imageWidth;
    int imageHeight;

    unsigned char* imageData=NULL;

    imageData=SOIL_load_image(imagePath,&imageWidth,&imageHeight,0,SOIL_LOAD_RGB);
    if(imageData==NULL)
    {
        bStatus=false;
        return bStatus;
    }
    else
    {
        bStatus=true;

        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glGenTextures(1,texture);
        glBindTexture(GL_TEXTURE_2D,*texture);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,imageWidth,imageHeight,0,GL_RGB,GL_UNSIGNED_BYTE,imageData);

        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,0);
        SOIL_free_image_data(imageData);
        return bStatus;
    }
   
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 rotationMatrixX;
	mat4 rotationMatrixY;
	mat4 rotationMatrixZ;
	mat4 scaleMatrix;
	


	modelMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrixX = mat4::identity();
	rotationMatrixY = mat4::identity();
	rotationMatrixZ = mat4::identity();
	viewMatrix=mat4::identity();


	//do necessary translation
	translationMatrix = translate(0.0f, 0.0f, -6.0f);
	
	//rotationMatrixX = rotate(angleCube, 1.0f, 0.0f, 0.0f);
	//rotationMatrixY = rotate(angleCube, 0.0f, 1.0f, 0.0f);
	//rotationMatrixZ = rotate(angleCube, 0.0f, 0.0f, 1.0f);

	rotationMatrix=rotate(angleCube, angleCube, angleCube);


	//do matrix multipliacation
	modelMatrix = modelMatrix * translationMatrix;
	
	//modelViewMatrix = modelViewMatrix * rotationMatrixX * rotationMatrixY* rotationMatrixZ;
	modelMatrix = modelMatrix * rotationMatrix;

	

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);


	if (bLight)
	{
		glUniform1i(LKeyIsPressedUniform, 1);
		glUniform3fv(laUniform, 1, light_ambient);		//glLightfv() 
		glUniform3fv(ldUniform, 1, light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniform, 1, light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position

		glUniform3fv(kaUniform, 1, material_ambient);	//glMaterialfv();
		glUniform3fv(kdUniform, 1, material_diffuse);	//glMaterialfv();
		glUniform3fv(ksUniform, 1, material_specular);	//glMaterialfv();
		glUniform1f(materialShininessUniform, material_shininess);	//glMaterialfv();
	}
	else
	{
		glUniform1i(LKeyIsPressedUniform, 0);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_marble);
	glUniform1i(samplerUniform, 0);
	glBindVertexArray(vao_cube);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	glBindVertexArray(0);

	glUseProgram(0);
	
	glXSwapBuffers(gpDisplay,gWindow);
}

void update()
{
	
	angleCube = angleCube + 0.5f;
	if (angleCube > 360.0f)
	{
		angleCube = 0.0f;
	}

}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
	
}

void uninitialize()
{
	if (vbo_cube)
	{
		glDeleteBuffers(1, &vbo_cube);
		vbo_cube = 0;
	}
	
	
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	GLsizei shaderCount;
	GLsizei shaderNumber;
	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		GLuint* pShaders=(GLuint*)malloc(shaderCount*sizeof(GLuint));
		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&shaderCount,pShaders);
			for(shaderNumber=0;shaderNumber<shaderCount;shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber]=0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;
		glUseProgram(0);
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();
	if(!currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	/*if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}*/

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
