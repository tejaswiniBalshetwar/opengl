#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include <memory.h>

#include<X11/Xutil.h>
#include <X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include"./../vmath.h"


using namespace std;
using namespace vmath;


int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;
GLXContext gGLXContext;

Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbIsFullscreen=false;

GLuint gShaderProgramObject;


enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_line;
GLuint vbo_line_position;
GLuint vbo_line_color;

GLuint vao_triangle=0;
GLuint vbo_triangle_position;
GLuint vbo_triangle_color;

GLuint vao_circle=0;
GLuint vbo_circle_position;
GLuint vbo_circle_color;


GLfloat verticesForOuterCircle[2000];
GLfloat colorForCircle[2000];

GLfloat verticesForInnerCircle[2000];

//geometry translation and rotation variables
GLfloat lineYTranslation = 3.0f;

GLfloat triangleXTranslation = 3.0f;
GLfloat triangleYTranslation = -3.0f;

GLfloat circleXTranslation = -3.0f;
GLfloat circleYTranslation = -3.0f;

GLfloat angleTriangle = 0.0f;
GLfloat angleCircle = 0.0f;



GLuint mvpUniform;

mat4 perspectiveProjectionMatrix;

int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void resize(int,int);
	void display(void);
	void ToggleFullscreen(void);
	void CreateWindow(void);
	void update(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;
	bool bDone=false;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
						bDone=true;
						break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							

							break;


					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;    
					}
					break;
				case MotionNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return 0;
}


void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestFBConfig;
	XVisualInfo *ptempXVisualInfo=NULL;

	int numberOfFBConfigs=0;


	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_STENCIL_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in gpDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	//retrive all the FBConfigdriver has
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&numberOfFBConfigs);
	printf("numberOfFBConfigs: %d\n",numberOfFBConfigs);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for(int i=0;i<numberOfFBConfigs;i++)
	{
		ptempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		//for each obtained FBConfig get temporary XVisualInfo its use to check capability of doing two following calls

		if(ptempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//get sample buffers from respective fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
		
			//get number of sample buffers from fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

			//more number of samples and samplebuffers more eligible fbConfig id
			if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNumberOfSamples )
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			if(worstFrameBufferConfig<0 ||!sampleBuffers ||samples<worstNumberOfSamples)
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}
		XFree(ptempXVisualInfo);
	}

	bestFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestFBConfig;

	printf("bestFrameBufferConfig: %d\n",bestFrameBufferConfig );
	printf("bestNumberOfSamples: %d\n",bestNumberOfSamples );
	printf("worstFrameBufferConfig: %d\n",worstFrameBufferConfig);
	printf("worstNumberOfSamples: %d\n",worstNumberOfSamples );

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestFBConfig);


	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error at XCreateWindow()\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My PP OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();

    GLfloat calculateLengthByDistanceFormula(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void calculateVerticesForCircle(GLfloat, GLfloat, GLfloat, GLfloat*,GLfloat*);
	GLfloat calculateInnerCircleRadius(GLfloat, GLfloat, GLfloat);

	GLfloat lengthOfASide = 0.0f;
	GLfloat lengthOfBSide = 0.0f;
	GLfloat lengthOfCSide = 0.0f;

	
	
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	GLenum status;

	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXCreateContextAttribsARB is not obtained\n");
		uninitialize();
		exit(0);
	}

	const int Attribs[7]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	if(!gGLXContext)
	{
		printf("Best gGLXContext is not available\n");
		const int Attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		
		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Context is not hardware context\n");
	}
	else
	{
		printf("context is hardware rendering context\n"); 
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	status=glewInit();
	if(status!=GLEW_OK)
	{
		printf("\n Error in glewInit()\n");
		uninitialize();
		exit(0);
	}

	//creating shader object
	gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	//write source code of vertex shader obejct
	const GLchar *vertexShaderSourceCode={
		"#version 450 core " \
		"\n" \
		"in vec4 vPosition;" \
        "in vec3 vColor;"   \
		"uniform mat4 u_mvp_matrix;" \
        "out vec3 out_color;"
		"void main(void)" \
		"{"  \
			"gl_Position=u_mvp_matrix* vPosition;" \
            "out_color=vColor;" \
        "}" \
	};

	//specify vertex shader object to source code
	glShaderSource(gVertexShaderObject,1,(const GLchar**)&vertexShaderSourceCode,NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	//error checking for vertex shader
	GLint iShaderCompileStatus=0;
	GLint iInfoLogLength=0;
	GLchar* szInfoLog=NULL;

	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus=GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				 GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("Vertex shader compilation error = %s\n",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader 
	//create fragment shader object
	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//write shder source code
	const GLchar* fragmentShaderSourceCode=
	{
		"#version 450 core" \
		"\n " \
		"out vec4 fragColor;" \
        "in vec3 out_color;" \
		"void main(void)" \
		"{" \
			"fragColor= vec4(out_color,1.0f);"
		"}" \
	};

	//specify shader source code to object
	glShaderSource(gFragmentShaderObject,1,(GLchar**)&fragmentShaderSourceCode,NULL);

	//compile Shader
	glCompileShader(gFragmentShaderObject);

	//error checking for fragment shader
	iShaderCompileStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;

	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("Fragment shader compilation error: %s\n",szInfoLog );

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create shader progrma object
	gShaderProgramObject=glCreateProgram();

	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);


	//starts the prelinking code for attributes linkage gpu with cpu
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_COLOR,"vColor");
	//link the program
	glLinkProgram(gShaderProgramObject);

	
	//error checking for linking of program
	GLint iShaderProgramLinkStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;

	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
	if(iShaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(GLchar*)malloc(iInfoLogLength*sizeof(GLchar));
			if(szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
				printf("Error in linking the gShaderProgramObject:= %s\n",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

    //post linking for uniform
	mvpUniform=glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    
    //wond
    GLfloat lineVertices[] = {
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};
	GLfloat colorForLine[] = {
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f
	};

	glGenVertexArrays(1,&vao_line);
	glBindVertexArray(vao_line);
		glGenBuffers(1,&vbo_line_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_line_position);
		glBufferData(GL_ARRAY_BUFFER,sizeof(lineVertices),lineVertices,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_line_color);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_line_color);
            glBufferData(GL_ARRAY_BUFFER,sizeof(colorForLine),colorForLine,GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);




    //triangle
    GLfloat verticesForTriangle[] = {
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f
	};

	GLfloat colorForTriangle[] = {
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f
	};

	glGenVertexArrays(1,&vao_triangle);
	glBindVertexArray(vao_triangle);
		glGenBuffers(1,&vbo_triangle_position);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_triangle_position);
		glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForTriangle),verticesForTriangle,GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_triangle_color);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(colorForTriangle),colorForTriangle,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


    lengthOfASide = calculateLengthByDistanceFormula(verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2], verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5]);
	lengthOfBSide = calculateLengthByDistanceFormula(verticesForTriangle[3], verticesForTriangle[4], verticesForTriangle[5], verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8]);
	lengthOfCSide = calculateLengthByDistanceFormula(verticesForTriangle[6], verticesForTriangle[7], verticesForTriangle[8], verticesForTriangle[0], verticesForTriangle[1], verticesForTriangle[2]);

	
	GLfloat XCenterOfCircle = ((verticesForTriangle[0] * lengthOfBSide) + (verticesForTriangle[3] * lengthOfCSide) + (verticesForTriangle[6] * lengthOfASide))/(lengthOfASide+lengthOfBSide+lengthOfCSide);
	GLfloat YCenterOfCircle = ((verticesForTriangle[1] * lengthOfBSide) + (verticesForTriangle[4] * lengthOfCSide) + (verticesForTriangle[7] * lengthOfASide))/ (lengthOfASide + lengthOfBSide + lengthOfCSide);

    GLfloat radius = 0.0f;
	radius = calculateInnerCircleRadius(lengthOfASide, lengthOfBSide, lengthOfCSide);

	calculateVerticesForCircle(radius, XCenterOfCircle, YCenterOfCircle, verticesForInnerCircle,colorForCircle);


    glGenVertexArrays(1,&vao_circle);
		glBindVertexArray(vao_circle);
		glGenBuffers(1,&vbo_circle_position);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,sizeof(verticesForOuterCircle),verticesForOuterCircle,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_circle_color);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER,sizeof(colorForCircle),colorForCircle,GL_DYNAMIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);

	//projection matrix to load identity
	perspectiveProjectionMatrix=mat4::identity();

	glClearColor(0.0f,0.0f,0.0,1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	resize(giWindowWidth,giWindowHeight);
}
GLfloat calculateLengthByDistanceFormula(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2 )
{
	GLfloat length = 0.0f;

	length = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2) + pow((z2 - z1), 2));

	return length;

}

GLfloat calculateInnerCircleRadius(GLfloat A, GLfloat B, GLfloat C)
{
	GLfloat semiperimeter = (A + B + C) / 2.0f;
	GLfloat area = (GLfloat)sqrt(semiperimeter*((semiperimeter - A)*(semiperimeter - B)*(semiperimeter - C)));

	GLfloat radius =GLfloat (area / semiperimeter);

	return radius;
}


void calculateVerticesForCircle(GLfloat radius, GLfloat XPointOfCenter, GLfloat YPointOfCenter, GLfloat* vertices, GLfloat *color)
{
	GLfloat x = 0.0f, y = 0.0f;
	//vertices[2000];
	for (int i = 0; i < 2000-3; i=i+3)
	{
		GLfloat angle = (2 * M_PI*i) / 2000;
		x =(GLfloat) XPointOfCenter + radius * cos(angle);
		y =(GLfloat) YPointOfCenter + radius * sin(angle);

		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = 0.0f; 

		color[i] = 1.0f;
		color[i + 1] = 0.0f;
		color[i + 2] = 1.0f;
	}

}



void display()
{
    void drawTriangle();
    void drawLine();
    void drawCircle(GLfloat *,GLfloat,GLfloat*,GLfloat);


	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

		mat4 modelViewMatrix;
		mat4 modelViewProjectionMatrix;
		mat4 translationMatrix;
		mat4 rotationMatrix;
		//initialize to identity
		modelViewMatrix=mat4::identity();
		modelViewProjectionMatrix=mat4::identity();
		translationMatrix=mat4::identity();
		rotationMatrix=mat4::identity();
		//do necessary transformation
		translationMatrix= translate(0.0f, lineYTranslation, -5.0f);
        //do necessary transformation matrix multiplication
		modelViewMatrix=modelViewMatrix* translationMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix*modelViewMatrix;

		//send matrices to necessary uniform 
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		//bind with recoreder related code
		
		drawLine();


        translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		
		//tranlation and rotation related code
		translationMatrix = translate(triangleXTranslation, triangleYTranslation,-5.0f);
		rotationMatrix = rotate(angleTriangle,0.0f,1.0f,0.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
		drawTriangle();


        translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();

		translationMatrix = translate(circleXTranslation, circleYTranslation,-5.0f);
		rotationMatrix = rotate(angleCircle,1.0f,0.0f,0.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix*rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);

		//for circle Translations

		drawCircle(verticesForInnerCircle,sizeof(verticesForInnerCircle),colorForCircle,sizeof(colorForCircle));
			


	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}


void drawCircle(GLfloat * vertices,GLfloat verticesSize,GLfloat *color,GLfloat colorSize)
{
	glBindVertexArray(vao_circle);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_circle_position);
			glBufferData(GL_ARRAY_BUFFER,verticesSize,vertices,GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
			glBufferData(GL_ARRAY_BUFFER, colorSize, color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		glPointSize(3.0f);
		glDrawArrays(GL_POINTS,0,2000);
	glBindVertexArray(0);
}

void drawTriangle()
{
	glBindVertexArray(vao_triangle);
	glDrawArrays(GL_LINE_LOOP,0,3);
	glBindVertexArray(0);
}

void drawLine()
{
	glBindVertexArray(vao_line);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
	
}

void uninitialize()
{
	if(vbo_line_position)
	{
		glDeleteBuffers(1,&vbo_line_position);
		vbo_line_position=0;
	}

	if(vbo_line_color)
	{
		glDeleteBuffers(1,&vbo_line_color);
		vbo_line_color=0;
	}
	if(vao_line)
	{
		glDeleteVertexArrays(1,&vao_line);
		vao_line=0;
	}

    if(vbo_triangle_position)
	{
		glDeleteBuffers(1,&vbo_triangle_position);
		vbo_triangle_position=0;
	}

	if(vbo_triangle_color)
	{
		glDeleteBuffers(1,&vbo_triangle_color);
		vbo_triangle_color=0;
	}
	if(vao_triangle)
	{
		glDeleteVertexArrays(1,&vao_triangle);
		vao_triangle=0;
	}

    if(vbo_circle_position)
	{
		glDeleteBuffers(1,&vbo_circle_position);
		vbo_circle_position=0;
	}

	if(vbo_circle_color)
	{
		glDeleteBuffers(1,&vbo_circle_color);
		vbo_circle_color=0;
	}
	if(vao_circle)
	{
		glDeleteVertexArrays(1,&vao_circle);
		vao_circle=0;
	}


	GLsizei shaderCount;
	GLsizei shaderNumber;
	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		GLuint* pShaders=(GLuint*)malloc(shaderCount*sizeof(GLuint));
		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&shaderCount,pShaders);
			for(shaderNumber=0;shaderNumber<shaderCount;shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber]=0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;
		glUseProgram(0);
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();
	if(!currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void update()
{
	if (lineYTranslation >= 0.0f)
	{
		lineYTranslation = lineYTranslation - 0.002f;
	}

	if (circleXTranslation < 0.0f && circleYTranslation < 0.0f)
	{
		circleXTranslation = circleXTranslation + 0.002f;
		//circleXTranslation = circleXTranslation - (-3.0f - 0.0f)/100;
		circleYTranslation = circleYTranslation + 0.002f;
	}

	if (triangleXTranslation >= 0.0f && triangleYTranslation <= 0.0f)
	{
		triangleXTranslation = triangleXTranslation - 0.002f;
		triangleYTranslation = triangleYTranslation + 0.002f;
	}

	angleCircle = angleCircle + 0.20f;
	angleTriangle = angleTriangle + 0.20f;
	if (circleXTranslation > 0.0f && circleYTranslation > 0.0f && triangleXTranslation<=0.0f && triangleYTranslation >= 0.0f)
	{
		angleTriangle = 0.0f;
		angleCircle = 0.0f;
	}
}
