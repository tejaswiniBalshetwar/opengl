#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>
#include<X11/Xutil.h>


#include"./../vmath.h"
#include "Sphere.h"

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

using namespace std;
using namespace vmath;

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

int giWindowWidth=800;
int giWindowHeight=600;


Window gWindow;
Display *gpDisplay=NULL;
Colormap gColormap;

// attributes enum 
enum
{
	AMC_ATTRIBUTE_POSITION1=0,
	AMC_ATTRIBUTE_COLOR1,
	AMC_ATTRIBUTE_NORMAL1,
	AMC_ATTRIBUTE_TEXCOORD01,
	AMC_ATTRIBUTE_POSITION2 ,
	AMC_ATTRIBUTE_COLOR2,
	AMC_ATTRIBUTE_NORMAL2,
	AMC_ATTRIBUTE_TEXCOORD02
};


GLuint gShaderProgramObject=0;

GLfloat angleSquare=0.0f;


GLXContext gGLXContext;
XVisualInfo *gpXVisualInfo=NULL;
GLXFBConfig gGLXFBConfig;

//fbConfig stroing function pointer variables
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;



mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

GLuint vao_sphere;

GLuint vbo_position_sphere=0;
GLuint vbo_normal_sphere=0;
GLuint vbo_element_sphere = 0;

//vertex lighting
GLuint modelUniformVertex;
GLuint viewUniformVertex;
GLuint projectionUniformVertex;
GLuint LKeyIsPressedUniformVertex;
GLuint laUniformVertex=0;
GLuint ldUniformVertex=0;
GLuint lsUniformVertex=0;
GLuint kaUniformVertex=0;
GLuint kdUniformVertex=0;
GLuint ksUniformVertex=0;
GLuint lightPositionUniformVertex=0;
GLuint materialShininessUniformVertex=0;


GLuint modelUniform=0;
GLuint viewUniform=0;
GLuint projectionUniform=0;
GLuint LKeyIsPressedUniform=0;
GLuint laUniform=0;
GLuint ldUniform=0;
GLuint lsUniform=0;
GLuint kaUniform=0;
GLuint kdUniform=0;
GLuint ksUniform=0;

GLuint lightPositionUniform=0;
GLuint materialShiniessUniform=0;

GLuint gShaderProgramObjectVertex = 0;
GLuint gShaderProgramObjectFragment = 0;

bool gbIsFragmentShader = true;


float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;



bool bRotation=false;
bool bLight=false;

float light_ambient[] = {0.0f,0.0f,0.0f,0.0f};
float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
float lightPosition[] = {100.0f,100.0f,100.0f,1.0f};

float material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 50.0f;


int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void ToggleFullscreen(void);
	void resize(int,int);
	void createWindow(void);

	XEvent event;
	KeySym keysym;

	static int windowWidth=giWindowWidth;
	static int windowHeight=giWindowHeight;

	bool bDone=false;
	char keys[26];

	createWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;


					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							
		
							break;
						case 'v':
						case 'V':
						if (gbIsFragmentShader == false)
							{
								gbIsFragmentShader = true;
							}
							else
							{
								gbIsFragmentShader = false;
							}
							break;
                        case 'l':
						case 'L':
							if(bLight==false)
							{
								bLight=true;
							}
							else
							{
								bLight=false;
							}
							break;
					}

					break;

					case ConfigureNotify:
						windowWidth=event.xconfigure.width;
						windowHeight=event.xconfigure.height;
						resize(windowWidth,windowHeight);
						break;
					case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1:
								break;
							case 2:
								break;
							case 3:
								break;
							case 4:
								break;
						}
						break;
					case MotionNotify:
						break;
					case 33:
						bDone=true;
						break;
			}
		}
		if(bRotation)
		{
			update();
		}
		display();
	}

	uninitialize();
	return 0;
}


void createWindow(void)
{
	void uninitialize();

	XSetWindowAttributes winAttribs;

	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumberOfFBConfig=0;

	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nError in XOpenDisplay()");
		uninitialize();
		exit(0);
	}
	defaultScreen=XDefaultScreen(gpDisplay);

	//code of GLXFBConfig
	pGLXFBConfig=glXGetFBConfigs(gpDisplay,defaultScreen,&iNumberOfFBConfig);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for (int i=0;i<iNumberOfFBConfig;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		if(pTempXVisualInfo)
		{
			int iSampleBuffer;
			int iSamples;

			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&iSampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&iSamples);

			if(bestNumberOfSamples <0 ||iSampleBuffer && iSamples > bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=iSamples;
			}
			if(worstNumberOfSamples<0 || !iSampleBuffer || iSamples<worstNumberOfSamples)
			{
				worstNumberOfSamples=iSamples;
				worstFrameBufferConfig=i;
			}
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo=NULL;
	}

	bestGLXFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestGLXFBConfig;

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|VisibilityChangeMask|PointerMotionMask;

	styleMask=CWBackPixel|CWBorderPixel|CWColormap|CWEventMask;


	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error in XCreateWindow()");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY PPOGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);	

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();
	void CreateProgramForPerFragment(void);
	void CreateProgramForPerVertex(void);
	GLuint vertexShaderObject=0;
	GLuint fragmentShaderObject=0;


	//gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	//getting required hardware renderable context for drawing on the screen which is best
	//this function is implementataion dependant for that we define funcrion poiter and the get the address for the same
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	if(glXCreateContextAttribsARB==NULL)
	{
		printf("\nError while getting the function pointer");
		uninitialize();
		exit(0);
	}


	const int attrib[]=
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attrib);
	if(!gGLXContext)
	{
		printf("\n Error in glXCreateContextAttribsARB the required fbConfigs hardware context is not availble");

		const int attrib[]={
			GLX_CONTEXT_MINOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};

		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attrib);

	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("WARNING: Context is not hardware rendering context!..\n\n");
	}
	else
	{
		printf("\n\nSUCCESS: Obtained context is hardware rendeing context..\n\n");

	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	GLenum result ;
	result=glewInit();
	if(result!=GLEW_OK)
	{
		printf("\n Error while initializing glew\n\n exiting");
		uninitialize();
		exit(0);
	}

	//shaders
	CreateProgramForPerFragment();
	CreateProgramForPerVertex();

    getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	//start recording creation and starting the buffers to access for triangle of gpu

	//for square vertices
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//position data
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	//for per vertex
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION1);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION2,3,GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//normal data
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	
		//for per vertex
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL1);
		
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL2,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL2);
		glBindBuffer(GL_ARRAY_BUFFER,0);

	
	glGenBuffers(1,&vbo_element_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	//recording stop
	glBindVertexArray(0);




	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);
	//glDisable(GL_FRONT_AND_BACK);
	//percpetive projection matrix load identity
	perspectiveProjectionMatrix=mat4::identity();
    modelMatrix=mat4::identity();
    viewMatrix=mat4::identity();

	resize(giWindowWidth,giWindowHeight);

}


void CreateProgramForPerFragment()
{
	void uninitialize();
	GLuint vertexShaderObjectFragment = 0;
	GLuint fragmentShaderObjectFragment = 0;
	vertexShaderObjectFragment = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCodeFragment =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObjectFragment, 1, (const GLchar**)&vertexShaderSourceCodeFragment, NULL);


	//compile shader
	glCompileShader(vertexShaderObjectFragment);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObjectFragment, iInfoLogLength, &written, szInfoLog);

				printf( "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObjectFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCodeFragment =
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
		"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		"		vec3 ambient= u_la * u_ka;																						\n " \
		"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																			\n" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		phong_ads_light=ambient + diffuse + specular;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";

	//specify source code to object
	glShaderSource(fragmentShaderObjectFragment, 1, (const GLchar**)&fragmentShaderSourceCodeFragment, NULL);

	//compile shader
	glCompileShader(fragmentShaderObjectFragment);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObjectFragment, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObjectFragment, iInfoLogLength, &written, szInfoLog);

				printf("\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObjectFragment = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObjectFragment, vertexShaderObjectFragment);
	glAttachShader(gShaderProgramObjectFragment, fragmentShaderObjectFragment);

	//prelinking the atttributes
	glBindAttribLocation(gShaderProgramObjectFragment, AMC_ATTRIBUTE_POSITION2, "vPosition");
	glBindAttribLocation(gShaderProgramObjectFragment, AMC_ATTRIBUTE_NORMAL2, "vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObjectFragment);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectFragment, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectFragment, iInfoLogLength, &written, szInfoLog);

				printf( "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	modelUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_projection_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_ks");

	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_LKeyIsPressed");

	lightPositionUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_light_position");
	materialShiniessUniform = glGetUniformLocation(gShaderProgramObjectFragment, "u_material_shininess");

}

void CreateProgramForPerVertex()
{
	void uninitialize();

	GLuint vertexShaderObject = 0;
	GLuint fragmentShaderObject = 0;

	//create source code object
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the shader source code
	const GLchar *vertexShaderSourceCode =
		"	#version 450 core											\n" \
		"	\n															\n" \
		"	in vec4 vPosition1;											\n" \
		"	in vec3 vNormal1;											\n" \
		"	uniform mat4 u_model_matrix1;								\n" \
		"	uniform mat4 u_view_matrix1;								\n" \
		"	uniform mat4 u_projection_matrix1;							\n" \
		"	uniform vec3 u_la1;											\n" \
		"	uniform vec3 u_ld1;											\n" \
		"	uniform vec3 u_ls1;											\n" \
		"	uniform vec3 u_ka1;											\n" \
		"	uniform vec3 u_kd1;											\n" \
		"	uniform vec3 u_ks1;											\n" \
		"	uniform int u_LKeyIsPressed1;															\n" \
		"	uniform float u_materialShininess1;														\n" \
		"	uniform vec4 u_light_position1;															\n" \
		"	out vec3 out_phong_ads_light1 ;															\n" \
		"	void main(void)																			\n" \
		"	{																						\n" \
		"		if(u_LKeyIsPressed1==1)																\n" \
		"		{																					\n" \
		"			vec4 eye_coordinates=u_view_matrix1 * u_model_matrix1 * vPosition1;															\n" \
		"			vec3 t_normal=normalize(mat3(u_view_matrix1*u_model_matrix1) * vNormal1) ;													\n" \
		"			vec3 light_direction=normalize(vec3(u_light_position1 - eye_coordinates));												\n" \
		"			float t_dot_ld=max(dot(light_direction,t_normal),0.0);																	\n" \
		"			vec3 reflection_vector=reflect(-light_direction,t_normal);																\n" \
		"			vec3 viewer_vector=normalize(vec3(-eye_coordinates));																	\n" \
		"			vec3 ambient= u_la1 * u_ka1;																								\n" \
		"			vec3 diffuse= u_ld1 * u_kd1 * t_dot_ld;																					\n" \
		"			vec3 specular= u_ls1 * u_ks1 * pow(max(dot(reflection_vector,viewer_vector),0.0),u_materialShininess1);					\n" \
		"			out_phong_ads_light1= ambient + diffuse + specular;																			\n" \
		"		}																															\n" \
		"		else																														\n" \
		"		{																															\n" \
		"			out_phong_ads_light1=vec3(1.0,1.0,1.0);																					\n" \
		"																																	\n" \
		"		}																															\n" \
		"		gl_Position= u_projection_matrix1 * u_view_matrix1 * u_model_matrix1 * vPosition1;																															\n" \
		"	}																																\n" \
		;

	//specify source code to object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking

	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char * szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf( "\n Error while compiling Vertex Shader: %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				exit(0);
			}
		}

	}

	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//writting shader source code for the fragment shader
	const GLchar* fragmentShaderSourceCode =
		"	#version 450 core																		" \
		"	\n																						" \
		"	in vec3 out_phong_ads_light1;															\n" \
		"	out vec4 fragColor;																		\n" \
		"	void main(void)																			\n" \
		"	{																						\n" \
		"		fragColor=vec4(out_phong_ads_light1,1.0);											\n" \
		"	}																						\n" \
		;

	//specify shader code to shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error cheking for compilation
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength * sizeof(char));
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("\nError in Fragment Shader:=%s\n",szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObjectVertex =glCreateProgram();
	
	//cattach shader to program
	glAttachShader(gShaderProgramObjectVertex, vertexShaderObject);
	glAttachShader(gShaderProgramObjectVertex,fragmentShaderObject);
	
	//prelinkg for attributes
	glBindAttribLocation(gShaderProgramObjectVertex,AMC_ATTRIBUTE_POSITION1,"vPosition1");
	glBindAttribLocation(gShaderProgramObjectVertex, AMC_ATTRIBUTE_NORMAL1, "vNormal1");

	//link the program
	glLinkProgram(gShaderProgramObjectVertex);

	//errror checking for linking the program

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectVertex,GL_LINK_STATUS,&iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectVertex,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			GLsizei written;
			glGetProgramInfoLog(gShaderProgramObjectVertex,iInfoLogLength,&written,szInfoLog);
			printf("\nError while linking the Program:=%s\n",szInfoLog);

			free(szInfoLog);

			uninitialize();
			exit(0);
		}	
	}

	//post linking binding uniform
	modelUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_model_matrix1");
	viewUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_view_matrix1");
	projectionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_projection_matrix1");
	
	laUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_la1");
	ldUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ld1");
	lsUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ls1");

	kaUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ka1");
	kdUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_kd1");
	ksUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_ks1");

	LKeyIsPressedUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_LKeyIsPressed1");

	lightPositionUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_light_position1");
	materialShininessUniformVertex = glGetUniformLocation(gShaderProgramObjectVertex, "u_materialShininess1");


}





void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
		
	//declaration for matrices
	mat4 modelViewMatrix;
	
	mat4 translationMatrix;
	
	//load identity
	modelMatrix=mat4::identity();

	translationMatrix=mat4::identity();


	//do necessary multiplication
	translationMatrix=translate(0.0f,0.0f,-3.0f);


	modelMatrix=modelMatrix*translationMatrix;
	

if (gbIsFragmentShader == true)
	{
		glUseProgram(gShaderProgramObjectFragment);
		glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);

		glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

		if (bLight)
		{
			glUniform1i(LKeyIsPressedUniform, 1);
			glUniform3fv(laUniform, 1, light_ambient);		//glLightfv() 
			glUniform3fv(ldUniform, 1, light_diffuse);		//glLightfv() 
			glUniform3fv(lsUniform, 1, light_specular);		//glLightfv() 
			glUniform4fv(lightPositionUniform, 1, lightPosition); //glLightfv() for position

			glUniform3fv(kaUniform, 1, material_ambient);	//glMaterialfv();
			glUniform3fv(kdUniform, 1, material_diffuse);	//glMaterialfv();
			glUniform3fv(ksUniform, 1, material_specular);	//glMaterialfv();
			glUniform1f(materialShiniessUniform, material_shininess);	//glMaterialfv();
		}
		else
		{
			glUniform1i(LKeyIsPressedUniform, 0);
		}
	}
	else
	{
		glUseProgram(gShaderProgramObjectVertex);

		glUniformMatrix4fv(modelUniformVertex, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniformVertex, 1, GL_FALSE, viewMatrix);

		glUniformMatrix4fv(projectionUniformVertex, 1, GL_FALSE, perspectiveProjectionMatrix);

		if (bLight)
		{
			glUniform1i(LKeyIsPressedUniformVertex, 1);
			glUniform3fv(laUniformVertex, 1, light_ambient);		//glLightfv() 
			glUniform3fv(ldUniformVertex, 1, light_diffuse);		//glLightfv() 
			glUniform3fv(lsUniformVertex, 1, light_specular);		//glLightfv() 
			glUniform4fv(lightPositionUniformVertex, 1, lightPosition); //glLightfv() for position

			glUniform3fv(kaUniformVertex, 1, material_ambient);	//glMaterialfv();
			glUniform3fv(kdUniformVertex, 1, material_diffuse);	//glMaterialfv();
			glUniform3fv(ksUniformVertex, 1, material_specular);	//glMaterialfv();
			glUniform1f(materialShininessUniformVertex, material_shininess);	//glMaterialfv();
		}
		else
		{
			glUniform1i(LKeyIsPressedUniformVertex, 0);
		}
	}

	
	
    glBindVertexArray(vao_sphere);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
        glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
	//glDrawArrays(GL_TRIANGLE_STRIP,0,gNumElements);
	glBindVertexArray(0);
	

	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}
void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize()
{
	if(vao_sphere)
	{
		glDeleteVertexArrays(1,&vao_sphere);
		vao_sphere=0;
	}

	if(vbo_position_sphere)
	{
		glDeleteBuffers(1,&vbo_position_sphere);
		vbo_position_sphere=0;
	}

	if(vbo_normal_sphere)
	{
		glDeleteBuffers(1,&vbo_normal_sphere);
		vbo_normal_sphere=0;
	}


	if(gShaderProgramObject)
	{
		int iShaderCount=0;
		int iShaderNumber=0;
		glUseProgram(gShaderProgramObject);
			glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&iShaderCount);

			GLuint  *pShaders=(GLuint*)malloc(sizeof(GLuint)*iShaderCount);

			for(iShaderNumber=0;iShaderNumber<iShaderCount;iShaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[iShaderNumber]);
				glDeleteShader( pShaders[iShaderNumber]);
				pShaders[iShaderNumber]=0;
			}
			free(pShaders);

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;

	}

	GLXContext currentContext;
	currentContext=glXGetCurrentContext();
	if(currentContext && gGLXContext==currentContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void update()
{
	
}