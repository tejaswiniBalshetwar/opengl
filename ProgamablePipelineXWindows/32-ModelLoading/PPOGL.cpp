#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include <memory.h>

#include<X11/Xutil.h>
#include <X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include"./../vmath.h"


using namespace std;
using namespace vmath;


int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;

XVisualInfo *gpXVisualInfo=NULL;
GLXContext gGLXContext;

Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

bool gbIsFullscreen=false;



enum{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint gShaderProgramObject = 0;
GLuint vbo_position = 0;
GLuint vbo_texture = 0;
GLuint vbo_Normal = 0;
GLuint vbo_element = 0;
GLuint vao = 0;

GLuint mvpUniform;

mat4 perspectiveProjectionMatrix;


//mesh loading code
struct vec_float {
	float *p;
	int size;
};


struct vec_int {
	int *p;
	int size;
};
vec_float *gpVertex;
vec_float *gpNormal;
vec_float *gpTexCoords;

vec_int *gpTextureIndices;
vec_int *gpNormalIndices;
vec_int *gpVertexIndices;

vec_float *gpVertexSorted;
vec_float *gpNormalSorted;
vec_float *gpTextureSorted;
FILE *gpFile_Mesh = NULL;
char buffer[1024];


int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void resize(int,int);
	void display(void);
	void ToggleFullscreen(void);
	void CreateWindow(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;
	bool bDone=false;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
						bDone=true;
						break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							

							break;


					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;    
					}
					break;
				case MotionNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		//update()
		display();
	}

	uninitialize();
	return 0;
}


void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestFBConfig;
	XVisualInfo *ptempXVisualInfo=NULL;

	int numberOfFBConfigs=0;


	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_STENCIL_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in gpDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	//retrive all the FBConfigdriver has
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&numberOfFBConfigs);
	printf("numberOfFBConfigs: %d\n",numberOfFBConfigs);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for(int i=0;i<numberOfFBConfigs;i++)
	{
		ptempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		//for each obtained FBConfig get temporary XVisualInfo its use to check capability of doing two following calls

		if(ptempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			//get sample buffers from respective fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
		
			//get number of sample buffers from fbConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

			//more number of samples and samplebuffers more eligible fbConfig id
			if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNumberOfSamples )
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			if(worstFrameBufferConfig<0 ||!sampleBuffers ||samples<worstNumberOfSamples)
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}
		XFree(ptempXVisualInfo);
	}

	bestFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestFBConfig;

	printf("bestFrameBufferConfig: %d\n",bestFrameBufferConfig );
	printf("bestNumberOfSamples: %d\n",bestNumberOfSamples );
	printf("worstFrameBufferConfig: %d\n",worstFrameBufferConfig);
	printf("worstNumberOfSamples: %d\n",worstNumberOfSamples );

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestFBConfig);


	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error at XCreateWindow()\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My PP OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();
	void loadMesh();

	
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;

	GLenum status;

	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXCreateContextAttribsARB is not obtained\n");
		uninitialize();
		exit(0);
	}

	const int Attribs[7]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	if(!gGLXContext)
	{
		printf("Best gGLXContext is not available\n");
		const int Attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		
		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,Attribs);
	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Context is not hardware context\n");
	}
	else
	{
		printf("context is hardware rendering context\n"); 
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	status=glewInit();
	if(status!=GLEW_OK)
	{
		printf("\n Error in glewInit()\n");
		uninitialize();
		exit(0);
	}

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//writing the source code for the shader
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position= u_mvp_matrix * vPosition ;"  \
		"}";

	//specify source code to obeject
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);


	//compile shader
	glCompileShader(vertexShaderObject);

	//error checking
	int iCompileStatus = 0;
	int iInfoLogLength = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);

				printf( "\nError in vertex Shader= %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//fragment shader object
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//source code for fragment shader
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor=vec4(1.0, 1.0, 0.0, 1.0);" \
		"}";

	//specify source code to object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(fragmentShaderObject);

	//error checking
	iCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iCompileStatus);
	if (iCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				printf( "\nError in fragment shader = %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//create Shader Program object
	gShaderProgramObject = glCreateProgram();

	//attach Shaders to program object
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL,"vNormal");

	//link the code to make it gpu specific shader processor understandble 
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(char)*iInfoLogLength);
			if (szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				printf( "Error while Linking the program: %s", szInfoLog);
				uninitialize();
				exit(0);
				free(szInfoLog);
			}
		}
	}

	//post linking retriving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	//loadMesh call
	loadMesh();



	//create the recoding casets reel == vao
	glGenVertexArrays(1, &vao);

	//bind with cassate  and start recording
	glBindVertexArray(vao);

	//gen buffers
	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*gpVertexSorted->size, gpVertexSorted->p, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1,&vbo_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,gpVertexIndices->size*sizeof(float),gpVertexIndices->p,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);


	//recording stop
	glBindVertexArray(0);

	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	glFrontFace(GL_CCW);
	//projection matrix to load identity
	perspectiveProjectionMatrix=mat4::identity();

	glClearColor(0.0f,0.0f,0.0,1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	resize(giWindowWidth,giWindowHeight);
}
void loadMesh(void)
{
	struct vec_float* create_vec_float(void);
	struct vec_int* create_vec_int(void);
	int push_back_vec_float(struct vec_float* , float );
	int push_back_vec_int(struct vec_int* , int );

	gpVertex = create_vec_float();
	gpNormal = create_vec_float();
	gpTexCoords = create_vec_float();
	
	gpVertexIndices = create_vec_int();
	gpTextureIndices = create_vec_int();
	gpNormalIndices = create_vec_int();

	gpVertexSorted = create_vec_float();
	gpNormalSorted = create_vec_float();
	gpTextureSorted = create_vec_float();

	gpFile_Mesh = fopen("MonkeyHead.OBJ","r");
	if (gpFile_Mesh==NULL)
	{
		printf("\n\n\n::Error while opening OBJ file");
		exit(0);
	}
	else
	{
		char* firstToken = NULL;
		char* token = NULL;
		const char* space =" ";
		int num = 0;
		const char* slash = "/";
		char* fEntries[] = {NULL,NULL,NULL};
		while ((fgets(buffer, 1024, gpFile_Mesh)) != NULL)
		{
			firstToken = strtok(buffer,space);
			if (strcmp(firstToken, "v") == 0)
			{
				token = strtok(NULL,space);	
				float x = atof(token);
				push_back_vec_float(gpVertex,x);
				

				token = strtok(NULL, space);
				float y = atof(token);
				push_back_vec_float(gpVertex,y);
				num++; 
			
				token = strtok(NULL, space);
				float z = atof(token);
				push_back_vec_float(gpVertex,z);
				num++;
			}
			else if (strcmp(firstToken,"vt") == 0)
			{
				token = strtok(NULL, space);
				float s = atof(token);
				push_back_vec_float(gpTexCoords, s);

				token = strtok(NULL, space);
				float t = atof(token);
				push_back_vec_float(gpTexCoords, t);
			}
			else if (strcmp(firstToken, "vn") == 0)
			{
				token = strtok(NULL, space);
				float u = atof(token);
				push_back_vec_float(gpNormal, u);

				token = strtok(NULL, space);
				float v = atof(token);
				push_back_vec_float(gpNormal, v);

				token = strtok(NULL, space);
				float w = atof(token);
				push_back_vec_float(gpNormal, w);
			}
			else if (strcmp(firstToken, "f") == 0)
			{
				for (int i = 0; i < 3; i++)
				{
					fEntries[i] = strtok(NULL,space);
				}
				for (int i = 0; i < 3; i++)
				{
					token = strtok(fEntries[i],slash);
					push_back_vec_int(gpVertexIndices,atoi(token)-1);
					
					token = strtok(NULL, slash);
					push_back_vec_int(gpTextureIndices, atoi(token)-1);
				
					token = strtok(NULL, slash);
					push_back_vec_int(gpNormalIndices, atoi(token)-1);
					
				}
			}
		}

	}
	fclose(gpFile_Mesh);
	
		for (unsigned int i = 0; i < gpVertexIndices->size; i++)
		{
			
			push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+0]);
			push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+1]);
			push_back_vec_float(gpVertexSorted, gpVertex->p[gpVertexIndices->p[i]*3+2]);
		}

		for (int i = 0; i < gpNormalIndices->size; i=i+3)
		{
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+0]);
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+1]);
			push_back_vec_float(gpNormalSorted, gpNormal->p[gpNormalIndices->p[i]*3+2]);
			
		}

		for (int i = 0; i < gpTextureIndices->size; i=i+2)
		{
			push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i]*2+0]);
			push_back_vec_float(gpTextureSorted, gpTexCoords->p[gpTextureIndices->p[i]*2+1]);
		}
}
struct vec_int* create_vec_int()
{
	struct vec_int* p_new = NULL;

	p_new = (struct vec_int*)malloc(sizeof(struct vec_int));
	if (p_new == NULL)
	{
		printf("Error in malloc");
		exit(0);
	}

	memset(p_new, 0, sizeof(struct vec_int));

	return p_new;
}


int push_back_vec_int(struct vec_int* p_vec, int new_data)
{
	p_vec->p = (int*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(int));

	if (p_vec->p == NULL)
	{
		printf("Errorr  realloc");
		exit(0);
	}

	p_vec->size = p_vec->size + 1;

	p_vec->p[p_vec->size - 1] = new_data;


	return True;
}

struct vec_float* create_vec_float()
{
	struct vec_float* p_new = NULL;

	p_new = (struct vec_float*)malloc(sizeof(struct vec_float));
	if (p_new == NULL)
	{
		printf("Error in malloc");
		exit(0);
	}

	memset(p_new, 0, sizeof(struct vec_float));

	return p_new;
}


int push_back_vec_float(struct vec_float* p_vec, float new_data)
{
	p_vec->p = (float*)realloc(p_vec->p, (p_vec->size + 1) * sizeof(float));


	if (p_vec->p == NULL)
	{
		printf("Errorr  realloc");
		exit(0);
	}

	p_vec->size = p_vec->size + 1;

	p_vec->p[p_vec->size - 1] = new_data;
	

	return True;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;
	//initialize abouve matrices
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	//do necessary for transformation
	translationMatrix = translate(0.0f, 0.0f, -6.0f);
	//do necessary for matrix multiplication
	modelViewMatrix = modelViewMatrix * translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//bind with vao so that it starts sound which is recorded
	glBindVertexArray(vao);

	//similaraly bind for textures if any
	/*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element);
	glDrawElements(GL_TRIANGLES,gpVertexIndices->size*sizeof(int), GL_UNSIGNED_SHORT,NULL);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
*/
	glDrawArrays(GL_TRIANGLES,0,gpVertexSorted->size);
	glBindVertexArray(0);
	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix=perspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
	
}

void uninitialize()
{
	int destroy_vec_float(struct vec_float*);
	int destroy_vec_int(struct vec_int*);
	if (vbo_position)
	{
		glDeleteBuffers(1, &vbo_position);
		vbo_position= 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	GLsizei shaderCount;
	GLsizei shaderNumber;
	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		GLuint* pShaders=(GLuint*)malloc(shaderCount*sizeof(GLuint));
		if(pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&shaderCount,pShaders);
			for(shaderNumber=0;shaderNumber<shaderCount;shaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber]=0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;
		glUseProgram(0);
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();
	if(!currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	/*if(gGLXFBConfig)
	{
		XFree(gGLXFBConfig);
	}*/


	if (gpVertex)
	{
		destroy_vec_float(gpVertex);
	}

	if (gpNormal)
	{
		destroy_vec_float(gpNormal);
	}

	if (gpTexCoords)
	{
		destroy_vec_float(gpTexCoords);
	}
	
	if (gpVertexIndices)
	{
		destroy_vec_int(gpVertexIndices);
	}

	if (gpNormalIndices)
	{
		destroy_vec_int(gpNormalIndices);
	}

	if (gpTextureIndices)
	{
		destroy_vec_int(gpTextureIndices);
	}

	if (gpVertexSorted)
	{
		destroy_vec_float(gpVertexSorted);
	}

	if (gpNormalSorted)
	{
		destroy_vec_float(gpNormalSorted);
	}

	if (gpTextureSorted)
	{
		destroy_vec_float(gpTextureSorted);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}



int destroy_vec_int(struct vec_int* p_vec)
{
	if (p_vec->p)
	{
		free(p_vec->p);
		p_vec->p = NULL;

	}
	if (p_vec)
	{
		free(p_vec);
	}
	p_vec = NULL;
	return true;
}


int destroy_vec_float(struct vec_float* p_vec)
{
	if (p_vec->p)
	{
		free(p_vec->p);
		p_vec->p = NULL;
	}

	if (p_vec)
	{
		free(p_vec);
	}
	p_vec = NULL;
	return true;
}