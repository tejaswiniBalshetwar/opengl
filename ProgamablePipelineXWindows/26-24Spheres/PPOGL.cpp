#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>
#include<X11/Xutil.h>


#include"./../vmath.h"
#include "Sphere.h"

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

using namespace std;
using namespace vmath;

bool gbIsActiveWindow=false;
bool gbIsFullscreen=false;

int giWindowWidth=800;
int giWindowHeight=600;


Window gWindow;
Display *gpDisplay=NULL;
Colormap gColormap;

// attributes enum 
enum
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};


GLuint gShaderProgramObject=0;

GLfloat angleSquare=0.0f;


GLXContext gGLXContext;
XVisualInfo *gpXVisualInfo=NULL;
GLXFBConfig gGLXFBConfig;

//fbConfig stroing function pointer variables
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;



mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

GLuint vao_sphere;

GLuint vbo_position_sphere=0;
GLuint vbo_normal_sphere=0;
GLuint vbo_element_sphere = 0;


GLuint modelUniform=0;
GLuint viewUniform=0;
GLuint projectionUniform=0;
GLuint LKeyIsPressedUniform=0;
GLuint laUniform=0;
GLuint ldUniform=0;
GLuint lsUniform=0;
GLuint kaUniform=0;
GLuint kdUniform=0;
GLuint ksUniform=0;

GLuint lightPositionUniform=0;
GLuint materialShiniessUniform=0;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elementes[2280];

int gNumVertices = 0;
int gNumElements = 0;


int keyPress=0;
bool bRotation=false;
bool bLight=false;

struct light
{
	float light_ambient[4];
	float light_diffuse[4];
	float light_specular[4];
	float light_position[4];
};

light lights;

vec4 lightPosition;
float glMaterialAmbient[4];
float glMaterialDiffuse[4];
float glMaterialSpecular[4];
float glMaterialShininess;

GLfloat windowWidth = 0;
GLfloat windowHeight = 0;

GLfloat lightAngleForX = 0.0f;
GLfloat lightAngleForY = 0.0f;
GLfloat lightAngleForZ = 0.0f;
int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void ToggleFullscreen(void);
	void resize(int,int);
	void createWindow(void);

	XEvent event;
	KeySym keysym;

	static int windowWidth=giWindowWidth;
	static int windowHeight=giWindowHeight;

	bool bDone=false;
	char keys[26];

	createWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;


					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							break;
						case 'a':
						case 'A':
							if(bRotation==false)
							{
								bRotation=true;
							}
							else
							{
								bRotation=false;
							}
							break;
                        case 'l':
						case 'L':
							if(bLight==false)
							{
								bLight=true;
							}
							else
							{
								bLight=false;
							}
							break;
						case 'x':
						case 'X':
							keyPress = 1;
							break;
					
						case 'y':
						case 'Y':
							keyPress = 2;
							break;
						case 'z':
						case 'Z':
							keyPress = 3;
							break;
					}

					break;

					case ConfigureNotify:
						windowWidth=event.xconfigure.width;
						windowHeight=event.xconfigure.height;
						resize(windowWidth,windowHeight);
						break;
					case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1:
								break;
							case 2:
								break;
							case 3:
								break;
							case 4:
								break;
						}
						break;
					case MotionNotify:
						break;
					case 33:
						bDone=true;
						break;
			}
		}
		if(bRotation)
		{
			update();
		}
		display();
	}

	uninitialize();
	return 0;
}


void createWindow(void)
{
	void uninitialize();

	XSetWindowAttributes winAttribs;

	int defaultScreen;
	int styleMask;

	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumberOfFBConfig=0;

	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nError in XOpenDisplay()");
		uninitialize();
		exit(0);
	}
	defaultScreen=XDefaultScreen(gpDisplay);

	//code of GLXFBConfig
	pGLXFBConfig=glXGetFBConfigs(gpDisplay,defaultScreen,&iNumberOfFBConfig);

	int bestFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNumberOfSamples=999;

	for (int i=0;i<iNumberOfFBConfig;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);
		if(pTempXVisualInfo)
		{
			int iSampleBuffer;
			int iSamples;

			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&iSampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&iSamples);

			if(bestNumberOfSamples <0 ||iSampleBuffer && iSamples > bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=iSamples;
			}
			if(worstNumberOfSamples<0 || !iSampleBuffer || iSamples<worstNumberOfSamples)
			{
				worstNumberOfSamples=iSamples;
				worstFrameBufferConfig=i;
			}
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo=NULL;
	}

	bestGLXFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestGLXFBConfig;

	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|VisibilityChangeMask|PointerMotionMask;

	styleMask=CWBackPixel|CWBorderPixel|CWColormap|CWEventMask;


	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error in XCreateWindow()");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY PPOGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);	

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void uninitialize();
	GLuint vertexShaderObject=0;
	GLuint fragmentShaderObject=0;


	//gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	//getting required hardware renderable context for drawing on the screen which is best
	//this function is implementataion dependant for that we define funcrion poiter and the get the address for the same
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	if(glXCreateContextAttribsARB==NULL)
	{
		printf("\nError while getting the function pointer");
		uninitialize();
		exit(0);
	}


	const int attrib[]=
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attrib);
	if(!gGLXContext)
	{
		printf("\n Error in glXCreateContextAttribsARB the required fbConfigs hardware context is not availble");

		const int attrib[]={
			GLX_CONTEXT_MINOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};

		gGLXContext=glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attrib);

	}

	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("WARNING: Context is not hardware rendering context!..\n\n");
	}
	else
	{
		printf("\n\nSUCCESS: Obtained context is hardware rendeing context..\n\n");

	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	GLenum result ;
	result=glewInit();
	if(result!=GLEW_OK)
	{
		printf("\n Error while initializing glew\n\n exiting");
		uninitialize();
		exit(0);
	}

	//shaders

	//vertex shader object
	vertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
	"#version 450 core" \
		"\n" \
		"in vec4 vPosition;								\n" \
		"in vec3 vNormal;								\n" \
		"uniform mat4 u_model_matrix;					\n" \
		"uniform mat4 u_view_matrix;					\n" \
		"uniform mat4 u_projection_matrix;				\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec4 u_light_position;					\n" \
		"out vec3 out_light_direction;					\n" \
		"out vec3 out_t_normal;							\n" \
		"out vec3 out_viewer_vector;					\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n" \
		"		vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;			\n" \
		"		out_t_normal=mat3(u_view_matrix*u_model_matrix) * vNormal;					\n" \
		"		out_light_direction=vec3(u_light_position - eye_coordinates);				\n" \
		"		out_viewer_vector =vec3(-eye_coordinates);									\n" \
		"	}																				\n" \
		"	gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;	\n"  \
		"}";


	//shader source to shaderobject
	glShaderSource(vertexShaderObject,1,(GLchar**)&vertexShaderSourceCode,NULL);

	//glCompileShader
	glCompileShader(vertexShaderObject);
	
	//error checing for shader compilation
	GLint iShadercompileStatus;
	int iInfoLogLength;
	char* szInfoLog=NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShadercompileStatus);
	if(iShadercompileStatus==GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
			if(szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("\n Error in vertex shader: %s",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//fragment shader

	//create shader object
	fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	//shader source code
	const GLchar* fragmentShaderSourceCode=
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_la;								\n" \
		"uniform vec3 u_ld;								\n" \
		"uniform vec3 u_ls;								\n" \
		"uniform int u_LKeyIsPressed;					\n" \
		"uniform vec3 u_ka;								\n" \
		"uniform vec3 u_kd;								\n" \
		"uniform vec3 u_ks;								\n" \
		"uniform float u_material_shininess;			\n" \
		"in vec3 out_light_direction;					\n" \
		"in vec3 out_t_normal;							\n" \
		"in vec3 out_viewer_vector;						\n" \
		"out vec4 fragColor;							\n" \
		"void main(void)								\n" \
		"{												\n" \
		"	vec3 phong_ads_light;						\n" \
		"	if(u_LKeyIsPressed==1)						\n" \
		"	{											\n"	\
		"		vec3 normalized_light_direction=normalize(out_light_direction);													\n"	\
		"		vec3 normalized_t_norm=normalize(out_t_normal);																	\n"	\
		"		float t_dot_ld=max(dot(normalized_light_direction,normalized_t_norm),0.0);										\n" \
		"		vec3 reflection_vetor=reflect(-normalized_light_direction ,normalized_t_norm);									\n" \
		"		vec3 normalized_viewer_vector=normalize(out_viewer_vector);															\n" \
		"		vec3 ambient= u_la * u_ka;																						\n " \
		"		vec3 diffuse=u_ld * u_kd * t_dot_ld;																			\n" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vetor,normalized_viewer_vector),0.0),u_material_shininess);			\n" \
		"		phong_ads_light=ambient + diffuse + specular;																\n" \
		"	}																													\n" \
		"	else																												\n" \
		"	{																													\n" \
		"			phong_ads_light=vec3(1.0,1.0,1.0);																		\n" \
		"	}																													\n" \
		"	fragColor=vec4(phong_ads_light,1.0);																			\n" \
		"}																														\n";

	//specify shader source to shader object
	glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);

	//compile shader source
	glCompileShader(fragmentShaderObject);

	//error checking for compilation


	iShadercompileStatus=0;
	szInfoLog=NULL;
	iInfoLogLength=0;

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShadercompileStatus);
	if(iShadercompileStatus==GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
			if(szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				printf("\nError in fragment shader source code compilation= %s",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//creating program for shaders for gpu
	gShaderProgramObject=glCreateProgram();

	//attach shader to program
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);


	//prelinking the atteibutes
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
	glBindAttribLocation(gShaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");
	//linking program
	glLinkProgram(gShaderProgramObject);

	//linking error 
	GLint iProgramLinkStatus=0;
	iInfoLogLength=0;
	szInfoLog=NULL;
	glGetProgramiv(gShaderProgramObject,GL_LINK_STATUS,&iProgramLinkStatus);

	if(iProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if(iInfoLogLength>0)
		{
			szInfoLog=(char*)malloc(sizeof(char)*iInfoLogLength);
			if(szInfoLog)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,iInfoLogLength,&written,szInfoLog);
				printf("\nError in linking the program:=%s ",szInfoLog );
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//post linking for uniform binding
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	
	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyIsPressed");
	
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	materialShiniessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");




    getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elementes);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	//start recording creation and starting the buffers to access for triangle of gpu

	//for square vertices
	glGenVertexArrays(1,&vao_sphere);
	glBindVertexArray(vao_sphere);
		glGenBuffers(1,&vbo_position_sphere);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_position_sphere);
			glBufferData(GL_ARRAY_BUFFER,sizeof(sphere_vertices),sphere_vertices,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);
	
		//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR,0.0f,0.0f,1.0f);
		glGenBuffers(1,&vbo_normal_sphere);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_normal_sphere);
			glBufferData(GL_ARRAY_BUFFER,sizeof(sphere_normals),sphere_normals,GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
		glBindBuffer(GL_ARRAY_BUFFER,0);

        glGenBuffers(1,&vbo_element_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_element_sphere);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elementes),sphere_elementes,GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	glBindVertexArray(0);

	//initializeing lights
	lights.light_ambient[0] = 0.0f;
	lights.light_ambient[1] = 0.0f;
	lights.light_ambient[2] = 0.0f;
	lights.light_ambient[3] = 0.0f;

	lights.light_diffuse[0] = 1.0f;
	lights.light_diffuse[1] = 1.0f;
	lights.light_diffuse[2] = 1.0f;
	lights.light_diffuse[3] = 0.0f;

	lights.light_specular[0] = 1.0f;
	lights.light_specular[1] = 1.0f;
	lights.light_specular[2] = 1.0f;
	lights.light_specular[3] = 0.0f;


	lights.light_position[0] = 0.0f;
	lights.light_position[1] = 0.0f;
	lights.light_position[2] = 0.0f;
	lights.light_position[3] = 1.0f;


	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glDisable(GL_CULL_FACE);
	//glDisable(GL_FRONT_AND_BACK);
	//percpetive projection matrix load identity
	perspectiveProjectionMatrix=mat4::identity();
    modelMatrix=mat4::identity();
    viewMatrix=mat4::identity();

	resize(giWindowWidth,giWindowHeight);

}

void display()
{
	void Spheres24();

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
		
	//declaration for matrices
	mat4 modelViewMatrix;
	
	mat4 translationMatrix;
	
	//for square

	modelMatrix=mat4::identity();

	translationMatrix=mat4::identity();


	//do necessary multiplication
	//translationMatrix=translate(0.0f,0.0f,-3.0f);


	modelMatrix=modelMatrix*translationMatrix;
	
	//glUniformMatrix4fv(modelUniform,1,GL_FALSE,modelMatrix);
	glUniformMatrix4fv(viewUniform,1,GL_FALSE,viewMatrix);
    glUniformMatrix4fv(projectionUniform ,1,GL_FALSE,perspectiveProjectionMatrix);


	if (bLight)
	{
		glUniform1i(LKeyIsPressedUniform,1);
		if (keyPress == 1)
		{
			lights.light_position[1] = 200.0 * cos(lightAngleForX);
			lights.light_position[2] = 200.0 * sin(lightAngleForX);
		}
		else if (keyPress == 2)
		{
			lights.light_position[2] = 200.0 * cos(lightAngleForY);
			lights.light_position[0] = 200.0 * sin(lightAngleForY);
		}
		else
		{
			lights.light_position[0] = 200.0 * cos(lightAngleForZ);
			lights.light_position[1] = 200.0 * sin(lightAngleForZ);
		}
		glUniform3fv(laUniform,1,lights.light_ambient);		//glLightfv() 
		glUniform3fv(ldUniform,1, lights.light_diffuse);		//glLightfv() 
		glUniform3fv(lsUniform,1, lights.light_specular);		//glLightfv() 
		glUniform4fv(lightPositionUniform,1, lights.light_position); //glLightfv() for position
	}
	else
	{
		glUniform1i(LKeyIsPressedUniform,0);
	}
	
	
	Spheres24();
	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void Spheres24()
{
	void drawSphere();
	mat4 translationMatrix;

	//make matrix identity 
	translationMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, 4.0f, -25.0f);

	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0215;
	glMaterialAmbient[1] = 0.1745;
	glMaterialAmbient[2] = 0.0215;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.07568;
	glMaterialDiffuse[1] = 0.61424;
	glMaterialDiffuse[2] = 0.07568;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.633;
	glMaterialSpecular[1] = 0.727811;
	glMaterialSpecular[2] = 0.633;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.6 *128.0f;

	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.135f;
	glMaterialAmbient[1] = 0.2225f;
	glMaterialAmbient[2] = 0.1575f;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.54f;
	glMaterialDiffuse[1] = 0.89f;
	glMaterialDiffuse[2] = 0.63f;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.316228;
	glMaterialSpecular[1] = 0.316228;
	glMaterialSpecular[2] = 0.316228;
	glMaterialSpecular[3] = 1.0f;

	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.1 *128.0f;

	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05375;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.06625;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.18275;
	glMaterialDiffuse[1] = 0.17;
	glMaterialDiffuse[2] = 0.22525;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.332741;
	glMaterialSpecular[1] = 0.328634;
	glMaterialSpecular[2] = 0.346435;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.3 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.25;
	glMaterialAmbient[1] = 0.20725;
	glMaterialAmbient[2] = 0.20725;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 1.0;
	glMaterialDiffuse[1] = 0.829;
	glMaterialDiffuse[2] = 0.829;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.296648;
	glMaterialSpecular[1] = 0.296648;
	glMaterialSpecular[2] = 0.296648;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.088 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.1745;
	glMaterialAmbient[1] = 0.01175;
	glMaterialAmbient[2] = 0.01175;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.61424;
	glMaterialDiffuse[1] = 0.04136;
	glMaterialDiffuse[2] = 0.04136;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.727811;
	glMaterialSpecular[1] = 0.626959;
	glMaterialSpecular[2] = 0.626959;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.6 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 1st col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-5.0f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.1;
	glMaterialAmbient[1] = 0.18725;
	glMaterialAmbient[2] = 0.1745;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.396;
	glMaterialDiffuse[1] = 0.74151;
	glMaterialDiffuse[2] = 0.69102;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.297254;
	glMaterialSpecular[1] = 0.30829;
	glMaterialSpecular[2] = 0.306678;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.1 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//2nd column
	//2nd row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, 4.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.329412;
	glMaterialAmbient[1] = 0.223529;
	glMaterialAmbient[2] = 0.027451;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.780392;
	glMaterialDiffuse[1] = 0.568627;
	glMaterialDiffuse[2] = 0.113725;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.992157;
	glMaterialSpecular[1] = 0.941176;
	glMaterialSpecular[2] = 0.807843;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.21794872 *128.0f;

	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.2125;
	glMaterialAmbient[1] = 0.1275f;
	glMaterialAmbient[2] = 0.054f;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.714;
	glMaterialDiffuse[1] = 0.4284;
	glMaterialDiffuse[2] = 0.18144;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.393548;
	glMaterialSpecular[1] = 0.271906;
	glMaterialSpecular[2] = 0.166721;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.2 *128.0f;

	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.25;
	glMaterialAmbient[1] = 0.25;
	glMaterialAmbient[2] = 0.25;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.4;
	glMaterialDiffuse[1] = 0.4;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.774597;
	glMaterialSpecular[1] = 0.774597;
	glMaterialSpecular[2] = 0.774597;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.6 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.19125;
	glMaterialAmbient[1] = 0.0735;
	glMaterialAmbient[2] = 0.0225;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.7038;
	glMaterialDiffuse[1] = 0.27048;
	glMaterialDiffuse[2] = 0.0828;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.256777;
	glMaterialSpecular[1] = 0.137622;
	glMaterialSpecular[2] = 0.086014;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.1 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.24725;
	glMaterialAmbient[1] = 0.1995;
	glMaterialAmbient[2] = 0.0745;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.75164;
	glMaterialDiffuse[1] = 0.60648;
	glMaterialDiffuse[2] = 0.22648;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.628281;
	glMaterialSpecular[1] = 0.555802;
	glMaterialSpecular[2] = 0.366065;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.4 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 2nd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(-2.50f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.19225;
	glMaterialAmbient[1] = 0.19225;
	glMaterialAmbient[2] = 0.19225;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.50754;
	glMaterialDiffuse[1] = 0.50754;
	glMaterialDiffuse[2] = 0.50754;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.508273;
	glMaterialSpecular[1] = 0.508273;
	glMaterialSpecular[2] = 0.508273;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.4 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3rd column
	//2nd row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, 4.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.01;
	glMaterialDiffuse[1] = 0.01;
	glMaterialDiffuse[2] = 0.01;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.50;
	glMaterialSpecular[1] = 0.50;
	glMaterialSpecular[2] = 0.50;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.1f;
	glMaterialAmbient[2] = 0.06f;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.0;
	glMaterialDiffuse[1] = 0.50980392;
	glMaterialDiffuse[2] = 0.50980392;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.50196078;
	glMaterialSpecular[1] = 0.50196078;
	glMaterialSpecular[2] = 0.50195078;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;

	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.1;
	glMaterialDiffuse[1] = 0.35;
	glMaterialDiffuse[2] = 0.1;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.45;
	glMaterialSpecular[1] = 0.55;
	glMaterialSpecular[2] = 0.45;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.0;
	glMaterialDiffuse[2] = 0.0;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.6;
	glMaterialSpecular[2] = 0.6;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.55;
	glMaterialDiffuse[1] = 0.55;
	glMaterialDiffuse[2] = 0.55;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.70;
	glMaterialSpecular[1] = 0.70;
	glMaterialSpecular[2] = 0.70;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(0.0f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.0;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.60;
	glMaterialSpecular[1] = 0.60;
	glMaterialSpecular[2] = 0.50;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.25 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	
	//4th column
	//1st 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, 4.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.02;
	glMaterialAmbient[1] = 0.02;
	glMaterialAmbient[2] = 0.02;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.01;
	glMaterialDiffuse[1] = 0.01;
	glMaterialDiffuse[2] = 0.01;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.4;
	glMaterialSpecular[1] = 0.4;
	glMaterialSpecular[2] = 0.4;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//2 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, 2.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.05;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.4;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.5;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.04;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.7;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;

	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//3 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, 1.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.0;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.4;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.04;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.04;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//4 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, -0.50f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05;
	glMaterialAmbient[1] = 0.0;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.4;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.04;
	glMaterialSpecular[2] = 0.04;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

	//5 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, -2.0f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.05;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.5;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.7;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();


	//6 row 3rd col
	//make matrix identity 
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	//do translation matrix
	translationMatrix = translate(2.5f, -3.5f, -25.0f);
	//do matrix multiplication
	modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);

	glMaterialAmbient[0] = 0.05;
	glMaterialAmbient[1] = 0.05;
	glMaterialAmbient[2] = 0.0;
	glMaterialAmbient[3] = 1.0f;
	glUniform3fv(kaUniform, 1, glMaterialAmbient);	//glMaterialfv();
	glMaterialDiffuse[0] = 0.5;
	glMaterialDiffuse[1] = 0.5;
	glMaterialDiffuse[2] = 0.4;
	glMaterialDiffuse[3] = 1.0f;
	glUniform3fv(kdUniform, 1, glMaterialDiffuse);	//glMaterialfv();
	glMaterialSpecular[0] = 0.7;
	glMaterialSpecular[1] = 0.7;
	glMaterialSpecular[2] = 0.04;
	glMaterialSpecular[3] = 1.0f;
	glUniform3fv(ksUniform, 1, glMaterialSpecular);	//glMaterialfv();
	glMaterialShininess = 0.078125 *128.0f;
	glUniform1f(materialShiniessUniform, glMaterialShininess);	//glMaterialfv();

	drawSphere();

}

void drawSphere()
{
	glBindVertexArray(vao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	glBindVertexArray(0);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	perspectiveProjectionMatrix = perspective(25.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}
void update()
{
	lightAngleForX = lightAngleForX + 0.020f;
	if (lightAngleForX >= 360.0f)
	{
		lightAngleForX = 0.0f;
	}

	lightAngleForY = lightAngleForY + 0.020f;
	if (lightAngleForY > 360.0f)
	{
		lightAngleForY = 0.0f;
	}

	lightAngleForZ = lightAngleForZ + 0.020f;
	if (lightAngleForZ > 360.0f)
	{
		lightAngleForZ = 0.0f;
	}
	
}
void uninitialize()
{
	if(vao_sphere)
	{
		glDeleteVertexArrays(1,&vao_sphere);
		vao_sphere=0;
	}

	if(vbo_position_sphere)
	{
		glDeleteBuffers(1,&vbo_position_sphere);
		vbo_position_sphere=0;
	}

	if(vbo_normal_sphere)
	{
		glDeleteBuffers(1,&vbo_normal_sphere);
		vbo_normal_sphere=0;
	}


	if(gShaderProgramObject)
	{
		int iShaderCount=0;
		int iShaderNumber=0;
		glUseProgram(gShaderProgramObject);
			glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&iShaderCount);

			GLuint  *pShaders=(GLuint*)malloc(sizeof(GLuint)*iShaderCount);

			for(iShaderNumber=0;iShaderNumber<iShaderCount;iShaderNumber++)
			{
				glDetachShader(gShaderProgramObject,pShaders[iShaderNumber]);
				glDeleteShader( pShaders[iShaderNumber]);
				pShaders[iShaderNumber]=0;
			}
			free(pShaders);

		glUseProgram(0);
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject=0;

	}

	GLXContext currentContext;
	currentContext=glXGetCurrentContext();
	if(currentContext && gGLXContext==currentContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

