#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include <GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#define CHECKIMAGEWIDTH 64
#define CHECKIMAGEHEIGHT 64

GLubyte checkImage[CHECKIMAGEWIDTH][CHECKIMAGEHEIGHT][4];

using namespace std;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

GLuint texImage;


int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void resize(int,int);
	void CreateWindow(void);
	void ToggleFullscreen(void);

	XEvent event;
	KeySym keysym;
	int windowWidth=800;
	int windowHeight=600;

	bool bDone=false;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
						
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		display();
	}

	uninitialize();

	return 0;
}

void CreateWindow()
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;
	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in CreateWindow at XOpenDisplay");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in CreateWindow at glXChooseVisual");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=StructureNotifyMask|ButtonPressMask|KeyPressMask|VisibilityChangeMask|ExposureMask|PointerMotionMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("\nError in CreateWindow at XCreateWindow");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 :1 ;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}

void initialize()
{
	void resize(int,int);
	void LoadTextures();

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	LoadTextures();

	resize(giWindowWidth,giWindowHeight);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-2.0f,-1.0f,0.0f);
		
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-2.0f,1.0f,0.0f);
		
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);

		glTexCoord2f(1.0f,0.0f);
		glVertex3f(0.0f,-1.0f,0.0f);

		//texture 2D
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);
		
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(1.0f,1.0f,0.0f);
		
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(2.414210f,1.0f,-1.414210f);

		glTexCoord2f(1.0f,0.0f);
		glVertex3f(2.414210f,-1.0f,-1.414210f);
	glEnd();

	glFlush();
}

void LoadTextures(void)
{
	void MakeCheckImage(void);

	MakeCheckImage();

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glGenTextures(1,&texImage);
	glBindTexture(GL_TEXTURE_2D,texImage);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,CHECKIMAGEWIDTH,CHECKIMAGEHEIGHT,0,GL_RGBA,GL_UNSIGNED_BYTE,checkImage);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

}

void MakeCheckImage()
{
	int i,j,c;
	for(i=0;i<CHECKIMAGEHEIGHT;i++)
	{
		for(j=0;j<CHECKIMAGEHEIGHT;j++)
		{
			c=(((i& 0x8)==0) ^ ((j & 0x8)==0))*255;
			checkImage[i][j][0]=(GLubyte)c;
			checkImage[i][j][1]=(GLubyte)c;
			checkImage[i][j][2]=(GLubyte)c;
			checkImage[i][j][4]=(GLubyte)c;
		}
	}
}

void resize(int width,int height )
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)	
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}