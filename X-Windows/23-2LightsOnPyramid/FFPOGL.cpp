#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xutil.h>
#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;

GLXContext gGLXContext;
Window gWindow;

Colormap gColormap;

int giWindowWidth=800;
int giWindowHeight=600;



GLfloat lightAmbientOne[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseOne[]={1.0f,0.0f,0.0f,1.0f};
GLfloat lightSpecularOne[]={1.0f,0.0f,0.0f,1.0f};
GLfloat lightPositionOne[]={-3.0f,1.0f,0.0f,1.0f};

GLfloat lightAmbientTwo[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseTwo[]={0.0f,0.0f,1.0f,1.0f};
GLfloat lightSpecularTwo[]={0.0f,0.0f,1.0f,1.0f};
GLfloat lightPositionTwo[]={3.0f,1.0f,0.0f,1.0f};

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialShininess[]={128.0f};

GLfloat bLight=false;
GLfloat angle=0.0f;

int main(void)
{
	void createWindow(void);
	void initialize(void);
	void uninitialize(void);
	void ToggleFullscreen(void);
	void display(void);
	void update(void);
	void resize(int,int);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	bool bDone=false;
	char keys[26];

	XEvent event;
	KeySym keysym;

	createWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						break;
				}

				XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(gbIsFullscreen==false)
						{
							gbIsFullscreen=true;
							ToggleFullscreen();
						}
						else
						{
							gbIsFullscreen=false;
							ToggleFullscreen();
						}
						break;
					case 'l':
					case 'L':
						if(bLight==false)
						{
							bLight=true;
							glEnable(GL_LIGHTING);
						}
						else
						{
							bLight=false;
							glDisable(GL_LIGHTING);
						}
						break;
				}
				break;
			case MotionNotify:
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					case 4:
						break;

				}
				break;
			case ConfigureNotify:
				windowWidth=event.xconfigure.width;
				windowHeight=event.xconfigure.height;
				resize(windowWidth,windowHeight);
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone=true;
				break;

			}
		}
		update();
		display();
	}

	uninitialize();

	return(0);
}	


void createWindow(void)
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in XOpenDisplay()");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("\n Error in initialize at glXChooseVisual()");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=StructureNotifyMask|VisibilityChangeMask|KeyPressMask|ButtonPressMask|ExposureMask|PointerMotionMask;

	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("\n Error in XCreateWindow()");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}


void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}
void initialize()
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbientOne);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuseOne);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecularOne);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1,GL_AMBIENT,lightAmbientTwo);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,lightDiffuseTwo);
	glLightfv(GL_LIGHT1,GL_SPECULAR,lightSpecularTwo);
	glEnable(GL_LIGHT1);

	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);


	resize(giWindowWidth,giWindowHeight);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-4.0f);

	glRotatef(angle,0.0f,1.0f,0.0f);
	glBegin(GL_TRIANGLES);
		glNormal3f(0.0f,0.441214f,0.894427f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

		glNormal3f(0.894427f,0.441214f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		glNormal3f(-0.894427f,0.441214f,0.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);

		glNormal3f(0.0f,0.441214f,-0.894427f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
	glEnd();
	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}


}

void update()
{
	angle=angle+0.5f;
	if(angle>360.0f)
	{
		angle=0.0;
	}
}