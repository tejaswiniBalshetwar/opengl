#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/Xutil.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

using namespace std;

int giWindowWidth=800;
int giWindowHeight=600;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;

GLXContext gGLXContext;

GLUquadric *sun=NULL;
GLUquadric *earth=NULL;
GLUquadric *moon=NULL;

GLint angleForSun=0;
GLint angleForEarth=0;
GLint angleForMoon=0;


int main(void)
{
	void CreateWindow(void);
	void initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);
	void resize(int,int);
	void ToggleFullscreen(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	bool bDone=false;
	XEvent event;
	KeySym keysym;
	char keys[26];

	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
						break;
						case 'y':
							angleForSun=(angleForSun+6)%360;
							break;
						case 'Y':
							angleForSun=(angleForSun-6)%360;
							break;

						case 'd':
							angleForEarth=(angleForEarth+6)%360;
							break;
						case 'D':
							angleForEarth=(angleForEarth-6)%360;
							break;

					}	
				break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		display();
	}

	uninitialize();

}


void CreateWindow()
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
			GLX_RGBA,
			GL_DOUBLEBUFFER,True,
			GLX_RED_SIZE,8,
			GLX_GREEN_SIZE,8,
			GLX_BLUE_SIZE,8,
			GLX_ALPHA_SIZE,8,
			GLX_DEPTH_SIZE,24,
			None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in XOpenDisplay\n");
		uninitialize();
		exit(0);
	}
	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in glXChooseVisualInfo\n");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|ExposureMask|KeyPressMask|ButtonPressMask|StructureNotifyMask|PointerMotionMask;

	styleMask=CWBackPixel|CWBorderPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("Error in XCreateWindow\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.window=gWindow;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 :1;

	printf("xev.xclient.data.l[0]: %ld \n", xev.xclient.data.l[0]);
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	printf("fullscreen: %ld \n", fullscreen);
	xev.xclient.data.l[1]=fullscreen;
	printf("xev.xclient.data.l[1]: %ld \n", xev.xclient.data.l[1]);

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	resize(giWindowWidth,giWindowHeight);

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0f,0.0f,4.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
	glPushMatrix();

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	sun=gluNewQuadric();
	glColor3f(1.0f,1.0f,0.0f);
	gluSphere(sun,0.75f,30,30);
	glPopMatrix();

	glRotatef(GLfloat(angleForSun),0.0f,1.0f,0.0f);
	glTranslatef(1.50f,0.0f,0.0f);

	glPushMatrix();
	earth=gluNewQuadric();
	glColor3f(0.4f,0.9f,1.0f);
	gluSphere(earth,0.4,20,20);

	glRotatef(angleForEarth,0.0f,1.0f,0.0f);
	glTranslatef(0.60f,0.0f,0.0f);

	glColor3f(1.0f,1.0f,1.0f);
	moon=gluNewQuadric();
	gluSphere(moon,0.1,15,15);

	glPopMatrix();

	glXSwapBuffers(gpDisplay,gWindow);
}
void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize()
{
	GLXContext CurrentGLXContext;
	CurrentGLXContext=glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);

	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);

	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
}



void update()
{

}