#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
XVisualInfo gxVisualInfo;
Colormap gColormap;
Window gWindow;

int giWindowWidth=800;
int giWindowHeight=600;

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static int winWidth=giWindowWidth;
	static int winHeight=giWindowHeight;

	char keys[26];

	static XFontStruct *pxFontStruct=NULL;
	static GC gc;
	XGCValues gcValues;
	XColor text_color;
	char str[255]="HELLO WORLD!!!";
	int strLength;
	int strWidth;
	int fontHeight;


	CreateWindow();

	XEvent event;
	KeySym keysym;

	while(1)
	{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
				pxFontStruct=XLoadQueryFont(gpDisplay,"fixed");
				if(pxFontStruct==NULL)
				{
					printf("Error while XLoadQueryFont\n");
					uninitialize();
					exit(0);

				}

				break;
			case KeyPress:
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
				switch(keysym)
				{
					case XK_Escape:
						uninitialize();
						exit(0);
				}
				XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(gbIsFullscreen==false)
						{
							ToggleFullscreen();
							gbIsFullscreen=true;
						}
						else
						{
							ToggleFullscreen();
							gbIsFullscreen=false;
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					case 4:
						break;
				}
				break;
			case MotionNotify:
				break;
			case Expose:
				gc=XCreateGC(gpDisplay,gWindow,0,&gcValues);

				XSetFont(gpDisplay,gc,pxFontStruct->fid);
				XAllocNamedColor(gpDisplay,gColormap,"green",&text_color,&text_color);

				XSetForeground(gpDisplay,gc,text_color.pixel);

				strLength=strlen(str);
				strWidth=XTextWidth(pxFontStruct,str,strLength);

				fontHeight=pxFontStruct->ascent+pxFontStruct->descent;

				XDrawString(gpDisplay,gWindow,gc,(winWidth/2- strWidth/2),(winHeight/2 - fontHeight/2),str,strLength);	

				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				break;
			case DestroyNotify:
				break;
			case 33:
				uninitialize();
				exit(0);
		}
	}

	uninitialize();
	return(0);
}


void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error while opening connection to the server\n\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	defaultDepth=DefaultDepth(gpDisplay,defaultScreen);

	Status status=XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gxVisualInfo);
	if(status==0)
	{
		printf("Error while getting matching visual\n\n");
		uninitialize();
		exit(0);
	}

	//window Attribut
	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gxVisualInfo.screen),gxVisualInfo.visual,AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|StructureNotifyMask|ButtonPressMask|KeyPressMask|PointerMotionMask;
	styleMask=CWBorderPixel|CWEventMask|CWBackPixel|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gxVisualInfo.screen),0,0,giWindowWidth,giWindowHeight,0,gxVisualInfo.depth,InputOutput,gxVisualInfo.visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("Error while creating the window\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My XWindow Application");

	Atom WindowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&WindowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev;

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]= gbIsFullscreen ? 0 : 1 ;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gxVisualInfo.screen),False,StructureNotifyMask,&xev);

}

void uninitialize()
{
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}