#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/Xlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisulInfo=NULL;
Colormap gColormap;
Window gWindow;

GLXContext gGLXContext;
bool gbIsFullscreen=false;

bool bLight=false;

GLfloat lightAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat lightPosition[]={1.0f,1.0f,1.0f,1.0f};
GLfloat lightSpecular[]={100.0f,100.0f,100.0f,1.0f};

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0};
GLfloat materialShininess[]={128.0f};

GLUquadric *quadric=NULL;


int main(void)
{	
	void initialize(void);
	void uninitialize(void);
	void createWindow(void);
	void display(void);
	void resize(int,int);
	void ToggleFullscreen(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;


	createWindow();
	initialize();

	bool bDone=false;
	char keys[26];

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							break;
						case 'b':
						case 'B':
							if(bLight==false)
							{
								glEnable(GL_LIGHTING);
								bLight=true;
							}
							else
							{
								glDisable(GL_LIGHTING);
								bLight=false;
							}
					}

					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		display();
	}


	uninitialize();
	return 0;
}

void createWindow()
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in XOpenDisplay");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisulInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(glXChooseVisual==NULL)
	{
		printf("Error in glXChooseVisual\n");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisulInfo->screen),gpXVisulInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.event_mask=PointerMotionMask|VisibilityChangeMask|StructureNotifyMask|ExposureMask|KeyPressMask|ButtonPressMask;

	styleMask=CWColormap|CWBorderPixel|CWBackPixel|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisulInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisulInfo->depth,InputOutput,gpXVisulInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("\n Error in XCreateWindow ");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My OPENGL APPLICATION");
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisulInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisulInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);


	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecular);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);
	glEnable(GL_LIGHT0);

	//material
	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);


	resize(giWindowWidth,giWindowHeight);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glTranslatef(0.0f,0.0f,-4.0f);
	quadric=gluNewQuadric();

	gluSphere(quadric,0.75f,30,30);



	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisulInfo)
	{
		free(gpXVisulInfo);
		gpXVisulInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}



