#include<iostream>
#include<stdio.h>
#include<memory.h>
#include<math.h>
#include<stdlib.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>


int giWindowWidth=800;
int giWindowHeight=600;

bool gbIsFullscreen=false;
Colormap gColormap;
Window gWindow;
GLXContext gGLXContext;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;


//plane
GLfloat translateI=-7.0f;

GLfloat translateA=7.0f;
GLfloat translateN=5.0f;
GLfloat translateI2=-5.0f;

GLfloat transperancyOfSquare=1.0f;


bool ShowPlanes=false;
bool ShowTricolor=false;

GLfloat incAngle=270.0f;
GLfloat upperPlaneAngle=M_PI;
GLfloat upperPlaneStopAngle=(3.0f*M_PI)/2.0f;

GLfloat lowerPlaneAngle=M_PI;
GLfloat decAngle=90.0f;

GLfloat incAngle2=0.0f;
GLfloat upperPlaneAngle2=(3.0f*M_PI)/2.0f;
GLfloat upperPlaneStopAngle2=2.5f*M_PI;

GLfloat lowerPlaneAngle2=(M_PI/2.0f);
GLfloat decAngle2=360.0f;

GLfloat translateAllInXDirection=-2.499705f;

bool meet=false; 
bool crossTheA=false;
GLfloat translateMiddle=-7.1f;

GLfloat XTranslatePointUP=0.0f;
GLfloat YTranslatePointUP=0.0f;

GLfloat XTranslatePointDown=0.0f;
GLfloat YTranslatePointDown=0.0f;

int main(void)
{	
	void CreateWindow(void);
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void resize(int,int);
	void update(void);
	void ToggleFullscreen();

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	bool bDone=false;
	char keys[26];
	XEvent event;
	KeySym keysym;


	CreateWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case MotionNotify:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		//update(); call
		display();
	}

	uninitialize();

	return 0;
}


void CreateWindow(void)
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int styleMask;
	int defaultScreen;
	
	static int frameBufferAttaributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in XOpenDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttaributes);
	if(gpXVisualInfo==NULL)
	{
		printf("Erro in glXChooseVisual\n");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	winAttribs.event_mask=PointerMotionMask|VisibilityChangeMask|StructureNotifyMask|ExposureMask|KeyPressMask|ButtonPressMask;


	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("Error in XCreateWindow\n\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My XWindow Application");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}	

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),false,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,1.0f);
	resize(giWindowWidth,giWindowHeight);

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	 void drawI();
    void drawN();
    void drawD();
   // void drawI();
    void drawA();
    void drawIndia(void);
    void translationsOfIndia(void);
    void PlaneTranslations(void);
    void drawPlane(void);
    void drawUpperPlane(void);
    void drawLowerPlane(void);
    void drawMiddelePlane(void);
     
    //variable declarations
 

    drawIndia();
   
   if(ShowPlanes)
   {
        //upperplane
        drawUpperPlane();

        //Lower Plane
        drawLowerPlane();
       
        //Middle Plane
        drawMiddelePlane();

   }
	glXSwapBuffers(gpDisplay,gWindow);
	 translationsOfIndia();
    if(ShowPlanes)
    {
        PlaneTranslations();
    }

}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext!=NULL && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}


void drawIndia()
{
    void drawI();
    void drawN();
    void drawD();
   // void drawI();
    void drawA();

    //variable dedclarations

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(-2.5f,0.0f,-8.0f);
    glTranslatef(translateI,0.0f,-8.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();


    /*-------N----------*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    /*glTranslatef(-2.0f,0.0f,-8.0f);*/
    glTranslatef(-2.0f,translateN,-8.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawN();
    glEnd();


    /*-------------D---------------------*/
   

    //Drawing D's Line
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-8.0f);

    glLineWidth(5.0f);
    //glColor3f(RColor,GColor,BColor);
    glBegin(GL_LINES);
        drawI();
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-8.0f);
    glPointSize(3.0f);
    glBegin(GL_POINTS);
        drawD();
    glEnd();

     glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-8.0f);
    glBegin(GL_QUADS);
        glColor4f(0.0f,0.0f,0.0f,transperancyOfSquare);
        glVertex3f(-0.30f,1.20f,0.0f);

        glVertex3f(-0.3,-1.20,0.0f);
        glVertex3f(1.20f,-1.20f,0.0f);
        glVertex3f(1.20f,1.20f,0.0f);
    glEnd();


    /*------------------I--------------------*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
   /* glTranslatef(1.0f,0.0f,-8.0f);*/
     glTranslatef(1.0f,translateI2,-8.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();


    /*--------------A-------------*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(2.0f,0.0f,-8.0f);
    glTranslatef(translateA,0.0f,-8.0f);
    
    glLineWidth(4.0f);
    glBegin(GL_LINES);
        drawA();
    glEnd();

}


void translationsOfIndia(void)
{
     if(translateI<=-2.5f)
    {
        translateI=translateI+0.005f;
    }


    if(translateA>=2.0f && translateI>=-2.5f )
    {
       //fprintf(gpFile, "translateA=%f\n",translateA);
        translateA=translateA-0.05f;
    }

    if(translateN>=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        translateN=translateN-0.05f;
    }

    if( translateI2 <= 0.0f && translateN<=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        translateI2=translateI2+0.05f;
    }

    if( transperancyOfSquare>=0.0f && translateI2 >= 0.0f && translateN<=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        transperancyOfSquare=transperancyOfSquare-0.5;
    }

    if(translateI2 >= 0.0f && translateN<=0.0f && translateA<=2.0f && translateI>=-2.5f )
    {
        ShowPlanes=true;
    }
   
    if(XTranslatePointUP>=3.499705 && XTranslatePointDown>=3.499705 && translateMiddle>=3.499705)
    {
        ShowTricolor=true;
    }

}


void drawI(void)
{
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);
}

void drawN(void)
{
    //|\|
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
  


}

void drawD()
{
   
   GLfloat RColor=0.0706f;
    GLfloat GColor=0.3831f;
    GLfloat BColor=0.02745f;
    for(GLfloat angle=(3*M_PI/2.0f);angle<=(5*M_PI)/2;angle+=0.02f)
    {
        glVertex3f(cos(angle),sin(angle),0.0f);
        glColor3f(RColor,GColor,BColor);
        if(RColor<=1.0)
        {
            RColor+=0.006f;
            
        }
        if(GColor<=0.6)
        {
            GColor+=0.006f;
        }
        if(BColor<=0.2)
        {
            BColor+=0.006f;
        }
    }


}

void drawA()
{
    //variable declarations
    GLfloat x1ForAHorizontalLine=0.0f;
    GLfloat y1ForAHorizontalLine=0.0f;
    
    GLfloat x2ForAHorizontalLine=0.0f;
    GLfloat y2ForAHorizontalLine=0.0f;
    


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.50f,-1.0f,0.0f);

    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-0.50f,-1.0f,0.0f);


      
    x1ForAHorizontalLine=(0.50f+0.0f)/2.0f;
    y1ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;
    
    x2ForAHorizontalLine=(-0.50f+(0.0f))/2.0f;
    y2ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;

    if(ShowTricolor)
   {
       glColor3f(1.0f,0.6f,0.2f);
       glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine,0.0f);
       glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine,0.0f);
   
       glColor3f(1.0f,1.0f,1.0f);
       glVertex3f(x1ForAHorizontalLine-0.01f,y1ForAHorizontalLine-0.04f,0.0f);
       glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine-0.04f,0.0f);
   
       glColor3f(0.0706f,0.3831f,0.02745f);
       glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine-0.08f,0.0f);
       glVertex3f(x2ForAHorizontalLine-0.02f,y2ForAHorizontalLine-0.08f,0.0f);
   }
    
}


void drawUpperPlane(void)
{
    void drawPlane(void);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLineWidth(8.0f); 
     if(meet==false)
     {
        XTranslatePointUP=-2.5f+4.60f*cos(upperPlaneAngle);
        YTranslatePointUP=4.60f+4.60f*sin(upperPlaneAngle);
        //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointUP,YTranslatePointUP);
        glTranslatef(XTranslatePointUP ,YTranslatePointUP,-10.0f);
        if(incAngle<=360.0f || incAngle>0.0f)
        {
            glRotatef(incAngle,0.0f,0.0f,1.0f);
        }
     }
     else if(translateAllInXDirection>3.50f)
     {
       
         XTranslatePointUP=2.5f+4.60f*cos(upperPlaneAngle2);
         YTranslatePointUP=4.60f+4.60f*sin(upperPlaneAngle2);
        // fprintf(gpFile, "XTranslatePoint2:%f  YTranslatePoint2%f\n",XTranslatePointUP2,YTranslatePointUP2);
        glTranslatef(XTranslatePointUP ,YTranslatePointUP,-10.0f);

        if(incAngle2<=90.0f || incAngle2>0.0f)
        {
            glRotatef(incAngle2,0.0f,0.0f,1.0f);
        }
     }
     else 
     {
         glTranslatef(translateAllInXDirection ,0.0f,-10.0f);
     }

    glScalef(0.45f,0.45f,0.45f);
    drawPlane();
    

}

void drawLowerPlane(void)
{
    void drawPlane(void);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLineWidth(6.0f);
   
     if(meet==false)
     {
         XTranslatePointDown=-2.5f+4.60f*cos(lowerPlaneAngle);
         YTranslatePointDown=-4.60f+4.60f*sin(lowerPlaneAngle);
         //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
        glTranslatef(XTranslatePointDown,YTranslatePointDown,-10.0f);
        if(decAngle<=90.0f || decAngle>0.0f)
        {
            glRotatef(decAngle,0.0f,0.0f,1.0f);
        }
     }

     else if(translateAllInXDirection>3.50f)
     {  
        XTranslatePointDown=2.5f+4.60f*cos(lowerPlaneAngle2);
        YTranslatePointDown=-4.60f+4.60f*sin(lowerPlaneAngle2);
         //fprintf(gpFile, "XTranslatePoint:%f  YTranslatePoint%f\n",XTranslatePointDown,YTranslatePointDown);
        glTranslatef(XTranslatePointDown,YTranslatePointDown,-10.0f);
        if(decAngle2<=360.0f || decAngle2>270.0f)
        {
            glRotatef(decAngle2,0.0f,0.0f,1.0f);
        }
     }
     else
     {
        glTranslatef(translateAllInXDirection ,0.0f,-10.0f);
     }

    
    
    glScalef(0.45f,0.45f,0.45f);
    drawPlane();

}

void drawMiddelePlane(void)
{
    void drawPlane(void);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor3f(1.0f,0.0f,0.0f);
    if(meet==false)
    {
        glTranslatef(translateMiddle,0.0f,-10.0f);
    }
    else if(translateAllInXDirection>3.50f)
    {
        glTranslatef(translateMiddle,0.0f,-10.0f);
    }
    else
    {
        glTranslatef(translateAllInXDirection ,0.0f,-10.0f);
    }
    glScalef(0.45f,0.45f,0.45f);
    drawPlane();
}

void drawPlane(void)
{
   /* glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_TRIANGLES);
   glColor3f(0.0729f,0.886f,0.9333f);
    glVertex3f(1.0f,0.0f,0.0f);
    glVertex3f(0.0,-0.35f,0.0f);
    glVertex3f(0.0f,0.35f,0.0f);

    glEnd();

   /* glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_QUADS);
        glColor3f(0.0729f,0.886f,0.9333f);
        glVertex3f(0.0f,0.350f,0.0f);
        glVertex3f(-1.0f,0.35f,0.0f);
        glVertex3f(-1.0f,-0.35,0.0f);
        glVertex3f(0.0,-0.35f,0.0f);
    glEnd();

  /*  glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_POLYGON);
        glColor3f(0.0729f,0.886f,0.9333f);
        glVertex3f(-1.0f,0.35f,0.0f);
        //up
        glVertex3f(-2.0f,1.75f,0.0f);
        glVertex3f(-2.4f,1.75f,0.0f);
        glVertex3f(-1.85f,0.35f,0.0f);
        glVertex3f(-3.0f,0.35f,0.0f);
    
        //down
        glVertex3f(-3.0f,-0.35f,0.0f);
        glVertex3f(-1.85f,-0.35f,0.0f);
        glVertex3f(-2.4f,-1.75f,0.0f);
        glVertex3f(-2.0f,-1.75f,0.0f);
        //end at same point
        glVertex3f(-1.0f,-0.35f,0.0f);
    glEnd();

  /*  glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f,0.0f,-10.0f);*/
    glBegin(GL_POLYGON);
        glColor3f(0.0729f,0.886f,0.9333f);
        glVertex3f(-3.0f,0.35f,0.0f);
        glVertex3f(-3.5f,1.0,0.0f);
        glVertex3f(-3.8f,1.0,0.0f);


        glVertex3f(-3.4f,0.0f,0.0f);

        glVertex3f(-3.8f,-1.0,0.0f);
        glVertex3f(-3.5f,-1.0,0.0f);
        glVertex3f(-3.0f,-0.35f,0.0f);

    glEnd();

     glBegin(GL_LINES);
       
        glVertex3f(-2.5f,0.12f,0.0f);
        glVertex3f(-4.5f,0.12f,0.0f);

       
        glVertex3f(-3.44f,0.01f,0.0f);
        glVertex3f(-4.5f,0.01f,0.0f);

       
        glVertex3f(-3.44f,-0.06f,0.0f);
        glVertex3f(-4.5f,-0.06f,0.0f);


    glEnd();


    glBegin(GL_LINES);
        glColor3f(1.0f,0.6f,0.2f);
        glVertex3f(-3.44f,0.12f,0.0f);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-5.0f,0.12f,0.0f);

        glColor3f(1.0f,1.0f,1.0f);
        glVertex3f(-3.44f,0.01f,0.0f);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-5.0f,0.01f,0.0f);

        glColor3f(0.0706f,0.3831f,0.02745f);
        glVertex3f(-3.44f,-0.06f,0.0f);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-5.0f,-0.06f,0.0f);


    glEnd();

    glBegin(GL_LINES);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-2.40f,0.12f,0.0f);
        glVertex3f(-2.40f,-0.3f,0.0f);
    glEnd();

     glBegin(GL_LINES);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-2.0f,0.12f,0.0f);
        glVertex3f(-2.2f,-0.3f,0.0f);

        glVertex3f(-2.0f,0.12f,0.0f);
        glVertex3f(-1.85f,-0.3f,0.0f);

        glVertex3f(-2.10f,-0.09f,0.0f);
        glVertex3f(-1.925f,-0.09f,0.0f);
    glEnd();

     glBegin(GL_LINES);
        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-1.6f,0.12f,0.0f);
        glVertex3f(-1.6f,-0.3f,0.0f);

        glVertex3f(-1.6f,0.1f,0.0f);
        glVertex3f(-1.4f,0.1f,0.0f);

        glVertex3f(-1.6f,-0.1f,0.0f);
        glVertex3f(-1.4f,-0.1f,0.0f);
    glEnd();


}

void PlaneTranslations(void)
{
    if(upperPlaneAngle<upperPlaneStopAngle)
    {
        upperPlaneAngle=upperPlaneAngle+0.02f;
       // upperPlaneAngle=0.0f;
    }
    if(incAngle<=360.0f)
    {
        incAngle=incAngle+0.13f;
       // incAngle=0.0f;
    }


    //lower Plane
    if(lowerPlaneAngle>(M_PI/2))
    {
        lowerPlaneAngle=lowerPlaneAngle-0.02;
    }
    if(decAngle>=0.0f)
    {
        decAngle=decAngle-0.13f;
       // incAngle=0.0f;
    }

    if(translateMiddle<=-2.499705)
    {
        translateMiddle=translateMiddle+0.058f;
    }

   
    if(translateAllInXDirection<=3.5f && meet==true)
    {
        translateAllInXDirection=translateAllInXDirection+0.05f;
        translateMiddle=translateAllInXDirection;
    }
  

    /**********************************************cheking the meet condition********************************/
     if(XTranslatePointUP>=-2.499705 && XTranslatePointDown>=-2.499705 && translateMiddle>=-2.499705)
    {
        meet=true;
    }



    /*******************************----------------2Nd Part---------------------*****************************/

    if(translateAllInXDirection>=3.5f && meet==true && translateMiddle<11.50f)
    {
        translateMiddle=translateMiddle+0.08f;

    }
    if(upperPlaneAngle2<upperPlaneStopAngle2 && translateAllInXDirection>=3.50f)
    {
        upperPlaneAngle2=upperPlaneAngle2+0.02f;
      
    }
    if(incAngle2<=90.0f && translateAllInXDirection>=3.50f)
    {
        incAngle2=incAngle2+0.13f;
       
    }

    if(lowerPlaneAngle2>-0.5f && translateAllInXDirection>=3.50f)
    {
        lowerPlaneAngle2=lowerPlaneAngle2-0.02f;
      
    }
    if(decAngle2>=270.0f && translateAllInXDirection>=3.50f)
    {
        decAngle2=decAngle2-0.13f;
      
    }
}