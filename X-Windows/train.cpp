#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/Xlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;


GLfloat gilength=3;
int giWindowWidth=800;
int giWindowHeight=600;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisulInfo=NULL;
Colormap gColormap;
Window gWindow;

GLXContext gGLXContext;
bool gbIsFullscreen=false;

bool bLight=false;

GLfloat lightAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuse[]={1.0f,1.0f,0.0f,1.0f};
GLfloat lightPosition[]={0.0f,2.0f,4.0f,1.0f};
GLfloat lightSpecular[]={1.0f,1.0f,1.0f,1.0f};

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0};
GLfloat materialShininess[]={128.0f};

GLUquadric *quadric=NULL;


struct vertex{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct boggie{
	vertex front[4];
	vertex right[4];
	vertex back[4];
	vertex left[4];
	vertex bottom[4];

	boggie *next;
	int boggieLength;
	GLUquadric *axel;
	GLfloat distance;

};
boggie *engine;

GLfloat angle=0.0f;
GLfloat moveTrain=-20.0f;

GLfloat eyePosX=0.0f;
GLfloat eyePosZ=10.0f;
GLfloat centerPosX=0.0f;
int main(void)
{	
	void initialize(void);
	void uninitialize(void);
	void createWindow(void);
	void display(void);
	void resize(int,int);
	void ToggleFullscreen(void);
	void update();
	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;


	createWindow();
	initialize();

	bool bDone=false;
	char keys[26];

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();

							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							break;
						case 'b':
						case 'B':
							if(bLight==false)
							{
								glEnable(GL_LIGHTING);
								bLight=true;
							}
							else
							{
								glDisable(GL_LIGHTING);
								bLight=false;
							}
							break;
						case 'm':
						case 'M':
								if(moveTrain<=5.0f)
								{
									moveTrain=moveTrain + 5.0f;
								}
							break;
						case 'n':
						case 'N':
								if(moveTrain<=5.0f || moveTrain>=5.0f)
								{
									moveTrain=moveTrain -1.0f;
								}
							break;
						case 'x':
							eyePosX=eyePosX+1.0f;
							centerPosX=centerPosX+1.0f;
							printf("upPosX: %f\n",eyePosX);
							printf("centerPosX: %f\n",centerPosX);
							break;
						case 'y':
							eyePosZ=eyePosZ-1.0f;
							//centerPosX=centerPosX+1.0f;
							printf("eyePosX: %f\n",eyePosZ);
							//printf("centerPosX: %f\n",centerPosX);
							break;
						

					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		display();
	}


	uninitialize();
	return 0;
}

void createWindow()
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in XOpenDisplay");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisulInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(glXChooseVisual==NULL)
	{
		printf("Error in glXChooseVisual\n");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisulInfo->screen),gpXVisulInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.event_mask=PointerMotionMask|VisibilityChangeMask|StructureNotifyMask|ExposureMask|KeyPressMask|ButtonPressMask;

	styleMask=CWColormap|CWBorderPixel|CWBackPixel|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisulInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisulInfo->depth,InputOutput,gpXVisulInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("\n Error in XCreateWindow ");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My OPENGL APPLICATION");
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisulInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize()
{
	void resize(int,int);
	void createTrain();
	void displayList();

	gGLXContext=glXCreateContext(gpDisplay,gpXVisulInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);


	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecular);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);
	glEnable(GL_LIGHT0);

	//material
	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);


	createTrain();
	createTrain();
	createTrain();
	createTrain();
	createTrain();
	

	displayList();
	resize(giWindowWidth,giWindowHeight);
}

void createTrain()
{
	void insertIntoList(boggie *);

	boggie *temp=NULL;

	if(engine==NULL)
	{
		engine=new boggie();
		insertIntoList(engine);
		engine->boggieLength=gilength;
		engine->distance=0.0f;
		engine->next=NULL;
		engine->axel=gluNewQuadric();
	}
	else
	{
		temp=new boggie();
		boggie *parser=engine;
		while(parser->next!=NULL)
		{
			parser=parser->next;
		}
		parser->next=temp;
		temp->next=NULL;
		temp->distance=2.2f;
		temp->axel=gluNewQuadric();
		insertIntoList(temp);
	}
	
	gilength=gilength+1;

}
void displayList()
{
	/*boggie *parser=engine;
	while(parser!=NULL)
	{
		printf("\nboggieLength: %d",parser->boggieLength);
		printf("\nboggieLength: %f",parser->distance);
		parser=parser->next;
	}*/
}	
void insertIntoList(boggie *trainBoggie)
{
	trainBoggie->front[0].x=1.0f;
	trainBoggie->front[0].y=1.0f;
	trainBoggie->front[0].z=1.0f;

	trainBoggie->front[1].x=-1.0f;
	trainBoggie->front[1].y=1.0f;
	trainBoggie->front[1].z=1.0f;

	trainBoggie->front[2].x=-1.0f;
	trainBoggie->front[2].y=-1.0f;
	trainBoggie->front[2].z=1.0f;
	
	trainBoggie->front[3].x=1.0f;
	trainBoggie->front[3].y=-1.0f;
	trainBoggie->front[3].z=1.0f;


	//boggoie
	trainBoggie->back[0].x=1.0f;
	trainBoggie->back[0].y=1.0f;
	trainBoggie->back[0].z=-1.0f;

	trainBoggie->back[1].x=-1.0f;
	trainBoggie->back[1].y=1.0f;
	trainBoggie->back[1].z=-1.0f;

	trainBoggie->back[2].x=-1.0f;
	trainBoggie->back[2].y=-1.0f;
	trainBoggie->back[2].z=-1.0f;
	
	trainBoggie->back[3].x=1.0f;
	trainBoggie->back[3].y=-1.0f;
	trainBoggie->back[3].z=-1.0f;


	trainBoggie->left[0].x=-1.0f;
	trainBoggie->left[0].y=1.0f;
	trainBoggie->left[0].z=1.0f;

	trainBoggie->left[1].x=-1.0f;
	trainBoggie->left[1].y=1.0f;
	trainBoggie->left[1].z=-1.0f;

	trainBoggie->left[2].x=-1.0f;
	trainBoggie->left[2].y=-1.0f;
	trainBoggie->left[2].z=-1.0f;
	
	trainBoggie->left[3].x=-1.0f;
	trainBoggie->left[3].y=-1.0f;
	trainBoggie->left[3].z=1.0f;



	trainBoggie->right[0].x=1.0f;
	trainBoggie->right[0].y=1.0f;
	trainBoggie->right[0].z=-1.0f;

	trainBoggie->right[1].x=1.0f;
	trainBoggie->right[1].y=1.0f;
	trainBoggie->right[1].z=1.0f;

	trainBoggie->right[2].x=1.0f;
	trainBoggie->right[2].y=-1.0f;
	trainBoggie->right[2].z=1.0f;
	
	trainBoggie->right[3].x=1.0f;
	trainBoggie->right[3].y=-1.0f;
	trainBoggie->right[3].z=-1.0f;


	//bottom
	trainBoggie->bottom[0].x=1.0f;
	trainBoggie->bottom[0].y=-1.0f;
	trainBoggie->bottom[0].z=-1.0f;

	trainBoggie->bottom[1].x=-1.0f;
	trainBoggie->bottom[1].y=-1.0f;
	trainBoggie->bottom[1].z=-1.0f;

	trainBoggie->bottom[2].x=-1.0f;
	trainBoggie->bottom[2].y=-1.0f;
	trainBoggie->bottom[2].z=1.0f;
	
	trainBoggie->bottom[3].x=1.0f;
	trainBoggie->bottom[3].y=-1.0f;
	trainBoggie->bottom[3].z=-1.0f;

	

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); 

	gluLookAt(eyePosX,4.0f,eyePosZ,centerPosX,0.0f,0.0f,0.0f,0.0f,-1.0f);
	//glTranslatef(0.0f,0.0f,-8.0f);

	glColor3f(0.0f,0.5f,0.5f);
	glBegin(GL_QUADS);
		glVertex3f(20.0f,-0.6f,-50.0f);
		glVertex3f(-20.0f,-0.6f,-50.0f);
		glVertex3f(-20.0f,-0.6f,50.0f);
		glVertex3f(20.0f,-0.6f,50.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(20.0f,-0.5f,1.0f);
		glVertex3f(-20.0f,-0.5f,1.0f);
		glVertex3f(-20.0f,-0.5f,0.80f);
		glVertex3f(20.0f,-0.5f,0.80f);

		glColor3f(0.5f,0.0f,1.0f);
		glVertex3f(20.0f,0.0f,1.0f);
		glVertex3f(-20.0f,0.0f,1.0f);
		glVertex3f(-20.0f,0.0f,0.8f);
		glVertex3f(20.0f,0.0f,0.8f);

		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(20.0f,-0.5f,-1.0f);
		glVertex3f(-20.0f,-0.5f,-1.0f);
		glVertex3f(-20.0f,-0.5f,-0.8f);
		glVertex3f(20.0f,-0.5f,-0.8f);

		glColor3f(0.5f,0.0f,1.0f);
		glVertex3f(20.0f,0.0f,-1.0f);
		glVertex3f(-20.0f,0.0f,-1.0f);
		glVertex3f(-20.0f,0.0f,-1.3f);
		glVertex3f(20.0f,0.0f,-1.3f);
	glEnd();

	glBegin(GL_QUADS);
		//front
		glColor3f(0.5f,1.0f,0.0f);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,0.0f,-2.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,0.0f,-2.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-2.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-2.0f);

		//top at middel
		glColor3f(1.0f,0.0f,0.0f);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,0.0f,-4.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,0.0f,-4.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,0.0f,-2.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,0.0f,-2.0f);

		/*//back
		glVertex3f(15.0f,0.0f,-4.0f);
		glVertex3f(10.0f,0.0f,-4.0f);
		glVertex3f(10.0f,-1.0f,-4.0f);
		glVertex3f(15.0f,-1.0f,-4.0f);*/

		//right
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,0.0f,-4.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(15.0f,0.0f,-2.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-2.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-4.0f);

		//left
		glColor3f(1.0f,1.0f,0.0f);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(10.0f,0.0f,-2.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,0.0f,-4.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-4.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-2.0f);


		glColor3f(0.0f,1.0f,0.50f);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,1.0f,-5.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,1.0f,-5.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-5.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-5.0f);


		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,1.0f,-5.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(15.0f,1.0f,-4.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-4.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-5.0f);

		glTexCoord2f(1.0f,1.0f);
		glVertex3f(10.0f,1.0f,-4.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,1.0f,-5.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-5.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-4.0f);

		///top
		glColor3f(0.0f,1.0f,1.0f);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,1.0f,-5.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,1.0f,-5.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,1.0f,-2.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,1.0f,-2.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D,texureBackPanel);
	glBegin(GL_QUADS);
		//back panel wall
		glColor3f(0.0f,1.0f,0.50f);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(15.0f,1.0f,-4.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(10.0f,1.0f,-4.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(10.0f,-1.0f,-4.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(15.0f,-1.0f,-4.0f);
	glEnd();


	boggie *node=engine;

	glTranslatef(moveTrain,0.0f,0.0f);
	//glRotatef(angle,0.0f,1.0f,0.0f);
	while(node!=NULL)
	{
		glTranslatef(-node->distance,0.0f,0.0f);
		glPushMatrix();
		glColor3f(0.45f,0.5f,0.0f);
		glBegin(GL_QUADS);
			glVertex3f(node->bottom[0].x,node->bottom[0].y,node->bottom[0].z);
			glVertex3f(node->bottom[1].x,node->bottom[1].y,node->bottom[1].z);
			glVertex3f(node->bottom[2].x,node->bottom[2].y,node->bottom[2].z);
			glVertex3f(node->bottom[3].x,node->bottom[3].y,node->bottom[3].z);

			glVertex3f(node->front[0].x,node->front[0].y,node->front[0].z);
			glVertex3f(node->front[1].x,node->front[1].y,node->front[1].z);
			glVertex3f(node->front[2].x,node->front[2].y,node->front[2].z);
			glVertex3f(node->front[3].x,node->front[3].y,node->front[3].z);


			glVertex3f(node->right[0].x,node->right[0].y,node->right[0].z);
			glVertex3f(node->right[1].x,node->right[1].y,node->right[1].z);
			glVertex3f(node->right[2].x,node->right[2].y,node->right[2].z);
			glVertex3f(node->right[3].x,node->right[3].y,node->right[3].z);

			glVertex3f(node->back[0].x,node->back[0].y,node->back[0].z);
			glVertex3f(node->back[1].x,node->back[1].y,node->back[1].z);
			glVertex3f(node->back[2].x,node->back[2].y,node->back[2].z);
			glVertex3f(node->back[3].x,node->back[3].y,node->back[3].z);

			glVertex3f(node->left[0].x,node->left[0].y,node->left[0].z);
			glVertex3f(node->left[1].x,node->left[1].y,node->left[1].z);
			glVertex3f(node->left[2].x,node->left[2].y,node->left[2].z);
			glVertex3f(node->left[3].x,node->left[3].y,node->left[3].z);

				
		glEnd();
		glPopMatrix();
		if(node->next!=NULL)
		{
			
			glPushMatrix();
			glTranslatef(-1.50f,-0.30f,0.60f);
			glRotatef(90.0f,0.0f,1.0f,0.0f);
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			gluCylinder(node->axel,0.2,0.2f,0.50f,20,20);
			glPopMatrix();
			
			glPushMatrix();
			glTranslatef(-1.50f,-0.30f,-0.60f);
			glRotatef(90.0f,0.0f,1.0f,0.0f);
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			gluCylinder(node->axel,0.2,0.2f,0.50f,20,20);
			glPopMatrix();

		}
		node=node->next;
	}


	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisulInfo)
	{
		free(gpXVisulInfo);
		gpXVisulInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}





void update()
{
	angle=angle+0.2f;
	if(angle>360.0f)
	{
		angle=0.0f;
	}

	
}