#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/Xutil.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

GLXContext gGLXContext;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Window gWindow;
Colormap gColormap;
bool gbIsFullscreen=false;


int giWindowWidth=800;
int giWindowHeight=600;

int main(void)
{
	void CreateWindow(void);
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void ToggleFullscreen(void);
	void resize(int,int);

	int windowWidth=800;
	int windowHeight=600;

	char keys[26];
	bool bDone=false;
	XEvent event;
	KeySym keysym;


	CreateWindow();
	initialize();


	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}

		}
		//update
		display();
	}
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int styleMask;
	int defaultScreen;

	static int frameBufferAttributes[]={
										GLX_RGBA,
										GL_DOUBLEBUFFER,True,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in XOpenDisplay()\n\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in glXChooseVisual()\n");
		uninitialize();
		exit(0);
	}


	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=PointerMotionMask|StructureNotifyMask|VisibilityChangeMask|ExposureMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("Error in CreateWindow at XCreateWindow\n\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY XWINDOW APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

	
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(XEvent));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 :1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),false,StructureNotifyMask,&xev);


}

void initialize()
{
	void resize(int,int);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,1.0f);
	resize(giWindowWidth,giWindowHeight);

}

void display()
{
	 //function declaration
    void drawI();
    void drawN();
    void drawD();
   // void drawI();
    void drawA();
    
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-2.5f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-2.0f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawN();
    glEnd();


    //Drawing D's Line
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-0.5f,0.0f,-10.0f);
    glPointSize(3.0f);
    glBegin(GL_POINTS);
        drawD();
    glEnd();

    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(1.0f,0.0f,-10.0f);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
        drawI();
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(2.0f,0.0f,-10.0f);
    glLineWidth(4.0f);
    glBegin(GL_LINES)k;
        drawA();
    glEnd();

	glXSwapBuffers(gpDisplay,gWindow);
}
void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize()
{
	GLXContext currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext!=NULL && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void drawI(void)
{
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);
}

void drawN(void)
{
    //|\|
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.0f,-1.0f,0.0f);


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
  


}

void drawD()
{
    // |) -
    /*glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(-1.0f,1.0f,0.0f);*/

    //|
   /* glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(-1.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-1.0f,-1.0f,0.0f);*/

    //-
    /*glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-1.0f,-1.0f,0.0f);    
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
*/
    //|
   /* glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(1.0f,-1.0f,0.0f);
    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(1.0f,1.0f,0.0f);*/

   GLfloat RColor=0.0706f;
    GLfloat GColor=0.3831f;
    GLfloat BColor=0.02745f;
    for(GLfloat angle=(3*M_PI/2.0f);angle<=(5*M_PI)/2;angle+=0.02f)
    {
        glVertex3f(cos(angle),sin(angle),0.0f);
        glColor3f(RColor,GColor,BColor);
        if(RColor<=1.0)
        {
            RColor+=0.006f;
            
        }
        if(GColor<=0.6)
        {
            GColor+=0.006f;
        }
        if(BColor<=0.2)
        {
            BColor+=0.006f;
        }
    }


}

void drawA()
{
    //variable declarations
    GLfloat x1ForAHorizontalLine=0.0f;
    GLfloat y1ForAHorizontalLine=0.0f;
    
    GLfloat x2ForAHorizontalLine=0.0f;
    GLfloat y2ForAHorizontalLine=0.0f;
    


    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(0.50f,-1.0f,0.0f);

    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(0.0f,1.0f,0.0f);
    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(-0.50f,-1.0f,0.0f);


      
    x1ForAHorizontalLine=(0.50f+0.0f)/2.0f;
    y1ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;
    
    x2ForAHorizontalLine=(-0.50f+(0.0f))/2.0f;
    y2ForAHorizontalLine=(1.0f+(-1.0f))/2.0f;

    glColor3f(1.0f,0.6f,0.2f);
    glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine,0.0f);
    glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine,0.0f);

    glColor3f(1.0f,1.0f,1.0f);
    glVertex3f(x1ForAHorizontalLine-0.01f,y1ForAHorizontalLine-0.04f,0.0f);
    glVertex3f(x2ForAHorizontalLine,y2ForAHorizontalLine-0.04f,0.0f);

    glColor3f(0.0706f,0.3831f,0.02745f);
    glVertex3f(x1ForAHorizontalLine,y1ForAHorizontalLine-0.1f,0.0f);
    glVertex3f(x2ForAHorizontalLine-0.02f,y2ForAHorizontalLine-0.1f,0.0f);
    
}
