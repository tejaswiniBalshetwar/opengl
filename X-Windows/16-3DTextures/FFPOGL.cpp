#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<memory.h>


#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>
#include<SOIL/SOIL.h>


using namespace std;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;

GLXContext gGLXContext;
int giWindowWidth=800;
int giWindowHeight=600;

GLfloat angleTri=0.0f;
GLfloat angleQuad=0.0f;
GLuint texture_triangle;
GLuint texture_Quad;


int main(void)
{
	void CreateWindow();
	void initialize(void);
	void uninitialize(void);
	void ToggleFullscreen(void);
	void display(void);
	void resize(int,int);
	void update(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	bool bDone=false;
	char keys[26];
	XEvent event;
	KeySym keysym;

	CreateWindow();
	initialize();
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();
							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow()
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int styleMask;
	int defaultScreen;

	static int frameBufferAttributes[]={
			GLX_RGBA,
			GL_DOUBLEBUFFER,True,
			GLX_RED_SIZE,8,
			GLX_GREEN_SIZE,8,
			GLX_BLUE_SIZE,8,
			GLX_ALPHA_SIZE,8,
			GLX_DEPTH_SIZE,24,
			None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in XOpenDisplay()\n\n");
		uninitialize();
		exit(0);
	}
	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in glXchooseVisual()\n\n");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=PointerMotionMask|VisibilityChangeMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|ExposureMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("Error in XCreateWindow()\n\n");
		uninitialize();
		exit(0);
	}
	XStoreName(gpDisplay,gWindow,"XWindow Application");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom fullscreen;
	Atom wm_state;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.window=gWindow;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),false,StructureNotifyMask,&xev);


}

void initialize()
{
	void resize(int,int);
	bool LoadTexture(GLuint *,const char*);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	LoadTexture(&texture_triangle,"./Stone.bmp");
	LoadTexture(&texture_Quad,"./Kundali.bmp");
	printf("Stone= %u\n",texture_triangle);
	printf("Kundali=%u\n",texture_Quad);

	resize(giWindowWidth,giWindowHeight);
}

bool LoadTexture(GLuint *texture,const char* path)
{
	int imageWidth;
	int imageHeight;
	bool bResult=false;
	unsigned char* imageData=NULL;

	imageData=SOIL_load_image(path,&imageWidth,&imageHeight,0,SOIL_LOAD_RGB);
	if(imageData==NULL)
	{
		bResult=false;
		return bResult;
	}
	else
	{
		bResult=true;

		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glGenTextures(1,texture);
		glBindTexture(GL_TEXTURE_2D,*texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		gluBuild2DMipmaps(GL_TEXTURE_2D,3,imageWidth,imageHeight,GL_RGB,GL_UNSIGNED_BYTE,imageData);

		SOIL_free_image_data(imageData);
		return bResult;
	}
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f,0.0f,-6.0f);
	glRotatef(angleTri,0.0f,1.0f,0.0f);
	
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D,texture_triangle);

	glRotatef(angleTri,0.0f,1.0f,0.0f);
	glBegin(GL_TRIANGLES);
		glTexCoord2f(0.5f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);

		glTexCoord2f(0.5f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		glTexCoord2f(0.5f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		glTexCoord2f(0.5f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
	glEnd();
	glPopMatrix();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f,0.0f,-6.0f);
	glScalef(0.75f,0.75f,0.75f);
	glRotatef(angleQuad,1.0f,1.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D,texture_Quad);
	glPushMatrix();
	glBegin(GL_QUADS);
		//top
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-1.0f,1.0f,-1.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,1.0f,1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,1.0f,1.0f);

		//bottom
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		//front
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-1.0f,1.0f,1.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		
		//right
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(1.0f,1.0f,1.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(1.0f,-1.0f,1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);

		//back
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(-1.0f,1.0f,-1.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(1.0f,1.0f,-1.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(1.0f,-1.0f,-1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);

		//left
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(-1.0f,1.0f,1.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(-1.0f,1.0f,-1.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,-1.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,1.0f);
	glEnd();
	glPopMatrix();
	glXSwapBuffers(gpDisplay,gWindow);
}


void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

}

void update()
{

	angleTri=angleTri+5.0f;
	if(angleTri>=360.0f)
	{
		angleTri=0.0f;
	}

	angleQuad=angleQuad+5.0f;
	if(angleQuad>=360.0f)
	{
		angleQuad=0.0f;
	}
}
