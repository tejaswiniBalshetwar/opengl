#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>
#include<X11/Xutil.h>


#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

bool gbIsFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;

GLXContext gGLXContext;

int giWindowWidth=800;
int giWindowHeight=600;

 GLfloat XtriangleTranslate=-3.0f;
 GLfloat YtriangleTranslate=-3.0f;

 GLfloat lineTranslate=3.0f;

 GLfloat XcircleTranslate=3.0f;
 GLfloat YcircleTranslate=-3.0f;


	//ratations
 GLfloat triangleRotation=0.0f;
 GLfloat circleRotation=0.0f;



int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void resize(int,int);
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	CreateWindow();
	initialize();

	bool bDone=false;
	XEvent event;
	KeySym keysym;
	char keys[26];
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullScreen==false)
							{
								ToggleFullscreen();
								gbIsFullScreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullScreen=false;
							}
						break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;

			}
		}	
			update();
			display();

	}
	uninitialize();
	return(0);

}

void CreateWindow(void)
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributs[]={GLX_RGBA,
										GL_DOUBLEBUFFER,True,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										None
									};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in XOpenDisplay()\n\n");
		uninitialize();
		exit(0);
	}
	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributs);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in glXChooseVisual()\n\n");
		uninitialize();
		exit(0);
	}
	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|ExposureMask;

	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("Error in XCreateWindow()\n\n ");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY XWINDOW APPLICATION");
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.window=gWindow;
	xev.xclient.data.l[0]=gbIsFullScreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);


}

void initialize()
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,1.0f);

	resize(giWindowWidth,giWindowHeight);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	GLfloat CalculateLength(GLfloat,GLfloat,GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat CalculateAreaOfTriangle(GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat CalculateSemiperimenterOfTriangle(GLfloat,GLfloat,GLfloat);
	GLfloat CalculateRadiusOfCircle(GLfloat,GLfloat);


	//variable declarations

	//translate Variables
	

	//circle vars
	GLfloat radiusOfInCircle=0.0f;
	GLfloat xCenterOfCircle=0.0f;
	GLfloat yCenterOfCircle=0.0f;
	GLfloat AreaOfTriangle=0.0f;
	GLfloat SemiperimeterOfTriangle=0.0f;

	//triangle cords
	GLfloat x1Triangle=0.0f,y1Triangle=1.0f,z1Triangle=0.0f;
	GLfloat x2Triangle=-1.0f,y2Triangle=-1.0f,z2Triangle=0.0f;
	GLfloat x3Triangle=1.0f,y3Triangle=-1.0f,z3Triangle=0.0f;

	//code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(XtriangleTranslate,YtriangleTranslate,-6.0f);
	if(XtriangleTranslate<0.0f && YtriangleTranslate<0.0f)
	{
		glRotatef(triangleRotation,0.0f,1.0f,0.0f);
	}
	glBegin(GL_LINE_LOOP);
		glColor3f(1.0f,0.5f,0.0f);
		
		glVertex3f(x1Triangle,y1Triangle,z1Triangle); 
		//glVertex3f(x2Triangle,y2Triangle,z2Triangle);

		glVertex3f(x2Triangle,y2Triangle,z2Triangle);
		//glVertex3f(x3Triangle,y3Triangle,z3Triangle);

		glVertex3f(x3Triangle,y3Triangle,z3Triangle);
		//glVertex3f(x1Triangle,y1Triangle,z1Triangle);
	glEnd();


	//incircle code
	GLfloat LenghtOfA=CalculateLength(x1Triangle,y1Triangle,z1Triangle,x2Triangle,y2Triangle,z2Triangle);
	GLfloat LenghtOfB=CalculateLength(x2Triangle,y2Triangle,z2Triangle,x3Triangle,y3Triangle,z3Triangle);
	GLfloat LenghtOfC=CalculateLength(x3Triangle,y3Triangle,z3Triangle,x1Triangle,y1Triangle,z1Triangle);


	SemiperimeterOfTriangle=CalculateSemiperimenterOfTriangle(LenghtOfA,LenghtOfB,LenghtOfC);
	AreaOfTriangle=CalculateAreaOfTriangle(SemiperimeterOfTriangle,LenghtOfA,LenghtOfB,LenghtOfC);

	radiusOfInCircle=CalculateRadiusOfCircle(SemiperimeterOfTriangle,AreaOfTriangle);

	xCenterOfCircle=((LenghtOfB*x1Triangle)+(LenghtOfC*x2Triangle)+(LenghtOfA*x3Triangle))/(SemiperimeterOfTriangle*2);
	yCenterOfCircle=((LenghtOfB*y1Triangle)+(LenghtOfC*y2Triangle)+(LenghtOfA*y3Triangle))/(SemiperimeterOfTriangle*2);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(XcircleTranslate,YcircleTranslate,-6.0f);
	if(XcircleTranslate>0.0f && YcircleTranslate<0.0f)
	{
		glRotatef(circleRotation,0.0f,1.0f,0.0f);
	}
	glBegin(GL_POINTS);

	for(GLfloat angle=0.0f;angle<=2*M_PI;angle=angle+0.002)
	{

		glColor3f(0.5f,0.5f,0.0f);
		glVertex3f(xCenterOfCircle+(radiusOfInCircle*cos(angle)),yCenterOfCircle+(radiusOfInCircle*sin(angle)),0.0f);
	}

	glEnd();



	//sword of hollows code
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLineWidth(2.0f);
	glTranslatef(0.0f,lineTranslate,-6.0f);
	
	glBegin(GL_LINES);

		glColor3f(0.0f,1.0f,1.0f);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(0.0f,-1.0f,0.0f);

	glEnd();


	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}
void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

}

void update()
{
	
	//rotation update
	triangleRotation=triangleRotation+0.4f;
	if(triangleRotation>360.0f)
	{
		triangleRotation=0.0f;
	}
	circleRotation=circleRotation+0.8f;
	if(circleRotation>360.0f)
	{
		circleRotation=0.0f;
	}

	//translation calls
	if(lineTranslate>=0.0f)
	{
		lineTranslate=lineTranslate-0.001f;
		
	}
	if(XtriangleTranslate<=0.0f && YtriangleTranslate<=0.0f)
	{
		XtriangleTranslate=XtriangleTranslate+0.001f;
		YtriangleTranslate=YtriangleTranslate+0.001f;
	}

	if(XcircleTranslate>=0.0f && YcircleTranslate<=0.0f)
	{
		XcircleTranslate=XcircleTranslate-0.001f;
		YcircleTranslate=YcircleTranslate+0.001f;
	}



}

GLfloat CalculateLength(GLfloat x1,GLfloat y1,GLfloat z1,GLfloat x2,GLfloat y2,GLfloat z2)
{
	GLfloat Length=0.0f;

	Length=sqrt(pow((x2-x1),2)+pow((y2-y1),2)+pow((z2-z1),2));

	return Length;
}

GLfloat CalculateSemiperimenterOfTriangle(GLfloat length1,GLfloat length2,GLfloat length3)
{
	GLfloat semiperimeter=0.0f;
	semiperimeter=(length1+length2+length3)/2;

	return semiperimeter;
}

GLfloat CalculateAreaOfTriangle(GLfloat semiperimeter,GLfloat length1,GLfloat length2,GLfloat length3)
{
	GLfloat area=0.0f;
	area=sqrt(semiperimeter*(semiperimeter- length1)*(semiperimeter- length2)*(semiperimeter- length3));

	return area;
}

GLfloat CalculateRadiusOfCircle(GLfloat semiperimeter,GLfloat area)
{
	GLfloat radius=0.0f;
	radius=(area/semiperimeter);
	return radius;
}