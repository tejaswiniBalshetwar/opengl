#include<iostream>
#include<stdio.h>
#include<memory.h>
#include<stdlib.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>


#include<GL/gl.h>
#include<GL/glx.h>

using namespace std;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
XVisualInfo *gpxVisualInfo=NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGlxContext;

int main(void)
{
	void initialize(void);
	void CreateWindow(void);
	void display(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	void resize(int,int);

	bool bDone=false;

	static int winWidth=giWindowWidth;
	static int winHeight=giWindowHeight;

	char keys[26];

	CreateWindow();
	initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					{
						switch(keysym)
						{
							case XK_Escape:
								bDone=true;
								break;

						}
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
					 	case 2:
					 		break;

					 	case 3:
					 		break;
					 	case 4:
					 		break;

					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}

		//update()
		display();
	}

	uninitialize();

	return(0);

}

void CreateWindow()
{
	void uninitialize();

	XSetWindowAttributes windowAttribs;

	int defaultScreen;
	int defaultDepth;

	int styleMask;

	static int frameBuferAttributes[]={
		GLX_RGBA,
		GLX_RED_SIZE,1,
		GLX_BLUE_SIZE,1,
		GLX_GREEN_SIZE,1,
		GLX_ALPHA_SIZE,1,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{	
		printf("Error while XOpenDisplay");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	gpxVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBuferAttributes);

	windowAttribs.border_pixel=0;
	windowAttribs.border_pixmap=0;
	windowAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);

	windowAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpxVisualInfo->screen),gpxVisualInfo->visual,AllocNone);

	gColormap=windowAttribs.colormap;
	windowAttribs.event_mask=ExposureMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|VisibilityChangeMask|PointerMotionMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpxVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpxVisualInfo->depth,InputOutput,gpxVisualInfo->visual,styleMask,&windowAttribs);

	if(!gWindow)
	{
		printf("Error while creating window\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY XWINDOW APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);

}


void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(XEvent));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpxVisualInfo->screen),false,StructureNotifyMask,&xev);

}

void initialize()
{

	void resize(int,int);

	gGlxContext=glXCreateContext(gpDisplay,gpxVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGlxContext);

	glClearColor(0.0f,0.0f,1.0f,1.0f);
	resize(giWindowWidth,giWindowWidth);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext!=NULL && currentGLXContext==gGlxContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay,gGlxContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpxVisualInfo)
	{
		free(gpxVisualInfo);
		gpxVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}


