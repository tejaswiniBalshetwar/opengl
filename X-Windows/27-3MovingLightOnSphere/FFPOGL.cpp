#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/Xutil.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

bool gbIsFullscreen=false;

int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;
XVisualInfo *gpXVisualInfo=NULL;
Display *gpDisplay=NULL;
Colormap gColormap;
Window gWindow;

GLfloat lightAmbientOne[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseOne[]={1.0f,0.0f,0.0f,1.0f};
GLfloat lightSpecularOne[]={1.0f,0.0f,0.0f,1.0f};
GLfloat lightPositionOne[]={0.0f,0.0f,0.0f,1.0f};


GLfloat lightAmbientTwo[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseTwo[]={0.0f,1.0f,0.0f,1.0f};
GLfloat lightSpecularTwo[]={0.0f,1.0f,0.0f,1.0f};
GLfloat lightPositionTwo[]={0.0f,0.0f,0.0f,1.0f};



GLfloat lightAmbientThree[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseThree[]={0.0f,0.0f,1.0f,1.0f};
GLfloat lightSpecularThree[]={0.0f,0.0f,1.0f,1.0f};
GLfloat lightPositionThree[]={0.0f,0.0f,0.0f,1.0f};

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialShininess[]={100.0f};

GLUquadric *quadric=NULL;

GLfloat lightAngleOne=0.0f;
GLfloat lightAngleTwo=0.0f;
GLfloat lightAngleThree=0.0f;

bool bLight=false;


int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void resize(int,int);
	void createWindow(void);
	void ToggleFullscreen(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	bool bDone=false;
	char keys[26];

	XEvent event;
	KeySym keysym;

	createWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								gbIsFullscreen=true;
								ToggleFullscreen();
							}
							else
							{
								gbIsFullscreen=false;
								ToggleFullscreen();
							}
							break;
						case 'l':
						case 'L':
							if(bLight==false)
							{
								bLight=true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								bLight=false;
								glDisable(GL_LIGHTING);
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;

					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.format=32;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);	

}

void createWindow()
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\n Error in XOpenDisplay()");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("\n Error in gpXVisualInfo()");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=VisibilityChangeMask|PointerMotionMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|ExposureMask;
	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error in XCreateWindow()");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}


void initialize()
{
	void resize(int,int);
	
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbientOne);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuseOne);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecularOne);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1,GL_AMBIENT,lightAmbientTwo);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,lightDiffuseTwo);
	glLightfv(GL_LIGHT1,GL_SPECULAR,lightSpecularTwo);
	glEnable(GL_LIGHT1);
	
	glLightfv(GL_LIGHT2,GL_AMBIENT,lightAmbientThree);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,lightDiffuseThree);
	glLightfv(GL_LIGHT2,GL_SPECULAR,lightSpecularThree);
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);

	resize(giWindowWidth,giWindowHeight);
	
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	gluLookAt(0.0f,0.0f,3.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);

	//first light
	glPushMatrix();
	glRotatef(lightAngleOne,1.0f,0.0f,0.0f);
	lightPositionOne[1]=lightAngleOne;
	glLightfv(GL_LIGHT0,GL_POSITION,lightPositionOne);

	glPopMatrix();

	glPushMatrix();
	glRotatef(lightAngleTwo,0.0f,1.0f,0.0f);
	lightPositionTwo[0]=lightAngleTwo;
	glLightfv(GL_LIGHT1,GL_POSITION,lightPositionTwo);
	glPopMatrix();

	glPushMatrix();
	glRotatef(lightAngleThree,0.0f,0.0f,1.0f);
	lightPositionThree[0]=lightAngleThree;
	glLightfv(GL_LIGHT2,GL_POSITION,lightPositionThree);
	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	quadric=gluNewQuadric();
	gluSphere(quadric,0.75f,30,30);

	glPopMatrix();

	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize()
{
	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && currentGLXContext==gGLXContext )
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}


void update()
{
	lightAngleOne=lightAngleOne+0.2f;
	if(lightAngleOne>360.0f)
	{
		lightAngleOne=0.0f;
	}
	lightAngleTwo=lightAngleTwo+0.2f;
	if(lightAngleTwo>360.0f)
	{
		lightAngleTwo=0.0f;
	}
	lightAngleThree=lightAngleThree+0.2f;
	if(lightAngleThree>360.0f)
	{
		lightAngleThree=0.0f;
	}
}