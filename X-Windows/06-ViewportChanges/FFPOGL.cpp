#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xutil.h>
#include<X11/Xlib.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

bool gbIsFullscreen=false;

Display *gpDisplay=NULL;
Colormap gColormap;
XVisualInfo *gpXVisualInfo=NULL;
Window gWindow;

int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void CreateWindow(void);
	void resize(int,int);
	void ToggleFullscreen(void);

	bool bDone=false;
	static int windowWidth=giWindowWidth;
	static int windowHeight=giWindowHeight;


	CreateWindow();
	initialize();

	char keys[26];
	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
							break;
						case '1':
								glViewport(0,0,GLsizei(windowWidth/2),GLsizei(windowHeight/2));
							break;
						case '2':
								glViewport(0,GLsizei(windowHeight/2),GLsizei(windowWidth/2),GLsizei(windowHeight/2));
							break;
						case '3':
							glViewport(GLsizei(windowWidth/2),0,GLsizei(windowWidth/2),GLsizei(windowHeight/2));
							break;
						case '4':
							glViewport(GLsizei(windowWidth/2),GLsizei(windowHeight/2),GLsizei(windowWidth/2), GLsizei(windowHeight/2));

							break;
						case '5':
							glViewport(0,GLsizei(windowHeight/2),GLsizei(windowWidth), GLsizei(windowHeight/2));

							break;
						case '6':
							glViewport(0,0,GLsizei(windowWidth), GLsizei(windowHeight/2));

							break;
						case '7':
							glViewport(0,0,GLsizei(windowWidth/2), GLsizei(windowHeight));

							break;
						case '8':
							glViewport(GLsizei(windowWidth/2),0,GLsizei(windowWidth/2), GLsizei(windowHeight));
							break;
						default:
							glViewport(0,0,GLsizei(windowWidth),GLsizei(windowHeight));
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;

					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
						windowWidth=event.xconfigure.width;
						windowHeight=event.xconfigure.height;
						resize(windowWidth,windowHeight);

					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	void uninitialize(void);

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributs[]={
									GLX_RGBA,
									GL_DOUBLEBUFFER,True,
									GLX_RED_SIZE,8,
									GLX_GREEN_SIZE,8,
									GLX_BLUE_SIZE,8,
									GLX_ALPHA_SIZE,8,
									None
								};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in CreateWindow at XOpenDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributs);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in CreateWindow at glXChooseVisual\n");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|StructureNotifyMask|PointerMotionMask|KeyPressMask|ButtonPressMask;
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("Error while XCreateWindow\n\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My XWindow Application");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;


	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}

void initialize(void)
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	resize(giWindowWidth,giWindowHeight);

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);
	glColor3f(0.5f,0.6f,1.0f);
	glBegin(GL_TRIANGLES);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);
	glEnd();

	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);

}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}