#include<iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

using namespace std;

GLXContext gGLXContext;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;

Colormap gColormap;
Window gWindow;

bool gbIsFullscreen=false;
bool bLight=false;

int giWindowWidth=800;
int giWindowHeight=600;

GLfloat lightAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat lightPosition[]={0.0f,0.0f,0.0f,1.0f};

GLuint keyPress=0;
GLfloat angleOfXRotation=0.0f;
GLfloat angleOfYRotation=0.0f;
GLfloat angleOfZRotation=0.0f;


GLfloat light_model_ambient[]={0.2f,0.2f,0.2f,1.0f};
GLfloat light_model_local_viewer[]={0.0f};
GLUquadric *quadric[24];

int main(void)
{
	void createWindow(void);
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void resize(int,int);
	void ToggleFullscreen(void);

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	char keys[26];
	bool bDone=false;

	XEvent event;
	KeySym keysym;

	createWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
				switch(event.type)
				{
					case MapNotify:
						break;
					case MotionNotify:
						break;
					case KeyPress:
					 	keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					 	switch(keysym)
					 	{
					 		case XK_Escape:
					 			bDone=true;
					 			break;
					 	}
					 	XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					 	switch(keys[0])
					 	{
					 		case 'f':
					 		case 'F':
					 			if(gbIsFullscreen==false)
					 			{
					 				gbIsFullscreen=true;
					 				ToggleFullscreen();
					 			}
					 			else
					 			{
					 				gbIsFullscreen=false;
					 				ToggleFullscreen();
					 			}
					 			break;
					 		case 'l':
					 		case 'L':
					 			if(bLight==false)
					 			{
					 				bLight=true;
					 				glEnable(GL_LIGHTING);

					 			}
					 			else
					 			{
					 				bLight=false;
					 				glDisable(GL_LIGHTING);
					 			}
					 			break;
					 		case 'x':
							case 'X':
								keyPress=1;
								angleOfXRotation=0.0f;
								break;
							case 'y':
							case 'Y':
								keyPress=2;
								angleOfYRotation=0.0f;
								break;
							case 'z':
							case 'Z':
								keyPress=3;
								angleOfZRotation=0.0f;
								break;

					 	}
						break;
					case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1:
								break;
							case 2:
								break;
							case 3:
								break;
							case 4:
								break;
						}
						break;
					case ConfigureNotify:
						windowWidth=event.xconfigure.width;
						windowHeight=event.xconfigure.height;
						resize(windowWidth,windowHeight);
						break;
					case DestroyNotify:
						break;
					case 33:
						bDone=true;
						break;
				}		
		}
		update();
		display();
	}

	uninitialize();
}

void createWindow()
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;

	int static frameBufferAttributes[]={
			GLX_RGBA,
			GL_DOUBLEBUFFER,True,
			GLX_RED_SIZE,8,
			GLX_BLUE_SIZE,8,
			GLX_GREEN_SIZE,8,
			GLX_ALPHA_SIZE,8,
			GLX_DEPTH_SIZE,24,
			None
	};
	int styleMask;

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error in XOpenDisplay\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error in glXChooseVisual()\n");
		uninitialize();
		exit(0);

	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=StructureNotifyMask|VisibilityChangeMask|PointerMotionMask|ExposureMask|KeyPressMask|ButtonPressMask;

	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("Error in XCreateWindow()\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY OPENGL WINDOW");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.window=gWindow;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}

void initialize()
{
	void resize(int,int);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,light_model_local_viewer);
	glEnable(GL_LIGHT0);

	for(int i=0;i<24;i++)
	{
		quadric[i]=gluNewQuadric();
	}

	resize(giWindowWidth,giWindowHeight);
}

void display()
{
	void draw24Spheres(void);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	if(keyPress==1)
	{
		glRotatef(angleOfXRotation,1.0f,0.0f,0.0f);
		lightPosition[1]=angleOfXRotation;
	}
	else if(keyPress==2)
	{
		glRotatef(angleOfYRotation,0.0f,1.0f,0.0f);
		lightPosition[2]=angleOfYRotation;
	}
	else if(keyPress==3)
	{
		glRotatef(angleOfZRotation,0.0f,0.0f,1.0f);
		lightPosition[0]=angleOfZRotation;
	}

	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);
	glPopMatrix();
	draw24Spheres();

	glXSwapBuffers(gpDisplay,gWindow);
}

void draw24Spheres()
{
	GLfloat glMaterialAmbient[4];
	GLfloat glMaterialDiffuse[4];
	GLfloat glMaterialSpecular[4];
	GLfloat glMaterialShininess;

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	glMaterialAmbient[0]=0.0215;
	glMaterialAmbient[1]=0.1745;
	glMaterialAmbient[2]=0.0215;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.07568;
	glMaterialDiffuse[1]=0.61424;
	glMaterialDiffuse[2]=0.07568;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.633;
	glMaterialSpecular[1]=0.727811;
	glMaterialSpecular[2]=0.633;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.6 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,14.0f,0.0f);
	
	gluSphere(quadric[0],1.0f,30,30);

	
	//2nd row 1st col
	glMaterialAmbient[0]=0.135f;
	glMaterialAmbient[1]=0.2225f;
	glMaterialAmbient[2]=0.1575f;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.54f;
	glMaterialDiffuse[1]=0.89f;
	glMaterialDiffuse[2]=0.63f;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.316228;
	glMaterialSpecular[1]=0.316228;
	glMaterialSpecular[2]=0.316228;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.1 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,11.50f,0.0f);

	gluSphere(quadric[1],1.0f,30,30);


	//3rd row 1 col
	glMaterialAmbient[0]=0.05375;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.06625;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.18275;
	glMaterialDiffuse[1]=0.17;
	glMaterialDiffuse[2]=0.22525;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.332741;
	glMaterialSpecular[1]=0.328634;
	glMaterialSpecular[2]=0.346435;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.3 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,9.0f,0.0f);

	gluSphere(quadric[2],1.0f,30,30);

	//4th row 1st col
	glMaterialAmbient[0]=0.25;
	glMaterialAmbient[1]=0.20725;
	glMaterialAmbient[2]=0.20725;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=1.0;
	glMaterialDiffuse[1]=0.829;
	glMaterialDiffuse[2]=0.829;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.296648;
	glMaterialSpecular[1]=0.296648;
	glMaterialSpecular[2]=0.296648;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.088 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,6.50f,0.0f);

	gluSphere(quadric[3],1.0f,30,30);

	//5th row and 1st col
	glMaterialAmbient[0]=0.1745;
	glMaterialAmbient[1]=0.01175;
	glMaterialAmbient[2]=0.01175;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.61424;
	glMaterialDiffuse[1]=0.04136;
	glMaterialDiffuse[2]=0.04136;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.727811;
	glMaterialSpecular[1]=0.626959;
	glMaterialSpecular[2]=0.626959;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.6 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,4.0f,0.0f);

	gluSphere(quadric[4],1.0f,30,30);

	//6th row 1st col
	glMaterialAmbient[0]=0.1;
	glMaterialAmbient[1]=0.18725;
	glMaterialAmbient[2]=0.1745;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.396;
	glMaterialDiffuse[1]=0.74151;
	glMaterialDiffuse[2]=0.69102;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.297254;
	glMaterialSpecular[1]=0.30829;
	glMaterialSpecular[2]=0.306678;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.1 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f,1.50f,0.0f);

	gluSphere(quadric[5],1.0f,30,30);




	//2nd column

	glMaterialAmbient[0]=0.329412;
	glMaterialAmbient[1]=0.223529;
	glMaterialAmbient[2]=0.027451;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.780392;
	glMaterialDiffuse[1]=0.568627;
	glMaterialDiffuse[2]=0.113725;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.992157;
	glMaterialSpecular[1]=0.941176;
	glMaterialSpecular[2]=0.807843;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.21794872 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,14.0f,0.0f);

	gluSphere(quadric[6],1.0f,30,30);

	
	//2nd row 2 col
	glMaterialAmbient[0]=0.2125;
	glMaterialAmbient[1]=0.1275f;
	glMaterialAmbient[2]=0.054f;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.714;
	glMaterialDiffuse[1]=0.4284;
	glMaterialDiffuse[2]=0.18144;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.393548;
	glMaterialSpecular[1]=0.271906;
	glMaterialSpecular[2]=0.166721;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.2 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,11.50f,0.0f);

	gluSphere(quadric[7],1.0f,30,30);


	//3rd row 2 col
	glMaterialAmbient[0]=0.25;
	glMaterialAmbient[1]=0.25;
	glMaterialAmbient[2]=0.25;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.4;
	glMaterialDiffuse[1]=0.4;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.774597;
	glMaterialSpecular[1]=0.774597;
	glMaterialSpecular[2]=0.774597;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.6 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,9.0f,0.0f);

	gluSphere(quadric[8],1.0f,30,30);

	//4th row 2nd col
	glMaterialAmbient[0]=0.19125;
	glMaterialAmbient[1]=0.0735;
	glMaterialAmbient[2]=0.0225;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.7038;
	glMaterialDiffuse[1]=0.27048;
	glMaterialDiffuse[2]=0.0828;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.256777;
	glMaterialSpecular[1]=0.137622;
	glMaterialSpecular[2]=0.086014;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.1 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,6.50f,0.0f);

	gluSphere(quadric[9],1.0f,30,30);

	//5th row and 2nd col
	glMaterialAmbient[0]=0.24725;
	glMaterialAmbient[1]=0.1995;
	glMaterialAmbient[2]=0.0745;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.75164;
	glMaterialDiffuse[1]=0.60648;
	glMaterialDiffuse[2]=0.22648;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.628281;
	glMaterialSpecular[1]=0.555802;
	glMaterialSpecular[2]=0.366065;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.4 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,4.0f,0.0f);

	gluSphere(quadric[10],1.0f,30,30);

	//6th row 2nd col
	glMaterialAmbient[0]=0.19225;
	glMaterialAmbient[1]=0.19225;
	glMaterialAmbient[2]=0.19225;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.50754;
	glMaterialDiffuse[1]=0.50754;
	glMaterialDiffuse[2]=0.50754;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.508273;
	glMaterialSpecular[1]=0.508273;
	glMaterialSpecular[2]=0.508273;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.4 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(8.0f,1.50f,0.0f);

	gluSphere(quadric[11],1.0f,30,30);






	//3rd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.01;
	glMaterialDiffuse[1]=0.01;
	glMaterialDiffuse[2]=0.01;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.50;
	glMaterialSpecular[1]=0.50;
	glMaterialSpecular[2]=0.50;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,14.0f,0.0f);

	gluSphere(quadric[12],1.0f,30,30);

	
	//2nd row 3 col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.1f;
	glMaterialAmbient[2]=0.06f;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.0;
	glMaterialDiffuse[1]=0.50980392;
	glMaterialDiffuse[2]=0.50980392;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.50196078;
	glMaterialSpecular[1]=0.50196078;
	glMaterialSpecular[2]=0.50195078;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,11.50f,0.0f);

	gluSphere(quadric[13],1.0f,30,30);


	//3rd row 3 col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.1;
	glMaterialDiffuse[1]=0.35;
	glMaterialDiffuse[2]=0.1;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.45;
	glMaterialSpecular[1]=0.55;
	glMaterialSpecular[2]=0.45;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,9.0f,0.0f);

	gluSphere(quadric[14],1.0f,30,30);

	//4th row 3nd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.0;
	glMaterialDiffuse[2]=0.0;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.6;
	glMaterialSpecular[2]=0.6;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,6.50f,0.0f);

	gluSphere(quadric[15],1.0f,30,30);

	//5th row and 3nd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.55;
	glMaterialDiffuse[1]=0.55;
	glMaterialDiffuse[2]=0.55;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.70;
	glMaterialSpecular[1]=0.70;
	glMaterialSpecular[2]=0.70;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,4.0f,0.0f);

	gluSphere(quadric[16],1.0f,30,30);

	//6th row 2nd col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.0;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.60;
	glMaterialSpecular[1]=0.60;
	glMaterialSpecular[2]=0.50;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.25 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.0f,1.50f,0.0f);

	gluSphere(quadric[17],1.0f,30,30);





	//4th column
	glMaterialAmbient[0]=0.02;
	glMaterialAmbient[1]=0.02;
	glMaterialAmbient[2]=0.02;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.01;
	glMaterialDiffuse[1]=0.01;
	glMaterialDiffuse[2]=0.01;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.4;
	glMaterialSpecular[1]=0.4;
	glMaterialSpecular[2]=0.4;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,14.0f,0.0f);

	gluSphere(quadric[18],1.0f,30,30);

	
	//2nd row 2 col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.05;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.4;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.5;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.04;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.7;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,11.50f,0.0f);

	gluSphere(quadric[19],1.0f,30,30);


	//3rd row 4col
	glMaterialAmbient[0]=0.0;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.4;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.04;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.04;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,9.0f,0.0f);

	gluSphere(quadric[20],1.0f,30,30);

	//4th row 2nd col
	glMaterialAmbient[0]=0.05;
	glMaterialAmbient[1]=0.0;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.4;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.04;
	glMaterialSpecular[2]=0.04;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,6.50f,0.0f);

	gluSphere(quadric[21],1.0f,30,30);

	//5th row and 4nd col
	glMaterialAmbient[0]=0.05;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.05;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.5;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.7;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,4.0f,0.0f);

	gluSphere(quadric[22],1.0f,30,30);

	//6th row 4nd col
	glMaterialAmbient[0]=0.05;
	glMaterialAmbient[1]=0.05;
	glMaterialAmbient[2]=0.0;
	glMaterialAmbient[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,glMaterialAmbient);

	glMaterialDiffuse[0]=0.5;
	glMaterialDiffuse[1]=0.5;
	glMaterialDiffuse[2]=0.4;
	glMaterialDiffuse[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_DIFFUSE,glMaterialDiffuse);
	
	glMaterialSpecular[0]=0.7;
	glMaterialSpecular[1]=0.7;
	glMaterialSpecular[2]=0.04;
	glMaterialSpecular[3]=1.0f;
	glMaterialfv(GL_FRONT,GL_SPECULAR,glMaterialSpecular);
	
	glMaterialShininess=0.078125 *128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,glMaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(16.0f,1.50f,0.0f);

	gluSphere(quadric[23],1.0f,30,30);

}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(width<height)
	{
		glOrtho(0.0f,15.5f,0.0f,15.5f* ((GLfloat)height/(GLfloat)width),-10.0f,10.0f);
	}
	else
	{
		glOrtho(0.0f,15.5f*(GLfloat(width)/GLfloat(height)),0.0f,15.5f,-10.0f,10.0f);	
	}
}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void update()
 {
 	angleOfXRotation=angleOfXRotation+10.0f;
 	if(angleOfXRotation>360.0f)
 	{
 		angleOfXRotation=0.0f;
 	}
 	angleOfYRotation=angleOfYRotation+5.0f;
 	if(angleOfYRotation>360.0f)
 	{
 		angleOfYRotation=0.0f;
 	}
 	angleOfZRotation=angleOfZRotation+5.0f;
 	if(angleOfZRotation>360.0f)
 	{
 		angleOfZRotation=0.0f;
 	}
 }