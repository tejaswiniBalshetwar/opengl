#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>


#include"OGL.h"

using namespace std;

int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;

struct lights
{
	GLfloat lightAmbient[4];
	GLfloat lightsDiffuse[4];
	GLfloat lightSpecular[4];
	GLfloat lightPosition[4];


};

lights light;

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materialDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialShininess[]={128.0f};


bool bLight=false;
bool gbIsFullscreen=false;
GLfloat angle=0.0f;

GLUquadric *quadric=NULL;

int main(void)
{
	void createwindow();
	void initialize();
	void uninitialize();
	void display();
	void update();
	void resize(int,int);
	void ToggleFullscreen(void);


	XEvent event;
	KeySym keysym;

	bool bDone=false;
	char keys[26];

	createwindow();
	initialize();

	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
							if(gbIsFullscreen==false)
							{
								ToggleFullscreen();
								gbIsFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbIsFullscreen=false;
							}
							break;
						case 'l':
						case 'L':
							if(bLight==false)
							{
								bLight=true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								bLight=false;
								glDisable(GL_LIGHTING);
							}
							break;

					}
					break;
				case MotionNotify:
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case 33:
					bDone=true;
					break;
			}
		}

		update();
		display();
	}

	uninitialize();
}

void createwindow()
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("error in initialize at XOpenDisplay()");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	if(gpXVisualInfo==NULL)
	{
		printf("error in initialize at XChooseVisual()");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask|VisibilityChangeMask|ExposureMask;

	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\nerror in XCreateWindow()");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WIDNOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

}


void initialize()
{
	void resize(int,int);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	light.lightAmbient[0]=0.5f;
	light.lightAmbient[1]=0.5f;
	light.lightAmbient[2]=0.5f;
	light.lightAmbient[3]=1.0f;

	light.lightsDiffuse[0]=1.0f;
	light.lightsDiffuse[1]=1.0f;
	light.lightsDiffuse[2]=1.0f;
	light.lightsDiffuse[3]=1.0f;

	light.lightSpecular[0]=1.0f;
	light.lightSpecular[1]=1.0f;
	light.lightSpecular[2]=1.0f;
	light.lightSpecular[3]=1.0f;


	light.lightPosition[0]=50.0f;
	light.lightPosition[1]=50.0f;
	light.lightPosition[2]=50.0f;
	light.lightPosition[3]=1.0f;


	glLightfv(GL_LIGHT0,GL_AMBIENT,light.lightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light.lightsDiffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light.lightSpecular);
	glLightfv(GL_LIGHT0,GL_POSITION,light.lightPosition);
	glEnable(GL_LIGHT0);

	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materialDiffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);
	

	resize(giWindowWidth,giWindowHeight);
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);
	glRotatef(-90.0f,1.0f,0.0f,0.0f);
	glRotatef(angle,0.0f,1.0f,0.0f);

	glBegin(GL_TRIANGLES);
	for(int i=0;i<sizeof(face_indicies)/sizeof(face_indicies[0]);i++)
	{
		for(int j=0;j<3;j++)
		{
			int vi=face_indicies[i][j];
			int ni=face_indicies[i][j+3];
			int ti=face_indicies[i][j+6];

			//glTexCoord2f(textures[ti][0],textures[ti][1]);
			glNormal3f(normals[ni][0],normals[ni][1],normals[ni][2]);
			glVertex3f(vertices[vi][0],vertices[vi][1],vertices[vi][2]);
		}
	}
	glEnd();
	glXSwapBuffers(gpDisplay,gWindow);
}
void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,0,GLsizei(width),GLsizei(height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}


void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();
	if(currentGLXContext && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
void update()
{
	angle=angle+10.0f;
	if(angle<360.0f)
	{
		angle=0.0f;
	}
}