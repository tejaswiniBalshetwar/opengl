#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/Xlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>
#include<SOIL/SOIL.h>


using namespace std;

int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;
XVisualInfo *gpXVisualInfo=NULL;
Window gWindow;
Colormap gColormap;

Display *gpDisplay=NULL;

bool gbIsFullscreen=false;

GLuint texture_smiley;
int keyPressed;

int main(void)
{
	void initialize(void);
	void uninitialize(void);
	void createWindow(void);
	void resize(int,int);
	void display(void);
	void ToggleFullscreen(void);


	int windowWidth=giWindowWidth;
	int windowHeight=giWindowHeight;

	XEvent event;
	KeySym keysym;

	bool bDone=false;
	char keys[26];

	createWindow();
	initialize();

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:	
							bDone=true;
							break;
						
					}
					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'f':
						case 'F':
						 if(gbIsFullscreen==false)
						 {
						 	gbIsFullscreen=true;
						 	ToggleFullscreen();

						 }
						 else
						 {
						 	gbIsFullscreen=false;
						 	ToggleFullscreen();
						 }
						 break;
						case '1':
							keyPressed=1;
							break;
						case '2':
							keyPressed=2;
							break;
						case '3':
							keyPressed=3;
							break;
						case '4':
							keyPressed=4;
							break;
						default:
							keyPressed=0;
							break;

					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;

					}
					break;
				case ConfigureNotify:
					windowWidth=event.xconfigure.width;
					windowHeight=event.xconfigure.height;
					resize(windowWidth,windowHeight);
					break;
				case MotionNotify:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
			}
		}
		display();
	}

	uninitialize();
	return(0);

}

void createWindow()
{
	void uninitialize();
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int frameBufferAttributes[]={
		GLX_RGBA,
		GL_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None
	}; 

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nError in XOpenDisplay");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	if(gpXVisualInfo==NULL)
	{
		printf("\n Error in glXChooseVisual");
		uninitialize();
		exit(0);
	}

	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.event_mask=PointerMotionMask|StructureNotifyMask|ButtonPressMask|KeyPressMask|VisibilityChangeMask|ExposureMask;

	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,gpXVisualInfo->visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("\n Error in XCreateWindow");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"MY OPENGL APPLICATION");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	Atom wm_state;
	Atom fullscreen;

	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.format=32;
	xev.xclient.message_type=wm_state;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}

void initialize()
{
	void resize(int,int);
	bool LoadTextures(GLuint *,const char*);

	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	LoadTextures(&texture_smiley,"./Smiley.bmp");

	printf("\ntexure_smilet: %u",texture_smiley);
	resize(giWindowWidth,giWindowHeight);
}

bool LoadTextures(GLuint *texture,const char *path )
{
	int imageWidth;
	int imageHeight;
	bool bResult=false;
	unsigned char* imageData=NULL;

	imageData=SOIL_load_image(path,&imageWidth,&imageHeight,0,SOIL_LOAD_RGB);
	if(imageData==NULL)
	{
		bResult=false;
		return bResult;
	}
	else
	{
		bResult=true;

		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glGenTextures(1,texture);
		glBindTexture(GL_TEXTURE_2D,*texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		gluBuild2DMipmaps(GL_TEXTURE_2D,3,imageWidth,imageHeight,GL_RGB,GL_UNSIGNED_BYTE,imageData);

		SOIL_free_image_data(imageData);
		return bResult;
	}
}


void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-3.0f);
	glRotatef(180.0f,0.0f,0.0f,1.0f);

	if(keyPressed==1)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smiley);
		glBegin(GL_QUADS);
			glTexCoord2f(0.50f,0.50f);
			glVertex3f(1.0f,1.0f,0.0f);

			glTexCoord2f(0.0f,0.50f);
			glVertex3f(-1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f,-1.0f,0.0f);

			glTexCoord2f(0.50f,0.0f);
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();

	}
	else if(keyPressed==2)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smiley);
		glBegin(GL_QUADS);
			glTexCoord2f(1.0f,1.0f);
			glVertex3f(1.0f,1.0f,0.0f);

			glTexCoord2f(0.0f,1.0f);
			glVertex3f(-1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f,-1.0f,0.0f);

			glTexCoord2f(1.0f,0.0f);
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();
	}
	else if(keyPressed==3)
	{
		glBindTexture(GL_TEXTURE_2D,texture_smiley);
		glBegin(GL_QUADS);
			glTexCoord2f(2.0f,2.0f);
			glVertex3f(1.0f,1.0f,0.0f);

			glTexCoord2f(0.0f,2.0f);
			glVertex3f(-1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f,-1.0f,0.0f);

			glTexCoord2f(2.0f,0.0f);
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();
	}
	else if(keyPressed==4)
	{
			glBindTexture(GL_TEXTURE_2D,texture_smiley);
			glBegin(GL_QUADS);
			glTexCoord2f(0.50f,0.50f);
			glVertex3f(1.0f,1.0f,0.0f);

			glTexCoord2f(0.50f,0.50f);
			glVertex3f(-1.0f,1.0f,0.0f);
			
			glTexCoord2f(0.50f,0.50f);
			glVertex3f(-1.0f,-1.0f,0.0f);

			glTexCoord2f(0.50f,0.50f);
			glVertex3f(1.0f,-1.0f,0.0f);

		glEnd();
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D,0);
		glBegin(GL_QUADS);
			glColor3f(1.0f,1.0f,1.0f);
			glVertex3f(1.0f,1.0f,0.0f);
			glVertex3f(-1.0f,1.0f,0.0f);
			glVertex3f(-1.0f,-1.0f,0.0f);
			glVertex3f(1.0f,-1.0f,0.0f);
		glEnd();
	}

	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,GLsizei(width),GLsizei(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,GLfloat(width)/GLfloat(height),0.1f,100.0f);
}
void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext && gGLXContext==currentGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);

	}


	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}