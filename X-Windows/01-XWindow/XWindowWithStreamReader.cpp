#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool gbIsFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo gxVisualInfo;
Colormap gColormap;
Window gWindow;

int giWindowWidth=800;
int giWindowHeight=600;

int main(void)
{
	void CreateWindow();
	void ToggeleFullscreen();
	void uninitialize();

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	char keys[26];

	CreateWindow();

	XEvent event;
	KeySym keysym;

	while(1)
	{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				//for keysym
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
				switch(keysym)
				{
					case XK_Escape:
						uninitialize();
						exit(0);
				}

				XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(gbIsFullscreen==false)
						{
							ToggeleFullscreen();
							gbIsFullscreen=true;
						}
						else
						{
							ToggeleFullscreen();
							gbIsFullscreen=false;
						}
						break;
				}
				break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case Expose:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
		}
	}

	uninitialize();
	return(0);

}


void CreateWindow()
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error while openning the connection to the display\n\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	defaultDepth=DefaultDepth(gpDisplay,defaultScreen);

	Status status=XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gxVisualInfo);
	if(status==0)
	{
		printf("Error while getting the matched viaul for default screen on default depth\n\n");
		uninitialize();
		exit(0);
	}

	//window attributes
	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gxVisualInfo.screen),gxVisualInfo.visual,AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gxVisualInfo.screen),0,0,giWindowWidth,giWindowHeight,0,gxVisualInfo.depth,InputOutput,gxVisualInfo.visual,styleMask,&winAttribs);
	if(!gWindow)
	{
		printf("Error while Creating window");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"My XWINDOW Application");

	Atom WindowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&WindowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggeleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(XEvent));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbIsFullscreen ? 0 : 1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gxVisualInfo.screen),False,StructureNotifyMask,&xev);


}



void uninitialize()
{
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);

	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}