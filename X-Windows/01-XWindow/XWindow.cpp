#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool gbFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo gVisualInfo;
/*XVisualInfo *gVisualInfo;*/
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

int main(void)
{
	//function declarations
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();

	XEvent event;
	KeySym keysym;

	while(1)
	{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;

			case KeyPress:
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
				switch(keysym)
				{
					case XK_Escape:
						uninitialize();
						exit(0);
					case XK_F:
					case XK_f:
						if(gbFullscreen==false)
						{
							ToggleFullscreen();
							gbFullscreen=true;
						}
						else
						{
							ToggleFullscreen();
							gbFullscreen=false;
						}
						break;
				}
				break;
				
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						printf("Left mouse button clicked\n");
					break;

					case 2:
						printf("Middle mouse button clicked\n");
						break;
					case 3:
						printf("right mouse button clicked\n");
						break;
					case 4:
						printf("mouse wheel up\n");
						break;
					case 5:
						printf("mouse wheel down\n" ); 
						break;
				}
				break;
			case MotionNotify: //mouse Move
				break;

			case ConfigureNotify:	//size 
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				break;
			case DestroyNotify:
				break;
			case 33:
				uninitialize();
				exit(0);
		}
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	void uninitialize();

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	//open connection to the XServer
	gpDisplay=XOpenDisplay(NULL);//default display de
	if(gpDisplay==NULL)
	{
		printf("\n\nError while opening the connection to xServer");
		uninitialize();
		exit(0);
	}

	//get the default screen from Xserver which is created by your connection
	defaultScreen=XDefaultScreen(gpDisplay);

	//per pixel kiti rangachya wires ahes he ghene
	defaultDepth=DefaultDepth(gpDisplay,defaultScreen);

	//defalut screen and despth varun defalut window la tyachya hardware pramane capability dene

	Status status=XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gVisualInfo);
	if(status==0)
	{
		printf("Error while getting matched visual\n\n");
		uninitialize();
		exit(0);
	}

	//window properties set karane
	winAttribs.border_pixel=0;
	winAttribs.border_pixmap=0;
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay,gVisualInfo.screen),gVisualInfo.visual,AllocNone);
	gColormap=winAttribs.colormap;

	//jitake event handle kele tyachya nusar evant mask tharatat ani tya window la apan he event handle karanar ahot he sangitala jata
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

	//apalya window la kiti style apan asssign kelye ani tya x la snagayachya he winAttribs varun tharavala jata
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;


	//finally apali window tayar kara style,event ani graphics card chya capability nusar asa XServer la sangane
	gWindow=XCreateWindow(gpDisplay,RootWindow(gpDisplay,gVisualInfo.screen),0,0,giWindowWidth,giWindowHeight,0,gVisualInfo.depth,InputOutput,gVisualInfo.visual,styleMask,&winAttribs);

	if(!gWindow)
	{
		printf("Error while window creation\n\n");
		uninitialize();
		exit(0);
	}


	//window la nav dene
	XStoreName(gpDisplay,gWindow,"My Xwindow Application");

	//case 33 handle karanyachi takay window manager kade magane so that tumachi window close button ani menu var close hoil tya sathi WindowManger la sange ki ha mazha ek close vala protol handle karr
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);

	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(XEvent));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=gbFullscreen?0:1;

	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gVisualInfo.screen),False,StructureNotifyMask,&xev);


}


void uninitialize()
{
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);

	}

	/*if(gVisualInfo)
	{


	}*/

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}